package cn.com.dkl.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import cn.com.dkl.logistics.entity.DataManager;

/**
 * 监听安装、卸载和替换
 *
 * @author guochaohui
 */
public class InstallAndUninstallBroadCastReceiver extends BroadcastReceiver {
    private SharedPreferences sp, spSetting;

    @Override
    public void onReceive(Context context, Intent intent) {
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        spSetting = context.getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);

        // 接收安装广播
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            String packageName = intent.getDataString();
//			LogUtils.i("安装了：" + packageName + "包名程序");
//			Toast.makeText(context, "安装了：" + packageName + "包名程序", Toast.LENGTH_SHORT).show();
        }
        // 接收卸载广播
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            String packageName = intent.getDataString();
//			LogUtils.i("卸载了：" + packageName + "包名程序");
//			Toast.makeText(context, "卸载了：" + packageName + "包名程序", Toast.LENGTH_SHORT).show();
//			Editor editor = sp.edit();
//			editor.clear();
//			editor.commit();
//			Editor editorSetting = spSetting.edit();
//			editorSetting.clear();
//			editorSetting.commit();
        }
        // 接收替换广播
        if (intent.getAction().equals("android.intent.action.PACKAGE_REPLACED")) {
            String packageName = intent.getDataString();
//			LogUtils.i("替换了：" + packageName + "包名程序");
//			Toast.makeText(context, "替换了：" + packageName + "包名程序", Toast.LENGTH_SHORT).show();
//			Editor editor = sp.edit();
//			editor.clear();
//			editor.commit();
//			Editor editorSetting = spSetting.edit();
//			editorSetting.clear();
//			editorSetting.commit();
        }

    }

}
