package cn.com.dkl.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.lidroid.xutils.util.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.GrabSingleActivity;
import cn.com.dkl.logistics.driver.MainActivity;
import cn.com.dkl.logistics.driver.NewMessageActivity;
import cn.com.dkl.logistics.driver.NewsDetailsActivity;
import cn.com.dkl.logistics.driver.PersonalMoreActivity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.XFClouldUtil;
import cn.jpush.android.api.JPushInterface;

public class JPushReceiver extends BroadcastReceiver {
    private static final String TAG = "JPush";
    private SharedPreferences sp;
    private String mDeviceID;

    @Override
    public void onReceive(Context context, Intent intent) {
         Bundle bundle = intent.getExtras();
        Log.d(TAG, "[MyReceiver] onReceive - " + intent.getAction() + ", extras: " + printBundle(bundle));
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            mDeviceID = tm.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sp.getBoolean(UserEntity.IS_lOGIN, false)) {
            if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
                String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
                Log.d(TAG, "[MyReceiver] 接收Registration Id : " + regId);
                // send the Registration Id to your server...
            } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
                // 接收到推送下来的自定义消息
            } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {    // 收到通知
                // 播放声音通知
                String text = bundle.getString(JPushInterface.EXTRA_ALERT);
                try {
                    String extra = bundle.getString(JPushInterface.EXTRA_EXTRA);
                    JSONObject obj = new JSONObject(extra);
                    String sourceType = obj.getString("SOURCETYPE");
                    // 0.运起来自定义推送 1.车源 2.货源 3.专线 4.同城配送司机订单通知 5.同城配送货主订单通知 6.同城配送司机普通通知
                    // 7.同城配送货主普通通知 8.同城配送司机单账号登陆 9.同城配送货主单账号登陆 10.运起来单账号登陆 11.通知同城司机抢单
                    if ("4".equals(sourceType)) {
                        if (sp.getString(UserEntity.IS_SET_MUTING, "0").equals("0")) {
                            Log.d(TAG, "语音播报内容: " + text);
                            XFClouldUtil.speek(context, text);
                        }
                        openActivity(context, bundle);
                    } else if ("8".equals(sourceType)) {
                        if (!mDeviceID.equals(obj.getString("DEVICEID"))){
                            Toast.makeText(context, "你的账号已在另一台设备登陆", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(context, PersonalMoreActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            bundle = parseExtras(bundle);
                            i.putExtras(bundle);
                            context.startActivity(i);
                        }

                    } else if ("6".equals(sourceType)) {    // 普通消息打开消息详情
                        Intent i = new Intent(context, NewsDetailsActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        bundle = parseExtras(bundle);
                        i.putExtras(bundle);
                        i.putExtra("jpush", "jpush");
                        context.startActivity(i);
                    } else if ("11".equals(sourceType)) {    // 抢单通知
                        if (sp.getString(UserEntity.IS_SET_MUTING, "0").equals("0")) {
                            Log.d(TAG, "语音播报内容: " + text);
                            XFClouldUtil.speek(context, text);
                        }
                        openGrabActivity(context, bundle);
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
                // 用户点击打开了通知
                String text = bundle.getString(JPushInterface.EXTRA_ALERT);
                try {
                    String extra = bundle.getString(JPushInterface.EXTRA_EXTRA);
                    JSONObject obj = new JSONObject(extra);
                    String sourceType = obj.getString("SOURCETYPE");
                    // 0运起来自定义推送1车源2货源3专线4.同城配送司机订单通知5同城配送货主订单通知6同城配送司机普通通知7同城配送货主普通通知
                    // 8同城配送司机单账号登陆9同城配送货主单账号登陆10运起来单账号登陆
                    if ("4".equals(sourceType)) {
                        Intent i = new Intent(context, MainActivity.class);
                        //bundle = parseExtras(bundle);
//						LogUtils.i("jpush:open***" + bundle.toString());
//						i.putExtras(bundle);
//						LogUtils.i("jpush:open***" + bundle.toString());
//						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//						context.startActivity(i);
                        openActivity(context, bundle);
                    } else if ("8".equals(sourceType)) {
                        if (!sp.getBoolean(UserEntity.IS_lOGIN, false)) {
                            if (!mDeviceID.equals(obj.getString("DEVICEID"))){
                                Toast.makeText(context, "你的账号已在另一台设备登陆", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else if ("6".equals(sourceType)) {    // 普通消息打开消息详情
                        Intent i = new Intent(context, NewsDetailsActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        bundle = parseExtras(bundle);
                        i.putExtras(bundle);
                        i.putExtra("jpush", "jpush");
                        context.startActivity(i);
                    } else if ("11".equals(sourceType)){
                        openGrabActivity(context, bundle);
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent.getAction())) {
                // 在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity，
                // 打开一个网页等..
            } else if (JPushInterface.ACTION_CONNECTION_CHANGE.equals(intent.getAction())) {
                boolean connected = intent.getBooleanExtra(JPushInterface.EXTRA_CONNECTION_CHANGE, false);
            } else {
                Log.d(TAG, "[MyReceiver] Unhandled intent - " + intent.getAction());
            }
        }
    }

    private void openActivity(Context context, Bundle bundle) {
        String mainActivity = context.getPackageName() + ".MainActivity";
        Intent intent = null;
        if (mainActivity.equals(CommonTools.getTopActivity(context))) { // 主界面前台显示
            intent = new Intent(context, NewMessageActivity.class);
        } else {
            intent = new Intent(context, MainActivity.class);
        }

        bundle = parseExtras(bundle);
        intent.putExtras(bundle);
        LogUtils.i("jpush:" + bundle.toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    private void openGrabActivity(Context context, Bundle bundle) {//打开抢单列表
        Intent intent = new Intent(context, GrabSingleActivity.class);
        bundle = parseExtras(bundle);
        intent.putExtras(bundle);
        LogUtils.i("jpush:" + bundle.toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    private Bundle parseExtras(Bundle bundle) {
        Bundle result = new Bundle();
        for (String key : bundle.keySet()) {
            if (key.equals(JPushInterface.EXTRA_EXTRA)) {
                if (!bundle.getString(JPushInterface.EXTRA_EXTRA).isEmpty()) {
                    try {
                        JSONObject obj = new JSONObject(bundle.getString(JPushInterface.EXTRA_EXTRA));
                        Iterator<String> it = obj.keys();
                        while (it.hasNext()) {
                            String keyValue = it.next().toString();
                            result.putString(keyValue, obj.getString(keyValue));
                        }
                    } catch (JSONException e) {
                        LogUtils.i(e.toString());
                    }
                }
            }
        }
        return result;
    }

    // 打印所有的 intent extra 数据
    private static String printBundle(Bundle bundle) {
        StringBuilder sb = new StringBuilder();
        for (String key : bundle.keySet()) {
            if (key.equals(JPushInterface.EXTRA_NOTIFICATION_ID)) {
                sb.append("\nkey:" + key + ", value:" + bundle.getInt(key));
            } else if (key.equals(JPushInterface.EXTRA_CONNECTION_CHANGE)) {
                sb.append("\nkey:" + key + ", value:" + bundle.getBoolean(key));
            } else if (key.equals(JPushInterface.EXTRA_EXTRA)) {
                if (bundle.getString(JPushInterface.EXTRA_EXTRA).isEmpty()) {
                    Log.i(TAG, "This message has no Extra data");
                    continue;
                }

                try {
                    JSONObject json = new JSONObject(bundle.getString(JPushInterface.EXTRA_EXTRA));
                    Iterator<String> it = json.keys();

                    while (it.hasNext()) {
                        String myKey = it.next().toString();
                        sb.append("\nkey:" + key + ", value: [" + myKey + " - " + json.optString(myKey) + "]");
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Get message extra JSON error!");
                }

            } else {
                sb.append("\nkey:" + key + ", value:" + bundle.getString(key));
            }
        }
        return sb.toString();
    }

}