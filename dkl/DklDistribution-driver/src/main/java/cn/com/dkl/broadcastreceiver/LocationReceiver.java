package cn.com.dkl.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cn.com.dkl.logistics.service.LocationService;

/**
 * 定位服务关闭后通知重启
 *
 * Created by LiuXing on 2017/2/23.
 */

public class LocationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, LocationService.class));
    }
}
