package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.util.LogUtils;

import java.io.File;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.view.CircleImageView;

/**
 * 评价列表数据适配器 Created by LiuXing on 2017/3/1.
 */

public class AllEvaluateAdapter extends BaseAdapter {
    private Context context;
    List<Map<String, String>> listEvaluate;
    private LayoutInflater inflater;
    private String mHeadPhotouURL;

    public AllEvaluateAdapter(Context context, List<Map<String, String>> listEvaluate) {
        this.context = context;
        this.listEvaluate = listEvaluate;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listEvaluate.size();
    }

    @Override
    public Object getItem(int position) {
        return listEvaluate.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_evaluate_list, null);
            viewHolder.mTvName = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.mTvTime = (TextView) convertView.findViewById(R.id.tvTime);
            viewHolder.mTvAttitudePoint = (TextView) convertView.findViewById(R.id.tvAttitudePoint);
            viewHolder.mTvCommentContent = (TextView) convertView.findViewById(R.id.tvCommentContent);
            viewHolder.rbServiceLV = (RatingBar) convertView.findViewById(R.id.rbServiceLV);
            viewHolder.imgUserImg = (CircleImageView) convertView.findViewById(R.id.imgUserImg);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Map<String, String> map = listEvaluate.get(position);
        viewHolder.mTvName.setText(map.get("NAME"));
        viewHolder.mTvTime.setText(map.get("COMMENTTIME"));
        viewHolder.mTvAttitudePoint.setText(map.get("SERVICEATTITUDE"));
        viewHolder.mTvCommentContent.setText(map.get("COMMENTCONTENT"));
        viewHolder.rbServiceLV.setNumStars(Integer.parseInt(map.get("SERVICEATTITUDE")));
        mHeadPhotouURL = map.get("PHOTO");

        if (!TextUtils.isEmpty(mHeadPhotouURL)){
            downLoadHeadImg(viewHolder, mHeadPhotouURL);
            String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + mHeadPhotouURL;
            File file = new File(imagePath);
            if (file.exists()){
                Bitmap bitmap = BitmapFactory.decodeFile(file.toString());
                if (bitmap != null){
                    viewHolder.imgUserImg.setImageBitmap(bitmap);
                }else {
                    downLoadHeadImg(viewHolder, mHeadPhotouURL);
                }
            }
        }
        return convertView;
    }

    private class ViewHolder {
        TextView mTvName;
        TextView mTvTime;
        TextView mTvAttitudePoint;
        TextView mTvCommentContent;
        CircleImageView imgUserImg;
        RatingBar rbServiceLV;
    }

    protected void downLoadHeadImg(final ViewHolder viewHolder, final String imageName) {
        if (TextUtils.isEmpty(imageName)) {
            return;
        }
        String url = context.getString(R.string.server_imgurl) + "upload/" + imageName;
        final String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.download(url, imagePath, true, true, new RequestCallBack<File>() { // true1代表允许断点续传，true2//
            // 如果从请求返回信息中获取到文件名，下载完成后自动重命名。
            @Override
            public void onSuccess(ResponseInfo<File> arg0) {
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                if (null != bitmap) {
                    viewHolder.imgUserImg.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("下载头像失败：" + arg0.getMessage() + arg1);
            }
        });
    }

}
