package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;

/**
 * 账单查询数据适配器
 *
 * @author Dao
 */
public class BillQueryAdapter extends BaseAdapter {
    private Context context;
    private List<Map<String, String>> dataList;
    private LayoutInflater inflater;

    public BillQueryAdapter(Context context, List<Map<String, String>> dataList) {
        this.context = context;
        this.dataList = dataList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.list_item_bill_query, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTime.setText(dataList.get(position).get("CONSUMETIME"));
        String num = dataList.get(position).get("AMOUNTCONSUMED");
        float money = TextUtils.isEmpty(num) ? 0 : Float.valueOf(num);
        String text = dataList.get(position).get("CONSUMEWAYDESP");
//		if(dataList.get(position).get("CONSUMETYPE").equals("2")) {
//			text = "支出";
//		}
//		if (money < 0) {
//			text = "支出";
//		}
        holder.tvDetail.setText(text);
        holder.tvMoney.setText(money + "元");
        return convertView;
    }

    static class ViewHolder {
        private TextView tvTime, tvDetail, tvMoney;

        public ViewHolder(View v) {
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvDetail = (TextView) v.findViewById(R.id.tvDetail);
            tvMoney = (TextView) v.findViewById(R.id.tvMoney);
        }
    }

}
