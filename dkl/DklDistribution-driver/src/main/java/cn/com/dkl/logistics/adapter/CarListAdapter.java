package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;

/**
 * Created by Xillen on 2017/7/11.
 */

public class CarListAdapter extends BaseAdapter {
    private Context context;
    private List<Map<String, String>> listCar = null;
    private LayoutInflater inflater;

    public CarListAdapter(Context context, List<Map<String, String>> listCar) {
        this.context = context;
        this.listCar = listCar;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listCar.size();
    }

    @Override
    public Object getItem(int position) {
        return listCar.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.item_car_list, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Map<String,String> map = listCar.get(position);
        String carNumber = map.get("CARNUMBER");
        String status = map.get("STATUS");
        String carType = map.get("CARTYPE");
        String carLength = map.get("CARLENGTH");
        String carVolume = map.get("VOLUMN");
        String carWeight = map.get("LOADWEIGHT");

        switch (Integer.parseInt(status)){
            case 0:
                status = "未提交认证";
                break;
            case 1:
                status = "等待认证";
                break;
            case 2:
                status = "认证通过";
                break;
            case 3:
                status = "认证不通过";
                break;
            default:
                status = "未提交认证";
                break;
        }

        holder.tvCarAuth.setText(status);
        holder.tvCarNumber.setText(carNumber);
        holder.tvCarType.setText(carType);
        holder.tvCarLength.setText(carLength + "米");
        holder.tvCarVolume.setText(carVolume + "方");
        holder.tvCarWeight.setText(carWeight + "吨");
        return convertView;
    }

    static class ViewHolder {
        private TextView tvCarAuth,tvCarNumber, tvCarType, tvCarLength, tvCarVolume, tvCarWeight;

        public ViewHolder(View v) {
            tvCarAuth = (TextView) v.findViewById(R.id.tvCarAuth);
            tvCarNumber = (TextView) v.findViewById(R.id.tvCarNumber);
            tvCarType = (TextView) v.findViewById(R.id.tvCarType);
            tvCarLength = (TextView) v.findViewById(R.id.tvCarLength);
            tvCarVolume = (TextView) v.findViewById(R.id.tvCarVolume);
            tvCarWeight = (TextView) v.findViewById(R.id.tvCarWeight);
        }

    }
}
