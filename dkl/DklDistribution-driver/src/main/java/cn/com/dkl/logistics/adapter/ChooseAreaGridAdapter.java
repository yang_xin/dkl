package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;

/**
 * 选择地区数据适配器
 *
 * @author Dao
 */
public class ChooseAreaGridAdapter extends BaseAdapter {

    private List<Map<String, Object>> mData;
    private LayoutInflater mInflater;
    private ViewHolder Holder = null;
    private Context mContext;
    private int clickedId;
    private int selectedColor = 0;// 条件选中的字体颜色
    private int defaultColor = 0;// 条件默认字体颜色

    /**
     * construct function
     */
    public ChooseAreaGridAdapter(Context mContext, List<Map<String, Object>> data) {
        super();
        this.mContext = mContext;
        this.mData = data;
        mInflater = LayoutInflater.from(mContext);
        // 初始化颜色
        selectedColor = mContext.getResources().getColor(R.color.text_color_deep_orange);
        defaultColor = mContext.getResources().getColor(R.color.black);
    }

    private static class ViewHolder {
        public TextView place_name;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int arg0) {
        return mData.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setData(List<Map<String, Object>> data) {
        this.mData = data;
    }

    public void setClickId(int clickedId) {
        this.clickedId = clickedId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.grid_view_item_choose, null);

            Holder = new ViewHolder();
            Holder.place_name = (TextView) convertView.findViewById(R.id.tvItemName);
            convertView.setTag(Holder);
        } else {
            Holder = (ViewHolder) convertView.getTag();
        }
        if (clickedId == position) {
            Holder.place_name.setTextColor(selectedColor);
        } else {
            Holder.place_name.setTextColor(defaultColor);
        }

        Holder.place_name.setText(mData.get(position).get("Name").toString().replace("null", ""));

        return convertView;
    }

}
