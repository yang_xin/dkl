package cn.com.dkl.logistics.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import cn.com.dkl.logistics.driver.R;

public class ChooseConditionGridAdapter extends BaseAdapter {

    private Context mContext;
    private String[] mData;
    private LayoutInflater mInflater;
    private ViewHolder holder = null;
    private int clickedId;
    private int selectedColor = 0;// 条件选中的字体颜色
    private int defaultColor = 0;// 条件默认字体颜色

    public ChooseConditionGridAdapter(Context context, String[] data) {
        this.mContext = context;
        this.mData = data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedColor = mContext.getResources().getColor(R.color.text_color_deep_orange);
        defaultColor = mContext.getResources().getColor(R.color.black);
    }

    public class ViewHolder {
        TextView tvItemName;
    }

    public void setData(String[] data) {
        this.mData = data;
    }

    @Override
    public int getCount() {
        return mData.length;
    }

    @Override
    public Object getItem(int position) {
        return mData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setClickId(int clickedId) {
        this.clickedId = clickedId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.grid_view_item_choose, null);
            holder = new ViewHolder();
            holder.tvItemName = (TextView) convertView.findViewById(R.id.tvItemName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (clickedId == position) {
            holder.tvItemName.setTextColor(selectedColor);
        } else {
            holder.tvItemName.setTextColor(defaultColor);
        }

        holder.tvItemName.setText(mData[position]);

        return convertView;
    }

}
