package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;

/**
 * Created by Xillen on 2017/8/21.
 */

public class GrabSingleAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Map<String, String>> listSingle = null;
    GrabSingle grabSingle;

    public GrabSingleAdapter(Context context, List<Map<String, String>> listSingle) {
        this.context = context;
        this.listSingle = listSingle;
        layoutInflater = LayoutInflater.from(context);
    }

    public interface GrabSingle{
        void grabSingle(int position);
    }

    public GrabSingle getGrabSingle() {
        return grabSingle;
    }

    public void setGrabSingle(GrabSingle grabSingle) {
        this.grabSingle = grabSingle;
    }

    @Override
    public int getCount() {
        return listSingle.size();
    }

    @Override
    public Object getItem(int position) {
        return listSingle.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_grab_single_list, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Map<String,String> map = listSingle.get(position);
        String startAddress = "";
        String endddress = "";
        String startalias = map.get("FROMADDRALIAS");
        String endalias = map.get("TOADDRALIAS");
        String orderStatus = map.get("ORDER_STATUS");
        String sendStatus = map.get("SENDSTATUS");
        if (!startalias.equals("") && startalias != null) {
            startAddress = map.get("FROMPROVINCE") + map.get("FROMCITY") + map.get("FROMDISTRICT") + startalias;
        } else {
            startAddress = map.get("FROMADDRESS");
        }
        if (!endalias.equals("") && endalias != null) {
            endddress = map.get("TOPROVINCE") + map.get("TOCITY") + map.get("TODISTRICT") +  endalias;
        } else {
            endddress = map.get("TOADDRESS");
        }

        holder.tvTime.setText(map.get("CREATE_TIME"));

        holder.tvOrderNo.setText(map.get("ORDER_NO"));

        holder.tvStartAddress.setText(startAddress);
        holder.tvToAddress.setText(endddress);

        holder.btnGrab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grabSingle.grabSingle(position);
            }
        });

        if (!orderStatus.equals("1") && !sendStatus.equals("1")){
            holder.btnGrab.setBackground(context.getResources().getDrawable(R.drawable.background_deep_gray_selector_corner));
            holder.btnGrab.setText("抢单结束");
            holder.btnGrab.setTextColor(context.getResources().getColor(R.color.black_text));
            holder.btnGrab.setEnabled(false);
        }else{
            holder.btnGrab.setBackground(context.getResources().getDrawable(R.drawable.background_deep_orange_selector_corner));
            holder.btnGrab.setText("立即抢单");
            holder.btnGrab.setTextColor(context.getResources().getColor(R.color.white));
            holder.btnGrab.setEnabled(true);
        }


        return convertView;
    }

    static class ViewHolder {
        private TextView tvTime, tvStartAddress, tvToAddress, tvOrderNo;
        private Button btnGrab;

        public ViewHolder(View v) {
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvStartAddress = (TextView) v.findViewById(R.id.tvStartAddress);
            tvToAddress = (TextView) v.findViewById(R.id.tvToAddress);
            tvOrderNo = (TextView) v.findViewById(R.id.tvOrderNo);
            btnGrab = (Button) v.findViewById(R.id.btnGrab);
        }

    }
}
