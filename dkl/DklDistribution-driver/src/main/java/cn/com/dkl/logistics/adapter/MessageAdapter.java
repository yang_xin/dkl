package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.OrderMessage;

/**
 * 消息数据适配器
 *
 * @author Dao
 */
public class MessageAdapter extends BaseAdapter {
    private Context context = null;
    private List<OrderMessage> listMsg;
    private LayoutInflater inflater;

    public MessageAdapter(Context context, List<OrderMessage> listMsg) {
        this.context = context;
        this.listMsg = listMsg;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listMsg.size();
    }

    @Override
    public Object getItem(int position) {
        return listMsg.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.message_item_layout, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvNewAlert.setText(listMsg.get(position).getTitle());
        holder.tvMsgTime.setText(listMsg.get(position).getCreateTime());
        holder.tvMsgContent.setText(listMsg.get(position).getSummary());

        return convertView;
    }

    static class ViewHolder {
        private TextView tvNewAlert, tvMsgTime, tvMsgContent;

        public ViewHolder(View v) {
            tvNewAlert = (TextView) v.findViewById(R.id.tvNewAlert);
            tvMsgTime = (TextView) v.findViewById(R.id.tvMsgTime);
            tvMsgContent = (TextView) v.findViewById(R.id.tvMsgContent);
        }

    }

}
