package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.util.LogUtils;

import java.io.File;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.view.CircleImageView;

/**
 * 我的评价数据适配器
 *
 * @author Dao
 */
public class MyEvaluateAdapter extends BaseAdapter {
    private Context contenxt;
    List<Map<String, String>> data;
    private LayoutInflater inflater;

    public MyEvaluateAdapter(Context context, List<Map<String, String>> data) {
        this.contenxt = context;
        this.data = data;
        inflater = LayoutInflater.from(contenxt);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_view_item_my_evaluate, null);
            viewHolder.tvUserName = (TextView) convertView.findViewById(R.id.tvUserName);
            viewHolder.tvRatingNum = (TextView) convertView.findViewById(R.id.tvRatingNum);
            viewHolder.tvEvaContent = (TextView) convertView.findViewById(R.id.tvEvaContent);
            viewHolder.tvEvaDate = (TextView) convertView.findViewById(R.id.tvEvaDate);
            viewHolder.tvOrderNumber = (TextView) convertView.findViewById(R.id.tvOrderNumber);
            viewHolder.imgUserImg = (CircleImageView) convertView.findViewById(R.id.imgUserImg);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Map<String, String> map = data.get(position);
        viewHolder.tvUserName.setText(map.get("UserName"));
        viewHolder.tvRatingNum.setText(("给我" + map.get("sellerCommentLV") + "星" + map.get("typeName")));
        viewHolder.tvEvaContent.setText(map.get("sellerComment"));
        viewHolder.tvEvaDate.setText(map.get("date"));
        viewHolder.tvOrderNumber.setText("订单编号：" + map.get("orderNo"));
        String headPhotouURL = map.get("headPhotoURL");
        if (!TextUtils.isEmpty(headPhotouURL)) {
            String imagePath = contenxt.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + headPhotouURL;
            File file = new File(imagePath);
            if (file.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file.toString());
                if (null != bitmap) {
                    viewHolder.imgUserImg.setImageBitmap(bitmap);
                }
            } else {
                downLoadHeadImg(viewHolder, headPhotouURL);
            }
        }

        return convertView;
    }

    private class ViewHolder {
        TextView tvUserName;
        TextView tvRatingNum;
        CircleImageView imgUserImg;
        TextView tvEvaContent;

        TextView tvEvaDate;
        TextView tvOrderNumber;
    }

    protected void downLoadHeadImg(final ViewHolder viewHolder, final String imageName) {
        if (TextUtils.isEmpty(imageName)) {
            return;
        }
        String url = contenxt.getString(R.string.server_imgurl) + "upload/" + imageName;
        //String url = contenxt.getString(R.string.server_url) + "upload/" + imageName;
        final String imagePath = contenxt.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.download(url, imagePath, true, true, new RequestCallBack<File>() { // true1代表允许断点续传，true2//
            // 如果从请求返回信息中获取到文件名，下载完成后自动重命名。
            @Override
            public void onSuccess(ResponseInfo<File> arg0) {
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                if (null != bitmap) {
                    viewHolder.imgUserImg.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("下载头像失败：" + arg0.getMessage() + arg1);
            }
        });
    }

}
