package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.OrderMessage;

/**
 * 消息列表
 * Created by LiuXing on 2016/12/13.
 */

public class MyNewsAdapter extends BaseAdapter {

    private Context context = null;
    private List<OrderMessage> listNews;
    private LayoutInflater inflater;

    public MyNewsAdapter(Context context, List<OrderMessage> listNews) {
        this.context = context;
        this.listNews = listNews;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return listNews.size();
    }

    @Override
    public Object getItem(int position) {
        return listNews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.my_news_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvNewsAlert.setText(listNews.get(position).getTitle());
        holder.tvNewsTime.setText(listNews.get(position).getCreateTime());
        holder.tvNewsContent.setText(listNews.get(position).getSummary());
        String isRead = listNews.get(position).getStatus();
        if ("0".equals(isRead)){
            holder.ivIsRead.setVisibility(View.VISIBLE);
        }else {
            holder.ivIsRead.setVisibility(View.GONE);
        }

        return convertView;
    }

    static class ViewHolder{
        private TextView tvNewsAlert, tvNewsTime, tvNewsContent;
        private ImageView ivIsRead;

        public ViewHolder(View v) {
            tvNewsAlert = (TextView) v.findViewById(R.id.tvNewsAlert);
            tvNewsTime = (TextView) v.findViewById(R.id.tvNewsTime);
            tvNewsContent = (TextView) v.findViewById(R.id.tvNewsContent);
            ivIsRead = (ImageView) v.findViewById(R.id.ivIsRead);
        }

    }
}
