package cn.com.dkl.logistics.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;

/**
 * 我的钱包数据适配器
 *
 * @author Dao
 */
public class MyWalletAdapter extends BaseAdapter {
    private Context context;
    private List<Map<String, String>> listDatas = null;
    private LayoutInflater inflater;
    private SharedPreferences sp;

    public MyWalletAdapter(Context context, List<Map<String, String>> listDatas) {
        this.context = context;
        this.listDatas = listDatas;
        inflater = LayoutInflater.from(context);
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return listDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return listDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ResourceAsColor")
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.item_withdraw_cash, parent, false);
            holder = new ViewHolder();
            holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
            holder.tvState = (TextView) convertView.findViewById(R.id.tvState);
            holder.tvMoney = (TextView) convertView.findViewById(R.id.tvMoney);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTime.setText(listDatas.get(position).get("CREATETIME"));
        String state = listDatas.get(position).get("STATUS");
        if ("0".equals(state)) {
            state = "已申请";
        } else if ("1".equals(state)) {
            state = "已完成";
        } else if ("2".equals(state)) {
            state = "处理失败";
        }
        holder.tvState.setText(state);
        holder.tvMoney.setText(listDatas.get(position).get("CASHMONEY") + "元");
        return convertView;
    }

    static class ViewHolder {
        private TextView tvTime, tvState, tvMoney;

    }

}
