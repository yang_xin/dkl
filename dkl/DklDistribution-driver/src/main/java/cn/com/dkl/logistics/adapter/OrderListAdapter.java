package cn.com.dkl.logistics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;

/**
 * 订单列表数据适配器
 *
 * @author Dao
 */
public class OrderListAdapter extends BaseAdapter {
    private Context context;
    private List<Map<String, String>> listOrder = null;
    private LayoutInflater inflater;
    private SharedPreferences sp;

    public OrderListAdapter(Context context, List<Map<String, String>> listOrder) {
        this.context = context;
        this.listOrder = listOrder;
        inflater = LayoutInflater.from(context);
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        stateMap.put(NO_LEAFLETS, "未派单");
        stateMap.put(NO_ORDER, "未接单");
        stateMap.put(ORDERED, "已接单");
        stateMap.put(PORTIONTAKEGOODS, "已部分提货");
        stateMap.put(TAKEGOODS, "已提货");
        stateMap.put(PORTIONARRIVED, "已部分到达");
        stateMap.put(ARRIVED, "已确认到达");
        stateMap.put(CALCULATED, "已结算");
        stateMap.put(BEEN_CANCELED, "已取消");
        stateMap.put(INVALID, "已作废");
        stateMap.put(REVOKED, "已撤单");
        stateMap.put(VENT, "放空");
        return listOrder.size();
    }

    @Override
    public Object getItem(int position) {
        return listOrder.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 未派单
     */
    public static final String NO_LEAFLETS = "0";
    /**
     * 已派单,未接单
     */
    public static final String NO_ORDER = "1";
    /**
     * 已接单
     */
    public static final String ORDERED = "2";
    /**
     * 已部分提货
     */
    public static final String PORTIONTAKEGOODS = "5";
    /**
     * 已提货
     */
    public static final String TAKEGOODS = "7";
    /**
     * 已部分到达
     */
    public static final String PORTIONARRIVED = "9";
    /**
     * 已确认到达
     */
    public static final String ARRIVED = "11";
    /**
     * 已结算
     */
    public static final String CALCULATED = "13";
    /**
     * 订单取消
     */
    public static final String BEEN_CANCELED = "-1";
    /**
     * 已作废
     */
    public static final String INVALID = "-3";
    /**
     * 已撤单
     */
    public static final String REVOKED = "-5";
    /**
     * 订单放空
     */
    public static final String VENT = "-7";

    private Map<String, String> stateMap = new HashMap<String, String>();

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.item_order_list, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String startAddress = "";
        String endddress = "";
        String startalias = listOrder.get(position).get("FROMADDRALIAS");
        String endalias = listOrder.get(position).get("TOADDRALIAS");
        if (!startalias.equals("") && startalias != null) {
            startAddress = listOrder.get(position).get("FROMPROVINCE") + listOrder.get(position).get("FROMCITY") + listOrder.get(position).get("FROMDISTRICT") + startalias;
        } else {
            startAddress = listOrder.get(position).get("FROMADDRESS");
        }
        if (!endalias.equals("") && endalias != null) {
            endddress = listOrder.get(position).get("TOPROVINCE") + listOrder.get(position).get("TOCITY") + listOrder.get(position).get("TODISTRICT") +  endalias;
        } else {
            endddress = listOrder.get(position).get("TOADDRESS");
        }
		
        holder.tvTime.setText(listOrder.get(position).get("UPDATE_DATE"));

        holder.tvStartAddress.setText(startAddress);
        holder.tvToAddress.setText(endddress);
        holder.tvTotalCost.setText(listOrder.get(position).get("DRIVERTOTALCOST") + "元");
//        holder.tvTotalCost.setText(listOrder.get(position).get("TOTAL_COST") + "元");
//        holder.tvTotalCost.setText(listOrder.get(position).get("REAL_TRANSPORT_COST") + "元");

        if (listOrder.get(position).get("ISURGENT").equals("1")) {
//            holder.tvUrgent.setVisibility(View.VISIBLE);
            holder.tvUrgent.setVisibility(View.GONE);
        } else {
            holder.tvUrgent.setVisibility(View.GONE);
        }

        final String state = listOrder.get(position).get("STATUS");

        String orderState = null;

        if (NO_ORDER.equals(state)) { // 未接订单，去接单
            orderState = stateMap.get(NO_ORDER);
        } else if (ORDERED.equals(state)) { // 已接单，去提货
            orderState = stateMap.get(ORDERED);
        } else if (PORTIONTAKEGOODS.equals(state)) { // 已部分提货
            orderState = stateMap.get(PORTIONTAKEGOODS);
        } else if (TAKEGOODS.equals(state)) { // 已提货
            orderState = stateMap.get(TAKEGOODS);
        } else if (PORTIONARRIVED.equals(state)) { // 已部分到达
            orderState = stateMap.get(PORTIONARRIVED);
        } else if (ARRIVED.equals(state)) { // 已确认到达
            orderState = stateMap.get(ARRIVED);
        } else if (CALCULATED.equals(state)) { // 已结算
            orderState = stateMap.get(CALCULATED);
        } else if (BEEN_CANCELED.equals(state)) { // 订单取消
            orderState = stateMap.get(BEEN_CANCELED);
        } else if (VENT.equals(state)) { // 订单放空
            orderState = stateMap.get(VENT);
        } else if (INVALID.equals(state)) { // 订单作废
            orderState = stateMap.get(INVALID);
        } else if (REVOKED.equals(state)) { // 订单撤单
            orderState = stateMap.get(REVOKED);
        }
        holder.tvOrderState.setText(orderState);

        return convertView;
    }

    static class ViewHolder {
        private TextView tvTime, tvStartAddress, tvToAddress, tvOrderState, tvTotalCost, tvUrgent;

        public ViewHolder(View v) {
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvStartAddress = (TextView) v.findViewById(R.id.tvStartAddress);
            tvToAddress = (TextView) v.findViewById(R.id.tvToAddress);
            tvOrderState = (TextView) v.findViewById(R.id.tvOrderState);
            tvTotalCost = (TextView) v.findViewById(R.id.tvTotalCost);
            tvUrgent = (TextView) v.findViewById(R.id.tvUrgent);
        }

    }

}
