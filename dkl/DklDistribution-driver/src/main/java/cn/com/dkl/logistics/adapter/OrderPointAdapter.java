package cn.com.dkl.logistics.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.model.LatLng;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.driver.AlertDeleteDialogwindow;
import cn.com.dkl.logistics.driver.ErrorUploadActivity;
import cn.com.dkl.logistics.driver.ImageChooseDialogWindow;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DeliverBean;
import cn.com.dkl.logistics.entity.PointInfo;
import cn.com.dkl.logistics.entity.TstOrderBean;
import cn.com.dkl.logistics.fragment.OrderDetailFragment;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.utils.StoragePopWindow;
import cn.com.dkl.logistics.utils.util;
import cn.com.dkl.logistics.view.CustomProgressDialog;
import cn.com.dkl.logistics.view.DialogWidget;
import cn.com.dkl.logistics.view.PayPasswordView;
import cn.com.dkl.logistics.view.PayPasswordView.OnPayListener;

public class OrderPointAdapter extends BaseAdapter implements util.NoStorageListener{
    private Context context;
    private List<PointInfo> lists;
    private LayoutInflater inflater;
    private ImageChooseDialogWindow window;
    private AlertDeleteDialogwindow wiDialogwindow;
    private static CustomProgressDialog dialog = null;
    private View infoView;
    private DialogWidget mDialogWidget;
    private int selectPosision = -1;
    private int skipPosition = 0;
    private String ordercode;
    private String validata;
    private String takeGoodsTime;
    private String orderNos;
    private String ApprovalState;
    private TstOrderBean mOrderBean;
    private ArrayList<String> waybillStatusList;
    private String waybillnum ;
    private static final String TAG = "OrderPointAdapter";
    StoragePopWindow mStoragePopWindow;
    util mUtil;

    private DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.icon_camera) // 设置图片下载期间显示的图片
            .showImageForEmptyUri(R.drawable.icon_camera) // 设置图片Uri为空或是错误的时候显示的图片
            .showImageOnFail(R.drawable.icon_camera) // 设置图片加载或解码过程中发生错误显示的图片
            .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
            .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
            .displayer(new RoundedBitmapDisplayer(0)) // 设置成圆角图片 new RoundedBitmapDisplayer(20)
            .build();    // 设置图片显示相关参数
    private ImageLoader imageLoader;
    private ViewHolder mHolder;

    public OrderPointAdapter(Context context, List<PointInfo> lists, String takeGoodsTime, String orderNos, String ApprovalState, TstOrderBean tstOrderBean, ArrayList<String> waybillStatusList, String waybillnum) {
        super();
        this.context = context;
        this.lists = lists;
        this.takeGoodsTime = takeGoodsTime;
        this.orderNos = orderNos;
        this.ApprovalState = ApprovalState;
        this.inflater = LayoutInflater.from(context);
        this.mOrderBean = tstOrderBean;
        this.waybillStatusList = waybillStatusList;
        this.waybillnum = waybillnum;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        mHolder = null;

        if (null == convertView) {
            convertView = inflater.inflate(R.layout.list_item_order_address, null);
            mHolder = new ViewHolder(convertView);
            convertView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) convertView.getTag();
        }

        infoView = convertView;
        final PointInfo pInfo = lists.get(position);
        mHolder.tvAddress.setText(pInfo.getProvince() + pInfo.getCity() + pInfo.getDistrict());
        String detailAddress = pInfo.getDetailsAddress();

        if (!pInfo.getAlias().equals("") && pInfo.getAlias() != null) {
            mHolder.tvAddressDetail.setText(pInfo.getAlias());
        } else {
            mHolder.tvAddressDetail.setText(pInfo.getDetailsAddress());
        }

        mHolder.tvLinkMan.setText(pInfo.getLinkMan());
        mHolder.tvPhone.setText(pInfo.getLinkPhone());
        mHolder.imvMark.setVisibility(View.GONE);


        mHolder.tvGoodsInfo.setText(pInfo.getGoodsname() + " " + pInfo.getGoodsweight() + pInfo.getWeightunit() + " " + pInfo.getGoodsvolume() + pInfo.getVolumeunit() + " " + pInfo.getPieceno() + "件 包装类型:" + pInfo.getPackagetype());
//        mHolder.tvGetGoodsName.setText(pInfo.getLinkPhone());
        String payway = pInfo.getPayway();
        if (payway==null || payway.trim().equals("")){
            payway = "0";
        }
        switch (Integer.parseInt(payway)){
            case 6:
                payway = "在线支付";
                break;
            case 11:
                payway = "现付";
                break;
            case 12:
                payway = "到付";
                break;
            case 13:
                payway = "回单付";
                break;
            default:
                payway = "";
                break;
        }
        mHolder.llGetRemark.setVisibility(View.GONE);
        if (!pInfo.getRemarks().equals("")){;
            mHolder.llGetRemark.setVisibility(View.VISIBLE);
            mHolder.tvGetRemark.setText(pInfo.getRemarks());
        }

        mHolder.llGetWorth.setVisibility(View.GONE);
        String addService = "";
        if (pInfo.getIsInsuredtransport().equals("1")){
            addService = isAddService(addService);
            addService += context.getString(R.string.is_insuredtransport);
        }
        if (pInfo.getIsPaymentcollection().equals("1")){
            addService = isAddService(addService);
            addService += context.getString(R.string.is_paymentcollection);
            mHolder.llGetWorth.setVisibility(View.VISIBLE);
            mHolder.tvGetWorth.setText(pInfo.getWorth() + "元");
        }
        if (pInfo.getIsDelivery().equals("1")){
            addService = isAddService(addService);
            addService += context.getString(R.string.is_delivery);
        }
        if (pInfo.getIsUnloading().equals("1")){
            addService = isAddService(addService);
            addService += context.getString(R.string.is_unloading);
        }
        if (pInfo.getIsUpstairs().equals("1")){
            addService = isAddService(addService);
            addService += context.getString(R.string.is_upstairs);
        }
        if (pInfo.getIsLoading().equals("1")){
            addService = isAddService(addService);
            addService += context.getString(R.string.is_loading);
        }

        if (addService.equals("")){
            mHolder.llAddService.setVisibility(View.GONE);
        }else {
            mHolder.llAddService.setVisibility(View.VISIBLE);
        }
        mHolder.tvAddService.setText(addService);
        mHolder.tvPayway.setText(payway + " " + pInfo.getTotalcost() + "元");
//        mHolder.tvGetPayway.setText(pInfo.getLinkPhone());

        mHolder.btnImg.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                imageChoose(position);
            }
        });

        if (pInfo.getIsFirst() == 1) {                      //当前是起点
            mHolder.repeatTop.setVisibility(View.INVISIBLE);
            mHolder.tvPoint.setBackgroundResource(R.drawable.img_order_start_point);
            //隐藏"异常上报"
            mHolder.btnErrorUpload.setVisibility(View.GONE);
            if (getCount()>2){
                mHolder.tvPoint.setText(position + 1 + "");
            }else {
                mHolder.tvPoint.setText("");
            }
            toTakeGoods(pInfo,position);
        } else if (pInfo.getIsLast() == 1) {                //当前是终点
            if (getCount()>2){
                mHolder.tvPoint.setText(position - getCount()/2 + 1 + "");
            }else {
                mHolder.tvPoint.setText("");
            }
            mHolder.tvPoint.setBackgroundResource(R.drawable.img_order_end_point);
            //显示"异常上报"
            mHolder.btnErrorUpload.setVisibility(View.GONE);
            if (position == lists.size() - 1) {
//                mHolder.tvPoint.setBackgroundResource(R.drawable.img_order_end_point);
                mHolder.txtVal.setVisibility(View.GONE);
                mHolder.repeatBottom.setVisibility(View.GONE);
            } else {
//                mHolder.tvPoint.setBackgroundResource(R.drawable.img_order_tu_point);
//                mHolder.tvPoint.setText(position + "");
            }
            mHolder.txtVal.setVisibility(View.VISIBLE);
            mHolder.repeatBottom.setVisibility(View.VISIBLE);
            toGetGoods(pInfo,position);
        } else if (pInfo.getType() == 1) {          //装货点
            mHolder.tvPoint.setBackgroundResource(R.drawable.img_order_take_point);
            toTakeGoods(pInfo,position);
        } else if (pInfo.getType() == 2) {          //卸货点
            mHolder.tvPoint.setBackgroundResource(R.drawable.img_order_ship_point);
            toGetGoods(pInfo,position);
        }

        if (OrderDetailFragment.checkPoint == position) {
            try {
//                holder.btnImg.setImageBitmap(BitmapFactory.decodeStream(context.getContentResolver()
//                        .openInputStream(Uri.parse("file://" + "/" + pInfo.getPicPath()))));
                mHolder.btnImg.setImageBitmap(BitmapFactory.decodeStream(context.getContentResolver()
                        .openInputStream(Uri.parse("file://" + "" + pInfo.getPicPath()))));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        String imgPath = pInfo.getImgpath();
        if (!imgPath.equals("")) {
            //如果当前是未通过的状态，则不执行下面方法.为-1，代表审核失败的订单
            if (lists.get(position).getPointStatus() == 2 && orderNos.indexOf(lists.get(position).getOrderno()) == -1) {
                //未通过的回单
                if ("0".equals(ApprovalState) || ApprovalState.equals("")) {
                    imageLoader = ImageLoader.getInstance();
                    String url = context.getString(R.string.server_imgurl) + "upload/" + imgPath;
                    //String url = context.getString(R.string.server_url) + "upload/" + imgPath;
                    imageLoader.displayImage(url, mHolder.btnImg, options);
                    mHolder.btnImg.setClickable(false);
                } else if ("2".equals(ApprovalState)) {

                }
            } else {
                imageLoader = ImageLoader.getInstance();
                String url = context.getString(R.string.server_imgurl) + "upload/" + imgPath;
                //String url = context.getString(R.string.server_url) + "upload/" + imgPath;
                imageLoader.displayImage(url, mHolder.btnImg, options);
                mHolder.btnImg.setClickable(false);
            }

        }
        ordercode = OrderDetailFragment.ordercode;

        int orderCo = Integer.parseInt(ordercode);
        if (orderCo < 0) {//异常单（作废、撤单、取消、放空）
            mHolder.tvVal.setVisibility(View.GONE);
            mHolder.btnImg.setVisibility(View.GONE);
            //holder.imvMark.setVisibility(View.GONE);
            mHolder.btnTakeGoods.setVisibility(View.GONE);
        }

        mHolder.btnErrorUpload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转至异常上报页面
                Intent intent = new Intent(context,ErrorUploadActivity.class);
                context.startActivity(intent);
            }
        });

        mHolder.btnTakeGoods.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                /**判断是否有存储空间 */
                if (mUtil == null){
                    mUtil = new util();
                }
                mUtil.setmNoStorageListener(OrderPointAdapter.this);
                mUtil.getStorageCapacity();

                if (lists.get(position).getPointStatus() == 0 && lists.get(position).getType() == 1) {
                    if (lists.get(position).getPicPath() == null || lists.get(position).getPicPath().equals("")) {
                        //Toast.makeText(context, "请选择提货凭证图片!", Toast.LENGTH_SHORT).show();
                        imageChoose(position);
                    } else {
                        doUpload(position, lists.get(position).getDeliverId(),lists.get(position).getPicPath());
                    }
                } else if (lists.get(position).getPointStatus() == 0) {
                    if (!isTakeGoods()) {          //是否提货
                        Toast.makeText(context, "尚未提货", Toast.LENGTH_SHORT).show();
                    } else {
                        String tDelete = "确认货物已送达？";
                        String tCancel = "确认";
                        ClickListener listener = new ClickListener(position);
                        wiDialogwindow = new AlertDeleteDialogwindow((Activity) context, listener, tDelete, tCancel);
                        wiDialogwindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                        wiDialogwindow.showAtLocation(infoView.findViewById(R.id.repeatTop), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    }
                } else if (lists.get(position).getPointStatus() == 1) {
                    String p = lists.get(position).getPicPath();
                    if (lists.get(position).getPicPath() == null || lists.get(position).getPicPath().equals("")) {
                        //Toast.makeText(context, "请选择回单凭证图片!", Toast.LENGTH_SHORT).show();
                        imageChoose(position);
                    } else {
                        selectPosision = position;
                        /*mDialogWidget = new DialogWidget((Activity) context, getDecorViewDialog());
                        mDialogWidget.show();*/
                        doUpload(position, lists.get(position).getDeliverId(),lists.get(position).getPicPath());
                    }
                } else if (lists.get(position).getPointStatus() == 2 || orderNos.indexOf(lists.get(position).getOrderno()) == -1) {
                    String p = lists.get(position).getPicPath();
                    if (lists.get(position).getPicPath() == null || lists.get(position).getPicPath().equals("")) {
                        //Toast.makeText(context, "请选择回单凭证图片!", Toast.LENGTH_SHORT).show();
                        imageChoose(position);
                    } else {
                        selectPosision = position;
                        /*mDialogWidget = new DialogWidget((Activity) context, getDecorViewDialog());
                        mDialogWidget.show();*/
                        doUpload(position, lists.get(position).getDeliverId(),lists.get(position).getPicPath());
                    }
                }
            }
        });

        //点击手机号码拨打电话(跳转拨号界面)
        mHolder.tvPhone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + lists.get(position).getLinkPhone())));
            }
        });
        return convertView;
    }


    private String isAddService(String str){
        if (!str.equals("")){
            str += "/";
        }
        return str;
    }

    protected void imageChoose(int position) {
        skipPosition = position;
        String imgCreden = "";
        String text = "";
        OrderDetailFragment.checkPoint = position;
        if (lists.get(position).getType() == 1) {
            imgCreden = context.getResources().getString(R.string.img_takeGoods);
            text = context.getResources().getString(R.string.img_skip_take);
        } else if (lists.get(position).getType() == 2) {
            imgCreden = context.getResources().getString(R.string.img_arrivedGoods);
            text = context.getResources().getString(R.string.img_skip_arrived);
        }
        window = new ImageChooseDialogWindow((Activity) context, imgCreden);
//        window.setFromPhotoGone();
//        window.setFromNoChoose(text,listener);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        window.showAtLocation(((Activity) context).findViewById(R.id.orderLinear), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    /**
     * 不知道干嘛的
     */
    protected View getDecorViewDialog() {
        return PayPasswordView.getInstance("请输入服务验证码", this.context, new OnPayListener() {

            @Override
            public void onSurePay(String password) {
                mDialogWidget.dismiss();
                mDialogWidget = null;
//				payTextView.setText(password);
//				Toast.makeText(getApplicationContext(), "确认支付", Toast.LENGTH_SHORT).show();

                if (selectPosision >= 0) {
//                    doUpload(selectPosision);
                    validata = password;
                }
            }

            @Override
            public void onCancelPay() {
                mDialogWidget.dismiss();
                mDialogWidget = null;
//				Toast.makeText(getApplicationContext(), "取消支付", Toast.LENGTH_SHORT).show();

            }
        }).getView();
    }

    static class ViewHolder {
        private TextView tvVal, txtVal, tvAddress, tvAddressDetail, tvLinkMan, tvPhone, tvPoint, tvGoodsTime, tvGetGoodsTime,
                tvSpendTime, tvSpendT, tvGoodsInfo, tvPayway, tvAddService, tvWorth, tvGetWorth, tvRemark, tvGetRemark;//地址、联系人信息text
        private ImageView btnImg, imvMark;//图标
        private Button btnTakeGoods,btnErrorUpload; //提货、确认到达、回单
        private LinearLayout repeatTop, repeatBottom, llGetGoodsTime, llSpendTime, llAddService, llGetWorth, llGetRemark; //上下虚线布局

        public ViewHolder(View v) {
            tvVal = (TextView) v.findViewById(R.id.tvVal);
            txtVal = (TextView) v.findViewById(R.id.txtVal);
            tvAddress = (TextView) v.findViewById(R.id.tvAddress);
            tvAddressDetail = (TextView) v.findViewById(R.id.tvAddressDetail);
            tvLinkMan = (TextView) v.findViewById(R.id.tvLinkMan);
            tvPhone = (TextView) v.findViewById(R.id.tvPhone);
            tvGoodsInfo = (TextView) v.findViewById(R.id.tvGoodsInfo);
            tvPayway = (TextView) v.findViewById(R.id.tvPayway);
            tvAddService = (TextView) v.findViewById(R.id.tvAddService);
            btnImg = (ImageView) v.findViewById(R.id.btnImg);
            btnErrorUpload = (Button) v.findViewById(R.id.btnErrorUpload);
            imvMark = (ImageView) v.findViewById(R.id.imvMark);
            btnTakeGoods = (Button) v.findViewById(R.id.btnTakeGoods);
            repeatTop = (LinearLayout) v.findViewById(R.id.repeatTop);
            repeatBottom = (LinearLayout) v.findViewById(R.id.repeatBottom);
            tvPoint = (TextView) v.findViewById(R.id.tvPoint);
            tvGoodsTime = (TextView) v.findViewById(R.id.tvGoodsTime);//提货时间 or 回单时间
            tvGetGoodsTime = (TextView) v.findViewById(R.id.tvGetGoodsTime);// 时间
            tvSpendTime = (TextView) v.findViewById(R.id.tvSpendTime);//花费时间
            llGetGoodsTime = (LinearLayout) v.findViewById(R.id.llGetGoodsTime);
            llSpendTime = (LinearLayout) v.findViewById(R.id.llSpendTime);
            llAddService = (LinearLayout) v.findViewById(R.id.llAddService);
            tvSpendT = (TextView) v.findViewById(R.id.tvSpendT);
            tvWorth = (TextView) v.findViewById(R.id.tvWorth);
            tvGetWorth = (TextView) v.findViewById(R.id.tvGetWorth);
            llGetWorth = (LinearLayout) v.findViewById(R.id.llGetWorth);
            tvRemark = (TextView) v.findViewById(R.id.tvRemark);
            tvGetRemark = (TextView) v.findViewById(R.id.tvGetRemark);
            llGetRemark = (LinearLayout) v.findViewById(R.id.llGetRemark);
        }
    }


    /**
     * 上传图片
     */
    private void doUpload(final int position, String DELIVER_ID, String imagePath) {
        dialog = new CommonTools(context).getProgressDialog(context, "上传图片中...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        final RequestParams params = new RequestParams();
        File file = new File(imagePath);
        if (file.exists()) {
            params.addBodyParameter("Filedata", file);
            params.addBodyParameter("DELIVER_ID", DELIVER_ID);
            String url = context.getString(R.string.server_url) + URLMap.UPLOAD_IMAGE;

            Log.i(TAG, "UploadUrl: " + url + "Filedata" + "=" + file);
            LogUtils.i(url + CommonTools.getParams(params));

            HttpUtils http = new HttpUtils(60 * 1000);
            http.configCurrentHttpCacheExpiry(1000 * 10);

            http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
                @Override
                public void onStart() {
                    super.onStart();
                    dialog.show();
                }

                @Override
                public void onSuccess(ResponseInfo<String> result) {
                    try {
                        LogUtils.i("result= " + result.result);
                        JSONObject obj = new JSONObject(result.result);
                        String message = obj.getString("message");
                        String status = obj.getString("status");
                        if ("1".equals(status)) {
                            JSONArray datas = obj.getJSONArray("data");
                            String imgFileName = null;
                            for (int i = 0; i < datas.length(); i++) {
                                JSONObject data = datas.getJSONObject(i);
                                imgFileName = CommonTools.judgeNull(data, "FILENAME", "");
                            }
                            if (lists.get(position).getPointStatus() == 0 && lists.get(position).getType() == 1) { //确认提货
                                if (CommonTools.isRangeInto(getLatlng(position))) {
                                    String ordernum = lists.get(position).getOrderno();
                                    OrderDetailFragment.doTakeGoods(imgFileName, position, waybillnum, ordernum);
                                } else {
                                    Toast.makeText(context, "尚未到达目的地", Toast.LENGTH_SHORT).show();
                                }
                            } else if (lists.get(position).getPointStatus() == 1) {    //重新请求   上传回单
                                if (CommonTools.isRangeInto(getLatlng(position))) {
                                    String ordernum = lists.get(position).getOrderno();
                                    OrderDetailFragment.doReceipt(imgFileName, position, validata, true, waybillnum, ordernum);
                                } else {
                                    Toast.makeText(context, "尚未到达目的地", Toast.LENGTH_SHORT).show();
                                }
                            } else if (lists.get(position).getPointStatus() == 2 && "2".equals(ApprovalState)) {  //上传回单
                                if (CommonTools.isRangeInto(getLatlng(position))) {
                                    String ordernum = lists.get(position).getOrderno();
                                    OrderDetailFragment.doReceipt(imgFileName, position, validata, true, waybillnum, ordernum);
                                } else {
                                    Toast.makeText(context, "尚未到达目的地", Toast.LENGTH_SHORT).show();
                                }
                            }
                            dialog.dismiss();
                        } else if ("0".equals(status)) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        } else if ("-1".equals(status)) {
                            Toast.makeText(context, "服务器异常", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(context, "图片上传失败！", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(HttpException arg0, String arg1) {
                    CommonTools.failedToast(context);
                    dialog.dismiss();
                }
            });
        }
    }


    class ClickListener implements OnClickListener {
        private int position;

        public ClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            OrderDetailFragment.checkPoint = position;
            //Toast.makeText(context, "确认到达", Toast.LENGTH_SHORT).show();
            wiDialogwindow.dismiss();
            OrderDetailFragment.doConfirmArrived(position);
        }

    }

    /**
     * 获取目的地坐标点
     */
    public LatLng getLatlng(final int position) {
        List<DeliverBean> devrlist = mOrderBean.getDeliverList();
        DeliverBean bean = devrlist.get(position);
        LatLng p2 = new LatLng(Double.valueOf(bean.getLATITUDE()), Double.valueOf(bean.getLONGITUDE()));
        return p2;
    }

    /**
     * 根据审批状态进行操作
     */
    public void toApproval() {
        if ("0".equals(ApprovalState)) {        //审批中
            mHolder.imvMark.setVisibility(View.VISIBLE);
            mHolder.imvMark.setImageDrawable(context.getResources().getDrawable(R.drawable.img_mark_ship));
            mHolder.btnTakeGoods.setVisibility(View.GONE);
            mHolder.btnImg.setClickable(false);
        } else if ("2".equals(ApprovalState)) {     //审批未通过
            mHolder.imvMark.setVisibility(View.VISIBLE);
            mHolder.imvMark.setImageDrawable(context.getResources().getDrawable(R.drawable.img_mark_reviewed));
            mHolder.btnTakeGoods.setVisibility(View.VISIBLE);
            mHolder.btnTakeGoods.setText("上传回单");
            mHolder.btnImg.setClickable(true);
        }else {
            mHolder.btnImg.setClickable(false);
        }
    }

    /**
     * 提货操作
     */
    public void toTakeGoods(PointInfo pInfo, final int position) {
        if (pInfo.getPointStatus() == 0) {      //提货
            mHolder.btnTakeGoods.setText("提货");
        } else if (pInfo.getPointStatus() == 1) {       //到达
            mHolder.btnImg.setClickable(false);
            mHolder.imvMark.setVisibility(View.VISIBLE);
            mHolder.imvMark.setImageDrawable(context.getResources().getDrawable(R.drawable.img_mark_take));
            mHolder.btnTakeGoods.setVisibility(View.GONE);
            /*
            mHolder.tvGetGoodsTime.setText(pInfo.getOperatetime());
            mHolder.tvGoodsTime.setText("提货时间 :");
            mHolder.tvSpendT.setText("提货用时 :");
            //TODO 时间错误
            if(position == 0){
                mHolder.tvSpendTime.setText(CommonTools.formatSeconds(CommonTools.compareTime(pInfo.getOperatetime(), takeGoodsTime)));
            }else {
                mHolder.tvSpendTime.setText(CommonTools.formatSeconds(CommonTools.compareTime(pInfo.getOperatetime(), lists.get(position - 1).getOperatetime())));
            }
            mHolder.llSpendTime.setVisibility(View.VISIBLE);
            mHolder.llGetGoodsTime.setVisibility(View.VISIBLE);
            */
        }
    }

    /**
     * 回单操作
     * @param pInfo
     * @param position
     */
    public void toGetGoods(PointInfo pInfo, final int position){
        if (pInfo.getPointStatus() == 0) {              //提货
            mHolder.btnImg.setVisibility(View.GONE);
            mHolder.btnTakeGoods.setText("确认到达");
        } else if (pInfo.getPointStatus() == 1) {       //到达
            mHolder.btnImg.setVisibility(View.VISIBLE);
            mHolder.btnTakeGoods.setText("上传回单");
        } else if (pInfo.getPointStatus() == 2) {       //回单
            /*
            mHolder.tvGetGoodsTime.setText(pInfo.getOperatetime());
            mHolder.tvGoodsTime.setText("配送时间 :");
            mHolder.tvSpendT.setText("配送用时 :");
            //TODO 时间错误
            mHolder.tvSpendTime.setText(CommonTools.formatSeconds(CommonTools.compareTime(pInfo.getOperatetime(),
                    lists.get(position - 1).getOperatetime())));

            mHolder.llSpendTime.setVisibility(View.VISIBLE);
            mHolder.llGetGoodsTime.setVisibility(View.VISIBLE);
            */
            mHolder.imvMark.setVisibility(View.VISIBLE);
            mHolder.imvMark.setImageDrawable(context.getResources().getDrawable(R.drawable.img_mark_ship));
            mHolder.btnTakeGoods.setVisibility(View.GONE);
            if ("11".equals(pInfo.getWaybillstatus()) || orderNos.indexOf(pInfo.getOrderno()) != -1 && pInfo.getPointStatus() == 2) {
                mHolder.imvMark.setVisibility(View.VISIBLE);
                mHolder.imvMark.setImageDrawable(context.getResources().getDrawable(R.drawable.img_mark_ship));
                mHolder.btnImg.setClickable(false);
                mHolder.btnTakeGoods.setVisibility(View.GONE);
            } else if (orderNos.indexOf(pInfo.getOrderno()) == -1) {
                //审批
                toApproval();
            }
        }
    }

    /**
     * 是否提货
     * @return
     */
    public boolean isTakeGoods(){
        for (int i = 0; i < waybillStatusList.size(); i++){
            String isTake = waybillStatusList.get(i);
            if ("7".equals(isTake)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void noStorage() {
        //清楚缓存
        clearCacheWindow();
    }
    /**
     * 清除缓存弹窗
     */
    private void clearCacheWindow() {
        mStoragePopWindow = new StoragePopWindow((Activity) context);
        mStoragePopWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mStoragePopWindow.showAtLocation(infoView, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            window.dismiss();
            String ordernum = lists.get(skipPosition).getOrderno();
            if (lists.get(skipPosition).getType() == 1) {
                OrderDetailFragment.doTakeGoods("", skipPosition, waybillnum, ordernum);
            } else if (lists.get(skipPosition).getType() == 2) {
                OrderDetailFragment.doReceipt("", skipPosition, validata, true, waybillnum, ordernum);
            }
        }
    };

}
