package cn.com.dkl.logistics.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.route.DrivingRouteLine;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.utils.Constant;

/**
 * Created by Hexway on 2017/1/10.
 */

public class RouteLine2Adapter extends BaseAdapter {

    private List<? extends RouteLine> routeLines;
    private LayoutInflater layoutInflater;
    private Type mtype;
    private int count = 0;
    private Activity mActivity;
    private List<Double> NodeList;
    private Intent mIntent;
    private Context mContext;
    private int position;
    public RouteLine2Adapter(Context context, List<? extends RouteLine> routeLines, Type type) {
        this.routeLines = routeLines;
        count = routeLines.size();
        layoutInflater = LayoutInflater.from(context);
        mtype = type;
        mActivity = (Activity) context;
        mContext = context;
    }

    public void setData(List<? extends RouteLine> routeLines, Type type) {
        this.routeLines = routeLines;
        count = routeLines.size();
        mtype = type;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return routeLines.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        mIntent = new Intent();
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_route_line, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.mBtnStartNavi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO 传坐标集合
//                MapNavigationUtil.setData(mActivity);
//                MapNavigationUtil.routeplanToNavi();
//                MapNavigationUtil.OpenBaiduNavi(mActivity);
//
                Intent it = new Intent("StartNavigation");
                it.putExtra("RountIndex", Constant.LastPosition);
                mContext.sendBroadcast(it);


            }
        });

        switch (mtype) {
            case TRANSIT_ROUTE:
            case WALKING_ROUTE:
            case BIKING_ROUTE:
//                holder.name.setText("路线" + (position + 1));
//                int time = routeLines.get(position).getDuration();
//                if ( time / 3600 == 0 ) {
//                    holder.lightNum.setText( "大约需要：" + time / 60 + "分钟" );
//                } else {
//                    holder.lightNum.setText( "大约需要：" + time / 3600 + "小时" + (time % 3600) / 60 + "分钟" );
//                }
//                holder.dis.setText("距离大约是：" + routeLines.get(position).getDistance() + "米");
                break;

            case DRIVING_ROUTE:
                DrivingRouteLine drivingRouteLine = (DrivingRouteLine) routeLines.get(position);
//                holder.tvLineNum.setText("线路" + (position + 1)+"，共"+count+"条线路");
                holder.tvLineNum.setText("共找到" + count + "条线路，" + "当前为第" + (position + 1) + "条线路");
//                holder.lightNum.setText( "红绿灯数：" + drivingRouteLine.getLightNum());
                holder.tvJamDistance.setText("拥堵距离" + drivingRouteLine.getCongestionDistance() + "米");

                double distance = drivingRouteLine.getDistance();
                DecimalFormat df = new DecimalFormat("###.00");
                holder.tvDistance.setText("共" + df.format(distance / 1000) + "公里");

                int time = drivingRouteLine.getDuration();
                if (time / 3600 == 0) {
                    holder.tvTime.setText("大约需要：" + time / 60 + "分钟");
                } else {
                    holder.tvTime.setText("大约需要：" + time / 3600 + "小时" + (time % 3600) / 60 + "分钟");
                }
                break;
            case MASS_TRANSIT_ROUTE:
//                MassTransitRouteLine massTransitRouteLine = (MassTransitRouteLine) routeLines.get(position);
//                holder.name.setText("线路" + (position + 1));
//                holder.lightNum.setText( "预计达到时间：" + massTransitRouteLine.getArriveTime() );
//                holder.dis.setText("总票价：" + massTransitRouteLine.getPrice() + "元");
                break;

            default:
                break;

        }

        return convertView;
    }






    public enum Type {
        MASS_TRANSIT_ROUTE, // 综合交通
        TRANSIT_ROUTE, // 公交
        DRIVING_ROUTE, // 驾车
        WALKING_ROUTE, // 步行
        BIKING_ROUTE // 骑行

    }


    static class ViewHolder {
        @Bind(R.id.tvLineNum)
        TextView tvLineNum;
        @Bind(R.id.tvTime)
        TextView tvTime;
        @Bind(R.id.tvDistance)
        TextView tvDistance;
        @Bind(R.id.tvJamDistance)
        TextView tvJamDistance;
        @Bind(R.id.btnStartNavi)
        Button mBtnStartNavi;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
