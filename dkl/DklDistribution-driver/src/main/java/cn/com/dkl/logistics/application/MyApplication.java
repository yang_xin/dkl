package cn.com.dkl.logistics.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.util.Log;

import com.baidu.mapapi.SDKInitializer;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.tencent.bugly.Bugly;
//import com.tencent.stat.StatConfig;
//import com.tencent.stat.StatService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.x;

import java.io.File;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.LogUtil;
import cn.jpush.android.api.JPushInterface;

public class MyApplication extends Application {
    private Context context = MyApplication.this;
    private SharedPreferences sp;

    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);
//        x.Ext.setDebug(BuildConfig.DEBUG);

        //输出类初始化
        LogUtil.initialize(getApplicationContext());

        //应用宝统计SDK
        try {
//            StatConfig.setDebugEnable(true);
//            StatConfig.initNativeCrashReport(this,null);
//            StatService.startStatService(this, getString(R.string.QQ_appKey),
//                    com.tencent.stat.common.StatConstants.VERSION);
        } catch (Exception e){
            e.printStackTrace();
            Log.e("Application", "应用宝统计启动失败" );
        }

        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        // 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext
        SDKInitializer.initialize(this);

        //初始化xutils的log
//		LogUtils.allowD = false;
//		LogUtils.allowE = false;
//		LogUtils.allowI = false;
//		LogUtils.allowV = false;
//		LogUtils.allowW = false;
//		LogUtils.allowWtf = false;

        // 获取定位配置
//        getLocationConfig();
        //初始化 JPush
        JPushInterface.setDebugMode(false);  // 设置开启日志,发布时请关闭日志
        JPushInterface.init(this);
        //初始化 讯飞语音
        SpeechUtility.createUtility(context, "appid=" + this.getString(R.string.xfcloud_appid) + SpeechConstant.FORCE_LOGIN + "=true");
        initImageLoader(getApplicationContext());
        // 异常上报
//        CrashReport.initCrashReport(getApplicationContext(), Constants.APPID, false);
        Bugly.init(getApplicationContext(), Constants.APPID, false);
    }

    private void getLocationConfig() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("CODE", "CDLocationFrequency");

        String url = getString(R.string.server_url) + URLMap.GET_CD_INTERFACE_CONFIG;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("获取定位配置信息失败..");
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONArray data = obj.getJSONArray("data");

                        if (data.length() > 0) {
                            JSONObject firstData = data.getJSONObject(0);
                            Editor edit = sp.edit();
                            edit.putInt(Constants.REPORT_FREQUENCY, Integer.parseInt(CommonTools.judgeNull(firstData, "CONFIG", "1")));
                            edit.commit();
                        }
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }

    public static void initImageLoader(Context context) {
        //缓存文件的目录
        File cacheDir = null;
        String sPath = Environment.getExternalStorageDirectory().toString() + "/" + context.getPackageName() + "/cache";
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//            cacheDir = StorageUtils.getOwnCacheDirectory(context, context.getExternalCacheDirs().toString());
//        }else {
            cacheDir = StorageUtils.getOwnCacheDirectory(context, sPath);
//        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(480, 800) // max width, max height，即保存的每个缓存文件的最大长宽
                .threadPoolSize(3) //线程池内加载的数量
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator()) //将保存的时候的URI名称用MD5 加密
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // You can pass your own memory cache implementation/你可以通过自己的内存缓存实现
                .memoryCacheSize(2 * 1024 * 1024) // 内存缓存的最大值
                .diskCacheSize(50 * 1024 * 1024)  // 50 Mb sd卡(本地)缓存的最大值
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                // 由原先的discCache -> diskCache
                .diskCache(new UnlimitedDiscCache(cacheDir))//自定义缓存路径
                .imageDownloader(new BaseImageDownloader(context, 5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)超时时间
                .writeDebugLogs() // Remove for release app
                .build();
        //全局初始化此配置
        ImageLoader.getInstance().init(config);
    }

}
