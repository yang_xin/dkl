package cn.com.dkl.logistics.constant;

public class Constants {
    /**
     * 定位类型
     */
    public static final int LOCATION_TYPE = 1;
    /**
     * 上报位置时间间隔(小时)
     */
    public final static String REPORT_FREQUENCY = "report_frequency";
    /**
     * 上报位置接口
     */
    public final static String REPORT_LOCATION = "api/wlpt/locationBiz/saveLocationInfo?";
    /**
     * 车牌号
     */
    public final static String CAR_ID = "car_id";
    /**
     * 最后一次上传时间
     */
    public final static String LAST_REPORT_LOCATION_TIME = "last_report_location_time";
    /**
     * 经度
     */
    public final static String LONGTITUDE = "longitude";
    /**
     * 纬度
     */
    public final static String LATITUDE = "latitude";
    /**
     * 速度
     */
    public final static String SPEED = "speed";
    /**
     * 标识同城配送，如注册、登录
     */
    public static final String ISSPECIAL = "1";
    /**
     * 社会车队ID
     */
    public static final String TEAMID = "b322ba32e8b54561b1f19bbf6ffa2e61";
    /**
     * 回单审核，通过
     */
    public static final String RECEIPTAGREEGEE = "1";
    /**
     * 回单审核，不通过
     */
    public static final String RECEIPTDISAGREEGEE = "2";
    /**
     * 腾讯自动更新Bugly APP ID
     */
    public static final String APPID = "67d3822070";
    /**
     * 目的地范围
     */
    public static final int RANGE = 1000;
    /**
     * 服务器地址
     */
    public static final String SERVER_URL = "http://www.wuetong.com/";
    /**
     * 是否是自有司机
     */
    public static String ISSOCIALUSER = "";
    /**
     * 静止的误差距离
     */
    public static final double STATICDISTANCE = 20.0;
    /**
     * 存储位置 0:内存 1:SD卡
     */
    public static String ISSDLOCATION = "1";
    /**
     * 照片存储路径
     */
    public static String STORAGEPATH = "";

}
