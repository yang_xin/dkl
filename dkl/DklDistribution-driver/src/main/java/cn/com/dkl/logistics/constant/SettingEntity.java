package cn.com.dkl.logistics.constant;

/**
 * 系统设置
 *
 * @author Dao
 */
public class SettingEntity {
    /**
     * 记住用户名
     */
    public final static String REMEMBER_USER_NAME = "remember_user_name";
    /**
     * 记住密码
     */
    public final static String REMEMBER_PASSWORD = "remember_password";
    /**
     * 数据库url
     */
    public final static String DATA_BASE_URL = "data_base_url";
    /**
     * apkurl
     */
    public final static String APK_URL = "apk_url";
    /**
     * 是否第一次使用
     */
    public final static String IS_FIRST_USE = "is_first_use";
    /**
     * 版本号
     */
    public final static String VERSION_CODE = "version_code";

}