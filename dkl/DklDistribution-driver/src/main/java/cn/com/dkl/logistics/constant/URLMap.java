package cn.com.dkl.logistics.constant;

/**
 * 接口地图
 *
 * @author guochaohui
 */

public class URLMap {
    public final static boolean isDebug = true;
    public static String DEFAULT_USER_ID = "";
    public static String DEFAULT_COMPANY_ID = "";

    private static final String STRING = "api/wlpt/";
    /**
     * 版本更新
     */
    public static final String VERSION_UPDATE = STRING + "version/versionUpdate?";
    /**
     * 登陆
     */
    public static final String LOGIN = "api/wlpt/user/login?";
    /**
     * 退出
     */
    public static final String LOGOUT = "api/wlpt/user/logout?";
    /**
     * 注册
     */
    public static final String REGISTER = "api/wlpt/user/reg?";
    /**
     * 注册验证码
     */
    public static final String SEND_REGISTER_SMS_CODE = "api/wlpt/sms/sendRegVerCode?";
    /**
     * 找回密码
     */
    public static final String FIND_PASSWORD = "api/wlpt/user/forgetPassWord?";
    /**
     * 找回密码验证码
     */
    public static final String FIND_PASSWORD_SMS_CODE = "api/wlpt/sms/sendFindPwdVerCode?";
    /**
     * 找回支付密码
     */
    public static final String FIND_PAYMENT_PASSWORD = "api/wlpt/user/forgetPaymentPassWord?";
    /**
     * 修改密码
     */
    public static final String MODIFY_PASSWORD = "api/wlpt/user/updatePassWord?";
    /**
     * 修改密码验证码
     */
    public static final String MODIFY_PASSWORD_SMS_CODE = "api/wlpt/sms/sendUpdatePwdVerCode?";
    /**
     * 个人认证
     */
    public static final String PERSONAL_AUTHEN = "api/wlpt/user/personalAuthen?";
    /**
     * 获取认证信息
     */
    public static final String GET_AUTHEN_INFO = "api/wlpt/user/getAuthen?";
    /**
     * 图片上传接口
     */
    public static final String UPLOAD_IMAGE = "api/common/upload?";
    /**
     * 编辑个人信息
     */
    public static final String EDIT_PERSONAL_INFO = "api/wlpt/user/editUserInfo?";
    /**
     * 获取我的消息
     */
    public static final String GET_My_MESSAGE = STRING + "message/myMessage?";
    /**
     * 删除我的消息
     */
    public static final String DELETE_MY_MESSAGE = STRING + "message/deleteMessage?";
    /**
     * 我的消息已读状态
     */
    public static final String IS_READ_MESSAGE = "api/wlpt/message/readMessage?";
    /**
     * 获取我的消息详情
     */
    public static final String MY_MESSAGE_RESULT_DETAILS = STRING + "message/myMessageDetails?";
    /**
     * 物流服务协议
     */
    public static final String SERVICE_AGREEMENT = "api/wlpt/agreement/getAgreement?";
    /**
     * 运单评价单
     */
    public static final String ORDER_COMMENT = "api/wlpt/order/orderComment?";
    /**
     * 文案-我的专享
     */
    public static final String MY_EXCLUSIVE_DETAILS = STRING + "agreement/getAgreement?";
    /**
     * 司机认证
     */
    public static final String DRIVER_AUTHEN = "api/wlpt/cduser/personalAuthen?";
    /**
     * 获取车型、车长信息
     */
    public static final String GET_CAR_TYPE_LIST = "api/wlpt/cdscartype/getCdsCarTypeList?";
    /**
     * 获取司机认证信息
     */
    public static final String GET_DRIVER_AUTHEN_INFO = "api/wlpt/cduser/getAuthen?";
    /**
     * 获取司机个人信息
     */
    public static final String GET_DRIVER_USERINFO = "api/wlpt/cduser/getSimpleUserInfo?";
    /**
     * 修改车辆状态
     */
    public static final String UPDATE_CAR_STATE = "api/wlpt/car/processCar?";
    /**
     * 处理订单
     */
    public static final String PROCESS_ORDER = "api/wlpt/cdorder/processOrder?";
    /**
     * 处理订单:发送提货码
     */
    public static final String SEND_PROCESS_ORDER = "api/wlpt/cdorder/sendProcessOrder?";
    /**
     * 处理订单-多提多送
     */
    public static final String PROCESS_CDSORDER = "api/wlpt/cdsorder/processOrder?";
    /**
     * 处理订单:发送提货码-多提多送
     */
    public static final String SEND_PROCESS_CDSORDER = "api/wlpt/cdsorder/sendProcessOrder?";
    /**
     * 获取消息列表
     */
    public static final String MY_MESSAGE = "api/wlpt/message/myMessage?";
    /**
     * 订单详情
     */
    public static final String ORDER_DETAIL = "api/wlpt/cdorder/orderDetails?";
    /**
     * 订单详情-多提多送
     */
    public static final String CDSORDER_DETAIL = "api/wlpt/cdsorder/orderDetails?";
    /**
     * 上传回单
     */
    public static final String UPLOAD_RECEIPTIMG = "api/wlpt/cdorder/uploadReceiptImg?";
    /**
     * 上传回单-多提多送
     */
    public static final String CDSUPLOAD_RECEIPTIMG = "api/wlpt/cdsorder/uploadReceiptImg?";
    /**
     * 订单列表
     */
    public static final String GET_ORDER_LIST = "api/wlpt/cdorder/findOrder?";
    /**
     * 订单列表-多提多送
     */
    public static final String GET_CDSORDER_LIST = "api/wlpt/waybill/waybillList?";
    /**
     * 确认提货
     */
    public static final String TAKE_GOODS = "api/wlpt/cdorder/takeGoods?";
    /**
     * 确认提货-多提多送
     */
    public static final String CDSTAKE_GOODS = "api/wlpt/cdsorder/takeGoods?";
    /**
     * 确认到达目的地
     */
    public static final String ARRIVED_DESTINATION = "api/wlpt/cdorder/arriveDestination?";
    /**
     * 确认到达目的地-多提多送
     */
    public static final String CDSARRIVED_DESTINATION = "api/wlpt/cdsorder/arriveDestination?";
    /**
     * 运单轨迹
     */
    public static final String ORDER_TRACK = "api/wlpt/cdorder/orderTrack?";
    /**
     * 提交审核
     */
    public static final String SUBMITE_ADDIT = "api/wlpt/waybill/uploadReviewed?";
    /**
     * 我的订单评价
     */
    public static final String ORDER_EVALUATE = "api/wlpt/cdorder/myOrderComment?";
    /**
     * 我的订单评价-多提多送
     */
    public static final String CDSORDER_EVALUATE = "api/wlpt/cdsordercomment/myOrderComment?";
    /**
     * 订单评价列表
     */
    public static final String All_WAYBILLEVALUATE = "api/wlpt/cdorder/myWayBillEvaluateList?";
    /**
     * 绑定银行卡号
     */
    public static final String BIND_BANKCARD = "api/wlpt/cduser/updateBankCard?";
    /**
     * 提现
     */
    public static final String WITHDRAWCASH = "api/wlpt/cash/withdrawCash?";
    /**
     * 设置支付/提现密码
     */
    public static final String UPDATAPAYPASSWORD = "api/wlpt/cduser/updatePaymentPassword?";
    /**
     * 支付密码是否为空
     */
    public static final String ISSETPAYMENTPAW = "api/wlpt/cduser/isSetPaymentPassword?";
    /**
     * 账单查询
     */
    public static final String MY_ACCOUNT_BILL = "api/wlpt/cduser/myAccountBill?";
    /**
     * 提现记录查询
     */
    public static final String GET_USER_CASH_LOG = "api/wlpt/cash/getUserCashLog?";
    /**
     * 意见反馈
     */
    public static final String FEEDBACK = "api/wlpt/feedback/saveFeedBack?";
    /**
     * 关于我们
     */
    public static final String GET_ABOUTS = "api/wlpt/aboutusapi/getAboutUs?";
    /**
     * 获取银行卡信息
     */
    public static final String GET_USERCASHINFO = "api/wlpt/cduser/getUserCashInfo?";
    /**
     * 获取定位配置
     */
    public static final String GET_CD_INTERFACE_CONFIG = "api/wlpt/interfaceconfig/getCDInterfaceConfig";
    /**
     * 获取周边车辆
     */
    public static final String GET_ARROUND_CAR = "api/wlpt/carsource/nearbyCar?";
    /**
     * 获取周边商超
     */
    public static final String GET_ARROUND_SUPERMARKET = "api/wlpt/businesssuper/getListAll?";
    /**
     * 获取司机运力统计
     */
    public static final String GET_DRIVER_STATISTICAL = "api/wlpt/statistical/driverStatistical?";
    /**
     * 获取车辆列表
     */
    public static final String GET_CARLIST = "api/wlpt/car/findCar?";
    /**
     * 获取车辆详情
     */
    public static final String GET_CARINFO = "api/wlpt/car/carInfo?";
    /**
     * 新增或编辑车辆
     */
    public static final String SAVE_EDIT_CAR = "api/wlpt/car/saveCar?";
    /**
     * 提交车辆认证
     */
    public static final String AUTH_CAR = "api/wlpt/car/carAuth?";
    /**
     * 删除车辆
     */
    public static final String DELETE_CAR = "api/wlpt/car/deleteCar?";
    /**
     * 获取园区列表全部
     */
    public static final String GET_PARK_LIST = "api/wlpt/park/listAll?";
    /**
     * 获取园区列表全部
     */
    public static final String GET_COMPANY_LIST = "api/wlpt/company/listAll?";
    /**
     * 获取抢单列表测试数据
     */
    public static final String GET_ORDERLIST = "api/wlpt/cdsorder/findGrabSingle?";
    /**
     * 获取抢单列表
     */
    public static final String GET_GRABORDERLIST = "api/wlpt/graborder/getDriverGrabOrderList?";
    /**
     * 抢单操作
     */
    public static final String DRIVER_GRABORDER = "api/wlpt/graborder/driverGrabOrder?";

}
