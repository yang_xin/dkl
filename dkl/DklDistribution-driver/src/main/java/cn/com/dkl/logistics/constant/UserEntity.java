package cn.com.dkl.logistics.constant;

/**
 * 用户信息
 *
 * @author Dao
 */
public class UserEntity {
    /**
     * 登录状态
     */
    public final static String IS_lOGIN = "is_login";
    /**
     * 当前所在省份
     */
    public final static String CURRENT_PROVINCE = "current_province";
    /**
     * 当前所在城市
     */
    public final static String CURRENT_CITY = "current_city";
    /**
     * 当前所在区
     */
    public final static String CURRENT_DISTRICT = "current_district";
    /**
     * 当前所在详细地址
     */
    public final static String CURRENT_ADDRESS = "current_address";
    /**
     * 当前所在街道
     */
    public final static String CURRENT_STREET = "current_street";
    /**
     * 当前所在街道门牌号
     */
    public final static String CURRENT_STREETNUMBER = "current_streetnumber";
    /**
     * 用户ID
     */
    public final static String USER_ID = "user_id";
    /**
     * 身份证号码
     */
    public final static String IDCARD_NUM = "idcard_num";
    /**
     * 身份证图片
     */
    public final static String IDCARD_IMG = "idcard_img";
    /**
     * 公司ID
     */
    public final static String COMPANY_ID = "company_id";
    /**
     * 公司名称
     */
    public final static String COMPANY_NAME = "company_name";
    /**
     * 园区编号
     */
    public final static String PARK_NO = "park_no";
    /**
     * 园区名称
     */
    public final static String PARK_NAME = "park_name";
    /**
     * 用户姓名
     */
    public final static String NAME = "name";
    /**
     * 用户名
     */
    public final static String USERNAME = "user_name";
    /**
     * 用户手机号码
     */
    public final static String PHONE = "phone";
    /**
     * 用户密码
     */
    public final static String PASSWORD = "password";
    /**
     * 性别
     */
    public final static String SEX = "sex";
    /**
     * 头像url
     */
    public final static String HEAD_PHOTO_URL = "head_photo_url";
    /**
     * 账户余额
     */
    public final static String WEB_BALANCE = "web_balance";
    /**
     * 担保金
     */
    public final static String GUARANTEE_MONEY = "guarantee_money";
    /**
     * 信誉积分
     */
    public final static String POINT = "point";
    /**
     * 信誉等级
     */
    public final static String POINT_LEVEL = "point_level";
    /**
     * 普通积分等级
     */
    public final static String ORDINARY_POINT_LEVEL = "ordinary_point_level";
    /**
     * 认证状态
     */
    public final static String STATUS = "status";
    /**
     * 认证类型    1 大可龙   2 社会车辆
     */
    public final static String AUTHTYPE = "authtype";
    /**
     * 角色ID
     */
    public final static String ROLE_ID = "role_id";
    /**
     *
     */
    public final static String UID = "uid";
    /**
     * 用户注册时间s
     */
    public final static String CREATE_TIME = "create_time";
    /**
     * 联系电话
     */
    public final static String CONTACT_TELEPHONE = "contact_telephone";
    /**
     * 联系地址
     */
    public final static String CONTACT_ADDRESS = "contact_address";
    /**
     * 联系Email
     */
    public final static String CONTACT_EMAIL = "contact_email";
    /**
     * 联系QQ
     */
    public final static String CONTACT_QQ = "contact_qq";
    /**
     * 车牌号
     */
    public final static String CAR_NUMBER = "car_number";
    /**
     * 车辆类型
     */
    public final static String CAR_TYPE = "car_type";
    /**
     * 车辆长度
     */
    public final static String CAR_LENGTH = "car_length";
    /**
     * 车辆载重
     */
    public final static String CAR_LOAD_WEIGHT = "car_load_weight";
    /**
     * 车辆体积
     */
    public final static String CAR_VOLUME = "car_volume";
    /**
     * 驾驶证图片
     */
    public final static String DRIVER_IMG = "driver_image";
    /**
     * 行驶证图片
     */
    public final static String DRIVING_IMG = "driving_image";
    /**
     * 人车图片
     */
    public final static String CAR_DRIVER_IMG = "car_driver_image";
    /**
     * 商业险图片
     */
    public final static String COMMERCIAL_INSURANCE_IMG = "commercial_insurance_img";
    /**
     * 交强险图片
     */
    public final static String COMPULSORY_TRAFFIC_INSURANCE_IMG = "compulsory_traffic_img";
    /**
     * 货运资格证图片
     */
    public final static String ROAD_TRANSPORT_IMG = "rosd_transport_img";
    /**
     * 司机id
     */
    public final static String DRIVER_ID = "driver_id";
    /**
     * 账户金额
     */
    public static final String BALANCE = "balance";
    /**
     * 今日成交数量
     */
    public static final String TURNOVER_TODAY = "turnover";
    /**
     * 今日在线小时数
     */
    public static final String ONLINE_TIME_TODAY = "online_time";
    /**
     * 好评率
     */
    public static final String HIGH_PRAISE_RATE = "high_praise_rate";
    /**
     * 评价数
     */
    public static final String EVALUATION_NUMBER = "evaluation_number";
    /**
     * 流水账金额
     */
    public static final String TRANSACTION_MONEY = "transaction_money";
    /**
     * 信誉
     */
    public static final String CREDIT = "credit";
    /**
     * 接单数量
     */
    public static final String TRANSACTION_COUNT = "transaction_count";
    /**
     * 指派成交率
     */
    public static final String ASSIGN_TURNOVER_RATE = "assign_turnover_rate";
    /**
     * 服务态度
     */
    public static final String SERIVICE_ATTITUDE_POINT = "service_attitiude_point";
    /**
     * 车辆状态
     */
    public static final String CAR_STATUS = "car_status";
    /**
     * 车辆载重状态
     */
    public static final String LOAD_STATUS = "load_status";
    /**
     * 是否设置过支付密码
     */
    public static final String IS_SET_PAYMENT_PWD = "is_set_payment_pwd";
    /**
     * 车辆id
     */
    public static final String CAR_ID = "car_id";
    /**
     * 是否设置静音
     *
     * 0 设置有声音    1设置静音
     */
    public static final String IS_SET_MUTING = "is_set_muting";
    /**
     * 是否绑定银行卡 0 是没有绑定   1是有绑定
     */
    public static final String IS_BAND_BANKCARD = "is_band_bankcard";
    /**
     * 营业执照照片
     */
    public final static String BUSINESS_LICENCE_IMG = "business_licence_img";
    /**
     * 公司地址
     */
    public final static String COMPANY_ADDRESS = "company_address";
    /**
     * 营业执照号码
     */
    public final static String BUSINESS_LICENCE_NUM = "business_licence_num";
    /**
     * 公司联系人(法人)
     */
    public final static String LEGAL_PERSON = "legal_person";
    /**
     * 公司联系人手机
     */
    public final static String HEADER_MOBILE = "header_mobile";
    /**
     * 驾驶策略  0： 躲避拥堵  1： 最短距离  2： 较少费用  3：时间优先
     */
    public final static String Driving_Policy = "driving_policy";
    /**
     * 是否自有车辆
     */
    public final static String ISSOCIALUSER = "0";

}