package cn.com.dkl.logistics.driver;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.utils.CommonTools;

/**
 * 关于我们
 *
 * @author hzq
 */
public class AboutUsActivity extends BaseActivity {
    @Bind(R.id.btnLeft)
    Button mBtnLeft;
    @Bind(R.id.tvTitle)
    TextView mTvTitle;
    @Bind(R.id.tvAboutUs)
    TextView mTvAboutUs;
    private Context mContext = AboutUsActivity.this;


    private Dialog progressDialog;
    private CommonTools tools = new CommonTools(this);
    private String callBackCode = "";
    private String callBackMessage = "";
    private String callBackState = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void initView() {
        ViewUtils.inject(this);
        mTvTitle.setText(R.string.about_us);
        progressDialog = tools.getProgressDialog(mContext, getString(R.string.loading));
        submitData();
    }

    private void submitData() {
        progressDialog.show();
        // 传入需要的参数
        final RequestParams params = new RequestParams();
        params.addQueryStringParameter("SCANTYPE", getString(R.string.SCANTYPE));
        params.addQueryStringParameter("CODE", "tyAboutUs");

        String url = getString(R.string.server_url) + URLMap.SERVICE_AGREEMENT;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);// 设置超时

        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                JSONObject jsonObject = null;

                try {
                    if (TextUtils.isEmpty(responseInfo.result)) {
                        Toast.makeText(mContext, getString(R.string.server_connected_failed), Toast.LENGTH_SHORT);
                        return;
                    } else {
                        jsonObject = new JSONObject(responseInfo.result);
                        LogUtils.i("responseInfo= " + responseInfo.result);
                        callBackMessage = jsonObject.getString("message");
                        callBackState = jsonObject.getString("state");
                        callBackCode = jsonObject.getString("code");
                        LogUtils.i("callBackMessage= " + callBackMessage + "\n" + "callBackState= " + callBackState + "\n" + "callBackCode= " + callBackCode);

                        if (!TextUtils.isEmpty(callBackMessage)) {
                            Toast.makeText(mContext, getString(R.string.loading_failed), Toast.LENGTH_SHORT).show();
                        } else {
                            mTvAboutUs.setText(Html.fromHtml(jsonObject.optJSONObject("data").getString("CONTENT")));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(mContext, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(HttpException error, String msg) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CommonTools.failedToast(getApplicationContext());
            }
        });
    }

    @OnClick(R.id.btnLeft)
    public void onClick() {
        finish();
    }
}
