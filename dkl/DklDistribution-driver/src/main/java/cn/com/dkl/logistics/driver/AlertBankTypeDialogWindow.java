package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;

import com.lidroid.xutils.util.LogUtils;

import java.util.Arrays;

import cn.com.dkl.logistics.view.WheelView;

public class AlertBankTypeDialogWindow extends PopupWindow {

    private View mMenuView;
    public static WheelView wvBankType;
    private Context context;
    private Button btnOK;
    private Button btnBackl;
    public static int bankType;

    public AlertBankTypeDialogWindow(final Activity context, OnClickListener listener, String[] type) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_banktype_dialog, null);
        wvBankType = (WheelView) mMenuView.findViewById(R.id.wvBankType);
        btnBackl = (Button) mMenuView.findViewById(R.id.btnBack);
        btnOK = (Button) mMenuView.findViewById(R.id.btnOk);

        wvBankType.setOffset(2);
        wvBankType.setItems(Arrays.asList(type));
        wvBankType.setSeletion(0);
        btnOK.setOnClickListener(listener);

        // 设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        // 设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        // mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });

        btnBackl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        wvBankType.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex1, String item1) {
                String str = item1;
                bankType = selectedIndex1 - 2;
                LogUtils.i(selectedIndex1 + "   " + str);
            }
        });

    }

}
