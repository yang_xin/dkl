package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.utils.DataCleanManager;

/**
 * Created by LiuXing on 2017/2/20.
 */

public class AlertClearCacheDialogWindow extends PopupWindow {
    private View mMenuView;
    private Context context;
    private TextView tvIsClearCache;
    private TextView tvClearCache;
    private TextView tvClearPictur;
    private TextView tvCancel;
    private String CacheSize;
    public AlertClearCacheDialogWindow(final Activity context, final String CacheSize) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.clear_cache_dialog, null);
        tvIsClearCache = (TextView) mMenuView.findViewById(R.id.tvIsClearCache);
        tvClearCache = (TextView) mMenuView.findViewById(R.id.tvClearCache);
        tvClearPictur = (TextView) mMenuView.findViewById(R.id.tvClearPictur);
        tvCancel = (TextView) mMenuView.findViewById(R.id.tvCancel);
        this.CacheSize = CacheSize;

        tvClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataCleanManager.clearAllCache(context);
                try {
                    tvClearCache.setText(DataCleanManager.getTotalCacheSize(context));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(context, "清除缓存", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
        tvClearPictur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DataCleanManager.deleteFolderFile(ImageUtils.sPath, true);
                if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
                    if (!TextUtils.isEmpty(Constants.STORAGEPATH)){
                        DataCleanManager.cleanCustomCache(Constants.STORAGEPATH);
                    }else {
                        DataCleanManager.deleteFolderFile(Constants.STORAGEPATH, true);
                    }
                }else if(Environment.getExternalStorageState().equals(Environment.MEDIA_REMOVED)){
                    if (!TextUtils.isEmpty(Constants.STORAGEPATH)){
                        DataCleanManager.cleanCustomCache(Constants.STORAGEPATH);
                    }else {
                        DataCleanManager.deleteFolderFile(Constants.STORAGEPATH, true);
                    }
                }
                DataCleanManager.clearAllCache(context);
                Toast.makeText(context, "照片已清除", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        // 设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        // 设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        // mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }


}
