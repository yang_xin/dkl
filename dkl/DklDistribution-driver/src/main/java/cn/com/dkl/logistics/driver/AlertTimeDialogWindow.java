package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;

import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import cn.com.dkl.logistics.view.WheelView;

public class AlertTimeDialogWindow extends PopupWindow {

    private View mMenuView;
    private WheelView wvYear;
    private WheelView wvMonth;
    private Context context;
    private Button btnOK;
    private Button btnBackl;
    private String[] month = {"1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"};
    private int startYear = 2016;   //从哪年开始
    private List<String> year;
    private int TmpYear;
    private int TmpMonth;

    public static String selectYear;
    public static String selectMonth;

    public AlertTimeDialogWindow(final Activity context, OnClickListener listener, String[] type) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_time_dialog, null);
        wvYear = (WheelView) mMenuView.findViewById(R.id.wvYear);
        wvMonth = (WheelView) mMenuView.findViewById(R.id.wvMonth);
        btnBackl = (Button) mMenuView.findViewById(R.id.btnBack);
        btnOK = (Button) mMenuView.findViewById(R.id.btnOk);
        year = new ArrayList<String>();
        btnOK.setOnClickListener(listener);

        Calendar cal = Calendar.getInstance();
        TmpMonth = cal.get(Calendar.MONTH) + 1;
        TmpYear = cal.get(Calendar.YEAR);
        selectYear = String.valueOf(TmpYear);
        selectMonth = String.valueOf(TmpMonth);

        for (int i = startYear; i <= TmpYear; i++) {
            year.add(String.valueOf(i) + "年");
            LogUtils.i(i + "");
        }

        wvYear.setOffset(2);
        wvYear.setItems(year);//
        wvYear.setSeletion(Integer.parseInt(selectYear) - startYear);

        wvMonth.setOffset(2);
        wvMonth.setItems(Arrays.asList(month));
        LogUtils.i(selectMonth + "");//
        wvMonth.setSeletion(Integer.parseInt(selectMonth) - 1);

        // 设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        // 设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        // mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });

        btnBackl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        wvYear.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex1, String item) {
                selectYear = item.substring(0, item.length() - 1);
                LogUtils.i(item);
                LogUtils.i(selectedIndex1 + "");
            }
        });

        wvMonth.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex1, String item1) {
                selectMonth = item1.substring(0, item1.length() - 1);
                LogUtils.i(item1);
                LogUtils.i(selectedIndex1 + "");
            }
        });

    }

}
