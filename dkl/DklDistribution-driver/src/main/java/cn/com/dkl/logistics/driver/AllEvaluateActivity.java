package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.adapter.AllEvaluateAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.view.CustomProgressDialog;

/**
 * 评价列表 Created by LiuXing on 2017/3/1.
 */

public class AllEvaluateActivity extends BaseActivity {
    private static final String TAG = "AllEvaluateActivity";
    private Context context = AllEvaluateActivity.this;

    @Bind(R.id.btnLeft)
    Button mBtnLeft;
    @Bind(R.id.tvTitle)
    TextView mTvTitle;
    @Bind(R.id.ptrListView)
    PullToRefreshListView mPtrListView;

    private AllEvaluateAdapter mAdapter;
    private SharedPreferences sp;
    private String orderNo;
    private CustomProgressDialog dialog = null;
    private List<Map<String, String>> listEvaluate = new ArrayList<Map<String, String>>();
    private boolean flag = true;
    private int currentPage = 1;
    private int pageCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_evaluate);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
//        listEvaluate.clear();
//        mAdapter.notifyDataSetChanged();
        requestData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        mTvTitle.setText("评价列表");
        dialog = new CommonTools(context).getProgressDialog(context, "正在查询中...");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        orderNo = getIntent().getExtras().getString("OrderNo");
    }

    /**
     * 请求数据 评价列表
     */
    private void requestData() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("WAYBILL_NUMBER", orderNo);

        String url = context.getResources().getString(R.string.server_url) + URLMap.All_WAYBILLEVALUATE;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpRequest.HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                flag = true;
                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(responseInfo.result);
                    LogUtils.i(object.toString());
                    String message = object.getString("message");
                    String state = object.getString("state");
                    if ("1".equals(state)) {
                        JSONArray jsonArray = object.getJSONArray("data");
                        Map<String, String> order = null;
                        JSONObject data = null;
                        LogUtils.i(jsonArray.toString());
                        listEvaluate.clear();
                        for (int i = 0; i < jsonArray.length(); i++){
                            order = new HashMap<String, String>();
                            data = jsonArray.getJSONObject(i);
                            order.put("ISCOMMENTED", CommonTools.judgeNull(data, "IS_COMMENTED", ""));
                            order.put("COMMENTTIME", CommonTools.judgeNull(data, "COMMENT_TIME", ""));
                            order.put("COMMENTCONTENT", CommonTools.judgeNull(data, "COMMENT_CONTENT", ""));
                            order.put("SERVICEATTITUDE", CommonTools.judgeNull(data, "SERVICE_ATTITUDE", ""));
                            order.put("ORDERNUMBER", CommonTools.judgeNull(data, "ORDER_NUMBER", ""));
                            order.put("NAME", CommonTools.judgeNull(data, "NAME", ""));
                            order.put("PHOTO", CommonTools.judgeNull(data, "PHOTO", ""));
                            listEvaluate.add(order);

                            mAdapter = new AllEvaluateAdapter(context, listEvaluate);
                            mPtrListView.setAdapter(mAdapter);
                        }


                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            }

            @Override
            public void onFailure(HttpException e, String s) {
                CommonTools.failedToast(context);
                flag = true;
                dialog.dismiss();
            }
        });
    }


    @OnClick(R.id.btnLeft)
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            default:
                break;
        }
    }





}
