package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.CdsCarTypeInfo;
import cn.com.dkl.logistics.entity.CompanyInfo;
import cn.com.dkl.logistics.entity.ConditionType;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.ParkInfo;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.view.CustomProgressDialog;

import static cn.com.dkl.logistics.constant.UserEntity.COMMERCIAL_INSURANCE_IMG;
import static cn.com.dkl.logistics.constant.UserEntity.COMPULSORY_TRAFFIC_INSURANCE_IMG;
import static cn.com.dkl.logistics.constant.UserEntity.ROAD_TRANSPORT_IMG;


/**
 * 司机认证
 *
 * @author txw 2015-11-3
 */
public class AuthActivity extends BaseActivity {

    @Bind(R.id.llSocialVehicle)
    LinearLayout mLlSocialVehicle;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.etRealName)
    private EditText etRealName;
    @ViewInject(R.id.etIDCardNum)
    private EditText etIDCardNum;
    @ViewInject(R.id.tvCarNumberHead)
    private TextView tvCarNumberHead;
    @ViewInject(R.id.etCarNumber)
    private EditText etCarNumber;
    @ViewInject(R.id.tvCarType)
    private TextView tvCarType;
    @ViewInject(R.id.tvParkName)
    private TextView tvParkName;
    @ViewInject(R.id.tvCompanyName)
    private TextView tvCompanyName;
    @ViewInject(R.id.etCarLength)
    private EditText etCarLength;
    @ViewInject(R.id.etLoadWeight)
    private EditText etLoadWeight;
    @ViewInject(R.id.etVolume)
    private EditText etVolume;
    @ViewInject(R.id.llCarNumberHead)
    private LinearLayout llCarNumberHead;
    @ViewInject(R.id.llParkName)
    private LinearLayout llParkName;
    @ViewInject(R.id.llCompanyName)
    private LinearLayout llCompanyName;
    @ViewInject(R.id.llCarType)
    private LinearLayout llCarType;
    // 司机认证证件图片
    @ViewInject(R.id.ivIDCard)
    private ImageView ivIDCard;
    @ViewInject(R.id.ivDriverLic)
    private ImageView ivDriverLic;
    @ViewInject(R.id.ivDrivingLic)
    private ImageView ivDrivingLic;
    @ViewInject(R.id.ivCarDriver)
    private ImageView ivCarDriver;
    @Bind(R.id.ivCarInsurance)
    ImageView mIvCarInsurance;
    @Bind(R.id.ivTransportPermit)
    ImageView mIvTransportPermit;
    @Bind(R.id.ivCarTransport)
    ImageView mIvCarTransport;

    @ViewInject(R.id.cbAgreeAgreement)
    private CheckBox cbAgreeAgreement;
    /* 提交认证 */
    @ViewInject(R.id.llApply)
    private LinearLayout llApply;
    /* 认证示例布局 */
    @ViewInject(R.id.llAuthSimple)
    private LinearLayout llAuthSimple;

    private Context context = AuthActivity.this;
    private CustomProgressDialog dialog = null;
    private SharedPreferences sp;

    private String realName;
    private String idCardNum;
    private String carNumber;
    private String carLength;
    private String carType;
    private String loadWeight;
    private String volumn;
    private String parkNo;
    private String companyId;

    private long lastClickTime = 0;

    private String[] driverNameArray = null;

    private ImageChooseDialogWindow menuWindow;

    /**
     * 身份证正面
     */
    private final String IDCARD_IMAGE = "SLOCALPHOTO";
    /**
     * VB 驾驶证正面
     */
    private final String DRIVER_LIC_IMAGE = "DRIVERLICIMG";
    /**
     * 行驶证正面
     */
    private final String DRIVING_LIC_IMAGE = "TRAVELLICIMG";
    /**
     * 人车正面
     */
    private final String CAR_DRIVER_IMAGE = "CARDRIVERIMG";
    /**
     * 商业险正面
     */
    private final String COMMERCIAL_INSURANCE_IMAGE = "BUSINESSRISKS";
    /**
     * 交强险正面
     */
    private final String COMPULSORY_TRAFFIC_INSURANCE_IMAGE = "INSURANCE";
    /**
     * 道路运输证正面
     */
    private final String ROAD_TRANSPORT_IMAGE = "TRANSPORTIMG";
    /**
     * 之前上传的图片
     */
    private Map<String, String> imageOld = new HashMap<String, String>();
    /**
     * 存储当前选择的图片路径
     */
    private Map<String, String> imagePath = new HashMap<String, String>();
    private boolean hadUpload = true;
    private String state = "0";
    private String authType = "1";
    private String driverId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_auth);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ImageUtils.picPath = null;
        for (Entry<String, String> image : imagePath.entrySet()) { // 删除生成的临时文件
            ImageUtils.deleteImage(image.getValue());
        }
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText("司机认证");
        dialog = new CommonTools(context).getProgressDialog(context, "提交认证中...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        state = sp.getString(UserEntity.STATUS, "0");
        authType = sp.getString(UserEntity.AUTHTYPE, "1");
        setPoint(etCarLength);
        setPoint(etLoadWeight);
        setPoint(etVolume);
        setView();
        if (!"0".equals(state)) { // 未认证状态
            getAuthInfo();
        }
        if (!"1".equals(authType)) {
            mLlSocialVehicle.setVisibility(View.VISIBLE);
        }
        etCarLength.setEnabled(false);
        etLoadWeight.setEnabled(false);
        etVolume.setEnabled(false);

    }

    /**
     * 设置界面显示
     */
    private void setView() {
        if ("1".equals(state) || "2".equals(state)) { // 认证中隐藏提交按钮
            etRealName.setFocusable(false);
            etIDCardNum.setFocusable(false);
            llCarNumberHead.setClickable(false);
            etCarNumber.setFocusable(false);
            llParkName.setClickable(false);
            llCompanyName.setClickable(false);
            llCarType.setClickable(false);
            etCarLength.setFocusable(false);
            etLoadWeight.setFocusable(false);
            etVolume.setFocusable(false);

            llApply.setVisibility(View.GONE);
            llAuthSimple.setVisibility(View.GONE);
//        } else if ("2".equals(state)) { // 认证通过
//            etRealName.setFocusable(false);
//            etIDCardNum.setFocusable(false);
////            llCarNumberHead.setClickable(false);
////            etCarNumber.setFocusable(false);
////            llCarType.setClickable(false);
////            etCarLength.setFocusable(false);
////            etLoadWeight.setFocusable(false);
////            etVolume.setFocusable(false);
//
//            llApply.setVisibility(View.VISIBLE);
//            llAuthSimple.setVisibility(View.VISIBLE);
        } else {
            llApply.setVisibility(View.VISIBLE);
            llAuthSimple.setVisibility(View.VISIBLE);
        }
        etRealName.setText(sp.getString(UserEntity.NAME, ""));
        etIDCardNum.setText(sp.getString(UserEntity.IDCARD_NUM, ""));
        carNumber = sp.getString(UserEntity.CAR_NUMBER, "");
        if (!TextUtils.isEmpty(carNumber)) { // 初始化车牌号码
            tvCarNumberHead.setText(carNumber.substring(0, 1));
            etCarNumber.setText(carNumber.substring(1, carNumber.length()));
        }
        tvParkName.setText(sp.getString(UserEntity.PARK_NAME, ""));
        tvCompanyName.setText(sp.getString(UserEntity.COMPANY_NAME, ""));
        tvCarType.setText(sp.getString(UserEntity.CAR_TYPE, ""));
        etCarLength.setText(sp.getString(UserEntity.CAR_LENGTH, ""));
        etLoadWeight.setText(sp.getString(UserEntity.CAR_LOAD_WEIGHT, ""));
        etVolume.setText(sp.getString(UserEntity.CAR_VOLUME, ""));

        // 获取之前上传图片名称
        imageOld.put(IDCARD_IMAGE, sp.getString(UserEntity.IDCARD_IMG, ""));
        imageOld.put(DRIVER_LIC_IMAGE, sp.getString(UserEntity.DRIVER_IMG, ""));
        imageOld.put(DRIVING_LIC_IMAGE, sp.getString(UserEntity.DRIVING_IMG, ""));
        imageOld.put(CAR_DRIVER_IMAGE, sp.getString(UserEntity.CAR_DRIVER_IMG, ""));
        imageOld.put(COMMERCIAL_INSURANCE_IMAGE, sp.getString(COMMERCIAL_INSURANCE_IMG, ""));//商业险
        imageOld.put(COMPULSORY_TRAFFIC_INSURANCE_IMAGE, sp.getString(COMPULSORY_TRAFFIC_INSURANCE_IMG, ""));//交强险
        imageOld.put(ROAD_TRANSPORT_IMAGE, sp.getString(ROAD_TRANSPORT_IMG, ""));//道路运输证

        // 初始化上传过的图片
        for (Entry<String, String> image : imageOld.entrySet()) {
            String imageItem = image.getKey();
            String imageName = image.getValue();
            if (!TextUtils.isEmpty(imageName)) {
                String imagePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;
                File file = new File(imagePath);
                if (file.exists()) {
                    setImage(imageItem, imagePath);
                } else {
                    downloadImage(imageItem, imageName);
                }
            }
        }
    }

    /**
     * 显示对应图片
     */
    private void setImage(String imageItem, String imagePath) {
        Bitmap bm = BitmapFactory.decodeFile(imagePath);
        if (null != bm) {
            if (IDCARD_IMAGE.equals(imageItem)) {
                ivIDCard.setImageBitmap(bm);
            } else if (DRIVER_LIC_IMAGE.equals(imageItem)) {
                ivDriverLic.setImageBitmap(bm);
            } else if (DRIVING_LIC_IMAGE.equals(imageItem)) {
                ivDrivingLic.setImageBitmap(bm);
            } else if (CAR_DRIVER_IMAGE.equals(imageItem)) {
                ivCarDriver.setImageBitmap(bm);
            } else if (COMMERCIAL_INSURANCE_IMAGE.equals(imageItem)) {
                mIvCarInsurance.setImageBitmap(bm);
            } else if (COMPULSORY_TRAFFIC_INSURANCE_IMAGE.equals(imageItem)) {
                mIvTransportPermit.setImageBitmap(bm);
            } else if (ROAD_TRANSPORT_IMAGE.equals(imageItem)) {
                mIvCarTransport.setImageBitmap(bm);
            }
        }
    }


    @SuppressWarnings("deprecation")
    @OnClick({R.id.btnLeft, R.id.llCarNumberHead, R.id.llParkName, R.id.llCompanyName, R.id.llCarType, R.id.ivIDCard, R.id.ivDriverLic, R.id.ivCarTransport,
            R.id.ivDrivingLic, R.id.ivCarDriver, R.id.tvAgreeAgreement, R.id.btnApplyAuth, R.id.ivCarInsurance, R.id.ivTransportPermit})
    public void onClick(View v) {
        Intent intent = new Intent(AuthActivity.this, ImageDetailActivity.class);

        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - lastClickTime > CommonTools.MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime;
            switch (v.getId()) {
                case R.id.btnLeft:
                    finish();
                    break;
                case R.id.llCarNumberHead:
                    selectCondition(ConditionType.TYPE_NUMBER_HEAD);
                    break;
                case R.id.llParkName:
                    selectCondition(ConditionType.TYPE_PARK);
                    break;
                case R.id.llCompanyName:
                    selectCondition(ConditionType.TYPE_COMPANY);
                    break;
                case R.id.llCarType:
                    selectCondition(ConditionType.TYPE_CAR_TYPE);
                    break;
                case R.id.ivIDCard://身份证
                    ImageUtils.uploadImageName = IDCARD_IMAGE;
                    intent.putExtra("imagename", sp.getString(UserEntity.IDCARD_IMG, ""));
                    if ("1".equals(state) || "2".equals(state)) { // 认证中，或认证通过，隐藏提交按钮
                        if (!sp.getString(UserEntity.IDCARD_IMG, "").equals("")){
                            startActivity(intent);
                        }
                    } else {
                        choosePhoto();
                    }

                    break;
                case R.id.ivDriverLic://驾驶证
                    ImageUtils.uploadImageName = DRIVER_LIC_IMAGE;
                    intent.putExtra("imagename", sp.getString(UserEntity.DRIVER_IMG, ""));
                    if ("1".equals(state) || "2".equals(state)) { // 认证中，或认证通过，隐藏提交按钮
                        if (!sp.getString(UserEntity.DRIVER_IMG, "").equals("")){
                            startActivity(intent);
                        }
                    } else {
                        choosePhoto();
                    }

                    break;
                case R.id.ivDrivingLic://行驶证
                    ImageUtils.uploadImageName = DRIVING_LIC_IMAGE;
                    intent.putExtra("imagename", sp.getString(UserEntity.DRIVING_IMG, ""));
                    if ("1".equals(state) || "2".equals(state)) { // 认证中，或认证通过，隐藏提交按钮
                        if (!sp.getString(UserEntity.DRIVING_IMG, "").equals("")){
                            startActivity(intent);
                        }
                    } else {
                        choosePhoto();
                    }

                    break;
                case R.id.ivCarDriver://人车照片
                    ImageUtils.uploadImageName = CAR_DRIVER_IMAGE;
                    intent.putExtra("imagename", sp.getString(UserEntity.CAR_DRIVER_IMG, ""));
                    if ("1".equals(state) || "2".equals(state)) { // 认证中，或认证通过，隐藏提交按钮
                        if (!sp.getString(UserEntity.CAR_DRIVER_IMG, "").equals("")){
                            startActivity(intent);
                        }
                    } else {
                        choosePhoto();
                    }

                    break;
                case R.id.ivCarInsurance://商业险
                    ImageUtils.uploadImageName = COMMERCIAL_INSURANCE_IMAGE;
                    intent.putExtra("imagename", sp.getString(UserEntity.COMMERCIAL_INSURANCE_IMG, ""));
                    if ("1".equals(state) || "2".equals(state)) { // 认证中，或认证通过，隐藏提交按钮
                        if (!sp.getString(UserEntity.COMMERCIAL_INSURANCE_IMG, "").equals("")){
                            startActivity(intent);
                        }
                    } else {
                        choosePhoto();
                    }

                    break;
                case R.id.ivTransportPermit://交强险
                    ImageUtils.uploadImageName = COMPULSORY_TRAFFIC_INSURANCE_IMAGE;
                    intent.putExtra("imagename", sp.getString(UserEntity.COMPULSORY_TRAFFIC_INSURANCE_IMG, ""));
                    if ("1".equals(state) || "2".equals(state)) { // 认证中，或认证通过，隐藏提交按钮
                        if (!sp.getString(UserEntity.COMPULSORY_TRAFFIC_INSURANCE_IMG, "").equals("")){
                            startActivity(intent);
                        }
                    } else {
                        choosePhoto();
                    }

                    break;
                case R.id.ivCarTransport://道路运输证
                    ImageUtils.uploadImageName = ROAD_TRANSPORT_IMAGE;
                    intent.putExtra("imagename", sp.getString(UserEntity.ROAD_TRANSPORT_IMG, ""));
                    if ("1".equals(state) || "2".equals(state)) { // 认证中，或认证通过，隐藏提交按钮
                        if (!sp.getString(UserEntity.ROAD_TRANSPORT_IMG, "").equals("")){
                            startActivity(intent);
                        }
                    } else {
                        choosePhoto();
                    }

                    break;
                case R.id.tvAgreeAgreement:
                    startActivity(new Intent(context, ServiceAgreementActivity.class).putExtra("Code", "Certification"));
                    break;
                case R.id.btnApplyAuth:
                    if (cbAgreeAgreement.isChecked()) {
                        checkAuth();
                    } else {
                        Toast.makeText(context, "请勾选同意协议", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void choosePhoto() {
        menuWindow = new ImageChooseDialogWindow(this);
        menuWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        menuWindow.showAtLocation(this.findViewById(R.id.authLinear), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    public void selectCondition(int chooseType) {
        Intent intent = new Intent();
        intent.putExtra("chooseType", chooseType);
        if (null != driverNameArray && 0 != driverNameArray.length) {
            intent.putExtra("driverName", driverNameArray);
        }
        intent.setClass(this, ChooseConditionActivity.class);
        startActivityForResult(intent, chooseType);
    }

    private void checkAuth() {
        realName = etRealName.getText().toString().trim();
        idCardNum = etIDCardNum.getText().toString().trim();
        String carNumberHead = tvCarNumberHead.getText().toString().trim();
        carNumber = etCarNumber.getText().toString().trim();
        carType = tvCarType.getText().toString().trim();
        carLength = etCarLength.getText().toString().trim();
        loadWeight = etLoadWeight.getText().toString().trim();
        volumn = etVolume.getText().toString().trim();
        if (TextUtils.isEmpty(realName)) {
            Toast.makeText(context, "真实姓名不能为空！", Toast.LENGTH_SHORT).show();
            return;
        } else if (!CommonTools.isChinese(realName)) {
            Toast.makeText(context, "真实姓名只能为汉字！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(idCardNum)) {
            Toast.makeText(context, "身份证号码不能为空！", Toast.LENGTH_SHORT).show();
            return;
        } else if (!CommonTools.checkIDCardNumber(idCardNum)) {
            Toast.makeText(context, "身份证号码输入有误！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(tvCompanyName.getText().toString().trim())){
            Toast.makeText(context, "请选择所属企业！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(carNumber)) {
            Toast.makeText(context, "车牌号不能为空！", Toast.LENGTH_SHORT).show();
            etCarNumber.requestFocus();
            return;
        }
        carNumber = carNumberHead + carNumber;
        if (!CommonTools.checkCarNumber(carNumber)) {
            Toast.makeText(context, "车牌号码输入有误", Toast.LENGTH_SHORT).show();
            etCarNumber.requestFocus();
            return;
        } else if (TextUtils.isEmpty(carType)) {
            Toast.makeText(context, "车辆类型不能为空！", Toast.LENGTH_SHORT).show();
            tvCarType.requestFocus();
            return;
        } else if (TextUtils.isEmpty(carLength)) {
            Toast.makeText(context, "车辆长度不能为空！", Toast.LENGTH_SHORT).show();
            etCarLength.requestFocus();
            return;
        } else if (TextUtils.isEmpty(loadWeight)) {
            Toast.makeText(context, "车辆载重不能为空！", Toast.LENGTH_SHORT).show();
            etLoadWeight.requestFocus();
            return;
        } else if (TextUtils.isEmpty(volumn)) {
            Toast.makeText(context, "车辆体积不能为空！", Toast.LENGTH_SHORT).show();
            etVolume.requestFocus();
            return;
        }
        /*else if (TextUtils.isEmpty(imagePath.get(DRIVING_LIC_IMAGE)) && TextUtils.isEmpty(imageOld.get(DRIVING_LIC_IMAGE))) {
            Toast.makeText(context, "请上传行驶证照片！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(imagePath.get(DRIVER_LIC_IMAGE)) && TextUtils.isEmpty(imageOld.get(DRIVER_LIC_IMAGE))) {
            Toast.makeText(context, "请上传驾驶证照片！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(imagePath.get(COMPULSORY_TRAFFIC_INSURANCE_IMAGE)) && TextUtils.isEmpty(imageOld.get(COMPULSORY_TRAFFIC_INSURANCE_IMAGE))) {
            Toast.makeText(context, "请上传交强险照片！", Toast.LENGTH_SHORT).show();
            return;
        }
        */
        /*
        else if (TextUtils.isEmpty(imagePath.get(IDCARD_IMAGE)) && TextUtils.isEmpty(imageOld.get(IDCARD_IMAGE))) {
            Toast.makeText(context, "请上传身份证照片！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(imagePath.get(CAR_DRIVER_IMAGE)) && TextUtils.isEmpty(imageOld.get(CAR_DRIVER_IMAGE))) {
            Toast.makeText(context, "请上传人车照片！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(imagePath.get(COMMERCIAL_INSURANCE_IMAGE)) && TextUtils.isEmpty(imageOld.get(COMMERCIAL_INSURANCE_IMAGE))) {
            Toast.makeText(context, "请上传商业险照片！", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(imagePath.get(ROAD_TRANSPORT_IMAGE)) && TextUtils.isEmpty(imageOld.get(ROAD_TRANSPORT_IMAGE))) {
            Toast.makeText(context, "请上传道路运输证照片！", Toast.LENGTH_SHORT).show();
            return;
        }
        */


        dialog.show();
        if (hadUpload) { // 图片上传过
            sendRequest();
        } else {
            doUpload();
        }
    }

    private void doUpload() {
        RequestParams params = new RequestParams();
        File file = null;
        if (imagePath.isEmpty()) {
            return;
        }
        for (Entry<String, String> image : imagePath.entrySet()) {
            String imageItem = image.getKey();
            String imagePath = image.getValue();
            if (!TextUtils.isEmpty(imagePath)) { // 图片路径不为空，并且图片存在
                file = new File(imagePath);
                if (file.exists()) {
                    params.addBodyParameter(imageItem, file);
                }
            }
        }

        String url = getString(R.string.server_url) + URLMap.UPLOAD_IMAGE;
        LogUtils.i(url + CommonTools.getParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String message = obj.getString("message");
                    String status = obj.getString("status");
                    if ("1".equals(status)) {
                        JSONArray datas = obj.getJSONArray("data");
                        for (int i = 0; i < datas.length(); i++) {
                            JSONObject data = datas.getJSONObject(i);
                            String imgFileName = data.getString("FILENAME");
                            String rawName = data.getString("RAWNAME");
                            if (rawName.contains(IDCARD_IMAGE)) {
                                imageOld.put(IDCARD_IMAGE, imgFileName);
                            } else if (rawName.contains(DRIVER_LIC_IMAGE)) {
                                imageOld.put(DRIVER_LIC_IMAGE, imgFileName);
                            } else if (rawName.contains(DRIVING_LIC_IMAGE)) {
                                imageOld.put(DRIVING_LIC_IMAGE, imgFileName);
                            } else if (rawName.contains(CAR_DRIVER_IMAGE)) {
                                imageOld.put(CAR_DRIVER_IMAGE, imgFileName);
                            } else if (rawName.contains(COMMERCIAL_INSURANCE_IMAGE)) {
                                imageOld.put(COMMERCIAL_INSURANCE_IMAGE, imgFileName);
                            } else if (rawName.contains(COMPULSORY_TRAFFIC_INSURANCE_IMAGE)) {
                                imageOld.put(COMPULSORY_TRAFFIC_INSURANCE_IMAGE, imgFileName);
                            } else if (rawName.contains(ROAD_TRANSPORT_IMAGE)) {
                                imageOld.put(ROAD_TRANSPORT_IMAGE, imgFileName);
                            }
                        }
                        hadUpload = true;
                        sendRequest();
                    } else if ("0".equals(status)) {
                        dialog.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        dialog.dismiss();
                        Toast.makeText(context, "服务器异常", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }

        });
    }

    private void getAuthInfo() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_DRIVER_AUTHEN_INFO;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    if ("1".equals(status)) {
                        SharedPreferences sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
                        JSONArray datas = obj.getJSONArray("data");
                        JSONObject data = datas.getJSONObject(0);

                        Editor edit = sp.edit();
                        edit.putString(UserEntity.NAME, CommonTools.judgeNull(data, "NAME", ""));
                        edit.putString(UserEntity.COMPANY_NAME, CommonTools.judgeNull(data, "COMPANY_NAME", ""));
                        edit.putString(UserEntity.COMPANY_ID, CommonTools.judgeNull(data, "COMPANY_ID", ""));
                        edit.putString(UserEntity.PARK_NAME, CommonTools.judgeNull(data, "PARK_NAME", ""));
                        edit.putString(UserEntity.PARK_NO, CommonTools.judgeNull(data, "PARK_NO", ""));
                        edit.putString(UserEntity.IDCARD_NUM, CommonTools.judgeNull(data, "IDENTITYCARD", ""));
                        edit.putString(UserEntity.CAR_NUMBER, CommonTools.judgeNull(data, "CARNUMBER", ""));
                        edit.putString(UserEntity.CAR_TYPE, CommonTools.judgeNull(data, "CARTYPE", ""));
                        edit.putString(UserEntity.CAR_LENGTH, CommonTools.judgeNull(data, "CARLENGTH", ""));
                        edit.putString(UserEntity.CAR_LOAD_WEIGHT, CommonTools.judgeNull(data, "LOADWEIGHT", ""));
                        edit.putString(UserEntity.DRIVER_ID, CommonTools.judgeNull(data, "DRIVERID", ""));
                        driverId = CommonTools.judgeNull(data, "DRIVERID", "");
                        parkNo = CommonTools.judgeNull(data, "PARK_NO", "");
                        companyId = CommonTools.judgeNull(data, "COMPANY_ID", "");
                        edit.putString(UserEntity.IDCARD_IMG, CommonTools.judgeNull(data, "SLOCALPHOTO", ""));//身份证
                        edit.putString(UserEntity.DRIVER_IMG, CommonTools.judgeNull(data, "DRIVERLICIMG", ""));//驾驶证
                        edit.putString(UserEntity.DRIVING_IMG, CommonTools.judgeNull(data, "TRAVELLICIMG", ""));//行驶证
                        edit.putString(UserEntity.CAR_DRIVER_IMG, CommonTools.judgeNull(data, "CARDRIVERIMG", ""));//人车


                        edit.putString(COMMERCIAL_INSURANCE_IMG, CommonTools.judgeNull(data, "BUSINESSRISKS", ""));//商业险
                        edit.putString(COMPULSORY_TRAFFIC_INSURANCE_IMG, CommonTools.judgeNull(data, "INSURANCE", ""));//交强险
                        edit.putString(ROAD_TRANSPORT_IMG, CommonTools.judgeNull(data, "TRANSPORTIMG", ""));//道路运输证

                        edit.putString(UserEntity.STATUS, CommonTools.judgeNull(data, "USER_STATUS", ""));
                        edit.putInt("AUTHSTATUSTHEN",Integer.valueOf(CommonTools.judgeNull(data, "USER_STATUS", "")));
                        edit.putString(UserEntity.CAR_VOLUME, CommonTools.judgeNull(data, "VOLUMN", ""));
                        edit.commit();
                        setView();

                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                dialog.dismiss();
            }

        });
    }

    /**
     * 提交认证信息
     */
    private void sendRequest() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("PHONE", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("NAME", realName);
        params.addQueryStringParameter("IDENTITYCARD", idCardNum);
        params.addQueryStringParameter("CARNUMBER", carNumber);
        params.addQueryStringParameter("CARTYPE", carType);
        params.addQueryStringParameter("CARLENGTH", carLength);
        params.addQueryStringParameter("LOADWEIGHT", loadWeight);
        params.addQueryStringParameter("VOLUMN", volumn);
        params.addQueryStringParameter("CARID", sp.getString(UserEntity.CAR_ID, null));
        params.addQueryStringParameter("DRIVERID", driverId);
        params.addQueryStringParameter("PARK_NO", parkNo);
        params.addQueryStringParameter("COMPANY_ID", companyId);
        // 初始化图片
        for (Entry<String, String> image : imageOld.entrySet()) {
            params.addBodyParameter(image.getKey(), image.getValue());
        }

        String url = getString(R.string.server_url) + URLMap.DRIVER_AUTHEN;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    dialog.dismiss();
                    JSONObject obj = new JSONObject(result.result);
                    String status = null;
                    status = obj.getString("status");
                     String message = obj.getString("message");
                    if ("1".equals(status)) {
                        SharedPreferences sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
                        Editor edit = sp.edit();
                        edit.putString(UserEntity.STATUS, "1"); // 修改认证状态为待审核
                        edit.commit();
                        message = "提交认证信息成功！";
                        SystemClock.sleep(1000);
                        setResult(Activity.RESULT_OK, new Intent());
                        finish();
                    } else if ("-1".equals(status)) {
                        message = "服务器异常";
                    }
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }

            @Override
            public void onCancelled() {
                Toast.makeText(context, "cancle", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImageUtils.GET_IMAGE_BY_CAMERA: // 拍照获取图片
                if (null != ImageUtils.imageUriFromCamera && resultCode != RESULT_CANCELED) {
                    ImageUtils.deleteImage(this.imagePath.get(ImageUtils.uploadImageName));
                    ImageUtils.uriToPath(this, ImageUtils.imageUriFromCamera);
                    setImageInfo();
                    hadUpload = false;
                }
                break;
            case ImageUtils.GET_IMAGE_FROM_PHONE: // 手机相册获取图片
                if (resultCode != RESULT_CANCELED && null != data.getData()) {
                    ImageUtils.deleteImage(this.imagePath.get(ImageUtils.uploadImageName));
                    ImageUtils.uriToPath(this, data.getData());
                    setImageInfo();
                    hadUpload = false;
                }
                break;
            case ConditionType.TYPE_NUMBER_HEAD:
                if (null != data) {
                    if (data.getBooleanExtra("isChosen", false)) {// 判断是否选中
                        tvCarNumberHead.setText(data.getStringExtra("chosenItem"));
                    }
                }
                break;
            case ConditionType.TYPE_CAR_TYPE: // 车型选择结果
                if (null != data) {
                    if (data.getBooleanExtra("isChosen", false)) {// 判断是否选中

                        CdsCarTypeInfo carTypeInfo = (CdsCarTypeInfo) data.getExtras().getSerializable("carTypeInfo");
                        tvCarType.setText(carTypeInfo.NAME);
                        etCarLength.setText(carTypeInfo.LENGTH);
                        etLoadWeight.setText(carTypeInfo.WEIGHT);
                        etVolume.setText(carTypeInfo.VOLUMN);
                    }
                }
                break;
            case ConditionType.TYPE_PARK: // 园区选择结果
                if (null != data) {
                    if (data.getBooleanExtra("isChosen", false)) {// 判断是否选中

                        ParkInfo parkInfo = (ParkInfo) data.getExtras().getSerializable("parkInfo");
                        tvParkName.setText(parkInfo.PARK_NAME);
                        parkNo = parkInfo.PARK_NO;
                    }
                }
                break;
            case ConditionType.TYPE_COMPANY: // 公司选择结果
                if (null != data) {
                    if (data.getBooleanExtra("isChosen", false)) {// 判断是否选中

                        CompanyInfo companyInfo = (CompanyInfo) data.getExtras().getSerializable("companyInfo");
                        tvCompanyName.setText(companyInfo.COMPANY_NAME);
                        companyId = companyInfo.COMPANY_ID;
                    }
                }
                break;
        }

    }

    private void setImageInfo() {
        if (TextUtils.isEmpty(ImageUtils.picPath)) {
            return;
        } else if (TextUtils.isEmpty(ImageUtils.uploadImageName)) {
            return;
        }

        ImageView uploadImage = null;
        if (ImageUtils.uploadImageName.equals(IDCARD_IMAGE)) {
            imagePath.put(IDCARD_IMAGE, ImageUtils.picPath);
            uploadImage = ivIDCard;
        } else if (ImageUtils.uploadImageName.equals(DRIVER_LIC_IMAGE)) {
            imagePath.put(DRIVER_LIC_IMAGE, ImageUtils.picPath);
            uploadImage = ivDriverLic;
        } else if (ImageUtils.uploadImageName.equals(DRIVING_LIC_IMAGE)) {
            imagePath.put(DRIVING_LIC_IMAGE, ImageUtils.picPath);
            uploadImage = ivDrivingLic;
        } else if (ImageUtils.uploadImageName.equals(CAR_DRIVER_IMAGE)) {
            imagePath.put(CAR_DRIVER_IMAGE, ImageUtils.picPath);
            uploadImage = ivCarDriver;
        } else if (ImageUtils.uploadImageName.equals(COMMERCIAL_INSURANCE_IMAGE)) {
            imagePath.put(COMMERCIAL_INSURANCE_IMAGE, ImageUtils.picPath);
            uploadImage = mIvCarInsurance;
        } else if (ImageUtils.uploadImageName.equals(COMPULSORY_TRAFFIC_INSURANCE_IMAGE)) {
            imagePath.put(COMPULSORY_TRAFFIC_INSURANCE_IMAGE, ImageUtils.picPath);
            uploadImage = mIvTransportPermit;
        } else if (ImageUtils.uploadImageName.equals(ROAD_TRANSPORT_IMAGE)) {
            imagePath.put(ROAD_TRANSPORT_IMAGE, ImageUtils.picPath);
            uploadImage = mIvCarTransport;
        }


        // 显示选择的图片
        uploadImage.setImageBitmap(BitmapFactory.decodeFile(ImageUtils.picPath));
    }

    /**
     * 下载图片
     */
    private void downloadImage(final String imageItem, String imageName) {
        if (TextUtils.isEmpty(imageName)) {
            return;
        }
        String url = getString(R.string.server_imgurl) + "upload/" + imageName;
        //String url = getString(R.string.server_url) + "upload/" + imageName;
        final String imagePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.download(url, imagePath, true, true, new RequestCallBack<File>() {

            @Override
            public void onSuccess(ResponseInfo<File> arg0) {
                setImage(imageItem, imagePath);
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("下载企业认证图片失败：" + arg0.getMessage() + arg1);
            }
        });
    }


    public void setPoint(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0,
                                s.toString().indexOf(".") + 3);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }

                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        editText.setText(s.subSequence(0, 1));
                        editText.setSelection(1);
                        return;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
