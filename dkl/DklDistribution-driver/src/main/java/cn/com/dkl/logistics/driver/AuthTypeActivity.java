package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;

/**
 * 选择认证类型 Created by LiuXing on 2016/12/15.
 */

public class AuthTypeActivity extends BaseActivity {

    private String state = "0";// 未认证状态

    @Bind(R.id.ivLeft)
    ImageView mIvLeft;
    @Bind(R.id.btn_jiaan)
    Button mBtnJiaan;
    @Bind(R.id.btn_other)
    Button mBtnOther;
    private Context mContext = AuthTypeActivity.this;
    private SharedPreferences sp;
    private SharedPreferences.Editor mEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authtype);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ButterKnife.unbind(this);
    }

    private void init(){

        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        mEdit = sp.edit();
    }

    @OnClick({R.id.ivLeft, R.id.btn_jiaan, R.id.btn_other})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivLeft:
                finish();
                break;
            case R.id.btn_jiaan:
                finish();
                startActivity(new Intent(mContext, AuthActivity.class));//大可龙实名认证
                mEdit.putString(UserEntity.AUTHTYPE,"1");
                mEdit.commit();
                break;
            case R.id.btn_other:
                finish();
                startActivity(new Intent(mContext, AuthActivity.class));//社会车辆实名认证
                mEdit.putString(UserEntity.AUTHTYPE,"2");
                mEdit.commit();
                break;

        }
    }
}
