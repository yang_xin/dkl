package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.adapter.BillQueryAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;

/**
 * 账单查询
 * @author Dao
 */
@SuppressLint("NewApi")
public class BillQueryActivity extends FragmentActivity {

    @ViewInject(R.id.tvYear)
    private TextView tvYear;
    @ViewInject(R.id.tvMonth)
    private TextView tvMonth;
    @ViewInject(R.id.tvWeek)
    private TextView tvWeek;
    @ViewInject(R.id.tvTotalIncome)
    private TextView tvTotalIncome;
    @ViewInject(R.id.tvTotalPay)
    private TextView tvTotalPay;
    @ViewInject(R.id.rbDetail)
    private RadioButton rbDetail;
    @ViewInject(R.id.rbIncome)
    private RadioButton rbIncome;
    @ViewInject(R.id.rbPay)
    private RadioButton rbPay;
    @ViewInject(R.id.ptrListView)
    private PullToRefreshListView ptrListView;

    private Context context = BillQueryActivity.this;
    private SharedPreferences sp;
    private BillQueryAdapter adapter;
    private List<Map<String, String>> dataList = new ArrayList<Map<String, String>>();
    private final String DETAIL = "0";
    private final String INCOME = "2";
    private final String PAY = "1";
    private String operateType = DETAIL;
    private String startTime;
    private String endTime;
    // 禁止二次回调，或点击
    boolean flag = true;
    private int currentYear;
    private int currentMonth;
    private int currentDay;
    AlertTimeDialogWindow window;
    private String year;
    private String month;
    private boolean isFirst = true;

    /**
     * 一页显示数量
     */
    private static final int SHOW_COUNT = 10;
    /* 当前页面 */
    private int currentPage = 1;
    /* 总页面数 */
    private int pageCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_bill_query);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        ViewUtils.inject(this);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        Calendar calendar = Calendar.getInstance();
        currentYear = calendar.get(Calendar.YEAR);
        currentMonth = calendar.get(Calendar.MONTH);
        currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        tvYear.setText(currentYear + "年");
        tvMonth.setText(String.valueOf(++currentMonth));

        initListView();

        initParams();
    }

    private void initListView() {
        adapter = new BillQueryAdapter(context, dataList);
        ptrListView.setAdapter(adapter);

        //TODO 进入账单详情页面要传什么数据
        ptrListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                context.startActivity(new Intent(context,BillDetailActivity.class));
            }
        });

        ptrListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if (flag) {
                    currentPage = 1;
                    getAccountBill();
                }
            }
        });
        ptrListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (currentPage <= pageCount) {
                    if (flag) {
                        flag = false;
                        getAccountBill();
                    }
                } else {
                    Toast.makeText(context, R.string.already_no_data, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @OnClick({R.id.ivLeft, R.id.rlWeek, R.id.rlMonth, R.id.rbDetail, R.id.rbIncome, R.id.rbPay})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft:
                finish();
                break;
            case R.id.rlWeek:

                break;
            case R.id.rlMonth:
                // datePickerDialog();
                window = new AlertTimeDialogWindow((Activity) context, listener, null);
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                window.showAtLocation(this.findViewById(R.id.llMain), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                flag = true;
                break;
            case R.id.rbDetail:
                if (flag) {
                    currentPage = 1;
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    flag = false;
                    initParams();
                }
                break;
            case R.id.rbIncome:
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    initParams();
                }
                break;
            case R.id.rbPay:
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    initParams();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 日期选择对话框
     */
    private void datePickerDialog() {
        flag = true;
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(context, new OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                tvYear.setText(year + "年");
                tvMonth.setText(String.valueOf(++monthOfYear));
                if (flag) {
                    flag = false;
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    currentPage = 1;
                    rbDetail.setChecked(true);
                    initParams();
                }
            }

        }, year, month, day).show();
    }

    private void initParams() {
        if (rbDetail.isChecked()) {
            operateType = DETAIL;
        } else if (rbIncome.isChecked()) {
            operateType = INCOME;
        } else if (rbPay.isChecked()) {
            operateType = PAY;
        }
        if (isFirst) {
            year = tvYear.getText().toString().trim().substring(0, 4);
            month = tvMonth.getText().toString().trim();
            isFirst = false;
        }
        startTime = year + "-" + month + "-01" + " 00:00:00";
        endTime = year + "-" + month + "-" + CommonTools.getDaysByYearMonth(Integer.valueOf(year), Integer.valueOf(month)) + " 23:59:00";
        getAccountBill();
    }

    /**
     * 获取订单信息
     */
    private void getAccountBill() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("OPERATETYPE", operateType); //
        params.addQueryStringParameter("TIMESTART", startTime); // 开始时间
        params.addQueryStringParameter("TIMEEND", endTime); // 结束时间
        params.addQueryStringParameter("SHOWCOUNT", String.valueOf(SHOW_COUNT));
        params.addQueryStringParameter("CURRENTPAGE", String.valueOf(currentPage));

        String url = getString(R.string.server_url) + URLMap.MY_ACCOUNT_BILL;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                flag = true;
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        if (ptrListView.isRefreshing()) { // 刷新显示中
                            ptrListView.onRefreshComplete();
                            dataList.clear();
                        }
                        JSONObject datas = obj.getJSONObject("data");
                        if (rbDetail.isChecked()) {
                            JSONObject account = datas.getJSONObject("toatalAccount");
                            tvTotalIncome.setText(CommonTools.judgeNull(account, "INCOME", "0.00"));
                            tvTotalPay.setText("" + Math.abs(Double.parseDouble(CommonTools.judgeNull(account, "PAY", "0.00"))));
                        }
                        pageCount = (int) Double.parseDouble(CommonTools.judgeNull(datas, "PAGECOUNT", "0"));
                        JSONArray array = datas.getJSONArray("consumeRecordList");
                        Map<String, String> map;
                        for (int i = 0; i < array.length(); i++) {
                            map = new HashMap<String, String>();
                            JSONObject data = array.getJSONObject(i);
                            Iterator<String> it = data.keys();
                            while (it.hasNext()) {
                                String key = it.next();
                                String value = CommonTools.judgeNull(data, key, "");
                                map.put(key, value);
                            }
                            dataList.add(map);
                        }
                        adapter.notifyDataSetChanged();
                        currentPage++;
                        flag = true;
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            }
        });
    }

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            year = AlertTimeDialogWindow.selectYear;
            month = AlertTimeDialogWindow.selectMonth;
            LogUtils.i("year = " + year + "month = " + month);
            tvYear.setText(year + "年");
            tvMonth.setText(String.valueOf(month));
            if (flag) {
                flag = false;
                dataList.clear();
                adapter.notifyDataSetChanged();
                currentPage = 1;
                rbDetail.setChecked(true);
                initParams();
            }

            window.dismiss();
        }
    };

}
