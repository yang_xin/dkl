package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import cn.com.dkl.logistics.adapter.MySpinnerAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.view.CustomProgressDialog;

/**
 * 绑定银行卡
 *
 * @author hzq
 */
public class BindBankCardAcitivity extends BaseActivity {
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;

    @ViewInject(R.id.tvBankCardType)
    private TextView tvBankCardType;

    @ViewInject(R.id.etBankCardNum)
    private EditText etBankCardNum;

    @ViewInject(R.id.etBankCardUserName)
    private EditText etBankCardUserName;

    @ViewInject(R.id.llSelectType)
    private LinearLayout llSelectType;

    @ViewInject(R.id.btnBindBankCard)
    private Button btnBindBankCard;

    private Context context = BindBankCardAcitivity.this;
    private SharedPreferences sp;
    private String UserName;
    private int cardType;
    private String bankCard;
    private String bankCardType;
    private long lastClickTime = 0;
    private CustomProgressDialog dialog = null;
    private AlertDialog.Builder builder;
    private LinearLayout layout;
    private ArrayList<String> myData;
    private MySpinnerAdapter adapter;
    private ListView listView;
    private PopupWindow popupWindow;
    private AlertBankTypeDialogWindow typeDialogWindow;
    // 指定下拉列表的显示数据
    final String[] type = {"工商银行", "农业银行", "建设银行", "中国银行", "交通银行", "招商银行", "中国邮件储蓄银行", "中国民生银行", "兴业银行", "浦发银行", "广发银行", "平安银行", "光大银行", "中信银行", "华夏银行"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_bankcard);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        dialog = new CommonTools(context).getProgressDialog(context, "获取数据中...");
        tvTitle.setText("绑定银行卡");
        myData = new ArrayList<String>(Arrays.asList(type));
        //myData = (ArrayList<String>) Arrays.asList(type);
        adapter = new MySpinnerAdapter(this, myData);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        builder = new AlertDialog.Builder(BindBankCardAcitivity.this);
        builder.setTitle("选择一个银行类型");

        // 设置一个下拉的列表选择项
        builder.setItems(type, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvBankCardType.setTextColor(getResources().getColor(R.color.gray_text));
                tvBankCardType.setText(type[which]);
                cardType = which;
            }
        });
        getUserCashInfo();
    }

    @OnClick({R.id.btnLeft, R.id.llSelectType, R.id.btnBindBankCard})
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - lastClickTime > CommonTools.MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime;
            switch (v.getId()) {
                case R.id.btnLeft:
                    finish();
                    break;
                case R.id.llSelectType:
                    //builder.show();
                    //showWindow(v);
                    typeDialogWindow.bankType = 0;
                    typeDialogWindow = new AlertBankTypeDialogWindow((Activity) context, listener, type);
                    typeDialogWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    typeDialogWindow.showAtLocation(this.findViewById(R.id.llMain), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    break;
                case R.id.btnBindBankCard:
                    UserName = etBankCardUserName.getText().toString();
                    bankCard = etBankCardNum.getText().toString();
                    bankCardType = tvBankCardType.getText().toString();
                    if (bankCardType.equals("请选择银行卡类型")) {
                        Toast.makeText(context, "请选择银行卡类型!", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (TextUtils.isEmpty(bankCard)) {
                        Toast.makeText(context, "银行卡号不能为空", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (CommonTools.checkBankCard(bankCard)) {
                        sendRquest();
                    } else {
                        Toast.makeText(context, "银行卡号格式不正确!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void sendRquest() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("BANKCARD", bankCard);
        params.addQueryStringParameter("BANKTYPE", String.valueOf(cardType));

        String url = getString(R.string.server_url) + URLMap.BIND_BANKCARD;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        Editor edit = sp.edit();
                        edit.putString(UserEntity.IS_BAND_BANKCARD, "1");
                        edit.commit();
                        Toast.makeText(context, "绑定银行卡成功！", Toast.LENGTH_SHORT).show();
                        setResult(Activity.RESULT_OK, new Intent());
                        finish();
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getUserCashInfo() {
        dialog.show();
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_USERCASHINFO;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils();
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                dialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    dialog.dismiss();
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        LogUtils.i(data.toString());
                        if (data != null) {
                            if (CommonTools.judgeNull(data, "BANKTYPE", null) != null) {
                                cardType = Integer.parseInt(CommonTools.judgeNull(data, "BANKTYPE", "0"));
                                tvBankCardType.setTextColor(getResources().getColor(R.color.gray_text));
                                tvBankCardType.setText(type[cardType]);
                                etBankCardNum.setText(CommonTools.judgeNull(data, "BANKCARD", ""));
                            }
                            etBankCardUserName.setText(CommonTools.judgeNull(data, "NAME", ""));
                        } else {
                            etBankCardUserName.setText(sp.getString(UserEntity.NAME, ""));
                        }
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void showWindow(View v) {
        //找到布局文件
        layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.myspinner_dropdown, null);
        listView = (ListView) layout.findViewById(R.id.listView);
        listView.setAdapter(adapter);
        popupWindow = new PopupWindow();
        //设置弹框的宽度为布局文件的宽度
        popupWindow.setWidth(tvBankCardType.getWidth());
        //高度随着内容变化
        popupWindow.setHeight(CommonTools.dp2px(context, 230));
        //设置一个透明背景
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //设置点击弹框外部，弹框消失
        popupWindow.setOutsideTouchable(true);
        //设置焦点
        popupWindow.setFocusable(true);
        //设置所在布局
        popupWindow.setContentView(layout);
        // 设置弹框出现的位置，在v的正下方横轴偏移textview的宽度，为了对齐~纵轴不偏移
        popupWindow.showAsDropDown(tvBankCardType, 0, 0);
        // listView的item点击事件
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                tvBankCardType.setText(myData.get(arg2));
                cardType = arg2;
                popupWindow.dismiss();
                popupWindow = null;
            }
        });
    }


    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            cardType = typeDialogWindow.bankType;
            tvBankCardType.setText(myData.get(cardType));
            LogUtils.e(typeDialogWindow.bankType + "");
            typeDialogWindow.dismiss();
        }
    };

}
