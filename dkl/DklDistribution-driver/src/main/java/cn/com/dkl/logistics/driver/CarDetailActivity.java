package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.utils.NetworkCallBack;

public class CarDetailActivity extends BaseActivity {
    private final Context context = CarDetailActivity.this;
    @Bind(R.id.btnLeft)
    Button btnLeft;
    @Bind(R.id.btnRight)
    Button btnRight;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.llCarNumberHead)
    LinearLayout llCarNumberHead;
    @Bind(R.id.tvCarNumberHead)
    TextView tvCarNumberHead;
    @Bind(R.id.etCarNumber)
    TextView etCarNumber;
    @Bind(R.id.llCarType)
    LinearLayout llCarType;
    @Bind(R.id.tvCarType)
    TextView tvCarType;
    @Bind(R.id.etCarLength)
    EditText etCarLength;
    @Bind(R.id.etLoadWeight)
    EditText etLoadWeight;
    @Bind(R.id.etVolume)
    EditText etVolume;

    @Bind(R.id.ivDrivingLic)
    ImageView ivDrivingLic;//行驶证
    @Bind(R.id.ivCarDriver)
    ImageView ivCarDriver; //人车照片
    @Bind(R.id.ivCarInsurance)
    ImageView ivCarInsurance; //商业险
    @Bind(R.id.ivCarTransport)
    ImageView ivCarTransport; //道路运输证
    @Bind(R.id.ivTransportPermit)
    ImageView ivTransportPermit; //交强险

    @Bind(R.id.btnAuth)
    Button btnAuth; //提交认证

    private Map<String, String> car;
    private SharedPreferences sp;
    private AlertDeleteDialogwindow window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        ButterKnife.bind(this);
        tvTitle.setText("车辆信息");
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setText("编辑车辆");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        car = new HashMap<String, String>();
        car = (Map<String, String>) getIntent().getSerializableExtra("carInfo");
        setView(car);
    }

    private void setView(Map<String, String> car) {
        String status = car.get("STATUS");
        if (status.equals("0") || status.equals("-1") || status.equals("3")){
            btnAuth.setVisibility(View.VISIBLE);
        }
        if (status.equals("2")){
            btnRight.setVisibility(View.GONE);
        }
        tvCarNumberHead.setText(car.get("CARNUMBER").substring(0, 1));
        etCarNumber.setText(car.get("CARNUMBER").substring(1));
        tvCarType.setText(car.get("CARTYPE"));
        etCarLength.setText(car.get("CARLENGTH"));
        etLoadWeight.setText(car.get("LOADWEIGHT"));
        etVolume.setText(car.get("VOLUMN"));
        ImageUtils.imageLoad(context,car.get("TRAVELLICIMG"),ivDrivingLic);
        ImageUtils.imageLoad(context,car.get("CARDRIVERIMG"),ivCarDriver);
        ImageUtils.imageLoad(context,car.get("BUSINESSRISKS"),ivCarInsurance);
        ImageUtils.imageLoad(context,car.get("TRANSPORTIMG"),ivCarTransport);
        ImageUtils.imageLoad(context,car.get("INSURANCE"),ivTransportPermit);
        isEnable();
    }

    private void isEnable() {
        llCarNumberHead.setEnabled(false);
        etCarNumber.setEnabled(false);
        llCarType.setEnabled(false);
        etCarLength.setEnabled(false);
        etLoadWeight.setEnabled(false);
        etVolume.setEnabled(false);
        ivDrivingLic.setEnabled(false);
        ivCarDriver.setEnabled(false);
        ivCarInsurance.setEnabled(false);
        ivCarTransport.setEnabled(false);
        ivTransportPermit.setEnabled(false);
    }

    @OnClick({R.id.btnLeft, R.id.btnRight, R.id.btnAuth, R.id.btnDelete})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnRight:
                startActivity(new Intent(context, CarEditActivity.class).putExtra("ToDo", "editCar").putExtra("carInfo",(Serializable)car));
                break;
            case R.id.btnAuth:
                carAuth();
                break;
            case R.id.btnDelete:
                String tDelete = "确认是否删除车辆？";
                String tCancel = "确认删除";
                ClickListener listener = new ClickListener();
                window = new AlertDeleteDialogwindow(CarDetailActivity.this, listener, tDelete, tCancel);
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                window.showAtLocation(this.findViewById(R.id.carDetail), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                break;
            default:
                break;
        }
    }

    class ClickListener implements View.OnClickListener {

        private ClickListener() {
        }

        @Override
        public void onClick(View arg0) {
            delete();
            window.dismiss();
        }

    }

    private void carAuth() {
        String url = getString(R.string.server_url) + URLMap.AUTH_CAR;
        RequestParams params = new RequestParams(url);
        params.addBodyParameter("USERNAME",  sp.getString(UserEntity.PHONE, null));
        params.addBodyParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addBodyParameter("CARID", car.get("CARID"));
        params.addBodyParameter("CARNUMBER", car.get("CARNUMBER"));

        NetworkCallBack callBack = new NetworkCallBack(this);
        callBack.networkRequest(params, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        btnAuth.setVisibility(View.GONE);
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void delete() {
        String url = getString(R.string.server_url) + URLMap.DELETE_CAR;
        RequestParams params = new RequestParams(url);
        params.addBodyParameter("USERNAME",  sp.getString(UserEntity.PHONE, null));
        params.addBodyParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addBodyParameter("CARID", car.get("CARID"));

        NetworkCallBack callBack = new NetworkCallBack(this);
        callBack.networkRequest(params, true, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        SystemClock.sleep(1000);
                        startActivity(new Intent(context,CarListActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
