package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.CdsCarTypeInfo;
import cn.com.dkl.logistics.entity.ConditionType;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.utils.NetworkCallBack;
import cn.com.dkl.logistics.view.CustomProgressDialog;

public class CarEditActivity extends BaseActivity {
    private final Context context = CarEditActivity.this;
    @Bind(R.id.btnLeft)
    Button btnLeft;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.llCarNumberHead)
    LinearLayout llCarNumberHead;
    @Bind(R.id.tvCarNumberHead)
    TextView tvCarNumberHead;
    @Bind(R.id.etCarNumber)
    EditText etCarNumber;
    @Bind(R.id.llCarType)
    LinearLayout llCarType;
    @Bind(R.id.tvCarType)
    TextView tvCarType;
    @Bind(R.id.etCarLength)
    EditText etCarLength;
    @Bind(R.id.etLoadWeight)
    EditText etLoadWeight;
    @Bind(R.id.etVolume)
    EditText etVolume;

    @Bind(R.id.ivDrivingLic)
    ImageView ivDrivingLic; //行驶证
    @Bind(R.id.ivCarDriver)
    ImageView ivCarDriver; //人车照片
    @Bind(R.id.ivCarInsurance)
    ImageView ivCarInsurance; //商业险
    @Bind(R.id.ivCarTransport)
    ImageView ivCarTransport; //道路运输证
    @Bind(R.id.ivTransportPermit)
    ImageView ivTransportPermit; //交强险

    private SharedPreferences sp;
    private String toDo;
    private final String ADD_CAR = "addCar";
    private final String EDIT_CAR = "editCar";
    private CustomProgressDialog dialog = null;

    private Map<String, String> driverDatasMap = new HashMap<String, String>();
    private List<String> nameDatas = new ArrayList<String>();
    private String[] driverNameArray = null;

    private ImageChooseDialogWindow menuWindow;
    /**
     * 行驶证正面
     */
    private final String DRIVING_LIC_IMAGE = "TRAVELLICIMG";
    /**
     * 人车正面
     */
    private final String CAR_DRIVER_IMAGE = "CARDRIVERIMG";
    /**
     * 商业险正面
     */
    private final String COMMERCIAL_INSURANCE_IMAGE = "BUSINESSRISKS";
    /**
     * 交强险正面
     */
    private final String COMPULSORY_TRAFFIC_INSURANCE_IMAGE = "INSURANCE";
    /**
     * 道路运输证正面
     */
    private final String ROAD_TRANSPORT_IMAGE = "TRANSPORTIMG";
    /**
     * 之前上传的图片
     */
    private Map<String, String> imageOld = new HashMap<String, String>();
    /**
     * 存储当前选择的图片路径
     */
    private Map<String, String> imagePath = new HashMap<String, String>();
    private boolean hadUpload = true;


    private List<String> uploadImgDatas = new ArrayList<String>();
    private String carNumber;
    private String isSocialCar;
    private String loadWeight;
    private String carType;
    private String carLength;
    private String volumn;

    private List<String> permissionList = new ArrayList<String>();
    boolean isCamera = false;
    Map<String, String> car = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_edit);
        init();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        ButterKnife.bind(this);
        dialog = new CommonTools(context).getProgressDialog(context, "保存车辆...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        toDo = getIntent().getStringExtra("ToDo");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        if (ADD_CAR.equals(toDo)) {
            tvTitle.setText(R.string.add_car);
        } else if (EDIT_CAR.equals(toDo)) {
            tvTitle.setText(R.string.edit_car);
            car = (Map<String, String>) getIntent().getSerializableExtra("carInfo");
            setView(car);
        }
    }

    private void setView(Map<String, String> car) {
        tvCarNumberHead.setText(car.get("CARNUMBER").substring(0, 1));
        etCarNumber.setText(car.get("CARNUMBER").substring(1));
        tvCarType.setText(car.get("CARTYPE"));
        etCarLength.setText(car.get("CARLENGTH"));
        etLoadWeight.setText(car.get("LOADWEIGHT"));
        etVolume.setText(car.get("VOLUMN"));
        imageOld.put(DRIVING_LIC_IMAGE, car.get("TRAVELLICIMG"));
        imageOld.put(CAR_DRIVER_IMAGE, car.get("CARDRIVERIMG"));
        imageOld.put(COMMERCIAL_INSURANCE_IMAGE, car.get("BUSINESSRISKS"));
        imageOld.put(ROAD_TRANSPORT_IMAGE, car.get("TRANSPORTIMG"));
        imageOld.put(COMPULSORY_TRAFFIC_INSURANCE_IMAGE, car.get("INSURANCE"));
        ImageUtils.imageLoad(context,car.get("TRAVELLICIMG"),ivDrivingLic);
        ImageUtils.imageLoad(context,car.get("CARDRIVERIMG"),ivCarDriver);
        ImageUtils.imageLoad(context,car.get("BUSINESSRISKS"),ivCarInsurance);
        ImageUtils.imageLoad(context,car.get("TRANSPORTIMG"),ivCarTransport);
        ImageUtils.imageLoad(context,car.get("INSURANCE"),ivTransportPermit);
    }

    @OnClick({R.id.btnLeft, R.id.llCarNumberHead, R.id.llCarType, R.id.btnSave,
            R.id.ivDrivingLic, R.id.ivCarDriver, R.id.ivCarInsurance, R.id.ivCarTransport, R.id.ivTransportPermit})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.llCarNumberHead:
                selectCondition(ConditionType.TYPE_NUMBER_HEAD);
                break;
            case R.id.llCarType:
                selectCondition(ConditionType.TYPE_CAR_TYPE);
                break;
            case R.id.ivDrivingLic:
                ImageUtils.uploadImageName = DRIVING_LIC_IMAGE;
                choosePhoto();
                break;
            case R.id.ivCarDriver:
                ImageUtils.uploadImageName = CAR_DRIVER_IMAGE;
                choosePhoto();
                break;
            case R.id.ivCarInsurance:
                ImageUtils.uploadImageName = COMMERCIAL_INSURANCE_IMAGE;
                choosePhoto();
                break;
            case R.id.ivCarTransport:
                ImageUtils.uploadImageName = ROAD_TRANSPORT_IMAGE;
                choosePhoto();
                break;
            case R.id.ivTransportPermit:
                ImageUtils.uploadImageName = COMPULSORY_TRAFFIC_INSURANCE_IMAGE;
                choosePhoto();
                break;
            case R.id.btnSave:
                saveCar();
                break;
            default:
                break;
        }
    }

    public void choosePhoto() {
        menuWindow = new ImageChooseDialogWindow(this);
        menuWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        menuWindow.showAtLocation(this.findViewById(R.id.editcar), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    public void selectCondition(int chooseType) {
        Intent intent = new Intent();
        intent.putExtra("chooseType", chooseType);
        if (null != driverNameArray && 0 != driverNameArray.length) {
            intent.putExtra("driverName", driverNameArray);
        }
        intent.setClass(this, ChooseConditionActivity.class);
        startActivityForResult(intent, chooseType);
    }

    private void saveCar() {
        String carNumberHead = tvCarNumberHead.getText().toString().trim();
        carNumber = etCarNumber.getText().toString().trim();
        loadWeight = etLoadWeight.getText().toString().trim();
        carType = tvCarType.getText().toString().trim();
        carLength = etCarLength.getText().toString().trim();
        volumn = etVolume.getText().toString().trim();
        if (TextUtils.isEmpty(carNumberHead)) {
            Toast.makeText(context, "请选择车牌所属地！", Toast.LENGTH_SHORT).show();
            tvCarNumberHead.requestFocus();
            return;
        } else if (TextUtils.isEmpty(carNumber)) {
            Toast.makeText(context, "车牌号不能为空！", Toast.LENGTH_SHORT).show();
            etCarNumber.requestFocus();
            return;
        }
        carNumber = carNumberHead + carNumber;
        if (!CommonTools.checkCarNumber(carNumber)) {
            Toast.makeText(context, "车牌号码输入有误", Toast.LENGTH_SHORT).show();
            etCarNumber.requestFocus();
            return;
        } else if (TextUtils.isEmpty(carType)) {
            Toast.makeText(context, "车辆类型不能为空！", Toast.LENGTH_SHORT).show();
            tvCarType.requestFocus();
            return;
        } else if (TextUtils.isEmpty(carLength)) {
            Toast.makeText(context, "车辆长度不能为空！", Toast.LENGTH_SHORT).show();
            etCarLength.requestFocus();
            return;
        } else if (TextUtils.isEmpty(loadWeight)) {
            Toast.makeText(context, "车辆载重不能为空！", Toast.LENGTH_SHORT).show();
            etLoadWeight.requestFocus();
            return;
        }else if (TextUtils.isEmpty(volumn)) {
            Toast.makeText(context, "车辆体积不能为空！", Toast.LENGTH_SHORT).show();
            etLoadWeight.requestFocus();
            return;
        }

        dialog.show();
        if (hadUpload) { // 图片上传过
            sendRequest();
        } else {
            doUpload();
        }
    }

    private void doUpload() {
        com.lidroid.xutils.http.RequestParams params = new com.lidroid.xutils.http.RequestParams();
        File file = null;
        if (imagePath.isEmpty()) {
            return;
        }
        for (Map.Entry<String, String> image : imagePath.entrySet()) {
            String imageItem = image.getKey();
            String imagePath = image.getValue();
            if (!TextUtils.isEmpty(imagePath)) { // 图片路径不为空，并且图片存在
                file = new File(imagePath);
                if (file.exists()) {
                    params.addBodyParameter(imageItem, file);
                }
            }
        }

        String url = getString(R.string.server_url) + URLMap.UPLOAD_IMAGE;
        LogUtils.i(url + CommonTools.getParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpRequest.HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String message = obj.getString("message");
                    String status = obj.getString("status");
                    if ("1".equals(status)) {
                        JSONArray datas = obj.getJSONArray("data");
                        for (int i = 0; i < datas.length(); i++) {
                            JSONObject data = datas.getJSONObject(i);
                            String imgFileName = data.getString("FILENAME");
                            String rawName = data.getString("RAWNAME");
                            if (rawName.contains(DRIVING_LIC_IMAGE)) {
                                imageOld.put(DRIVING_LIC_IMAGE, imgFileName);
                            } else if (rawName.contains(CAR_DRIVER_IMAGE)) {
                                imageOld.put(CAR_DRIVER_IMAGE, imgFileName);
                            } else if (rawName.contains(COMMERCIAL_INSURANCE_IMAGE)) {
                                imageOld.put(COMMERCIAL_INSURANCE_IMAGE, imgFileName);
                            } else if (rawName.contains(ROAD_TRANSPORT_IMAGE)) {
                                imageOld.put(ROAD_TRANSPORT_IMAGE, imgFileName);
                            } else if (rawName.contains(COMPULSORY_TRAFFIC_INSURANCE_IMAGE)) {
                                imageOld.put(COMPULSORY_TRAFFIC_INSURANCE_IMAGE, imgFileName);
                            }
                        }
                        hadUpload = true;
                        sendRequest();
                    } else if ("0".equals(status)) {
                        dialog.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        dialog.dismiss();
                        Toast.makeText(context, "服务器异常", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }

        });
    }

    private void sendRequest() {
        String url = getString(R.string.server_url) + URLMap.SAVE_EDIT_CAR;
        RequestParams params = new RequestParams(url);
        params.addBodyParameter("USERNAME",  sp.getString(UserEntity.PHONE, null));
        params.addBodyParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        if (EDIT_CAR.equals(toDo)) {
            if (TextUtils.isEmpty(car.get("CARID"))) { // 车辆ID不能为空
                return;
            } else {
                params.addBodyParameter("CARID", car.get("CARID"));
            }
        }
        params.addQueryStringParameter("CARNUMBER", carNumber);
        params.addQueryStringParameter("CARTYPE", carType);
        params.addQueryStringParameter("CARLENGTH", carLength);
        params.addQueryStringParameter("LOADWEIGHT", loadWeight);
        params.addQueryStringParameter("VOLUMN", volumn);
        // 初始化图片
        for (Map.Entry<String, String> image : imageOld.entrySet()) {
            params.addBodyParameter(image.getKey(), image.getValue());
        }

        NetworkCallBack callBack = new NetworkCallBack(this);
        callBack.networkRequest(params, true, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        if (ADD_CAR.equals(toDo)) {
                            message = "添加车辆成功！";
                        } else {
                            message = "修改车辆信息成功！";
                        }
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        SystemClock.sleep(1000);
                        startActivity(new Intent(context,CarListActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImageUtils.GET_IMAGE_BY_CAMERA: // 拍照获取图片
                if (null != ImageUtils.imageUriFromCamera && resultCode != RESULT_CANCELED) {
                    ImageUtils.deleteImage(this.imagePath.get(ImageUtils.uploadImageName));
                    ImageUtils.uriToPath(this, ImageUtils.imageUriFromCamera);
                    setImageInfo();
                    hadUpload = false;
                }
                break;
            case ImageUtils.GET_IMAGE_FROM_PHONE: // 手机相册获取图片
                if (resultCode != RESULT_CANCELED && null != data.getData()) {
                    ImageUtils.deleteImage(this.imagePath.get(ImageUtils.uploadImageName));
                    ImageUtils.uriToPath(this, data.getData());
                    setImageInfo();
                    hadUpload = false;
                }
                break;
            case ConditionType.TYPE_NUMBER_HEAD:
                if (null != data) {
                    if (data.getBooleanExtra("isChosen", false)) {// 判断是否选中
                        tvCarNumberHead.setText(data.getStringExtra("chosenItem"));
                    }
                }
                break;
            case ConditionType.TYPE_CAR_TYPE: // 车型选择结果
                if (null != data) {
                    if (data.getBooleanExtra("isChosen", false)) {// 判断是否选中

                        CdsCarTypeInfo carTypeInfo = (CdsCarTypeInfo) data.getExtras().getSerializable("carTypeInfo");
                        tvCarType.setText(carTypeInfo.NAME);
                        etCarLength.setText(carTypeInfo.LENGTH);
                        etLoadWeight.setText(carTypeInfo.WEIGHT);
                        etVolume.setText(carTypeInfo.VOLUMN);
                    }
                }
                break;
        }
    }


    private void setImageInfo() {
        if (TextUtils.isEmpty(ImageUtils.picPath)) {
            return;
        } else if (TextUtils.isEmpty(ImageUtils.uploadImageName)) {
            return;
        }

        ImageView uploadImage = null;
        if (ImageUtils.uploadImageName.equals(DRIVING_LIC_IMAGE)) {
            imagePath.put(DRIVING_LIC_IMAGE, ImageUtils.picPath);
            uploadImage = ivDrivingLic;
        } else if (ImageUtils.uploadImageName.equals(CAR_DRIVER_IMAGE)) {
            imagePath.put(CAR_DRIVER_IMAGE, ImageUtils.picPath);
            uploadImage = ivCarDriver;
        } else if (ImageUtils.uploadImageName.equals(COMMERCIAL_INSURANCE_IMAGE)) {
            imagePath.put(COMMERCIAL_INSURANCE_IMAGE, ImageUtils.picPath);
            uploadImage = ivCarInsurance;
        } else if (ImageUtils.uploadImageName.equals(ROAD_TRANSPORT_IMAGE)) {
            imagePath.put(ROAD_TRANSPORT_IMAGE, ImageUtils.picPath);
            uploadImage = ivCarTransport;
        } else if (ImageUtils.uploadImageName.equals(COMPULSORY_TRAFFIC_INSURANCE_IMAGE)) {
            imagePath.put(COMPULSORY_TRAFFIC_INSURANCE_IMAGE, ImageUtils.picPath);
            uploadImage = ivTransportPermit;
        }

        // 显示选择的图片
        uploadImage.setImageBitmap(BitmapFactory.decodeFile(ImageUtils.picPath));
    }

}
