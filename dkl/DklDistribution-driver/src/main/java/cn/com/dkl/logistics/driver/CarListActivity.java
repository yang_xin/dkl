package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.adapter.CarListAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.NetworkCallBack;

public class CarListActivity extends BaseActivity {

    private Context context = CarListActivity.this;
    @Bind(R.id.btnLeft)
    Button btnLeft; //返回
    @Bind(R.id.btnRight)
    Button btnRight; //新增按钮
    @Bind(R.id.tvTitle)
    TextView tvTitle; // 标题
    @Bind(R.id.ptrListView)
    PullToRefreshListView ptrListView;

    private SharedPreferences sp;
    private int currentPage = 1;
    private int SHOW_COUNT = 10;
    private int pageCount = 0;

    private List<Map<String, String>> listCar = new ArrayList<Map<String, String>>();
    private CarListAdapter adapter;
    private boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        init();
    }
    private void init(){
        ButterKnife.bind(this);
        tvTitle.setText("车辆管理");
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setText("添加车辆");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        initListView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
        currentPage = 1;
        listCar.clear();
        adapter.notifyDataSetChanged();
        sendRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initListView() {
        adapter = new CarListAdapter(context, listCar);
        ptrListView.setAdapter(adapter);

        ptrListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(context, CarDetailActivity.class).putExtra("carInfo",(Serializable) listCar.get(position-1)));
            }
        });


        ptrListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    sendRequest();
                }
            }

        });
        ptrListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (currentPage <= pageCount) {
                    if (flag) {
                        flag = false;
                        sendRequest();
                    }
                } else {
                    Toast.makeText(context, R.string.already_no_data, Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    @OnClick(value = {R.id.btnLeft, R.id.btnRight})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnRight: // 添加车辆
                startActivity(new Intent(context, CarEditActivity.class).putExtra("ToDo", "addCar"));
                break;
            default:
                break;
        }
    }



    private void sendRequest() {
        final String url = getString(R.string.server_url) + URLMap.GET_CARLIST;
        final org.xutils.http.RequestParams params = new org.xutils.http.RequestParams(url);
        params.addBodyParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addBodyParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addBodyParameter("CURRENTPAGE", String.valueOf(currentPage));
        params.addBodyParameter("SHOWCOUNT", String.valueOf(SHOW_COUNT));
        params.addBodyParameter("DEL_FLAG", "0");

        NetworkCallBack callBack = new NetworkCallBack(this);
        Callback.Cancelable cancel = callBack.networkRequest(params, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                flag = true;
                try {
                    JSONObject obj = new JSONObject(result);
                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    pageCount = obj.getInt("PAGECOUNT");
                    if ("1".equals(state)) {
                        if (ptrListView.isRefreshing()) { // 刷新显示中
                            ptrListView.onRefreshComplete();
                            listCar.clear();
                        }
                        JSONArray array = obj.getJSONArray("carList");
                        Map<String, String> car = null;
                        for (int i = 0; i < array.length(); i++) {
                            car = new HashMap<String, String>();
                            JSONObject carObject = array.getJSONObject(i);
                            car.put("CARID",CommonTools.judgeNull(carObject,"CARID",""));
                            car.put("STATUS",CommonTools.judgeNull(carObject,"STATUS","0"));
                            car.put("USERID",CommonTools.judgeNull(carObject,"USERID",""));
                            car.put("USERNAME",CommonTools.judgeNull(carObject,"USERNAME",""));
                            car.put("DRIVERMOBILE",CommonTools.judgeNull(carObject,"DRIVERMOBILE",""));
                            car.put("CARLENGTH",CommonTools.judgeNull(carObject,"CARLENGTH",""));
                            car.put("CARTYPE",CommonTools.judgeNull(carObject,"CARTYPE",""));
                            car.put("LOADWEIGHT",CommonTools.judgeNull(carObject,"LOADWEIGHT",""));
                            car.put("VOLUMN",CommonTools.judgeNull(carObject,"VOLUMN",""));
                            car.put("CARNUMBER",CommonTools.judgeNull(carObject,"CARNUMBER",""));
                            car.put("TRAVELLICIMG",CommonTools.judgeNull(carObject,"TRAVELLICIMG",""));
                            car.put("CARDRIVERIMG",CommonTools.judgeNull(carObject,"CARDRIVERIMG",""));
                            car.put("BUSINESSRISKS",CommonTools.judgeNull(carObject,"BUSINESSRISKS",""));
                            car.put("TRANSPORTIMG",CommonTools.judgeNull(carObject,"TRANSPORTIMG",""));
                            car.put("INSURANCE",CommonTools.judgeNull(carObject,"INSURANCE",""));
                            listCar.add(car);
                        }
                        if (listCar.size() > 0) {
                            adapter.notifyDataSetChanged();
                        }
                        // 当前页+1
                        currentPage += 1;
                    } else {
                        Toast.makeText(context, "获取订单失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }
        });
    }

}
