package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.adapter.ChooseAreaGridAdapter;
import cn.com.dkl.logistics.constant.SettingEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.AreaSqliteHelper;
import cn.com.dkl.logistics.utils.InitDataBase;

/**
 * 选择地区界面
 *
 * @author Dao
 */
public class ChooseAreaActivity extends BaseActivity implements OnItemClickListener {
    @ViewInject(R.id.gvArea)
    private GridView gvArea;
    @ViewInject(R.id.ivSearch)
    private ImageView ivSearch;
    @ViewInject(R.id.tvChosenArea)
    private TextView tvChosenArea;
    @ViewInject(R.id.tvClear)
    private TextView tvClear;
    @ViewInject(R.id.actSearch)
    private AutoCompleteTextView actSearch;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.btnRight)
    private Button btnRight;
    @ViewInject(R.id.btnNews)
    private Button btnNews;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.llPopularCity)
    private LinearLayout llPopularCity;
    @ViewInject(R.id.llProvinces)
    private LinearLayout llProvinces;
    @ViewInject(R.id.tvPopularCity)
    private TextView tvPopularCity;
    @ViewInject(R.id.tvProvinces)
    private TextView tvProvinces;

    private ChooseAreaGridAdapter gridAdapter;
    private SharedPreferences spSetting;
    private List<Map<String, Object>> provinceData = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> cityData = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> districtData = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> popularAreaData = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> popularDistrictData = new ArrayList<Map<String, Object>>();
    private AreaSqliteHelper mySqliteHelper;
    private String provinceName = "";
    private String provinceId = "";
    private String cityName = "";
    private String cityId = "";
    private String districtName = "";
    private String inputText = "";
    private String popularProvinceName = "";
    private String popularCityName = "";
    private String popularCityId = "";
    private String popularDistrictName = "";
    private int CHOOSE_INDEX = 1;// 非指定热门城市时的页面引导，1：省份，2：城市，3：地区
    private boolean isRelease = false;// 是否发布
    private int currentPageIndex = 0;// 当前页面。0：省份列表，1：城市列表，2：地区列表。
    private int selectedColor = 0;
    private int disselectedColor = 0;
    private boolean isPopularCity = true;
    private int POLULAR_INDEX = 1;// 指定是热门城市时的页面引导，1：城市，2：地区
    private int provinceSelected = 0;
    private int citySelected = 0;
    private int popularCitySelected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_area_grid_view);
        init();// 初始化

    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    /**
     * 初始化
     *
     * @author guochaohui
     */
    public void init() {
        ViewUtils.inject(this);
        isRelease = getIntent().getBooleanExtra("isRelease", false);// 是否发布

        spSetting = getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
        String databaseURl = spSetting.getString(SettingEntity.DATA_BASE_URL, "");// 获取数据库url
        mySqliteHelper = InitDataBase.getMySqliteHelper(this, databaseURl);
        // 根据条件获取热门城市列表
        popularAreaData = mySqliteHelper.getPopularCityAndProvince(mySqliteHelper, getString(R.string.area_table_name));// 查询数据
        // 将数据导入gridView
        gridAdapter = new ChooseAreaGridAdapter(getApplicationContext(), popularAreaData);
        gvArea.setAdapter(gridAdapter);
        gvArea.setOnItemClickListener(this);
        // 设置标题
        // if (isPopularCity) {
        // tvTitle.setText(getString(R.string.popular_city));
        // }
        // else {
        // tvTitle.setText(getString(R.string.province_list));
        // }

        tvTitle.setText("选择地区");

        actSearch.addTextChangedListener(textWatcher);
        btnLeft.setVisibility(View.VISIBLE);
        btnNews.setVisibility(View.INVISIBLE);

        selectedColor = getResources().getColor(R.color.text_color_deep_orange);
        disselectedColor = getResources().getColor(R.color.text_color_light_gray);
        popularCitySelected();
    }

    @OnClick(value = {R.id.btnLeft, R.id.btnRight, R.id.tvClear, R.id.llPopularCity, R.id.llProvinces})
    public void onClicked(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                if (isPopularCity) {// 当前是否热门城市列表
                    switch (POLULAR_INDEX) {
                        case 1:
                            Intent intent = new Intent();
                            intent.putExtra("provinceName", popularProvinceName);
                            intent.putExtra("cityName", popularCityName);
                            intent.putExtra("isChosen", false);
                            setResult(RESULT_OK, intent);
                            finish();
                            break;

                        case 2:
                            btnRight.setVisibility(View.INVISIBLE);
                            gridAdapter.setData(popularAreaData);
                            gridAdapter.setClickId(popularCitySelected);
                            gridAdapter.notifyDataSetChanged();
                            POLULAR_INDEX = 1;
                            // tvTitle.setText(getString(R.string.city_list));
                            popularCityName = "";
                            tvTitle.setText(popularProvinceName + popularCityName);
                            break;

                        default:
                            break;
                    }

                } else {
                    switch (CHOOSE_INDEX) {
                        case 1:
                            Intent intent = new Intent();
                            intent.putExtra("provinceName", provinceName);
                            intent.putExtra("cityName", cityName);
                            intent.putExtra("isChosen", false);
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 2:
                            btnRight.setVisibility(View.INVISIBLE);// 隐藏右上角按钮
                            gridAdapter.setData(provinceData);// 刷新gridView数据
                            gridAdapter.setClickId(provinceSelected);
                            gridAdapter.notifyDataSetChanged();
                            CHOOSE_INDEX = 1;// 将页面引导设置为1
                            currentPageIndex = 0;
                            // tvTitle.setText(getString(R.string.province_list));//
                            // 设置标题
                            provinceName = "";
                            tvTitle.setText(provinceName + cityName);// 显示选中的地区信息
                            actSearch.setHint(R.string.search_province);
                            break;
                        case 3:
                            btnRight.setVisibility(View.INVISIBLE);
                            gridAdapter.setData(cityData);
                            gridAdapter.setClickId(citySelected);
                            gridAdapter.notifyDataSetChanged();
                            CHOOSE_INDEX = 2;
                            currentPageIndex = 1;
                            // tvTitle.setText(getString(R.string.city_list));
                            cityName = "";
                            tvTitle.setText(provinceName + cityName);
                            actSearch.setHint(R.string.search_city);
                            break;
                        default:
                            break;
                    }
                }
                break;

            case R.id.btnRight:
                Intent i = new Intent();
                if (isPopularCity) {// 是否热门城市列表
                    i.putExtra("provinceName", popularProvinceName);
                    i.putExtra("cityName", popularCityName);
                    i.putExtra("districtName", popularDistrictName);
                } else {
                    i.putExtra("provinceName", provinceName);
                    i.putExtra("cityName", cityName);
                    i.putExtra("districtName", districtName);
                }
                i.putExtra("isChosen", true);
                setResult(RESULT_OK, i);
                finish();
                break;
            case R.id.tvClear:// 清除输入框
                if (actSearch.getText().toString() != null) {
                    actSearch.setText("");
                }
                break;

            case R.id.llPopularCity:// 选中热门城市列表
                popularCitySelected();
                popularAreaData = mySqliteHelper.getPopularCityAndProvince(mySqliteHelper, getString(R.string.area_table_name));// 查询数据
                gridAdapter.setData(popularAreaData);
                gridAdapter.notifyDataSetChanged();
                break;
            case R.id.llProvinces:// 选中所有省份列表
                popularCityDisselected();
                provinceData = mySqliteHelper.query(mySqliteHelper, getString(R.string.area_table_name), "0");// 查询数据
                gridAdapter.setData(provinceData);
                gridAdapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }

    /**
     * 选中热门城市
     *
     * @author guochaohui
     */
    public void popularCitySelected() {
        isPopularCity = true;
        tvPopularCity.setTextColor(selectedColor);
        tvProvinces.setTextColor(disselectedColor);
    }

    /**
     * 选中省份
     *
     * @author guochaohui
     */
    public void popularCityDisselected() {
        isPopularCity = false;
        tvPopularCity.setTextColor(disselectedColor);
        tvProvinces.setTextColor(selectedColor);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        if (isPopularCity) {// 是否热门城市列表

            switch (POLULAR_INDEX) {
                case 1:
                    popularProvinceName = popularAreaData.get(arg2).get("ParentName").toString();
                    popularCityName = popularAreaData.get(arg2).get("Name").toString();
                    popularCityId = popularAreaData.get(arg2).get("ID").toString();
                    gridAdapter.setClickId(arg2);
                    gridAdapter.notifyDataSetChanged();
                    tvTitle.setText(popularProvinceName + popularCityName);
                    popularCitySelected = arg2;
                    // 根据id查询该地区下一级的地区列表
                    popularDistrictData = mySqliteHelper.query(mySqliteHelper, getString(R.string.area_table_name), popularCityId);

                    if (popularDistrictData.size() != 0 && isRelease) {// 判断是否发布
                        gridAdapter.setData(popularDistrictData);
                        gridAdapter.setClickId(-1);
                        gridAdapter.notifyDataSetChanged();
                        btnRight.setVisibility(View.VISIBLE);
                        // tvTitle.setText(getString(R.string.district_list));
                        POLULAR_INDEX = 2;
                        actSearch.setHint(R.string.search_district);
                    } else {

                        Intent intent = new Intent();
                        intent.putExtra("provinceName", popularProvinceName);
                        intent.putExtra("cityName", popularCityName);
                        intent.putExtra("districtName", "");
                        intent.putExtra("isChosen", true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                    break;
                case 2:
                    // 获取地区名
                    popularDistrictName = popularDistrictData.get(arg2).get("Name").toString();
                    gridAdapter.setClickId(arg2);
                    gridAdapter.notifyDataSetChanged();

                    Intent intent = new Intent();
                    intent.putExtra("provinceName", popularProvinceName);
                    intent.putExtra("cityName", popularCityName);
                    intent.putExtra("districtName", popularDistrictName);
                    intent.putExtra("isChosen", true);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                default:
                    break;
            }

        } else {
            switch (CHOOSE_INDEX) {
                case 1:
                    provinceName = provinceData.get(arg2).get("Name").toString();
                    provinceId = provinceData.get(arg2).get("ID").toString();
                    tvTitle.setText(provinceName);
                    gridAdapter.setClickId(arg2);
                    gridAdapter.notifyDataSetChanged();
                    provinceSelected = arg2;
                    cityData = mySqliteHelper.query(mySqliteHelper, getString(R.string.area_table_name), provinceId);
                    if (cityData.size() != 0) {
                        gridAdapter.setData(cityData);
                        gridAdapter.setClickId(-1);
                        gridAdapter.notifyDataSetChanged();
                        btnRight.setVisibility(View.VISIBLE);
                        // tvTitle.setText(getString(R.string.city_list));
                        CHOOSE_INDEX = 2;
                        currentPageIndex = 1;
                        actSearch.setHint(R.string.search_city);

                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("provinceName", provinceName);
                        intent.putExtra("cityName", cityName);
                        intent.putExtra("districtName", districtName);
                        intent.putExtra("isChosen", true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                    break;
                case 2:
                    cityName = cityData.get(arg2).get("Name").toString();
                    cityId = cityData.get(arg2).get("ID").toString();
                    tvTitle.setText(provinceName + cityName);
                    gridAdapter.setClickId(arg2);
                    gridAdapter.notifyDataSetChanged();
                    citySelected = arg2;
                    districtData = mySqliteHelper.query(mySqliteHelper, getString(R.string.area_table_name), cityId);

                    if (districtData.size() != 0 && isRelease) {// 判断是否发布
                        gridAdapter.setData(districtData);
                        gridAdapter.setClickId(-1);
                        gridAdapter.notifyDataSetChanged();
                        btnRight.setVisibility(View.VISIBLE);
                        // tvTitle.setText(getString(R.string.district_list));
                        CHOOSE_INDEX = 3;
                        currentPageIndex = 2;
                        actSearch.setHint(R.string.search_district);
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("provinceName", provinceName);
                        intent.putExtra("cityName", cityName);
                        intent.putExtra("districtName", districtName);
                        intent.putExtra("isChosen", true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                    break;
                case 3:
                    gridAdapter.setClickId(arg2);
                    gridAdapter.notifyDataSetChanged();
                    districtName = districtData.get(arg2).get("Name").toString();

                    Intent intent = new Intent();
                    intent.putExtra("provinceName", provinceName);
                    intent.putExtra("cityName", cityName);
                    intent.putExtra("districtName", districtName);
                    intent.putExtra("isChosen", true);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;

                default:
                    break;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isPopularCity) {
                switch (POLULAR_INDEX) {
                    case 1:
                        Intent intent = new Intent();
                        intent.putExtra("provinceName", popularProvinceName);
                        intent.putExtra("cityName", popularCityName);
                        intent.putExtra("isChosen", false);
                        setResult(RESULT_OK, intent);
                        finish();
                        break;

                    case 2:
                        btnRight.setVisibility(View.INVISIBLE);
                        gridAdapter.setData(popularAreaData);
                        gridAdapter.setClickId(popularCitySelected);
                        gridAdapter.notifyDataSetChanged();
                        POLULAR_INDEX = 1;
                        // tvTitle.setText(getString(R.string.city_list));
                        popularCityName = "";
                        tvTitle.setText(popularProvinceName + popularCityName);
                        break;

                    default:
                        break;
                }

            } else {
                switch (CHOOSE_INDEX) {
                    case 1:
                        Intent intent = new Intent();
                        intent.putExtra("provinceName", provinceName);
                        intent.putExtra("cityName", cityName);
                        intent.putExtra("isChosen", false);
                        setResult(RESULT_OK, intent);
                        finish();
                        break;
                    case 2:
                        btnRight.setVisibility(View.INVISIBLE);
                        gridAdapter.setData(provinceData);
                        gridAdapter.setClickId(provinceSelected);
                        gridAdapter.notifyDataSetChanged();
                        CHOOSE_INDEX = 1;
                        currentPageIndex = 0;
                        // tvTitle.setText(getString(R.string.province_list));
                        provinceName = "";
                        tvTitle.setText(provinceName + cityName);
                        actSearch.setHint(R.string.search_province);
                        break;
                    case 3:
                        btnRight.setVisibility(View.INVISIBLE);
                        gridAdapter.setData(cityData);
                        gridAdapter.setClickId(citySelected);
                        gridAdapter.notifyDataSetChanged();
                        CHOOSE_INDEX = 2;
                        currentPageIndex = 1;
                        // tvTitle.setText(getString(R.string.city_list));
                        cityName = "";
                        tvTitle.setText(provinceName + cityName);
                        actSearch.setHint(R.string.search_city);
                        break;
                    default:
                        break;
                }
            }

        }
        return true;
    }

    TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (currentPageIndex == 0) {
                refresh(provinceData);// 刷新数据
                inputText = actSearch.getText().toString().trim();// 获取输入数据
                // 查询得到数据
                provinceData = mySqliteHelper.search(mySqliteHelper, getString(R.string.area_table_name), inputText, "0");
                // 将数据库加载入适配器
                gridAdapter.setData(provinceData);

            } else if (currentPageIndex == 1) {
                refresh(cityData);
                inputText = actSearch.getText().toString().trim();
                cityData = mySqliteHelper.search(mySqliteHelper, getString(R.string.area_table_name), inputText, provinceId);
                gridAdapter.setData(cityData);
            } else if (currentPageIndex == 2) {
                refresh(districtData);
                inputText = actSearch.getText().toString().trim();
                districtData = mySqliteHelper.search(mySqliteHelper, getString(R.string.area_table_name), inputText, cityId);
                gridAdapter.setData(districtData);
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * 刷新列表
     *
     * @param list 输入数据
     * @author guochaohui
     */
    public void refresh(List<Map<String, Object>> list) {
        gridAdapter.setData(list);
        gridAdapter.notifyDataSetChanged();
        tvClear.setVisibility(View.VISIBLE);
        if (currentPageIndex == 1) {
            actSearch.setHint(R.string.search_city);
        } else if (currentPageIndex == 2) {
            actSearch.setHint(R.string.search_district);
        }
    }

}
