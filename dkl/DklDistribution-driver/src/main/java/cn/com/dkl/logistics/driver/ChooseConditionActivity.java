package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.com.dkl.logistics.adapter.ChooseConditionGridAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.entity.CdsCarTypeInfo;
import cn.com.dkl.logistics.entity.CompanyInfo;
import cn.com.dkl.logistics.entity.ConditionType;
import cn.com.dkl.logistics.entity.ParkInfo;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.NetworkCallBack;
import cn.com.dkl.logistics.view.CustomProgressDialog;

public class ChooseConditionActivity extends BaseActivity implements OnItemClickListener {

    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.btnRight)
    private Button btnRight;
    @ViewInject(R.id.btnNews)
    private Button btnNews;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.gvCondition)
    private GridView gvCondition;

    private ChooseConditionGridAdapter adapter;
    private String[] carType, parkName, companyName, carLength, carLoad, goodsWeight, transportWay, goodsType, carNumber, effectiveDay, numberHead,
            carFrom, driverName, frequencyUnit, periodUnit;
    private String chosenItem = "";
    private int chooseType = 0;
    private CustomProgressDialog dialog = null;
    private Context context = ChooseConditionActivity.this;
    private static List<CdsCarTypeInfo> lists = new ArrayList<CdsCarTypeInfo>();
    private static List<ParkInfo> parkList = new ArrayList<ParkInfo>();
    private static List<CompanyInfo> companyList = new ArrayList<CompanyInfo>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_condition_grid_view);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    /**
     * 初始化
     *
     * @author guochaohui
     */
    public void init() {
        ViewUtils.inject(this);
        // 设置视图
        btnRight.setVisibility(View.INVISIBLE);
        btnNews.setVisibility(View.INVISIBLE);

        // 获取选择类型
        chooseType = getIntent().getIntExtra("chooseType", 0);

        // 从资源中获取数组
        Resources res = getResources();
        carLength = res.getStringArray(R.array.carLength);// 车长
        carType = res.getStringArray(R.array.carType);// 车型
        carLoad = res.getStringArray(R.array.carLoad);// 载重
        goodsWeight = res.getStringArray(R.array.goodsWeight);// 货物重量
        transportWay = res.getStringArray(R.array.transportWay);// 运输方式
        goodsType = res.getStringArray(R.array.goodsType);// 货物类型
        effectiveDay = res.getStringArray(R.array.effectiveDay);
        numberHead = res.getStringArray(R.array.numberHead);
        carFrom = res.getStringArray(R.array.carFrom); // 车辆来源
        frequencyUnit = res.getStringArray(R.array.frequencyUnit); // 定位频率单位
        periodUnit = res.getStringArray(R.array.periodUnit); // 定位时间单位

        if (getIntent().getStringArrayExtra("carNumber") != null) {
            carNumber = getIntent().getStringArrayExtra("carNumber");
        }

        if (null != getIntent().getStringArrayExtra("driverName")) {
            driverName = getIntent().getStringArrayExtra("driverName");
        }

        if (chooseType == ConditionType.TYPE_CAR_TYPE) {// 选择车型
//			adapter = new ChooseConditionGridAdapter(getApplicationContext(), carType);
            tvTitle.setText(getString(R.string.condition_car_type));
            selectCarType();
        } else if (chooseType == ConditionType.TYPE_PARK) {// 选择园区
            getPark();
            tvTitle.setText(getString(R.string.park));
        } else if (chooseType == ConditionType.TYPE_COMPANY) {// 选择园区
            getCompany();
            tvTitle.setText(getString(R.string.company));
        } else if (chooseType == ConditionType.TYPE_CAR_LENGTH) {// 选择车长
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), carLength);
            tvTitle.setText(getString(R.string.condition_car_length));
        } else if (chooseType == ConditionType.TYPE_CAR_LOAD) {// 选择载重
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), carLoad);
            tvTitle.setText(getString(R.string.condition_car_load));
        } else if (chooseType == ConditionType.TYPE_GOOD_WEIGHT) {// 选择货物重量
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), goodsWeight);
            tvTitle.setText(getString(R.string.condition_goods_weight));
        } else if (chooseType == ConditionType.TYPE_TRANSPORT_WAY) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), transportWay);
            tvTitle.setText(getString(R.string.condition_transport_way));
        } else if (chooseType == ConditionType.TYPE_GOODS_TYPE) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), goodsType);
            tvTitle.setText(getString(R.string.condition_goods_type));
        } else if (chooseType == ConditionType.TYPE_CARS_NUMBER) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), carNumber);
            tvTitle.setText(getString(R.string.condition_cars_number));
        } else if (chooseType == ConditionType.TYPE_MIN_EFFECTIVE_DAY || chooseType == ConditionType.TYPE_MAX_EFFECTIVE_DAY) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), effectiveDay);
            tvTitle.setText(getString(R.string.reference_effective_day));
        } else if (chooseType == ConditionType.TYPE_NUMBER_HEAD) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), numberHead);
            tvTitle.setText(getString(R.string.numberHead));
        } else if (chooseType == ConditionType.TYPE_DRIVER) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), driverName);
            tvTitle.setText(getString(R.string.choose_driver));
        } else if (chooseType == ConditionType.TYPE_CAR_FROM) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), carFrom);
            tvTitle.setText(getString(R.string.choose_car_from));
        } else if (chooseType == ConditionType.TYPE_FREQUENCY_UNIT) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), frequencyUnit);
            tvTitle.setText(getString(R.string.choose_time_unit));
        } else if (chooseType == ConditionType.TYPE_PERIOD_UNIT) {
            adapter = new ChooseConditionGridAdapter(getApplicationContext(), periodUnit);
            tvTitle.setText(getString(R.string.choose_time_unit));
        }

        gvCondition.setAdapter(adapter);
        gvCondition.setOnItemClickListener(this);


        dialog = new CommonTools(context).getProgressDialog(context, "请求数据中...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    private void getCompany() {
        if (companyList.size() <= 0) {
            getCompanyList();
            return;
        }

        List<String> companyString = new ArrayList<String>();
        for (int i = 0; i < companyList.size(); i++) {
            CompanyInfo companyInfo = companyList.get(i);

            companyString.add(companyInfo.COMPANY_NAME);// + "(" + carTypeInfo.LENGTH + "米)"
        }

        companyName = new String[companyString.size()];
        companyString.toArray(companyName);

        adapter = new ChooseConditionGridAdapter(getApplicationContext(), companyName);
        gvCondition.setAdapter(adapter);
        gvCondition.setOnItemClickListener(this);
    }

    private void getPark() {
        if (parkList.size() <= 0) {
            getParkList();
            return;
        }

        List<String> parkString = new ArrayList<String>();
        for (int i = 0; i < parkList.size(); i++) {
            ParkInfo parkInfo = parkList.get(i);

            parkString.add(parkInfo.PARK_NAME);// + "(" + carTypeInfo.LENGTH + "米)"
        }

        parkName = new String[parkString.size()];
        parkString.toArray(parkName);

        adapter = new ChooseConditionGridAdapter(getApplicationContext(), parkName);
        gvCondition.setAdapter(adapter);
        gvCondition.setOnItemClickListener(this);
    }

    private void selectCarType() {
        if (lists.size() <= 0) {
            sendRequest();
            return;
        }

        List<String> carTypeString = new ArrayList<String>();
        for (int i = 0; i < lists.size(); i++) {
            CdsCarTypeInfo carTypeInfo = lists.get(i);

            carTypeString.add(carTypeInfo.NAME);// + "(" + carTypeInfo.LENGTH + "米)"
        }

        carType = new String[carTypeString.size()];
        carTypeString.toArray(carType);

        adapter = new ChooseConditionGridAdapter(getApplicationContext(), carType);
        gvCondition.setAdapter(adapter);
        gvCondition.setOnItemClickListener(this);
    }

    @OnClick(value = {R.id.btnLeft})
    public void onClicked(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                Intent intent = new Intent();
                intent.putExtra("isChosen", false);
                setResult(RESULT_OK, intent);
                finish();
                break;

            default:
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        adapter.setClickId(arg2);
        adapter.notifyDataSetInvalidated();

        if (chooseType == ConditionType.TYPE_CAR_TYPE) {
            chosenItem = carType[arg2];
        } else if (chooseType == ConditionType.TYPE_PARK) {
            chosenItem = parkName[arg2];
        } else if (chooseType == ConditionType.TYPE_COMPANY) {
            chosenItem = companyName[arg2];
        } else if (chooseType == ConditionType.TYPE_CAR_LENGTH) {
            chosenItem = carLength[arg2];
        } else if (chooseType == ConditionType.TYPE_CAR_LOAD) {
            chosenItem = carLoad[arg2];
        } else if (chooseType == ConditionType.TYPE_GOOD_WEIGHT) {
            chosenItem = goodsWeight[arg2];
        } else if (chooseType == ConditionType.TYPE_TRANSPORT_WAY) {
            chosenItem = transportWay[arg2];
        } else if (chooseType == ConditionType.TYPE_GOODS_TYPE) {
            chosenItem = goodsType[arg2];
        } else if (chooseType == ConditionType.TYPE_CARS_NUMBER) {
            chosenItem = carNumber[arg2];
        } else if (chooseType == ConditionType.TYPE_MIN_EFFECTIVE_DAY || chooseType == ConditionType.TYPE_MAX_EFFECTIVE_DAY) {
            chosenItem = effectiveDay[arg2];
        } else if (chooseType == ConditionType.TYPE_NUMBER_HEAD) {
            chosenItem = numberHead[arg2];
        } else if (chooseType == ConditionType.TYPE_CAR_FROM) {
            chosenItem = carFrom[arg2];
        } else if (chooseType == ConditionType.TYPE_DRIVER) {
            chosenItem = driverName[arg2];
        } else if (chooseType == ConditionType.TYPE_FREQUENCY_UNIT) {
            chosenItem = frequencyUnit[arg2];
        } else if (chooseType == ConditionType.TYPE_PERIOD_UNIT) {
            chosenItem = periodUnit[arg2];
        }

        if (chooseType == ConditionType.TYPE_CAR_TYPE) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable("carTypeInfo", lists.get(arg2));
            intent.putExtras(bundle);
            intent.putExtra("chosenItem", chosenItem);
            intent.putExtra("itemIndex", arg2);
            intent.putExtra("isChosen", true);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else if (chooseType == ConditionType.TYPE_PARK) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable("parkInfo", parkList.get(arg2));
            intent.putExtras(bundle);
            intent.putExtra("chosenItem", chosenItem);
            intent.putExtra("itemIndex", arg2);
            intent.putExtra("isChosen", true);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else if (chooseType == ConditionType.TYPE_COMPANY) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable("companyInfo", companyList.get(arg2));
            intent.putExtras(bundle);
            intent.putExtra("chosenItem", chosenItem);
            intent.putExtra("itemIndex", arg2);
            intent.putExtra("isChosen", true);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            // 结束时把选中的数据返回到之前的页面
            Intent intent = new Intent();
            intent.putExtra("chosenItem", chosenItem);
            intent.putExtra("itemIndex", arg2);
            intent.putExtra("isChosen", true);
            setResult(RESULT_OK, intent);
            finish();
        }

    }

    /**
     * 查询车型信息
     */
    private void sendRequest() {
        RequestParams params = new RequestParams();
        String url = getString(R.string.server_url) + URLMap.GET_CAR_TYPE_LIST;
        LogUtils.i(url + CommonTools.getParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, null, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    dialog.dismiss();
                    JSONObject obj = new JSONObject(result.result);
                    String status = null;
                    status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        JSONArray array = obj.getJSONArray("cdsCarTypeList");
                        JSONObject data = null;
                        for (int i = 0; i < array.length(); i++) {
                            CdsCarTypeInfo carTypeInfo = new CdsCarTypeInfo();
                            data = array.getJSONObject(i);

                            carTypeInfo.NAME = CommonTools.judgeNull(data, "NAME", "");
                            carTypeInfo.CREATE_TIME = CommonTools.judgeNull(data, "CREATE_TIME", "");
                            carTypeInfo.REMARKS = CommonTools.judgeNull(data, "REMARKS", "");
                            carTypeInfo.HEIGHT = CommonTools.judgeNull(data, "HEIGHT", "");
                            carTypeInfo.IS_DELETE = CommonTools.judgeNull(data, "IS_DELETE", "");
                            carTypeInfo.UPDATE_TIME = CommonTools.judgeNull(data, "UPDATE_TIME", "");
                            carTypeInfo.WIDTH = CommonTools.judgeNull(data, "WIDTH", "");
                            carTypeInfo.ICON = CommonTools.judgeNull(data, "ICON", "");
                            carTypeInfo.SORT = CommonTools.judgeNull(data, "SORT", "");
                            carTypeInfo.WEIGHT = CommonTools.judgeNull(data, "WEIGHT", "");
                            carTypeInfo.VOLUMN = CommonTools.judgeNull(data, "VOLUMN", "");
                            carTypeInfo.WEIGHT_UNIT = CommonTools.judgeNull(data, "WEIGHT_UNIT", "");
                            carTypeInfo.BRAND = CommonTools.judgeNull(data, "BRAND", "");
                            carTypeInfo.LENGTH = CommonTools.judgeNull(data, "LENGTH", "");
                            carTypeInfo.IS_FORBID = CommonTools.judgeNull(data, "IS_FORBID", "");
                            carTypeInfo.CAR_TYPE_ID = CommonTools.judgeNull(data, "CAR_TYPE_ID", "");

                            lists.add(carTypeInfo);
                        }
                        if (lists.size() > 0) {
                            selectCarType();
                        }
                    } else if ("-1".equals(status)) {
                        message = "服务器异常";
                    }
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }

            @Override
            public void onCancelled() {
                Toast.makeText(context, "cancle", Toast.LENGTH_SHORT).show();
            }

        });
    }

    /**
     * 查询园区列表信息
     */
    private void getParkList(){
        String url = getString(R.string.server_url) + URLMap.GET_PARK_LIST;
        org.xutils.http.RequestParams params = new org.xutils.http.RequestParams(url);

        NetworkCallBack callBack = new NetworkCallBack(this);
        callBack.networkRequest(params, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        JSONArray array = obj.getJSONArray("data");
                        JSONObject data = null;
                        for (int i = 0; i < array.length(); i++) {
                            ParkInfo parkInfo = new ParkInfo();
                            data = array.getJSONObject(i);

                            parkInfo.PARK_NO = CommonTools.judgeNull(data, "PARK_NO", "");
                            parkInfo.PARK_NAME = CommonTools.judgeNull(data, "PARK_NAME", "");
                            parkInfo.GROUP_NAME = CommonTools.judgeNull(data, "GROUP_NAME", "");
                            parkInfo.LOGO_NAME = CommonTools.judgeNull(data, "LOGO_NAME", "");
                            parkInfo.CAPITAL = CommonTools.judgeNull(data, "CAPITAL", "");
                            parkInfo.DOWNTOWN = CommonTools.judgeNull(data, "DOWNTOWN", "");
                            parkInfo.HASCOMPANY_NUM = CommonTools.judgeNull(data, "HASCOMPANY_NUM", "0");
                            parkInfo.HASLINE_NUM = CommonTools.judgeNull(data, "HASLINE_NUM", "0");
                            parkInfo.LINK_MAN = CommonTools.judgeNull(data, "LINK_MAN", "");
                            parkInfo.LINK_MOBILE = CommonTools.judgeNull(data, "LINK_MOBILE", "");
                            parkInfo.LINK_TELEPHONE = CommonTools.judgeNull(data, "LINK_TELEPHONE", "");
                            parkInfo.LINK_QQ = CommonTools.judgeNull(data, "LINK_QQ", "");
                            parkInfo.LINK_FAX = CommonTools.judgeNull(data, "LINK_FAX", "");

                            parkList.add(parkInfo);
                        }
                        if (parkList.size() > 0) {
                            getPark();
                        }
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * 查询企业列表信息
     */
    private void getCompanyList(){
        String url = getString(R.string.server_url) + URLMap.GET_COMPANY_LIST;
        org.xutils.http.RequestParams params = new org.xutils.http.RequestParams(url);

        NetworkCallBack callBack = new NetworkCallBack(this);
        callBack.networkRequest(params, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        JSONArray array = obj.getJSONArray("data");
                        JSONObject data = null;
                        for (int i = 0; i < array.length(); i++) {
                            CompanyInfo companyInfo = new CompanyInfo();
                            data = array.getJSONObject(i);

                            companyInfo.COMPANY_ID = CommonTools.judgeNull(data, "COMPANY_ID", "");
                            companyInfo.COMPANY_NO = CommonTools.judgeNull(data, "COMPANY_NO", "");
                            companyInfo.COMPANY_NAME = CommonTools.judgeNull(data, "COMPANY_NAME", "");
                            companyInfo.LOGO_NAME = CommonTools.judgeNull(data, "LOGO_NAME", "");
                            companyInfo.COMPANY_PROVINCE = CommonTools.judgeNull(data, "COMPANY_PROVINCE", "");
                            companyInfo.COMPANY_CITY = CommonTools.judgeNull(data, "COMPANY_CITY", "");
                            companyInfo.COMPANY_ADDRESS = CommonTools.judgeNull(data, "COMPANY_ADDRESS", "");
                            companyInfo.COMPANY_INTRODUCTION = CommonTools.judgeNull(data, "COMPANY_INTRODUCTION", "0");
                            companyInfo.LINK_MAN = CommonTools.judgeNull(data, "LINK_MAN", "");
                            companyInfo.LINK_MOBILE = CommonTools.judgeNull(data, "LINK_MOBILE", "");
                            companyInfo.LINK_TELEPHONE = CommonTools.judgeNull(data, "LINK_TELEPHONE", "");
                            companyInfo.LINK_QQ = CommonTools.judgeNull(data, "LINK_QQ", "");
                            companyInfo.STATUS = CommonTools.judgeNull(data, "STATUS", "");
                            companyList.add(companyInfo);
                        }
                        if (companyList.size() > 0) {
                            getCompany();
                        }
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
