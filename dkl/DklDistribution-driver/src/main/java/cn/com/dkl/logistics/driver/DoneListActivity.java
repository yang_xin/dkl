package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tencent.stat.StatService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.adapter.OrderListAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.DeliverBean;
import cn.com.dkl.logistics.entity.TstOrderBean;
import cn.com.dkl.logistics.fragment.ArroundCarFragment;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.Constant;
import cn.com.dkl.logistics.view.CustomProgressDialog;


/**
 * 待运输订单列表
 *
 * @author txw
 */
@SuppressLint("NewApi")
public class DoneListActivity extends FragmentActivity {

    private static final String TAG = "DoneListActivity";
    @ViewInject(R.id.ivLeft)
    private ImageView ivLeft;
    @ViewInject(R.id.rbTransaction)
    private RadioButton rbTransaction;
    @ViewInject(R.id.rbFinished)
    private RadioButton rbFinished;
    @ViewInject(R.id.ptrListView)
    private PullToRefreshListView ptrListView;

    private Context context = DoneListActivity.this;
    private SharedPreferences sp;
    private int currentPage = 1;
    private int pageCount = 0;
    static TstOrderBean mOrderBean; //配送点列表

    /**
     * 一页显示数量
     */
    private static final int SHOW_COUNT = 10;
    /**
     * 1买家，2卖家
     */
    private static final String DATA_TYPE = "1";
    /**
     * 已完成订单
     */
    private final String FINISHED = "1";
    /***
     * 交易中订单
     */
    private final String UNFINISHED = "2";
    /**
     * 订单完成状态
     */
    private String isFinished = UNFINISHED;

    private List<Map<String, String>> listOrder = new ArrayList<Map<String, String>>();
    private OrderListAdapter adapter;
    private CustomProgressDialog dialog = null;
    private boolean flag = true;
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_list);
        IntentFilter filter = new IntentFilter();
        filter.addAction("finish");
        filter.addAction("seveLocation");
        registerReceiver(receiver, filter);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        StatService.onResume(this);
        FlowerCollector.onResume(this);
        currentPage = 1;
        listOrder.clear();
        adapter.notifyDataSetChanged();
        sendRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        StatService.onPause(this);
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("finish")) {
                finish();
            }
        }
    };

    private void init() {
        ViewUtils.inject(this);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        dialog = new CommonTools(context).getProgressDialog(context, "正在为您获取配送计划列表...");
        initListView();
    }

    private void initListView() {
        adapter = new OrderListAdapter(context, listOrder);
        ptrListView.setAdapter(adapter);
        mIntent = new Intent(DoneListActivity.this, MainActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        ptrListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String endddress = "";
                String endalias = listOrder.get(position - 1).get("TOADDRALIAS");
                if (!endalias.equals("") && endalias != null) {
                    endddress = endalias;
                } else {
                    endddress = listOrder.get(position - 1).get("TOADDRESS");
                }
                String orderOn = listOrder.get(position - 1).get("WAYBILL_NUMBER");
                getOrderDetail(orderOn, endddress);
                SaveLocation(orderOn,"");

            }
        });
        ptrListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    sendRequest();
                }
            }

        });
        ptrListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (currentPage <= pageCount) {
                    if (flag) {
                        flag = false;
                        sendRequest();
                    }
                } else {
                    Toast.makeText(context, R.string.already_no_data, Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    @OnClick({R.id.ivLeft, R.id.rbTransaction, R.id.rbFinished})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft:
                finish();
                break;
            case R.id.rbTransaction: // 交易订单
                if (flag) {
                    flag = false;
                    isFinished = UNFINISHED;
                    currentPage = 1;
                    listOrder.clear();
                    adapter.notifyDataSetChanged();
                    sendRequest();
                }
                break;
    /*	case R.id.rbFinished: // 已完成订单
            if (flag) {
				flag = false;
				isFinished = FINISHED;
				currentPage = 1;
				listOrder.clear();
				adapter.notifyDataSetChanged();
				sendRequest();
			}
			break;*/
            default:
                break;
        }
    }

    ResponseInfo<String> setTestData(ResponseInfo<String> arg0) {
        if (arg0 == null) {
            Toast.makeText(context, "ResponseInfo 为 null", Toast.LENGTH_SHORT).show();
            return null;
        }
        arg0.result = getString(R.string.testData);
        return arg0;
    }

    /**
     * 请求数据
     */
    private void sendRequest() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("CURRENTPAGE", String.valueOf(currentPage));
        params.addQueryStringParameter("SHOWCOUNT", String.valueOf(SHOW_COUNT));
        params.addQueryStringParameter("DATATYPE", DATA_TYPE);
        params.addQueryStringParameter("ISPLAN", "1");
        params.addQueryStringParameter("ISFINISH", UNFINISHED);

        String url = getString(R.string.server_url) + URLMap.GET_CDSORDER_LIST;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                flag = true;
                dialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                flag = true;
                dialog.dismiss();
                try {
//					setTestData(arg0);
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        if (ptrListView.isRefreshing()) { // 刷新显示中
                            ptrListView.onRefreshComplete();
                            listOrder.clear();
                        }
//                        pageCount = obj.getInt("PAGECOUNT");

                        JSONArray array = obj.getJSONArray("data");
                        Map<String, String> order = null;
                        JSONObject data = null;
                        for (int i = 0; i < array.length(); i++) {
                            order = new HashMap<String, String>();
                            data = array.getJSONObject(i);
                            Iterator<String> it = data.keys();
                            while (it.hasNext()) {
                                String key = it.next();
                                String value = CommonTools.judgeNull(data, key, "");
                                order.put(key, value);
                            }
                            JSONArray orderArray = new JSONArray(order.get("orderList"));
                            JSONObject orderObject = orderArray.getJSONObject(0);
                            order.put("is_urgent", CommonTools.judgeNull(orderObject, "is_urgent", ""));
                            JSONArray jArray = new JSONArray(order.get("deliverList"));
                            JSONObject object = jArray.getJSONObject(0);
                            order.put("FROMPROVINCE", CommonTools.judgeNull(object, "PROVINCE", ""));
                            order.put("FROMCITY", CommonTools.judgeNull(object, "CITY", ""));
                            order.put("FROMDISTRICT", CommonTools.judgeNull(object, "DISTRICT", ""));
                            order.put("FROMADDRALIAS", CommonTools.judgeNull(object, "CONTACT_ADDRESS", ""));
                            JSONObject jsonObject = jArray.getJSONObject(jArray.length() - 1);
                            order.put("TOPROVINCE", CommonTools.judgeNull(jsonObject, "PROVINCE", ""));
                            order.put("TOCITY", CommonTools.judgeNull(jsonObject, "CITY", ""));
                            order.put("TODISTRICT", CommonTools.judgeNull(jsonObject, "DISTRICT", ""));
                            order.put("TOADDRALIAS", CommonTools.judgeNull(jsonObject, "CONTACT_ADDRESS", ""));
                            listOrder.add(order);
//                            if (!order.get("ORDER_STATUS").equals("-7")) {
//                            }
                        }
                        if (listOrder.size() > 0) {
                            adapter.notifyDataSetChanged();
                        }
                        // 当前页+1
                        currentPage += 1;
                    } else {
                        Toast.makeText(context, "获取订单失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }


    /**
     * 获取订单详情
     */
    private void getOrderDetail(String orderNo, final String endddress) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
        String url = context.getResources().getString(R.string.server_url) + URLMap.CDSORDER_DETAIL;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    Log.d(TAG, "onSuccess: " + arg0.result);
                    JSONObject obj = new JSONObject(arg0.result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    JSONObject data = obj.getJSONObject("data");
                    LogUtils.i(data.toString());
                    if ("1".equals(status)) {

                        mOrderBean = getDeverList(data);

                        if (mOrderBean == null) {
                            Toast.makeText(context, "此运单没有起点或终点，请检查后再试！", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        ArroundCarFragment.mOrderStatus2 = mOrderBean.getORDER_STATUS();
                        mIntent.putExtra("endddress", endddress);
                        mIntent.putExtra("flag", Constant.WORKORDER);
                        mIntent.putExtra("data", mOrderBean);
                        setResult(RESULT_OK, mIntent);
                        automaticWorkBroadcast(mIntent, endddress);

                        finish();
                    } else {
                        Toast.makeText(context, "获取信息失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }
        });
    }


    /**
     * 获取所有的配送点
     */
    public TstOrderBean getDeverList(JSONObject data) {
        TstOrderBean orderBean = new TstOrderBean();
        orderBean.setORDER_STATUS(CommonTools.judgeNull(data, "STATUS", ""));
        orderBean.setORDERNO(CommonTools.judgeNull(data, "WAYBILL_NUMBER", ""));
        List<DeliverBean> deliverList = new ArrayList<>();
        JSONArray jArray = null;
        try {
            jArray = data.getJSONArray("deliverList");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (jArray == null) {
            Toast.makeText(context, "没有找到起点或终点！", Toast.LENGTH_SHORT).show();
            return null;
        }
        for (int i = 0; i < jArray.length(); i++) {
            DeliverBean deliver = new DeliverBean();
            JSONObject jObject = null;
            try {
                jObject = jArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (jObject == null) {
                Toast.makeText(context, "没有找到起点或终点！", Toast.LENGTH_SHORT).show();
                return null;
            }
            deliver.setPROVINCE(CommonTools.judgeNull(jObject, "PROVINCE", ""));
            deliver.setCITY(CommonTools.judgeNull(jObject, "CITY", ""));
            deliver.setDISTRICT(CommonTools.judgeNull(jObject, "DISTRICT", ""));
            deliver.setCONTACT_ADDRESS(CommonTools.judgeNull(jObject, "CONTACT_ADDRESS", ""));
            deliver.setLATITUDE(CommonTools.judgeNull(jObject, "LATITUDE", ""));
            deliver.setLONGITUDE(CommonTools.judgeNull(jObject, "LONGITUDE", ""));
            deliverList.add(deliver);
        }
        orderBean.setDeliverList(deliverList);
        return orderBean;
    }

    /**
     * 设置工作单
     */
    void automaticWorkBroadcast(Intent intent, String endddress) {
        int requestCode = getIntent().getIntExtra("requestCode", 0);
        if (requestCode == ArroundCarFragment.DoneList) {
            return;
        }
        getApplicationContext().sendBroadcast(new Intent("exitmynews"));
        Intent it = new Intent(Constant.AUTOMATICBROADCAST);
        it.putExtra("endddress", endddress);
        it.putExtra("flag", Constant.WORKORDER);
        it.putExtra("data", mOrderBean);
        sendBroadcast(it);
    }


    /**
     * 上传位置
     * @param waybillnum 运单号
     * @param ordernum   订单号
     */
    public void SaveLocation(String waybillnum, String ordernum) {
        Intent intent = new Intent("SaveLocation");  //Itent就是我们要发送的内容
        intent.putExtra("WAYBILLNUMBER", waybillnum);
        intent.putExtra("ORDERNUMBER", ordernum);
        context.sendBroadcast(intent);   //发送广播
    }


}
