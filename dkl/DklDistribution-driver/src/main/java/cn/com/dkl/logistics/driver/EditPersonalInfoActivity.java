package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.RoleType;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.view.CircleImageView;
import cn.com.dkl.logistics.view.CustomProgressDialog;

/**
 * 编辑个人信息
 *
 * @author txw
 */
public class EditPersonalInfoActivity extends BaseActivity {
    private Context context = EditPersonalInfoActivity.this;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.civHeadImg)
    private CircleImageView civHeadImg;
    @ViewInject(R.id.btnChangeHeadImg)
    private Button btnChangeHeadImg;
    @ViewInject(R.id.etUserName)
    private EditText etUserName;
    @ViewInject(R.id.rgSex)
    private RadioGroup rgSex;
    @ViewInject(R.id.rbMan)
    private RadioButton rbMan;
    @ViewInject(R.id.rbWoman)
    private RadioButton rbWoman;
    @ViewInject(R.id.etTelephone)
    private EditText etTelephone;
    @ViewInject(R.id.tvParkName)
    private TextView tvParkName;
    @ViewInject(R.id.llCompanyName)
    private LinearLayout llCompanyName;
    @ViewInject(R.id.tvCompanyName)
    private TextView tvCompanyName;
    @ViewInject(R.id.etContactEmail)
    private EditText etContactEmail;
    @ViewInject(R.id.etContactQQ)
    private EditText etContactQQ;
    @ViewInject(R.id.etRegisterTime)
    private EditText etRegisterTime;
    private SharedPreferences sp;
    private String userID = null;
    private String userName = null;
    private String companyId = null;
    private String parkNo = null;
    private String parkName = null;
    private String phone = null;
    private String password = null;
    private String sex = null;
    private String contactTelephone = null;
    private String companyName = null;
    private String contactEmail = null;
    private String contactQQ = null;
    private String headImageName = null;
    private CustomProgressDialog dialog;
    private String roleID;
    private long lastClickTime = 0;
    private ImageChooseDialogWindow menuWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_personalinfo);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ImageUtils.deleteImage(ImageUtils.picPath, ImageUtils.uploadImageName);
        ImageUtils.picPath = null; // 清空图片链接
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText(R.string.title_edit_personalinfo);
        dialog = new CommonTools(context).getProgressDialog(context, "正在保存...");
        dialog.setCanceledOnTouchOutside(false);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        headImageName = sp.getString(UserEntity.HEAD_PHOTO_URL, "");
        if (TextUtils.isEmpty(headImageName)) {
            civHeadImg.setImageResource(R.drawable.img_default_head_image);
        } else {
            ImageUtils.downLoadImage(context, civHeadImg, headImageName);
        }

        userID = sp.getString(UserEntity.USER_ID, null);
        phone = sp.getString(UserEntity.PHONE, null);
        password = sp.getString(UserEntity.PASSWORD, null);
        roleID = sp.getString(UserEntity.ROLE_ID, null);
        userName = sp.getString(UserEntity.USERNAME, "");
        parkNo = sp.getString(UserEntity.PARK_NO, "");
        parkName = sp.getString(UserEntity.PARK_NAME, "");
        companyId = sp.getString(UserEntity.COMPANY_ID, "");
        companyName = sp.getString(UserEntity.COMPANY_NAME, "");
        sex = sp.getString(UserEntity.SEX, null);
        contactTelephone = sp.getString(UserEntity.CONTACT_TELEPHONE, "");
        contactEmail = sp.getString(UserEntity.CONTACT_EMAIL, "");
        contactQQ = sp.getString(UserEntity.CONTACT_QQ, "");
        String registerTime = sp.getString(UserEntity.CREATE_TIME, "");
        etTelephone.setText(sp.getString(UserEntity.PHONE, ""));
        //if (TextUtils.isEmpty(userName)) { // 当用户名为空才可编辑
        etUserName.setEnabled(true);
        etUserName.requestFocus();
        //}
        if ("1".equals(sex)) {
            rbMan.setChecked(true);
        } else if ("0".equals(sex)) {
            rbWoman.setChecked(true);
        } else {
            rbMan.setChecked(true);
        }
        if (!TextUtils.isEmpty(registerTime)) {
            etRegisterTime.setText(registerTime.substring(0, 10).replace("-", "/"));
        }
        if (!TextUtils.isEmpty(userName)) {
            etUserName.setText(userName);
        }
        if (!TextUtils.isEmpty(parkName)) {
            tvParkName.setText(parkName);
        }
        if (!TextUtils.isEmpty(companyName)) {
            tvCompanyName.setText(companyName);
        }
        if (!TextUtils.isEmpty(contactEmail)) {
            etContactEmail.setText(contactEmail);
        }
        if (!TextUtils.isEmpty(contactQQ)) {
            etContactQQ.setText(contactQQ);
        }
    }

    @OnClick({R.id.btnLeft, R.id.btnChangeHeadImg, R.id.btnSaveEdit})
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - lastClickTime > CommonTools.MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime;
            switch (v.getId()) {
                case R.id.btnLeft:
                    finish();
                    break;
                case R.id.btnChangeHeadImg: // 更换头像
                    menuWindow = new ImageChooseDialogWindow(this);
                    menuWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    menuWindow.showAtLocation(this.findViewById(R.id.editLinear), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    break;
                case R.id.btnSaveEdit: // 保存修改
                    saveEdit();
                    break;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void saveEdit() {
        userName = etUserName.getText().toString().trim();
        sex = (R.id.rbMan == rgSex.getCheckedRadioButtonId() ? "1" : "0");
        contactTelephone = etTelephone.getText().toString().trim();
        companyName = tvCompanyName.getText().toString().trim();
        contactEmail = etContactEmail.getText().toString().trim();
        contactQQ = etContactQQ.getText().toString().trim();
        if (TextUtils.isEmpty(userName)) {
            Toast.makeText(context, "请输入用户名！", Toast.LENGTH_SHORT).show();
            etUserName.requestFocus();
            return;
        } else if (TextUtils.isEmpty(sex)) {
            Toast.makeText(context, "请选择性别！", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!TextUtils.isEmpty(contactEmail)) {
            String reg = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
            if (!CommonTools.isCorrect(reg, contactEmail)) {
                Toast.makeText(context, "联系邮箱输入有误", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (!TextUtils.isEmpty(contactTelephone)) {
            if (!CommonTools.checkPhoneNumber(contactTelephone) && !CommonTools.checkTelephoneNum(contactTelephone)) {
                Toast.makeText(context, "电话号码输入有误!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (RoleType.LOGISTICS_ENTERPRISE.equals(roleID)) {
            if (TextUtils.isEmpty(companyName)) {
                Toast.makeText(context, "请输入公司名称！", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        dialog.show();
        if (TextUtils.isEmpty(ImageUtils.picPath)) { // 图片路径为空，代表没选择头像
            sendRequest(headImageName);
        } else {
            doUpload();
        }
    }

    /**
     * 上传头像图片
     */
    private void doUpload() {
        final RequestParams params = new RequestParams();
        File file = new File(ImageUtils.picPath);
        if (file.exists()) {
            params.addBodyParameter("UserHeadImage", file);
            String url = getString(R.string.server_url) + URLMap.UPLOAD_IMAGE;

            HttpUtils http = new HttpUtils(60 * 1000);
            http.configCurrentHttpCacheExpiry(1000 * 10);
            http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

                @Override
                public void onSuccess(ResponseInfo<String> result) {
                    try {
                        LogUtils.i("result= " + result.result);
                        JSONObject obj = new JSONObject(result.result);
                        String message = obj.getString("message");
                        String status = obj.getString("status");
                        if ("1".equals(status)) {
                            JSONArray datas = obj.getJSONArray("data");
                            String imgFileName = null;
                            for (int i = 0; i < datas.length(); i++) {
                                JSONObject data = datas.getJSONObject(i);
                                imgFileName = CommonTools.judgeNull(data, "FILENAME", "");
                            }
                            sendRequest(imgFileName);
                        } else if ("0".equals(status)) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        } else if ("-1".equals(status)) {
                            Toast.makeText(context, "服务器异常", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        dialog.dismiss();
                        Toast.makeText(context, "图片上传失败！", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(HttpException arg0, String arg1) {
                    dialog.dismiss();
                    Toast.makeText(context, "图片上传失败！", Toast.LENGTH_SHORT).show();
                }

            });
        }

    }

    private void sendRequest(String imgFileName) {
        RequestParams params = new RequestParams();
        userID = sp.getString(UserEntity.USER_ID, null);
        params.addQueryStringParameter("USER_ID", userID);
        params.addQueryStringParameter("PASSWORD", password);
        params.addQueryStringParameter("PHONE", phone);
        params.addQueryStringParameter("USERNAME", phone);
        params.addQueryStringParameter("REALUSERNAME", userName);
        params.addQueryStringParameter("SEX", sex);
//        params.addQueryStringParameter("COMPANYNAME", companyName);
        params.addQueryStringParameter("TELEPHONE", contactTelephone);
        params.addQueryStringParameter("EMAIL", contactEmail);
        params.addQueryStringParameter("QQ", contactQQ);
        params.addQueryStringParameter("HEADPHOTOURL", imgFileName);

        String url = getString(R.string.server_url) + URLMap.EDIT_PERSONAL_INFO;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    dialog.dismiss();
                    JSONObject obj = new JSONObject(result.result);
                    String status = obj.getString("status");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        JSONObject data = obj.getJSONObject("data");
                        Editor edit = sp.edit();
                        edit.putString(UserEntity.PHONE, CommonTools.judgeNull(data, "Phone", ""));
                        edit.putString(UserEntity.CONTACT_TELEPHONE, CommonTools.judgeNull(data, "Telephone", ""));
                        edit.putString(UserEntity.COMPANY_NAME, CommonTools.judgeNull(data, "CompanyName", ""));
                        edit.putString(UserEntity.NAME, CommonTools.judgeNull(data, "Name", ""));
                        edit.putString(UserEntity.USERNAME, CommonTools.judgeNull(data, "username", ""));
                        edit.putString(UserEntity.CONTACT_EMAIL, CommonTools.judgeNull(data, "Email", ""));
                        edit.putString(UserEntity.WEB_BALANCE, CommonTools.judgeNull(data, "WebBalance", ""));
                        edit.putString(UserEntity.CONTACT_QQ, CommonTools.judgeNull(data, "QQ", "QQ"));
                        edit.putString(UserEntity.GUARANTEE_MONEY, CommonTools.judgeNull(data, "GuaranteeMoney", ""));
                        edit.putString(UserEntity.ORDINARY_POINT_LEVEL, CommonTools.judgeNull(data, "OrdinaryPointLevel", ""));
                        edit.putString(UserEntity.POINT_LEVEL, CommonTools.judgeNull(data, "PointLevel", ""));
                        edit.putString(UserEntity.USER_ID, CommonTools.judgeNull(data, "Uid", ""));
                        edit.putString(UserEntity.POINT, CommonTools.judgeNull(data, "Point", ""));
                        edit.putString(UserEntity.SEX, CommonTools.judgeNull(data, "Sex", ""));
                        edit.putString(UserEntity.HEAD_PHOTO_URL, CommonTools.judgeNull(data, "HeadPhotoUrl", ""));
                        edit.commit();

                        Toast.makeText(context, "修改信息成功！", Toast.LENGTH_SHORT).show();
                        SystemClock.sleep(1000);
//                        sendBroadcast(new Intent("cn.com.dkl.logistics.driver.MainDataUpdate"));
                        finish();
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    dialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }
        });
    }

    protected void downLoadHeadImg(final String imageName) {
        if (TextUtils.isEmpty(headImageName)) {
            return;
        }
        String url = getString(R.string.server_imgurl) + "upload/" + headImageName;
        //String url = getString(R.string.server_url) + "upload/" + headImageName;
        final String imagePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + headImageName;
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.download(url, imagePath, true, true, new RequestCallBack<File>() { // true1代表允许断点续传，true2//
            // 如果从请求返回信息中获取到文件名，下载完成后自动重命名。

            @Override
            public void onSuccess(ResponseInfo<File> arg0) {
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                if (null != bitmap) {
                    civHeadImg.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.e("下载用户头像失败：" + arg0.getMessage() + arg1);
            }
        });
    }

    @SuppressLint({"NewApi"})
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImageUtils.isHeadImage = true;
        ImageUtils.uploadImageName = "IMG_UserHead";
        switch (requestCode) {
            case ImageUtils.GET_IMAGE_BY_CAMERA: // 拍照获取图片
                if (null != ImageUtils.imageUriFromCamera) {
                    ImageUtils.uriToPath(this, ImageUtils.imageUriFromCamera);
                    setImageInfo();
                }
                break;
            case ImageUtils.GET_IMAGE_FROM_PHONE: // 手机相册获取图片
                if (null != data && null != data.getData()) {
                    ImageUtils.uriToPath(this, data.getData());
                    setImageInfo();
                }
                break;
            case ImageUtils.CROP_IMAGE: // 裁剪图片后结果
                if (null != ImageUtils.cropImageUri && null != data) { // 可以直接显示图片,或者进行其他处理(如压缩等)
                    setImageInfo();
                } else { // 取消裁剪
                    ImageUtils.picPath = null;
                }
                break;
        }
    }

    private void setImageInfo() {
        civHeadImg.setImageBitmap(BitmapFactory.decodeFile(ImageUtils.picPath));
    }

}
