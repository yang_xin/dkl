package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 异常上报页面
 * Created by magic on 2017/9/27.
 */

public class ErrorUploadActivity extends BaseActivity {
    Context context;
    @Bind(R.id.btnLeft)
    Button btnLeft;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.editText)
    EditText editText;
    @Bind(R.id.btnSubmit)
    Button btnSubmit;

    String strErorr = "";//异常信息
    private MyTextWatcher textWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.error_upload_activity);
        ButterKnife.bind(this);
        init();
    }

    void init(){
        tvTitle.setText(R.string.error_title);
        setListener();
    }

    private void setListener() {
        textWatcher = new MyTextWatcher();
        editText.addTextChangedListener(textWatcher);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.btnLeft, R.id.btnSubmit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnSubmit:
                break;
        }
    }

    private class MyTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable arg0) {
            strErorr = editText.getText().toString().trim();
            if (TextUtils.isEmpty(strErorr)) {
                btnSubmit.setEnabled(false);
                btnSubmit.setBackgroundResource(R.drawable.btn_gray_shape);
            } else {
                btnSubmit.setEnabled(true);
                btnSubmit.setBackgroundResource(R.drawable.background_deep_orange_selector_corner);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

    }
}
