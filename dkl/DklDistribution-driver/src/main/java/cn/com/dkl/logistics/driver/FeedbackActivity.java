package cn.com.dkl.logistics.driver;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONObject;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;

/**
 * 意见反馈
 *
 * @author hzq
 */
public class FeedbackActivity extends BaseActivity {
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;

    @ViewInject(R.id.etFeedback)
    private EditText etFeedback;

    @ViewInject(R.id.btnSubmitFeedback)
    private Button btnSubmitFeedback;

    @ViewInject(R.id.btnRight)
    private Button btnRight;

    private Context context = FeedbackActivity.this;
    private MyTextWatcher textWatcher;
    private String feedback;
    private Dialog progressDialog;
    private SharedPreferences sp;
    private CommonTools tools = new CommonTools(context);
    private String callBackCode = "";
    private String callBackMessage = "";
    private String callBackState = "";
    // 0 运起来 1 同城配运司机版 2 同城配运货主版
    private final int versonType = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        init();
        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText(R.string.feedback);
        btnRight.setText("提交");
        btnRight.setVisibility(View.INVISIBLE);
        btnRight.setTextColor(context.getResources().getColor(R.color.white));
        textWatcher = new MyTextWatcher();
        progressDialog = tools.getProgressDialog(context, "提交中...");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
    }

    private void setListener() {
        etFeedback.addTextChangedListener(textWatcher);
    }

    @OnClick({R.id.btnLeft, R.id.btnRight, R.id.btnSubmitFeedback})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnRight:
            case R.id.btnSubmitFeedback:
                String feedBack = etFeedback.getText().toString().trim();
                if (TextUtils.isEmpty(feedBack)) {
                    Toast.makeText(context, "请填写意见!", Toast.LENGTH_SHORT).show();
                } else {
                    submitData();
                }
                break;
            default:
                break;
        }
    }

    private void submitData() {
        progressDialog.show();

        final RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("SCANTYPE", getString(R.string.SCANTYPE));
        params.addQueryStringParameter("CONTENT", feedback);
        params.addQueryStringParameter("VERSIONTYPE", String.valueOf(versonType));

        params.addQueryStringParameter("TYPE", "2");
        params.addQueryStringParameter("CLIENT", "1");
        params.addQueryStringParameter("TEXT_CONTENT", feedback);
        params.addQueryStringParameter("COMPLAINT", "");


        String url = getString(R.string.server_url) + URLMap.FEEDBACK;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);// 设置超时
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                JSONObject jsonObject = null;

                try {
                    if (TextUtils.isEmpty(responseInfo.result)) {
                        Toast.makeText(context, getString(R.string.server_connected_failed), Toast.LENGTH_SHORT);
                        return;
                    } else {
                        jsonObject = new JSONObject(responseInfo.result);
                        LogUtils.i("responseInfo= " + responseInfo.result);
                        callBackMessage = jsonObject.getString("message");
                        callBackState = jsonObject.getString("state");
                        callBackCode = jsonObject.getString("code");
                        LogUtils.i("callBackMessage= " + callBackMessage + "\n" + "callBackState= " + callBackState + "\n" + "callBackCode= " + callBackCode);

                        if (!"1".equals(callBackState)) {
                            Toast.makeText(context, getString(R.string.commit_failed), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "反馈成功，感谢您的建议!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                } catch (Exception e) {
                    Toast.makeText(context, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class MyTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable arg0) {
            feedback = etFeedback.getText().toString().trim();
            if (TextUtils.isEmpty(feedback)) {
                btnSubmitFeedback.setEnabled(false);
                btnSubmitFeedback.setBackgroundResource(R.drawable.btn_gray_shape);
            } else {
                btnSubmitFeedback.setEnabled(true);
                btnSubmitFeedback.setBackgroundResource(R.drawable.background_deep_orange_selector_corner);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

    }


}
