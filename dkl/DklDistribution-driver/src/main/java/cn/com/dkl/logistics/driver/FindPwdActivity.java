package cn.com.dkl.logistics.driver;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.EncryptTools;

/**
 * 找回密码
 *
 * @author txw
 */
public class FindPwdActivity extends BaseActivity {
    private Context context = FindPwdActivity.this;
    @ViewInject(R.id.btnLeft)
    private Button ivBack;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.etPhoneNumber)
    private EditText etPhoneNumber;
    @ViewInject(R.id.etSmsCode)
    private EditText etSmsCode;
    @ViewInject(R.id.etNewPwd)
    private EditText etNewPwd;
    @ViewInject(R.id.etConfirmPwd)
    private EditText etConfirmPwd;
    @ViewInject(R.id.btnGetSmsCode)
    private Button btnGetSmsCode;
    @ViewInject(R.id.btnSave)
    private Button btnSave;

    private SharedPreferences sp;
    private String forget = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pwd);
        ViewUtils.inject(this);
        forget = getIntent().getStringExtra("forget");
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        if (forget.equals("login")) {
            tvTitle.setText(R.string.title_find_pwd);
        } else if (forget.equals("payment")) {
            tvTitle.setText(R.string.title_find_pmwd);
            sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
            etPhoneNumber.setText(sp.getString(UserEntity.PHONE, ""));
            etPhoneNumber.setEnabled(false);
        }
    }

    @OnClick({R.id.btnLeft, R.id.btnGetSmsCode, R.id.btnSave})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft: // 返回
                finish();
                return;
            case R.id.btnGetSmsCode: // 获取验证码
                String phoneNumber = etPhoneNumber.getText().toString().trim();
                if (TextUtils.isEmpty(phoneNumber)) {
                    Toast.makeText(context, R.string.hint_input_phone_number, Toast.LENGTH_SHORT).show();
                } else {
                    boolean result = CommonTools.checkPhoneNumber(phoneNumber);
                    if (result) {
                        sendSmsCode(phoneNumber);
                    } else {
                        Toast.makeText(context, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.btnSave: // 提交修改密码
                resetPassword();
                break;
        }
    }

    /**
     * 重置密码
     */
    private void resetPassword() {
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String smsCode = etSmsCode.getText().toString().trim();
        String newPassword = etNewPwd.getText().toString().trim();
        String confirmPwd = etConfirmPwd.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNumber)) {
            Toast.makeText(context, R.string.hint_input_phone_number, Toast.LENGTH_SHORT).show();
            etPhoneNumber.requestFocus();
            return;
        } else if (!CommonTools.checkPhoneNumber(phoneNumber)) {
            Toast.makeText(context, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
            etPhoneNumber.requestFocus();
            return;
        } else if (TextUtils.isEmpty(smsCode)) {
            Toast.makeText(context, R.string.hint_input_smscode, Toast.LENGTH_SHORT).show();
            etSmsCode.requestFocus();
            return;
        } else if (6 != smsCode.length()) {
            Toast.makeText(context, "请输入正确的验证码", Toast.LENGTH_SHORT).show();
            etSmsCode.requestFocus();
            return;
        } else if (TextUtils.isEmpty(newPassword)) {
            Toast.makeText(context, R.string.hint_input_password, Toast.LENGTH_SHORT).show();
            etNewPwd.requestFocus();
            return;
        } else if (6 > newPassword.length()) {
            Toast.makeText(context, "密码长度不小于6位", Toast.LENGTH_SHORT).show();
            etNewPwd.requestFocus();
            return;
        } else if (TextUtils.isEmpty(confirmPwd)) {
            Toast.makeText(context, R.string.hint_input_confirm_pwd, Toast.LENGTH_SHORT).show();
            etConfirmPwd.requestFocus();
            return;
        } else if (!newPassword.equals(confirmPwd)) {
            Toast.makeText(context, "两次密码输入不一致", Toast.LENGTH_SHORT).show();
            etConfirmPwd.requestFocus();
            return;
        }

        sendRequest(phoneNumber, smsCode, EncryptTools.getBASE64Str(newPassword).toString(), EncryptTools.getBASE64Str(confirmPwd).toString());
    }

    private void sendRequest(String phoneNumber, String smsCode, String newPassword, String confirmPwd) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("PHONE", phoneNumber);
        params.addQueryStringParameter("VALIDCODE", smsCode);
        params.addQueryStringParameter("NEWPASSWORD", newPassword);
        params.addQueryStringParameter("CONFIRMNEWPASSWORD", confirmPwd);
        params.addQueryStringParameter("CLIENT", "2");

        String url = "";
        if (forget.equals("login")) {
            url = getString(R.string.server_url) + URLMap.FIND_PASSWORD;
        } else if (forget.equals("payment")) {
            url = getString(R.string.server_url) + URLMap.FIND_PAYMENT_PASSWORD;
        }

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            AlertDialog dialog = null;

            @Override
            public void onStart() {
                dialog = new AlertDialog.Builder(context).setMessage("密码重置中...").create();
                dialog.show();
                super.onStart();
            }

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(result.result);
                    String status = obj.getString("status");
                    String code = obj.getString("code");
                    String message = obj.getString("message");
                    if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("1".equals(status)) {
                        if ("1".equals(code)) {
                            Toast.makeText(context, "重置密码成功", Toast.LENGTH_SHORT).show();
                            SystemClock.sleep(1000);
                            finish();
                        }
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException e, String result) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }

        });
    }

    /**
     * 发送找回密码验证码
     *
     * @param phoneNumber 手机号码
     */
    private void sendSmsCode(String phoneNumber) {
        btnGetSmsCode.setClickable(false);
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("PHONE", phoneNumber);
        params.addQueryStringParameter("CLIENT", "2");

        String url = getString(R.string.server_url) + URLMap.FIND_PASSWORD_SMS_CODE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    String code = obj.getString("code");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        setButtonState(); // 更新短信发送按钮状态
                    } else if ("0".equals(status)) {
                        btnGetSmsCode.setClickable(true);
                        Toast.makeText(context, "验证码发送失败", Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        btnGetSmsCode.setClickable(true);
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                btnGetSmsCode.setClickable(true);
                CommonTools.failedToast(context);
            }
        });

    }

    /**
     * 设置发送短信按钮状态
     */
    private void setButtonState() {
        btnGetSmsCode.setClickable(false);
        final Timer timer = new Timer(); // 定时器对象
        TimerTask task = new TimerTask() {
            int i = 60;

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        i--;
                        btnGetSmsCode.setText(i + "秒");
                        if (i == 0) {
                            timer.cancel();
                            btnGetSmsCode.setText("点击重发");
                            btnGetSmsCode.setClickable(true);
                        }
                    }
                });
            }
        };
        timer.schedule(task, 0, 1000);
    }

}
