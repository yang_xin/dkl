package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.adapter.GrabSingleAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.NetworkCallBack;

public class GrabSingleActivity extends BaseActivity {
    private Context context = GrabSingleActivity.this;

    @Bind(R.id.btnLeft)
    Button btnLeft; //返回
    @Bind(R.id.tvTitle)
    TextView tvTitle; // 标题
    @Bind(R.id.ptrListView)
    PullToRefreshListView ptrListView;

    private SharedPreferences sp;
    private int currentPage = 1;
    private int SHOW_COUNT = 10;
    private int pageCount = 0;

    private List<Map<String, String>> listSingle = new ArrayList<Map<String, String>>();
    private GrabSingleAdapter adapter;
    private boolean flag = true;

    private AlertDeleteDialogwindow window;
    private int positionInt = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grab_single);
        init();
    }
    private void init(){
        ButterKnife.bind(this);
        tvTitle.setText("抢单列表");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        initListView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
        currentPage = 1;
        listSingle.clear();
        adapter.notifyDataSetChanged();
        sendRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initListView() {
        adapter = new GrabSingleAdapter(context, listSingle);
        ptrListView.setAdapter(adapter);

        ptrListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    sendRequest();
                }
            }

        });
        ptrListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (currentPage <= pageCount) {
                    if (flag) {
                        flag = false;
                        sendRequest();
                    }
                } else {
                    Toast.makeText(context, R.string.already_no_data, Toast.LENGTH_SHORT).show();
                }
            }

        });
        adapter.setGrabSingle(new GrabSingleAdapter.GrabSingle() {
            @Override
            public void grabSingle(int position) {
                positionInt = position;
                windows();
            }
        });
    }

    @OnClick(value = {R.id.btnLeft, R.id.btnRight})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            default:
                break;
        }
    }

    private void sendRequest() {
        final String url = getString(R.string.server_url) + URLMap.GET_GRABORDERLIST;
        final RequestParams params = new org.xutils.http.RequestParams(url);
        params.addBodyParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addBodyParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addBodyParameter("CURRENTPAGE", String.valueOf(currentPage));
        params.addBodyParameter("SHOWCOUNT", String.valueOf(SHOW_COUNT));

        NetworkCallBack callBack = new NetworkCallBack(this);
        Callback.Cancelable cancel = callBack.networkRequest(params, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                flag = true;
                try {
                    JSONObject obj = new JSONObject(result);
                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    pageCount = obj.getInt("PAGECOUNT");
                    if ("1".equals(state)) {
                        if (ptrListView.isRefreshing()) { // 刷新显示中
                            ptrListView.onRefreshComplete();
                            listSingle.clear();
                        }
                        JSONArray array = obj.getJSONArray("varList");
                        Map<String, String> sinale = null;
                        for (int i = 0; i < array.length(); i++) {
                            sinale = new HashMap<String, String>();
                            JSONObject object = array.getJSONObject(i);
                            sinale.put("ORDER_NO", CommonTools.judgeNull(object,"ORDER_NO",""));
                            sinale.put("ORDER_STATUS", CommonTools.judgeNull(object, "ORDER_STATUS", ""));
                            sinale.put("SENDSTATUS", CommonTools.judgeNull(object, "SENDSTATUS", ""));
                            sinale.put("CREATE_TIME",CommonTools.judgeNull(object,"CREATE_TIME",""));
                            sinale.put("FROMPROVINCE", CommonTools.judgeNull(object, "F_PROVINCE", ""));
                            sinale.put("FROMCITY", CommonTools.judgeNull(object, "F_CITY", ""));
                            sinale.put("FROMDISTRICT", CommonTools.judgeNull(object, "F_DISTRICT", ""));
                            sinale.put("FROMADDRALIAS", CommonTools.judgeNull(object, "IS_FIRST", ""));
                            sinale.put("TOPROVINCE", CommonTools.judgeNull(object, "T_PROVINCE", ""));
                            sinale.put("TOCITY", CommonTools.judgeNull(object, "T_CITY", ""));
                            sinale.put("TODISTRICT", CommonTools.judgeNull(object, "T_DISTRICT", ""));
                            sinale.put("TOADDRALIAS", CommonTools.judgeNull(object, "IS_LAST", ""));
                            listSingle.add(sinale);
                        }
                        if (listSingle.size() > 0) {
                            adapter.notifyDataSetChanged();
                        }
                        // 当前页+1
                        currentPage += 1;
                    } else {
                        Toast.makeText(context, "获取订单失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }
        });
    }

    private void driverGrab(String orderNo) {
        final String url = getString(R.string.server_url) + URLMap.DRIVER_GRABORDER;
        final RequestParams params = new org.xutils.http.RequestParams(url);
        params.addBodyParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addBodyParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addBodyParameter("orderno", orderNo);

        NetworkCallBack callBack = new NetworkCallBack(this);
        Callback.Cancelable cancel = callBack.networkRequest(params, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                flag = true;
                try {
                    JSONObject obj = new JSONObject(result);
                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        onResume();
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }
        });
    }

    private void windows() {
        String tDelete = "确认是否抢单？";
        String tCancel = "抢单";
        ClickListener listener = new ClickListener();
        window = new AlertDeleteDialogwindow(GrabSingleActivity.this, listener, tDelete, tCancel);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        window.showAtLocation(this.findViewById(R.id.activity_grab_single), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    class ClickListener implements View.OnClickListener {

        private ClickListener() {
        }

        @Override
        public void onClick(View arg0) {
            driverGrab(listSingle.get(positionInt).get("ORDER_NO"));
            window.dismiss();
        }

    }

}
