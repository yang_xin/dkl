package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

import cn.com.dkl.logistics.utils.ImageUtils;

public class ImageChooseDialogWindow extends PopupWindow {

    private View mMenuView;
    public static TextView tvCredentials;//提示语
    public static View creVal;        //间隔线
    public static TextView tvPhotoGraph;
    public static TextView tvPhoneChoose;
    public static TextView tvNoChoose;
    public static TextView tvCancel;
    private Activity activity;

    public ImageChooseDialogWindow(Activity context) {
        super(context);
        this.activity = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.image_choose_dialog, null);
        tvPhotoGraph = (TextView) mMenuView.findViewById(R.id.tvPhotoGraph);
        tvPhoneChoose = (TextView) mMenuView.findViewById(R.id.tvPhoneChoose);
        tvCancel = (TextView) mMenuView.findViewById(R.id.tvCancel);

        tvPhoneChoose.setVisibility(View.VISIBLE);
        tvPhotoGraph.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ImageUtils.openCameraImage(activity);
                dismiss();
            }
        });

        tvPhoneChoose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ImageUtils.openLocalImage(activity);
                dismiss();
            }
        });

        tvCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // 设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        // 设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        // mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int height = mMenuView.findViewById(R.id.pop_img_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
    public void setFromPhotoGone(){
        tvPhoneChoose.setVisibility(View.GONE);
    }
    public void setFromNoChoose(String text,OnClickListener onClickListener){
        tvNoChoose.setText(text);
        tvNoChoose.setVisibility(View.VISIBLE);
        tvNoChoose.setOnClickListener(onClickListener);
    }
    public ImageChooseDialogWindow(Activity context, String credentials) {
        super(context);
        this.activity = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.image_choose_dialog, null);
        tvCredentials = (TextView) mMenuView.findViewById(R.id.tvCredentials);
        creVal = (View) mMenuView.findViewById(R.id.creVal);
        tvPhotoGraph = (TextView) mMenuView.findViewById(R.id.tvPhotoGraph);
        tvPhoneChoose = (TextView) mMenuView.findViewById(R.id.tvPhoneChoose);
        tvNoChoose = (TextView) mMenuView.findViewById(R.id.tvNoChoose);
        tvCancel = (TextView) mMenuView.findViewById(R.id.tvCancel);

        tvCredentials.setVisibility(View.VISIBLE);
        creVal.setVisibility(View.VISIBLE);
        tvCredentials.setText(credentials);

        tvPhotoGraph.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ImageUtils.openCameraImage(activity);
                dismiss();
            }
        });

        tvPhoneChoose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ImageUtils.openLocalImage(activity);
                dismiss();
            }
        });

        tvCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // 设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        // 设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        // mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int height = mMenuView.findViewById(R.id.pop_img_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
}

