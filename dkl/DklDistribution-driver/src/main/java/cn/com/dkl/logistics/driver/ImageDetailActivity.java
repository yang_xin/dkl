package cn.com.dkl.logistics.driver;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.util.LogUtils;

import uk.co.senab.photoview.PhotoView;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 图片详情(点击大图) Created by LiuXing on 2016/12/19.
 */

public class ImageDetailActivity extends BaseActivity {


    @Bind(R.id.pv_bigimg)
    PhotoView mPvBigimg;
    private int scale;
    private String mImageName;
    private Bitmap mBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showbig_img);
        ButterKnife.bind(this);

        mImageName = getIntent().getStringExtra("imagename");

        if (!TextUtils.isEmpty(mImageName)) {
            String imagePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + mImageName;
            File file = new File(imagePath);
            if (file.exists()) {
                setImage(imagePath);
            } else {
                downloadImage(mImageName);
            }
        } else {
            mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.laceholder_img);
//            displayImage();
        }
    }


    /**
     * 防止图片失真
     */
    private void displayImage() {
        //想让图片宽是屏幕的宽度
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.yangyang);
        //测量
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;//只测量
        int height = mBitmap.getHeight();
        int width = mBitmap.getWidth();
        //再拿到屏幕的宽
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        int screenWidth = display.getWidth();
        //计算如果让照片是屏幕的宽，选要乘以多少？
        scale = screenWidth / width;
        //这个时候。只需让图片的宽是屏幕的宽，高乘以比例
        int displayHeight = height * scale;//要显示的高，这样避免失真
        //最终让图片按照宽是屏幕 高是等比例缩放的大小
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth, displayHeight);
        mPvBigimg.setLayoutParams(layoutParams);
    }


    /**
     * 显示图片
     */
    private void setImage(String imagePath) {
        mBitmap = BitmapFactory.decodeFile(imagePath);
        if (mBitmap != null) {
            mPvBigimg.setImageBitmap(mBitmap);
//            displayImage();
        }
    }


    /**
     * 加载图片
     */
    private void downloadImage(String imageName) {
        if (!TextUtils.isEmpty(imageName)) {
            final String imagePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;
            File file = new File(imagePath);
            if (!file.exists()) {
                String url = getString(R.string.server_imgurl) + "upload/" + imageName;
                HttpUtils http = new HttpUtils(60 * 1000);
                http.configCurrentHttpCacheExpiry(1000 * 10);
                http.download(url, imagePath, true, true, new RequestCallBack<File>() {

                    @Override
                    public void onSuccess(ResponseInfo<File> arg0) {
                        mPvBigimg.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                        displayImage();
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        LogUtils.i("图片加载失败：" + arg0.getMessage() + arg1);
                    }
                });
            }
        }
    }



//    if (!TextUtils.isEmpty()) {
//        String url = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + ;
//        File file = new File(url);
//        if (file.exists()) {
//            Bitmap bitmap = BitmapFactory.decodeFile(file.toString());
//            if (null != bitmap) {
//                mPvBigimg.setImageBitmap(bitmap);
//            }
//        } else {
//            downloadImage();
//        }
//    }



}
