package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.SettingEntity;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.RoleType;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.EncryptTools;
import cn.com.dkl.logistics.utils.NetworkCallBack;

/**
 * 登陆界面
 *
 * @author Dao
 */
@SuppressLint("NewApi")
public class LoginActivity extends BaseActivity {
    private Context context = LoginActivity.this;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft; // 返回
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle; // 标题
    @ViewInject(R.id.etUserName)
    private EditText etUserName; // 用户名
    @ViewInject(R.id.etPassword)
    private EditText etPassWord; // 密码
    @ViewInject(R.id.btnLogin)
    private Button btnLogin; // 登陆
    @ViewInject(R.id.tvRegister)
    private TextView tvRegister; // 免费注册
    @ViewInject(R.id.tvForgetPwd)
    private TextView tvForgetPwd; // 忘记密码
    @ViewInject(R.id.llLoginBackground)
    private LinearLayout llLoginBackground;
    private Dialog progressDialog;
    private CommonTools tools = new CommonTools(context);
    private boolean isExit = false;
    private boolean isFromMineFragment = false;
    private SharedPreferences sp = null;
    private String deviceID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    // 初始化
    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText(R.string.title_login);
        btnLeft.setVisibility(View.GONE);
        sp = context.getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
        etUserName.setText(sp.getString(SettingEntity.REMEMBER_USER_NAME, ""));
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {
            deviceID = tm.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 初始化进度框
        progressDialog = tools.getProgressDialog(context, "正在登陆中...");

        isExit = getIntent().getBooleanExtra("isExit", false);

        if (getIntent().getBooleanExtra("isFromMineFragment", false)) {
            isFromMineFragment = true;
        }

    }

    @OnClick({R.id.btnLeft, R.id.btnLogin, R.id.tvRegister, R.id.tvForgetPwd})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft: // 返回
                finish();
                return;
            case R.id.btnLogin: // 登陆
                doLogin();
                break;
            case R.id.tvRegister: // 注册
                Intent regIntent = new Intent(context, RegisterActivity.class);
                regIntent.putExtra("RoleID", RoleType.CARS_OWNER);
                startActivity(regIntent);
                break;
            case R.id.tvForgetPwd: // 忘记密码
                startActivity(new Intent(context, FindPwdActivity.class).putExtra("forget", "login"));
                break;

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 执行登陆操作
     */
    private void doLogin() {
        String userName = etUserName.getText().toString().trim();
        String password = etPassWord.getText().toString().trim();
        if (TextUtils.isEmpty(userName)) { // 判断用户名是否为空
            Toast.makeText(context, R.string.hint_input_username_or_number, Toast.LENGTH_SHORT).show();
            etUserName.requestFocus();
            return;
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(context, R.string.hint_input_password, Toast.LENGTH_SHORT).show();
            etPassWord.requestFocus();
            return;
        } else if (6 > password.length()) {
            Toast.makeText(context, R.string.hint_error_pwd_length, Toast.LENGTH_SHORT).show();
        }
//        startActivity(new Intent(context, MainActivity.class));
        sendRequest(userName, EncryptTools.getBASE64Str(password).toString());

    }

    private void sendRequest(final String userName, final String password) {

        final String url = getString(R.string.server_url) + URLMap.LOGIN;
        final RequestParams params = new RequestParams(url);
        params.addBodyParameter("USERNAME", userName);
        params.addBodyParameter("PASSWORD", password);
        params.addBodyParameter("CLIENT", getString(R.string.SCANTYPE));
        params.addBodyParameter("SCANTYPE", getString(R.string.SCANTYPE));
        params.addBodyParameter("ISSPECIAL", Constants.ISSPECIAL);
        params.addBodyParameter("DEVICEID", deviceID);

//        LogUtils.i(url + CommonTools.getQuryParams(params));
        NetworkCallBack callBack = new NetworkCallBack(this);
        Callback.Cancelable cancel = callBack.networkRequest(params, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
//                Toast.makeText(context, "请求成功", Toast.LENGTH_SHORT).show();
                try {
                    JSONObject obj = new JSONObject(result);
                    String status = obj.getString("status");
                    String code = obj.getString("code");
                    String message = obj.getString("message");
                    if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("1".equals(status)) {
                        if ("1".equals(code)) {
                            JSONObject data = obj.getJSONObject("data");
                            LogUtils.i("登陆返回数据：" + data.toString());
                            String roleID = CommonTools.judgeNull(data, "Role_ID", "");
                            if (RoleType.CARS_OWNER.equals(roleID)) {
                                SharedPreferences sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
                                SharedPreferences.Editor edit = sp.edit();
                                edit.putString(UserEntity.PHONE, CommonTools.judgeNull(data, "Phone", ""));
                                edit.putString(UserEntity.ROLE_ID, roleID);
                                edit.putString(UserEntity.UID,CommonTools.judgeNull(data, "Uid", ""));
                                edit.putString(UserEntity.COMPANY_ID, CommonTools.judgeNull(data, "COMPANY_ID", ""));
                                edit.putString(UserEntity.COMPANY_NAME, CommonTools.judgeNull(data, "COMPANY_NAME", ""));
                                edit.putString(UserEntity.PARK_NO, CommonTools.judgeNull(data, "PARK_NO", ""));
                                edit.putString(UserEntity.PARK_NAME, CommonTools.judgeNull(data, "PARK_NAME", ""));
                                edit.putString(UserEntity.USERNAME, CommonTools.judgeNull(data, "UserName", ""));
                                edit.putString(UserEntity.CREATE_TIME, CommonTools.judgeNull(data, "CreatTime", ""));
                                edit.putString(UserEntity.NAME, CommonTools.judgeNull(data, "Name", ""));
                                edit.putString(UserEntity.WEB_BALANCE, CommonTools.judgeNull(data, "WebBalance", ""));
                                edit.putString(UserEntity.HEAD_PHOTO_URL, CommonTools.judgeNull(data, "HeadPhotoUrl", ""));
                                edit.putString(UserEntity.GUARANTEE_MONEY, CommonTools.judgeNull(data, "GuaranteeMoney", ""));
                                edit.putString(UserEntity.POINT_LEVEL, CommonTools.judgeNull(data, "PointLevel", ""));
                                edit.putString(UserEntity.ORDINARY_POINT_LEVEL, CommonTools.judgeNull(data, "OrdinaryPointLevel", ""));
                                edit.putString(UserEntity.POINT, CommonTools.judgeNull(data, "Point", ""));
                                edit.putString(UserEntity.SEX, CommonTools.judgeNull(data, "Sex", ""));
                                edit.putString(UserEntity.USER_ID, CommonTools.judgeNull(data, "Uid", ""));
                                edit.putString(UserEntity.CONTACT_TELEPHONE, CommonTools.judgeNull(data, "TelePhone", ""));
                                edit.putString(UserEntity.CONTACT_EMAIL, CommonTools.judgeNull(data, "Email", ""));
                                edit.putString(UserEntity.CONTACT_QQ, CommonTools.judgeNull(data, "QQ", ""));
                                edit.putString(UserEntity.IS_SET_PAYMENT_PWD, CommonTools.judgeNull(data, "IsSetPaymentPW", "0"));
                                edit.putString(UserEntity.PASSWORD, password);
                                edit.putBoolean(UserEntity.IS_lOGIN, true);
                                edit.putString(UserEntity.IS_BAND_BANKCARD, CommonTools.judgeNull(data, "ISBDBANKCARD", "0"));
                                edit.putString(UserEntity.ISSOCIALUSER, CommonTools.judgeNull(data, "ISSOCIALUSER", "0"));
                                edit.commit();
                                sp = getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
                                edit = sp.edit();
                                edit.putString(SettingEntity.REMEMBER_USER_NAME, userName);
                                edit.commit();

                                // 是否社会用户
                                Constants.ISSOCIALUSER = CommonTools.judgeNull(data, "ISSOCIALUSER", "0");

//                                JPushUtil.setJPushAliasAndTags(context, data);
                                Toast.makeText(context, "登陆成功！", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(context, MainActivity.class));
//                                context.startService(new Intent(context, LocationService.class));
                                finish();
                            } else {
                                Toast.makeText(context, "本应用仅供车主用户登录!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (message.isEmpty()) {
                            Toast.makeText(context, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();

                }
            }
        });
//        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
//
//            @Override
//            public void onStart() {
//                super.onStart();
//                progressDialog.show();
//            }
//
//            @Override
//            public void onSuccess(ResponseInfo<String> result) {
//                progressDialog.dismiss();
//                try {
//                    JSONObject obj = new JSONObject(result.result);
//                    String status = obj.getString("status");
//                    String code = obj.getString("code");
//                    String message = obj.getString("message");
//                    if ("-1".equals(status)) {
//                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
//                    } else if ("0".equals(status)) {
//                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                    } else if ("1".equals(status)) {
//                        if ("1".equals(code)) {
//                            JSONObject data = obj.getJSONObject("data");
//                            LogUtils.i("登陆返回数据：" + data.toString());
//                            String roleID = CommonTools.judgeNull(data, "Role_ID", "");
//                            if (RoleType.CARS_OWNER.equals(roleID)) {
//                                SharedPreferences sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
//                                Editor edit = sp.edit();
//                                edit.putString(UserEntity.PHONE, CommonTools.judgeNull(data, "Phone", ""));
//                                edit.putString(UserEntity.ROLE_ID, roleID);
//                                edit.putString(UserEntity.UID,CommonTools.judgeNull(data, "Uid", ""));
//                                if (roleID.equals(RoleType.LOGISTICS_ENTERPRISE)) {
//                                    edit.putString(UserEntity.COMPANY_ID, CommonTools.judgeNull(data, "CompanyId", ""));
//                                    edit.putString(UserEntity.COMPANY_NAME, CommonTools.judgeNull(data, "CompanyName", ""));
//                                }
//                                edit.putString(UserEntity.USERNAME, CommonTools.judgeNull(data, "UserName", ""));
//                                edit.putString(UserEntity.CREATE_TIME, CommonTools.judgeNull(data, "CreatTime", ""));
//                                edit.putString(UserEntity.NAME, CommonTools.judgeNull(data, "Name", ""));
//                                edit.putString(UserEntity.WEB_BALANCE, CommonTools.judgeNull(data, "WebBalance", ""));
//                                edit.putString(UserEntity.HEAD_PHOTO_URL, CommonTools.judgeNull(data, "HeadPhotoUrl", ""));
//                                edit.putString(UserEntity.GUARANTEE_MONEY, CommonTools.judgeNull(data, "GuaranteeMoney", ""));
//                                edit.putString(UserEntity.POINT_LEVEL, CommonTools.judgeNull(data, "PointLevel", ""));
//                                edit.putString(UserEntity.ORDINARY_POINT_LEVEL, CommonTools.judgeNull(data, "OrdinaryPointLevel", ""));
//                                edit.putString(UserEntity.POINT, CommonTools.judgeNull(data, "Point", ""));
//                                edit.putString(UserEntity.SEX, CommonTools.judgeNull(data, "Sex", ""));
//                                edit.putString(UserEntity.USER_ID, CommonTools.judgeNull(data, "Uid", ""));
//                                edit.putString(UserEntity.CONTACT_TELEPHONE, CommonTools.judgeNull(data, "TelePhone", ""));
//                                edit.putString(UserEntity.CONTACT_EMAIL, CommonTools.judgeNull(data, "Email", ""));
//                                edit.putString(UserEntity.CONTACT_QQ, CommonTools.judgeNull(data, "QQ", ""));
//                                edit.putString(UserEntity.IS_SET_PAYMENT_PWD, CommonTools.judgeNull(data, "IsSetPaymentPW", "0"));
//                                edit.putString(UserEntity.PASSWORD, password);
//                                edit.putBoolean(UserEntity.IS_lOGIN, true);
//                                edit.putString(UserEntity.IS_BAND_BANKCARD, CommonTools.judgeNull(data, "ISBDBANKCARD", "0"));
//                                edit.commit();
//                                sp = getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
//                                edit = sp.edit();
//                                edit.putString(SettingEntity.REMEMBER_USER_NAME, userName);
//                                edit.commit();
//
//                                // 是否社会用户
//                                Constants.ISSOCIALUSER = CommonTools.judgeNull(data, "ISSOCIALUSER", "0");
//
////                                JPushUtil.setJPushAliasAndTags(context, data);
//                                Toast.makeText(context, "登陆成功！", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(context, MainActivity.class));
////                                context.startService(new Intent(context, LocationService.class));
//                                finish();
//                            } else {
//                                Toast.makeText(context, "本应用仅供车主用户登录!", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    } else {
//                        if (message.isEmpty()) {
//                            Toast.makeText(context, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(context, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
//
//                }
//            }
//
//            @Override
//            public void onFailure(HttpException e, String result) {
//                progressDialog.dismiss();
//                CommonTools.failedToast(context);
//            }
//        });
    }

}
