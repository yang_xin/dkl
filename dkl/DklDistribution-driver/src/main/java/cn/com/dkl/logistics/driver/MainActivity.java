package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.tencent.stat.StatService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.SettingEntity;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.TstOrderBean;
import cn.com.dkl.logistics.fragment.ArroundCarFragment;
import cn.com.dkl.logistics.fragment.MenuFragment;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.Constant;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.utils.util;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

/**
 * 主界面
 *
 * @author Dao
 */
@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity {
    private static final String TAG = "MainActivity";
    /**
     * 侧滑控件
     */
    public static SlidingMenu menu;

    private Context context = MainActivity.this;
    private SharedPreferences sp;
    private SharedPreferences preferences;
    private int verCode = 0;
    private int newVerCode = 0;
    private String newVer = "";
    private String versionString = "";
    private String downloadUrl = "";
    private String appName = "";
    private String versionName = null;
    private String callBackCode = "";
    private String callBackMessage = "";
    private String callBackState = "";
    String apkFolderUrl = "";
    // 0 运起来 1 同城配运司机版 2 同城配运货主版
    private final int versonType = 1;
    private AlertDialog alertdialog;
    workListener mWork;
    private showWorkPlan mShowPlan;

    //规划路线次数
    int workSize;
    //调取实名认证次数
    int mAuthNumber;

    /**
     * 认证状态初始值
     */
    public static int AUTHSTATUSINIT;
    /**
     * 认证状态后来值
     */
    public static int AUTHSTATUSTHEN;
    //设置极光别名与标签
    private static final int MSG_SET_ALIAS = 1001;
    WorkReceiver mWorkReceiver = new WorkReceiver();

    Handler mainHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SET_ALIAS:
                    Log.d("设置标签与别名", "Set alias in handler.");
                    // 调用 JPush 接口来设置别名。
                    setJpush();
                    break;
            }
        }
    };

    orderDetailListener OnorderDetailListener;
    AuthChangeListener AuthChangeListener;
    OrderBackListener OrderBackListener;

    public interface orderDetailListener {
        void orderDerail(int type);
    }

    public interface workListener {
        void work(TstOrderBean tstOrderBean);
    }

    public interface showWorkPlan {
        void showPlan(String endddress);
    }

    public interface AuthChangeListener {
        void authChange(boolean isNeed);
    }

    public interface OrderBackListener {
        void orderBack();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter filter = new IntentFilter();
        filter.addAction("finish");
        registerReceiver(receiver, filter);
        IntentFilter workFilter = new IntentFilter();
        workFilter.addAction(Constant.AUTOMATICBROADCAST);
        registerReceiver(mWorkReceiver, workFilter);

        util mUtil = new util();
        mUtil.getStorageCapacity();
//        mWorkReceiver
       /* if (getIntent().getExtras() != null) {
            Intent in = new Intent(context, NewMessageActivity.class);
            in.putExtras(getIntent().getExtras());
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(in);
        }*/
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        StatService.onResume(this);
        FlowerCollector.onResume(this);
        requestData();
//        startWork();
//        showplan();

        getLocationConfig();
    }

    @Override
    protected void onPause() {
        super.onPause();
        StatService.onPause(this);
        if (menu.isMenuShowing()) {
//            menu.toggle();
        }
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        unregisterReceiver(mWorkReceiver);
        LogUtils.i("MainActivity.onDestroy()");

//        long overRxBytes = sp.getLong("RxBytes", 0L);
//        long overTxBytes = sp.getLong("TxBytes", 0L);
//        long nowRxBytes = CommonTools.getMobileRxBytes(context);
//        long nowTxBytes = CommonTools.getMobileTxBytes(context);
//        long Rxbytes = nowRxBytes - overRxBytes;
//        long Txbytes = nowTxBytes - overTxBytes;
//        Log.i(TAG, "退出--上行流量:" + Rxbytes +
//                "--下行流量:" + Txbytes);
    }

    public orderDetailListener getOnorderDetailListener() {
        return OnorderDetailListener;
    }

    public void setOnorderDetailListener(orderDetailListener onorderDetailListener) {
        OnorderDetailListener = onorderDetailListener;
    }

    public MainActivity.OrderBackListener getOrderBackListener() {
        return OrderBackListener;
    }

    public void setOrderBackListener(MainActivity.OrderBackListener orderBackListener) {
        OrderBackListener = orderBackListener;
    }

    public workListener getmWork() {
        return mWork;
    }

    public void setmWork(workListener mWork) {
        this.mWork = mWork;
    }

    public showWorkPlan getmShowPlan() {
        return mShowPlan;
    }

    public void setmShowPlan(showWorkPlan mShowPlan) {
        this.mShowPlan = mShowPlan;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("finish")) {
                finish();
            }
        }
    };

    public int getWorkSize() {
        return workSize;
    }

    public void setWorkSize(int workSize) {
        this.workSize = workSize;
    }

    private void init() {
        ViewUtils.inject(this);
        FlowerCollector.setCaptureUncaughtException(true);
        preferences = getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
        apkFolderUrl = preferences.getString(SettingEntity.APK_URL, "");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

//        Log.i(TAG, "初始化--上行流量:" + CommonTools.getMobileRxBytes(context) +
//                "--下行流量:" + CommonTools.getMobileTxBytes(context));
//        Editor editor = sp.edit();
//        long MobileRxBytes = CommonTools.getMobileRxBytes(context);
//        long MobileTxBytes = CommonTools.getMobileTxBytes(context);
//        editor.putLong("RxBytes", MobileRxBytes);
//        editor.putLong("TxBytes", MobileTxBytes);
//        editor.commit();
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            // 文件夹
            Constants.STORAGEPATH = Environment.getExternalStorageDirectory().toString()
                    + "/" + context.getPackageName() + "/pictures";
        } else if (Environment.getExternalStorageState().equals(Environment.MEDIA_REMOVED)) {
            Constants.STORAGEPATH = context.getFilesDir().getPath() + "/" + context.getPackageName() + "/pictures";
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
                new ArroundCarFragment());

        menu = new SlidingMenu(this);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);

        int width = CommonTools.getWindowWidth(this);
        menu.setBehindWidth(width - CommonTools.dp2px(context, 60));
        menu.setMenu(R.layout.menu_frame);
        menu.setBehindScrollScale(1.0f);//滑动样式
        menu.setOnClosedListener(new SlidingMenu.OnClosedListener() {
            @Override
            public void onClosed() {
                //如果是从未认证状态变成认证状态，才能请求数据
                if (AuthChangeListener != null) {
                    AuthChangeListener.authChange(isNeedSelfInfo());
                }
            }
        });
        ft.replace(R.id.menu_frame, new MenuFragment()).commit();
        try {
            versionName = context.getPackageManager().getPackageInfo(getPackageName(),
                    PackageManager.GET_CONFIGURATIONS).versionName;
            verCode = context.getPackageManager().getPackageInfo(context.getPackageName(),
                    PackageManager.GET_CONFIGURATIONS).versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        mainHandler.sendEmptyMessage(MSG_SET_ALIAS);
//        checkUpdate();
//        context.startService(new Intent(context, LocationService.class));
        nomedia();
    }

    private void nomedia(){
        File nomedia = new File( Environment.getExternalStorageDirectory().toString()
                + "/" + context.getPackageName() , ".nomedia");
        try {
            if (!nomedia.exists())
                nomedia.createNewFile();
            FileOutputStream nomediaFos = new FileOutputStream(nomedia);
            nomediaFos.flush();
            nomediaFos.close();
        } catch (IOException e) {
            Log.e("IOException", "exception in createNewFile() method");
            return;
        }
    }

    /**
     * // 获取定位配置
     */
    private void getLocationConfig() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("CODE", "CDLocationFrequency");

        String url = getString(R.string.server_url) + URLMap.GET_CD_INTERFACE_CONFIG;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("获取定位配置信息失败..");
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONArray data = obj.getJSONArray("data");

                        if (data.length() > 0) {
                            JSONObject firstData = data.getJSONObject(0);
                            Editor edit = sp.edit();
                            edit.putInt(Constants.REPORT_FREQUENCY, Integer.parseInt(CommonTools.judgeNull(firstData, "CONFIG", "1")));
                            edit.commit();
                        }
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }
    private void requestData() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_DRIVER_USERINFO;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        JSONObject data = obj.getJSONObject("data");
                        JSONObject userInfo = data.getJSONObject("userInfo");

                        Editor edit = sp.edit();
                        edit.putString(UserEntity.HIGH_PRAISE_RATE, CommonTools.judgeNull(userInfo, "HIGHPRAISERATE", null)); // 好评率
                        edit.putString(UserEntity.EVALUATION_NUMBER, CommonTools.judgeNull(userInfo, "EVALUATIONCOUNT", null)); // 评价数
                        edit.putString(UserEntity.BALANCE, CommonTools.judgeNull(userInfo, "BALANCE", null)); // 账户
                        edit.putString(UserEntity.NAME, CommonTools.judgeNull(userInfo, "NAME", null)); // 用户姓名
                        edit.putString(UserEntity.USER_ID, CommonTools.judgeNull(userInfo, "USER_ID", null)); // 用户id
                        edit.putString(UserEntity.ONLINE_TIME_TODAY, CommonTools.judgeNull(userInfo, "ONLINETIMECOUNT", null)); // 在线小时数
                        edit.putString(UserEntity.POINT_LEVEL, CommonTools.judgeNull(userInfo, "LEVEL", null)); // 信誉
                        edit.putString(UserEntity.TRANSACTION_COUNT, CommonTools.judgeNull(userInfo, "TRANSACTIONCOUNT", null)); // 交易数量
                        edit.putString(UserEntity.STATUS, CommonTools.judgeNull(userInfo, "STATUS", null));
                        edit.putString(UserEntity.WEB_BALANCE, CommonTools.judgeNull(userInfo, "WEBBALANCE", null)); // 钱包余额
                        edit.putString(UserEntity.SERIVICE_ATTITUDE_POINT, CommonTools.judgeNull(userInfo, "SERVICEATTITUDEPOINT", null)); // 服务态度
                        edit.putString(UserEntity.CREDIT, CommonTools.judgeNull(userInfo, "CREDIT", null)); // 授信额度
                        edit.putString(UserEntity.TRANSACTION_MONEY, CommonTools.judgeNull(userInfo, "TRANSACTIONMONEY", null)); // 流水账金额
                        String headImageName = CommonTools.judgeNull(userInfo, "HEADPHOTOURL", null);
                        edit.putString(UserEntity.HEAD_PHOTO_URL, headImageName); // 用户头像
                        edit.putString(UserEntity.GUARANTEE_MONEY, CommonTools.judgeNull(userInfo, "GUARANTEEMONEY", null)); // 担保金
                        edit.putString(UserEntity.TURNOVER_TODAY, CommonTools.judgeNull(userInfo, "TRANSACTIONCOUNTTODATE", null)); // 今日接单数

                        JSONArray carArr = data.getJSONArray("carList");
                        if (carArr.length() > 0) {
                            edit.putString(UserEntity.CAR_ID, CommonTools.judgeNull(carArr.getJSONObject(0), "CARID", null));
                            edit.putString(UserEntity.CAR_NUMBER, CommonTools.judgeNull(carArr.getJSONObject(0), "CARNUMBER", null));
                            edit.putString(UserEntity.CAR_STATUS, CommonTools.judgeNull(carArr.getJSONObject(0), "DISPATCHSTATUS", null));
                            edit.putString(UserEntity.LOAD_STATUS, CommonTools.judgeNull(carArr.getJSONObject(0), "LOADSTATUS", null));
                        }
                        edit.commit();
                        ImageUtils.downLoadImage(context, headImageName);

                        sendDataUpdateBroadcast();
                    } else {
                        Toast.makeText(context, "获取用户信息失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            private void sendDataUpdateBroadcast() {
                //Intent intent = new Intent();
                //intent.setAction("cn.com.dkl.logistics.driver.MainDataUpdate");
                sendBroadcast(new Intent("cn.com.dkl.logistics.driver.MainDataUpdate"));
            }

        });
    }

    public static final int USER_EXIT = 1;
    // 是否退出应用
    private static boolean isExit = false;

    private static Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case USER_EXIT:
                    isExit = false;
                    break;
                default:
                    break;
            }
        }

        ;
    };

    @Override
    public void onBackPressed() {
        if (isExit) {
            finish(); // 关闭当前界面
        } else {
            Toast.makeText(context, "再按一次退出应用", Toast.LENGTH_SHORT).show();
            isExit = true;
            Message msg = new Message();
            msg.what = USER_EXIT;
            handler.sendMessageDelayed(msg, 2000);
        }
    }

    public static SlidingMenu getSlidingMenu() {
        return menu;
    }

    /**
     * 检查升级
     */
    public void checkUpdate() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("Version", String.valueOf(verCode));
        params.addQueryStringParameter("VERSIONTYPE", String.valueOf(versonType));

        String url = getString(R.string.server_url) + URLMap.VERSION_UPDATE;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                //CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                LogUtils.i("responseInfo.result= " + arg0.result);
                versionString = arg0.result;
                try {
                    if (!versionString.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(versionString);
                        callBackMessage = jsonObject.getString("message");
                        callBackState = jsonObject.getString("state");
                        callBackCode = jsonObject.getString("code");
                        if (callBackState.equals("1")) {
                            JSONObject dataObject = jsonObject.optJSONObject("data");
                            newVerCode = dataObject.getInt("VERSION");
                            newVer = CommonTools.judgeNull(dataObject, "VERSION", "");
                            downloadUrl = CommonTools.judgeNull(dataObject, "LINK", "");
                            appName = CommonTools.judgeNull(dataObject, "NAME", "");
                            // description = CommonTools.judgeNull(dataObject,
                            // "DESCRIPTION", "");
                            if (verCode < newVerCode) {
                                alertDialog();
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    /**
     * 下载弹窗
     */
    public void alertDialog() {
        alertdialog = new AlertDialog.Builder(context).create();
        alertdialog.show();
        Window window = alertdialog.getWindow();
        window.setContentView(R.layout.dialog_main_info);
        TextView tvDialogVersonCode = (TextView) window.findViewById(R.id.tvDialogVersonCode);
        final ImageView ivLoading = (ImageView) window.findViewById(R.id.ivLoading);
        RotateAnimation animation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(900);
        animation.setRepeatCount(-1);
        ivLoading.startAnimation(animation);
        int start = appName.lastIndexOf("-") + 1;
        int end = appName.lastIndexOf(".");
        String versonCode = appName.substring(start, end);
        tvDialogVersonCode.setText(versonCode);
        final Button btnUpdate = (Button) window.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ivLoading.setVisibility(View.VISIBLE);
                btnUpdate.setVisibility(View.GONE);
                downloadApk();
            }
        });
    }

    /**
     * 下载apk
     */
    public void downloadApk() {
        final String fileName = apkFolderUrl + "/" + appName;
        LogUtils.i(fileName);
        File file = new File(fileName);
        if (file.exists()) {
            install(fileName);
        } else {
            HttpUtils httpUtils = new HttpUtils(60 * 1000);
            httpUtils.configCurrentHttpCacheExpiry(1000 * 10);
            httpUtils.download(downloadUrl, fileName, false, false, new RequestCallBack<File>() {
                @Override
                public void onLoading(long total, long current, boolean isUploading) {
                    super.onLoading(total, current, isUploading);
                }

                @Override
                public void onSuccess(ResponseInfo<File> arg0) {
                    alertdialog.dismiss();
                    install(fileName);
                    Toast.makeText(context, "下载完成", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(HttpException arg0, String arg1) {
                    alertdialog.dismiss();
                    //CommonTools.failedToast(context);
                }
            });
        }
    }

    /**
     * 安装apk
     */
    public void install(String path) {
        alertdialog.dismiss();
        File file = new File(path);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(file);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        startActivity(intent);
    }

    void startWork(Intent data) {
        if (mWork == null) {
            Toast.makeText(context, "无人工作，请先出车！", Toast.LENGTH_SHORT).show();
            return;
        }
        //清楚路线
        Intent it = new Intent("clearRountOverlay");
        it.putExtra("Flag", true);
        context.sendBroadcast(it);
//        context.sendBroadcast(new Intent("clearRountOverlay"));

        TstOrderBean tstOrderBean = (TstOrderBean) data.getSerializableExtra("data");
        //跟framment通信,告诉它，规划路线
        mWork.work(tstOrderBean);
        workSize++;
        String endddress = data.getStringExtra("endddress");
        if (!TextUtils.isEmpty(endddress)) {
            mShowPlan.showPlan(endddress);
        }
    }
   /* *//**
     * 规划路线
     *//*
    void startWork() {
        Intent startIntent = getIntent();
        int flag = startIntent.getIntExtra("flag", Constant.NON);
        switch (flag) {
            case Constant.WORKORDER://工作单
                if (mWork == null) {
                    Toast.makeText(context, "无人工作，请先出车！", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (workSize == 1){
                    return;
                }
                TstOrderBean tstOrderBean = (TstOrderBean) startIntent.getSerializableExtra("data");
                //跟framment通信,告诉它，规划路线
                mWork.work(tstOrderBean);
                workSize++;
                break;
        }
    }
    */

    /**
     * 显示目的地
     *//*
    void showplan(){
        String endddress = getIntent().getStringExtra("endddress");
        if (!TextUtils.isEmpty(endddress)){
            mShowPlan.showPlan(endddress);
        }
    }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ArroundCarFragment.ORDERDEAIL) {
                if (OnorderDetailListener != null) {
                    OnorderDetailListener.orderDerail(ArroundCarFragment.ORDERDEAIL);
                }
            }
            if (requestCode == ArroundCarFragment.DoneList) {
                startWork(data);
            }
        } else {
            if (requestCode == ArroundCarFragment.ORDERDEAIL) {
                if (OrderBackListener != null) {
                    OrderBackListener.orderBack();
                }
            }
        }

    }

    public int getmAuthNumber() {
        return mAuthNumber;
    }

    public void setmAuthNumber(int mAuthNumber) {
        this.mAuthNumber = mAuthNumber;
    }

    /**
     * 是否需要调个人信息接口 true，需要
     */
    boolean isNeedSelfInfo() {
        int AUTHSTATUSTHEN = sp.getInt("AUTHSTATUSTHEN", 0);
        int AUTHSTATUSINIT = sp.getInt("AUTHSTATUSINIT", 0);
        if (AUTHSTATUSTHEN == 2 && AUTHSTATUSINIT != 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 设置认证状态，并记录。 如果第一次为为认证，以后n次为已认证，则需要调个人信息接口
     */
    void setAuthStatus(int authStatus) {
        if (mAuthNumber == 0) {
            MainActivity.AUTHSTATUSINIT = authStatus;
        } else {
            MainActivity.AUTHSTATUSTHEN = authStatus;
        }
        mAuthNumber++;
    }

    class AuthBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    }

    public MainActivity.AuthChangeListener getAuthChangeListener() {
        return AuthChangeListener;
    }

    public void setAuthChangeListener(MainActivity.AuthChangeListener authChangeListener) {
        AuthChangeListener = authChangeListener;
    }

    void setJpush() {
        SharedPreferences sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        String roleId = sp.getString(UserEntity.ROLE_ID, "");
        Set<String> sets = new HashSet<String>();
        sets.add(roleId);
        String alias = sp.getString(UserEntity.UID, "");
        try {
            //调用JPush API 同时设置Alias 与 Tag
            JPushInterface.setAlias(context, alias, tagAliasCallback);
//            JPushInterface.setAliasAndTags(context, tag, sets, tagAliasCallback);
//            JPushInterface.setAlias();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    TagAliasCallback tagAliasCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int i, String s, Set<String> set) {
            String tag = "设置标签与别名回调";
            String logs;
            switch (i) {
                case 0:
                    logs = "Set tag and alias success";
                    Log.i(tag, logs);
                    // 建议这里往 SharePreference 里写一个成功设置的状态。成功设置一次后，以后不必再次设置了。
                    break;
                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                    Log.i(tag, logs);
                    // 延迟 60 秒来调用 Handler 设置别名
                    mainHandler.sendMessageDelayed(mainHandler.obtainMessage(MSG_SET_ALIAS), 1000 * 60);
                    break;
                default:
                    logs = "Failed with errorCode = " + i;
                    Log.e(tag, logs);
            }
        }
    };

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    class WorkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                startWork(intent);
            }
        }
    }
}