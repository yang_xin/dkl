package cn.com.dkl.logistics.driver;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.EncryptTools;

/**
 * 修改密码
 *
 * @author txw
 */
public class ModifyPasswordActivity extends BaseActivity {
    private Context context = ModifyPasswordActivity.this;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.etPhoneNumber)
    private EditText etPhoneNumber;
    @ViewInject(R.id.etSmsCode)
    private EditText etSmsCode;
    @ViewInject(R.id.etOriginalPwd)
    private EditText etOriginalPwd;
    @ViewInject(R.id.etNewPwd)
    private EditText etNewPwd;
    @ViewInject(R.id.etConfirmPwd)
    private EditText etConfirmPwd;
    @ViewInject(R.id.btnGetSmsCode)
    private Button btnGetSmsCode;
    @ViewInject(R.id.btnSaveModify)
    private Button btnSaveModify;
    private SharedPreferences sp;
    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_password);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText(R.string.title_modify_pwd);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        etPhoneNumber.setText(sp.getString(UserEntity.PHONE, ""));
        etPhoneNumber.setEnabled(false);


        btnGetSmsCode.setClickable(true);
    }

    @OnClick({R.id.btnLeft, R.id.btnGetSmsCode, R.id.btnSaveModify, R.id.btnSaveModify})
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - lastClickTime > CommonTools.MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime;
            switch (v.getId()) {
                case R.id.btnLeft: // 返回
                    finish();
                    return;
                case R.id.btnGetSmsCode: // 获取验证码
                    String phoneNumber = etPhoneNumber.getText().toString().trim();
                    if (TextUtils.isEmpty(phoneNumber)) {
                        Toast.makeText(context, R.string.hint_input_phone_number, Toast.LENGTH_SHORT).show();
                    } else {
                        boolean result = CommonTools.checkPhoneNumber(phoneNumber);
                        if (result) {
                            sendSmsCode(phoneNumber);
                        } else {
                            Toast.makeText(context, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case R.id.btnSaveModify: // 提交修改密码
                    modifyPassword();
                    break;
            }
        }
    }

    private void modifyPassword() {
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String smsCode = etSmsCode.getText().toString().trim();
        String originalPwd = etOriginalPwd.getText().toString().trim();
        String newPassword = etNewPwd.getText().toString().trim();
        String confirmPwd = etConfirmPwd.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNumber)) {
            Toast.makeText(context, R.string.hint_input_phone_number, Toast.LENGTH_SHORT).show();
            etPhoneNumber.requestFocus();
            return;
        } else if (!CommonTools.checkPhoneNumber(phoneNumber)) {
            Toast.makeText(context, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
            etPhoneNumber.requestFocus();
            return;
        } else if (TextUtils.isEmpty(smsCode)) {
            Toast.makeText(context, "短信验证码不能为空", Toast.LENGTH_SHORT).show();
            etSmsCode.requestFocus();
            return;
        } else if (6 != smsCode.length()) {
            Toast.makeText(context, "请输入正确的验证码", Toast.LENGTH_SHORT).show();
            etSmsCode.requestFocus();
            return;
        } else if (TextUtils.isEmpty(originalPwd)) {
            Toast.makeText(context, "请输入原密码", Toast.LENGTH_SHORT).show();
            etOriginalPwd.requestFocus();
            return;
        } else if (6 > originalPwd.length()) {
            Toast.makeText(context, "原密码长度不小于6位", Toast.LENGTH_SHORT).show();
            etNewPwd.requestFocus();
            return;
        } else if (TextUtils.isEmpty(newPassword)) {
            Toast.makeText(context, R.string.hint_input_password, Toast.LENGTH_SHORT).show();
            etNewPwd.requestFocus();
            return;
        } else if (6 > newPassword.length()) {
            Toast.makeText(context, "密码长度不小于6位", Toast.LENGTH_SHORT).show();
            etNewPwd.requestFocus();
            return;
        } else if (TextUtils.isEmpty(confirmPwd)) {
            Toast.makeText(context, R.string.hint_input_confirm_pwd, Toast.LENGTH_SHORT).show();
            etConfirmPwd.requestFocus();
            return;
        } else if (!newPassword.equals(confirmPwd)) {
            Toast.makeText(context, "两次密码输入不一致", Toast.LENGTH_SHORT).show();
            etConfirmPwd.requestFocus();
            return;
        } else if (originalPwd.equals(newPassword)) {
            Toast.makeText(context, "新密码与原密码不能一样", Toast.LENGTH_SHORT).show();
            etNewPwd.requestFocus();
            return;
        } else {
            sendRequest(phoneNumber, smsCode, EncryptTools.getBASE64Str(originalPwd).toString(), EncryptTools.getBASE64Str(newPassword).toString(),
                    EncryptTools.getBASE64Str(confirmPwd).toString());
        }

    }

    private void sendRequest(String phoneNumber, String smsCode, String originalPwd, String newPassword, final String confirmPwd) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("PHONE", phoneNumber);
        params.addQueryStringParameter("VALIDCODE", smsCode);
        params.addQueryStringParameter("PASSWORD", originalPwd);
        params.addQueryStringParameter("NEWPASSWORD", newPassword);
        params.addQueryStringParameter("CONFIRMNEWPASSWORD", confirmPwd);
        params.addQueryStringParameter("CLIENT", "2");

        String url = getString(R.string.server_url) + URLMap.MODIFY_PASSWORD;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            AlertDialog dialog = null;

            @Override
            public void onStart() {
                dialog = new AlertDialog.Builder(context).setMessage("密码修改中...").create();
                dialog.show();
                super.onStart();
            }

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(result.result);
                    String status = obj.getString("status");
                    String code = obj.getString("code");
                    String message = obj.getString("message");
                    System.out.println("修改密码结果：" + obj.toString());
                    if ("1".equals(status)) {
                        if ("1".equals(code)) {
                            Toast.makeText(context, "修改密码成功！", Toast.LENGTH_SHORT).show();
                            SharedPreferences sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
                            Editor edit = sp.edit();
                            edit.clear();
                            edit.commit();
                            startActivity(new Intent(context, LoginActivity.class));
                            finish();
                        }
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException e, String result) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }

        });
    }

    /**
     * 发送修改密码验证码
     *
     * @param phoneNumber 手机号码
     */
    private void sendSmsCode(String phoneNumber) {
        btnGetSmsCode.setClickable(false);
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("PHONE", phoneNumber);
        params.addQueryStringParameter("CLIENT", "2");

        String url = getString(R.string.server_url) + URLMap.MODIFY_PASSWORD_SMS_CODE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    JSONObject obj = new JSONObject(result.result);
                    String status = obj.getString("state");
                    String code = obj.getString("code");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        setButtonState(); // 更新短信发送按钮状态
                    } else if ("0".equals(status)) {
                        btnGetSmsCode.setClickable(true);
                        Toast.makeText(context, "验证码发送失败", Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        btnGetSmsCode.setClickable(true);
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                btnGetSmsCode.setClickable(true);
                CommonTools.failedToast(context);
            }
        });

    }

    /**
     * 设置发送短信按钮状态
     */
    private void setButtonState() {
        btnGetSmsCode.setClickable(false);
        final Timer timer = new Timer(); // 定时器对象
        TimerTask task = new TimerTask() {
            int i = 60;

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        i--;
                        btnGetSmsCode.setText(i + "秒");
                        if (i == 0) {
                            timer.cancel();
                            btnGetSmsCode.setText("点击重发");
                            btnGetSmsCode.setClickable(true);
                        }
                    }
                });
            }
        };
        timer.schedule(task, 0, 1000);
    }

}
