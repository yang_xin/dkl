package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.EncryptTools;
import cn.com.dkl.logistics.view.CustomProgressDialog;

/**
 * 设置支付密码
 *
 * @author hzq
 */
public class ModifypaymentPawActivity extends BaseActivity {
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;

    @ViewInject(R.id.tvForgetPwd)
    private TextView tvForgetPwd;

    @ViewInject(R.id.etOriginalPayPwd)
    private EditText etOriginalPayPwd;

    @ViewInject(R.id.etNewPayPwd)
    private EditText etNewPayPwd;

    @ViewInject(R.id.etConfirmPayPwd)
    private EditText etConfirmPayPwd;

    @ViewInject(R.id.btnSaveModify)
    private Button btnSaveModify;

    @ViewInject(R.id.llOldPaymentPwd)
    private LinearLayout llOldPaymentPwd;

    private Context context = ModifypaymentPawActivity.this;
    private SharedPreferences sp;
    private String newPayPwd;
    private String confirmPayPwd;
    private String originalPayPwd;
    private CustomProgressDialog dialog = null;
    private boolean isSetPaymentPwd = false;
    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_payment_password);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText("设置支付密码");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        dialog = new CommonTools(context).getProgressDialog(context, "提交数据中...");
        isSetPaymentPassword();
    }

    private void sendRequest(String oldPayPwd, String newPayPwd) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("PAYMENTPASSWORD", newPayPwd);
        params.addQueryStringParameter("SRCPAYMENTPASSWORD", oldPayPwd);

        String url = getString(R.string.server_url) + URLMap.UPDATAPAYPASSWORD;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                dialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if (state.equals("1")) {
                        Toast.makeText(context, "支付密码修改成功！", Toast.LENGTH_SHORT).show();
                        Editor edit = sp.edit();
                        edit.putString(UserEntity.IS_SET_PAYMENT_PWD, "1");
                        edit.commit();
                        dialog.dismiss();
                        setResult(Activity.RESULT_OK, new Intent());
                        finish();
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @OnClick({R.id.btnLeft, R.id.btnSaveModify, R.id.tvForgetPwd})
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnSaveModify:
                if (currentTime - lastClickTime > CommonTools.MIN_CLICK_DELAY_TIME) {
                    lastClickTime = currentTime;
                    originalPayPwd = etOriginalPayPwd.getText().toString();
                    newPayPwd = etNewPayPwd.getText().toString();
                    confirmPayPwd = etConfirmPayPwd.getText().toString();
                    if (llOldPaymentPwd.getVisibility() == View.VISIBLE && originalPayPwd.length() != 6) {
                        Toast.makeText(context, "请输入六位数的支付密码!", Toast.LENGTH_SHORT).show();
                    } else if (newPayPwd.equals("") || confirmPayPwd.equals("")) {
                        Toast.makeText(context, "请填写完整信息!", Toast.LENGTH_SHORT).show();
                    } else if (newPayPwd.length() != 6 || confirmPayPwd.length() != 6) {
                        Toast.makeText(context, "请输入六位数的支付密码!", Toast.LENGTH_SHORT).show();
                    } else if (!newPayPwd.equals(confirmPayPwd)) {
                        Toast.makeText(context, "两次输入密码不一样", Toast.LENGTH_SHORT).show();
                        etNewPayPwd.setText("");
                        etConfirmPayPwd.setText("");
                    } else {
                        if (isSetPaymentPwd) {
                            sendRequest(EncryptTools.getBASE64Str(originalPayPwd).toString(), EncryptTools.getBASE64Str(confirmPayPwd).toString());
                        } else {
                            sendRequest("", EncryptTools.getBASE64Str(confirmPayPwd).toString());
                        }
                    }
                }
                break;
            case R.id.tvForgetPwd: // 忘记密码
                startActivity(new Intent(context, FindPwdActivity.class).putExtra("forget", "payment"));
                break;
            default:
                break;
        }
    }

	/*
     * private void isSetPaymentPassword() { RequestParams params = new
	 * RequestParams(); params.addBodyParameter("USERNAME",
	 * sp.getString(UserEntity.PHONE, "")); params.addBodyParameter("PASSWORD",
	 * sp.getString(UserEntity.PASSWORD, ""));
	 * 
	 * String url = getString(R.string.server_url) + URLMap.ISSETPAYMENTPAW;
	 * 
	 * LogUtils.i(url + CommonTools.getParams(params));
	 * 
	 * HttpUtils http = new HttpUtils(); http.send(HttpMethod.POST, url, params,
	 * new RequestCallBack<String>() {
	 * 
	 * @Override public void onStart() { super.onStart(); dialog.show(); }
	 * 
	 * @Override public void onFailure(HttpException arg0, String arg1) {
	 * Toast.makeText(context, R.string.request_failed,
	 * Toast.LENGTH_SHORT).show(); dialog.dismiss(); }
	 * 
	 * @Override public void onSuccess(ResponseInfo<String> arg0) { try {
	 * JSONObject obj = new JSONObject(arg0.result); String status =
	 * obj.getString("state"); if ("1".equals(status)) { JSONObject data =
	 * obj.getJSONObject("data"); String isSetPwd = data.getString("BESET"); if
	 * (!isSetPwd.equals("true")) { llOldPaymentPwd.setVisibility(View.GONE);
	 * isSetPaymentPwd = false; } else { isSetPaymentPwd = true; }
	 * LogUtils.i(data.toString()); } dialog.dismiss(); } catch (JSONException
	 * e) { e.printStackTrace(); } } });
	 * 
	 * }
	 */

    private void isSetPaymentPassword() {
        String isSetPaymentPD = sp.getString(UserEntity.IS_SET_PAYMENT_PWD, "0");
        LogUtils.i("isSetPaymentPD = " + isSetPaymentPD);
        if (isSetPaymentPD.equals("1")) {
            isSetPaymentPwd = true;
        } else {
            llOldPaymentPwd.setVisibility(View.GONE);
            isSetPaymentPwd = false;
            Toast.makeText(context, "还未设置支付密码!", Toast.LENGTH_SHORT).show();
        }
    }

}
