package cn.com.dkl.logistics.driver;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.adapter.MyEvaluateAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;

/**
 * 评论列表
 *
 * @author hzq
 */
public class MyEvaluateActivity extends BaseActivity {
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;

    @ViewInject(R.id.rBarPointLevel)
    private RatingBar rBarPointLevel;

    @ViewInject(R.id.tvRespecService)
    private TextView tvRespecService;

    @ViewInject(R.id.tvPunctualityRate)
    private TextView tvPunctualityRate;

    @ViewInject(R.id.tvBreachNum)
    private TextView tvBreachNum;

    @ViewInject(R.id.tvMyEvaluateNum)
    private TextView tvMyEvaluateNum;

    @ViewInject(R.id.listEvaluate)
    private PullToRefreshListView listEvaluate;

    private Context context = MyEvaluateActivity.this;
    private SharedPreferences sp;
    private Dialog progressDialog;
    private CommonTools tools = new CommonTools(context);
    private int currentPage = 1;
    private int pageCount;
    private MyEvaluateAdapter adapter;
    private boolean flag = true;
    /**
     * 一页显示数量
     */
    private static final int SHOW_COUNT = 10;

    private List<Map<String, String>> myData = new ArrayList<Map<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_evaluate);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
        currentPage = 1;
        myData.clear();
        adapter.notifyDataSetChanged();
        sendRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText("我的评价");
        progressDialog = tools.getProgressDialog(context, "正在查询中...");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        adapter = new MyEvaluateAdapter(context, myData);
        listEvaluate.setAdapter(adapter);
        listEvaluate.setOnRefreshListener(new OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    sendRequest();
                }
            }
        });
        listEvaluate.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
            @Override
            public void onLastItemVisible() {
                if (currentPage <= pageCount) {
                    if (flag) {
                        flag = false;
                        sendRequest();
                    }
                } else {
                    Toast.makeText(context, R.string.already_no_data, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendRequest() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("CURRENTPAGE", String.valueOf(currentPage));
        params.addQueryStringParameter("SHOWCOUNT", String.valueOf(SHOW_COUNT));
        params.addQueryStringParameter("DATATYPE", "1");

        String url = getString(R.string.server_url) + URLMap.ORDER_EVALUATE;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                flag = true;
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                flag = true;
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        if (listEvaluate.isRefreshing()) {
                            listEvaluate.onRefreshComplete();
                            myData.clear();
                        }
                        JSONObject data = obj.getJSONObject("data");
                        pageCount = data.getInt("PAGECOUNT");
                        JSONObject userInfo = data.getJSONObject("userInfo");
                        tvRespecService.setText("服务态度" + CommonTools.judgeNull(userInfo, "SERVICEATTITUDEPOINT", "5.0"));
                        rBarPointLevel.setNumStars((int) Double.parseDouble(CommonTools.judgeNull(userInfo, "LEVEL", "5")));
                        int totalCount = data.getInt("TOTALCOUNT");
                        LogUtils.i(data.toString());
                        JSONArray array = data.getJSONArray("orderComments");

                        for (int i = 0; i < array.length(); i++) {
                            Map<String, String> map = new HashMap<String, String>();
                            JSONObject item = array.getJSONObject(i);

                            String type = CommonTools.judgeNull(item, "SELLERCOMMENTLV", "0");
                            String typeName = "好评";
                            if ("0".equals(type)) {
                                typeName = "好评";
                            } else if ("1".equals(type)) {
                                typeName = "中评";
                            } else if ("2".equals(type)) {
                                typeName = "差评";
                            }
                            map.put("typeName", typeName);
                            map.put("UserName", CommonTools.judgeNull(item, "USERNAME", ""));
                            map.put("headPhotoURL", CommonTools.judgeNull(item, "HEADPHOTOURL", ""));
                            map.put("sellerCommentLV", CommonTools.judgeNull(item, "SERVICEATTITUDEPOINT", "5"));
                            map.put("sellerComment", CommonTools.judgeNull(item, "SELLERCOMMENT", ""));
                            map.put("date", CommonTools.judgeNull(item, "SELLERCOMMENTTIME", ""));
                            map.put("orderNo", CommonTools.judgeNull(item, "ORDER_NO", ""));
                            myData.add(map);
                        }
                        tvMyEvaluateNum.setText("我的评价（" + totalCount + "次）");
                        progressDialog.dismiss();
                        if (myData.size() > 0) {
                            adapter.notifyDataSetChanged();
                        }
                        // 当前页+1
                        currentPage += 1;
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }

            }
        });
    }

    @OnClick({R.id.btnLeft})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            default:
                break;
        }
    }

}
