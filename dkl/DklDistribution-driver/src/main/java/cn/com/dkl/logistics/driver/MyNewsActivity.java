package cn.com.dkl.logistics.driver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.iflytek.sunflower.FlowerCollector;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.fragment.BasicFragment;
import cn.com.dkl.logistics.fragment.MyNewsFragment;

/**
 * 消息列表 Created by LiuXing on 2016/12/12.
 */

public class MyNewsActivity extends FragmentActivity {

    @Bind(R.id.imgBack)
    ImageView mImgBack;

    private BasicFragment currentFragment = null;
    private Map<String, BasicFragment> mFragments;
    public static String currentTag = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_news);
        IntentFilter filter = new IntentFilter();
        filter.addAction("exitmynews");
        registerReceiver(receiver, filter);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("exitmynews")) {
                finish();
            }
        }
    };

    private void init() {

        // 存入页面
        mFragments = new HashMap<String, BasicFragment>();
        mFragments.put(MyNewsFragment.class.getName(), new MyNewsFragment());

        // 设置默认显示首页
        currentTag = MyNewsFragment.class.getName();
        showFragment(currentTag, null);

    }

    public void showFragment(String tag, Object dataObject) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        mFragments.get(currentTag).onPause();
        currentTag = tag;
        BasicFragment fragmentBeShow = mFragments.get(tag);
        fragmentBeShow.setData(dataObject);
        if (fragmentBeShow.isAdded()) {
            fragmentBeShow.onResume();
        } else {
            ft.add(R.id.news_content_frame, fragmentBeShow, tag);
        }

        ft.setCustomAnimations(R.anim.anim_slide_in_from_right, R.anim.anim_slide_out_to_left);
        Iterator<Map.Entry<String, BasicFragment>> fragmentInterator = mFragments.entrySet().iterator();
        while (fragmentInterator.hasNext()) {
            Map.Entry<String, BasicFragment> entry = fragmentInterator.next();
            BasicFragment fragment = (BasicFragment) entry.getValue();
            String tagTemp = entry.getKey();
            if (!tagTemp.equals(tag)) {
                ft.hide(fragment);
            } else {
                ft.show(fragment);
            }
        }
        ft.commit();

    }

    @OnClick(R.id.imgBack)
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBack:
                finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }
}
