package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.adapter.MyWalletAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;

/**
 * 我的钱包界面
 *
 * @author Dao
 */
public class MyWalletActivity extends BaseActivity {
    @ViewInject(R.id.ivLeft)
    private ImageView ivLeft;
    @ViewInject(R.id.tvBalance)
    private TextView tvBalance;
    @ViewInject(R.id.tvWithdrawCash)
    private TextView tvWithdrawCash;
    @ViewInject(R.id.ivBillSearch)
    private TextView ivBillSearch;
    @ViewInject(R.id.rbWithdrawCash)
    private RadioButton rbWithdrawCash;
    @ViewInject(R.id.rbCoupon)
    private RadioButton rbCoupon;

    @ViewInject(R.id.ptrListView)
    private PullToRefreshListView ptrListView;
    private MyWalletAdapter adapter;
    private List<Map<String, String>> dataList = new ArrayList<Map<String, String>>();

    private Context context = MyWalletActivity.this;
    private SharedPreferences sp;
    private static final String WITHDRAW_CASH = "withdraw_cash";
    private static final String COUPON = "coupon";
    private String searchType = WITHDRAW_CASH;
    private boolean flag = true;
    private int currentPage = 1;
    private int pageCount = 0;
    private final String SHOW_COUNT = "10";

    private String endTime;
    private String startTime;
    private String cardNo;

    private static final int AUTH = 101;//跳转认证
    private static final int BINDBACK = 102;//跳转绑定银行卡
    private static final int SETPAYPASSWORD = 103;//跳转设置支付密码

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
        //getUserCashInfo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        tvBalance.setText(sp.getString(UserEntity.WEB_BALANCE, "0.00"));
        initListView();
        initParams();
    }

    private void initListView() {
        adapter = new MyWalletAdapter(context, dataList);
        ptrListView.setAdapter(adapter);
        ptrListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if (flag) {
                    currentPage = 1;
                    initParams();
                }
            }
        });
        ptrListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (currentPage <= pageCount) {
                    if (flag) {
                        flag = false;
                        requestDatas();
                    }
                } else {
                    Toast.makeText(context, R.string.already_no_data, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @OnClick({R.id.ivLeft, R.id.tvWithdrawCash, R.id.rbWithdrawCash, R.id.rbCoupon, R.id.ivBillSearch})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft: // 返回按钮
                finish();
                break;
            case R.id.tvCoupon: // 优惠券
                Toast.makeText(context, "优惠券功能暂未开通！", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvWithdrawCash: // 提现
                procondition();
                break;
            case R.id.rbWithdrawCash: // 显示提现历史记录
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    initParams();
                }
                break;
            case R.id.rbCoupon: // 显示优惠券记录
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    initParams();
                }
                break;
            case R.id.ivBillSearch: // 账单查询
                startActivity(new Intent(context, BillQueryActivity.class));
                break;
            default:
                break;
        }
    }

    private void procondition() {
        String isStatus = sp.getString(UserEntity.STATUS, "0");
        String isSetPaymentPwd = sp.getString(UserEntity.IS_SET_PAYMENT_PWD, "0");
        String isBankCard = sp.getString(UserEntity.IS_BAND_BANKCARD, "0");
        Intent in = new Intent();
        if (isStatus.equals("0")) {
            Toast.makeText(context, "请先实名认证", Toast.LENGTH_SHORT).show();
            in.setClass(context, AuthActivity.class);
            startActivityForResult(in, AUTH);
        } else if (isStatus.equals("1")) {
            Toast.makeText(context, "认证审核中，请耐心等候！", Toast.LENGTH_SHORT).show();
        } else if (isStatus.equals("3")) {
            Toast.makeText(context, "认证未通过，请重新认证！", Toast.LENGTH_SHORT).show();
            in.setClass(context, AuthActivity.class);
            startActivityForResult(in, AUTH);
        } else if (isStatus.equals("2")) {
            if (!isBankCard.equals("1")) {
                in.setClass(context, BindBankCardAcitivity.class);
                startActivityForResult(in, BINDBACK);
            } else if (isSetPaymentPwd.equals("1")) {
                startActivity(new Intent(context, WithdrawCashActivity.class));
            } else {
                in.setClass(context, ModifypaymentPawActivity.class);
                startActivityForResult(in, SETPAYPASSWORD);
            }
        }
    }

    private void initParams() {
        if (rbWithdrawCash.isChecked()) {
            searchType = WITHDRAW_CASH;
        } else if (rbCoupon.isChecked()) {
            searchType = COUPON;
        }
        requestDatas();
    }

    public void requestDatas() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("SHOWCOUNT", SHOW_COUNT);
        params.addQueryStringParameter("CURRENTPAGE", String.valueOf(currentPage));

        String url = getString(R.string.server_url) + URLMap.GET_USER_CASH_LOG;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                flag = true;
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    flag = true;
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        if (ptrListView.isRefreshing()) { // 刷新显示中
                            ptrListView.onRefreshComplete();
                            dataList.clear();
                        }
                        if (rbCoupon.isChecked()) { // 当前无优惠券记录
                            Toast.makeText(context, "无优惠券使用记录！", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        JSONObject datas = obj.getJSONObject("data");
                        pageCount = (int) Double.parseDouble(CommonTools.judgeNull(datas, "PAGECOUNT", "0"));
                        JSONArray array = datas.getJSONArray("list");
                        Map<String, String> map;
                        for (int i = 0; i < array.length(); i++) {
                            map = new HashMap<String, String>();
                            JSONObject data = array.getJSONObject(i);
                            tvBalance.setText(CommonTools.judgeNull(data, "WEBBALANCE", "0.00"));
                            Iterator<String> it = data.keys();
                            while (it.hasNext()) {
                                String key = it.next();
                                String value = CommonTools.judgeNull(data, key, "");
                                map.put(key, value);
                            }
                            dataList.add(map);
                        }
                        adapter.notifyDataSetChanged();
                        currentPage++;
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            }

        });
    }

    private void getUserCashInfo() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_USERCASHINFO;

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        LogUtils.i(data.toString());
                        if (data != null) {
                            cardNo = CommonTools.judgeNull(data, "BANKCARD", "");
                        }
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == AUTH) {
                procondition();
            } else if (requestCode == BINDBACK) {
                procondition();
            } else if (requestCode == SETPAYPASSWORD) {
                procondition();
            }
        }
    }


}
