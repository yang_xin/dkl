package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.PointInfo;
import cn.com.dkl.logistics.fragment.ArroundCarFragment;
import cn.com.dkl.logistics.fragment.MainFragment;
import cn.com.dkl.logistics.service.LocationService;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.Constant;
import cn.com.dkl.logistics.view.CustomProgressDialog;

import static cn.com.dkl.logistics.utils.CommonTools.judgeNull;

/**
 * 新消息显示界面
 *
 * @author Dao
 */
public class NewMessageActivity extends BaseActivity {
    @Bind(R.id.ivUrgent)
    ImageView mIvUrgent;
    private Context context = NewMessageActivity.this;
    @ViewInject(R.id.tvTime)
    private TextView tvTime;
    @ViewInject(R.id.tvGoodsName)
    private TextView tvGoodsName;
    @ViewInject(R.id.tvGoodsNumber)
    private TextView tvGoodsNumber;
    @ViewInject(R.id.tvGoodsSize)
    private TextView tvGoodsSize;
    @ViewInject(R.id.tvGoodsWeight)
    private TextView tvGoodsWeight;
    @ViewInject(R.id.tvGoodsWorth)
    private TextView tvGoodsWorth;
    @ViewInject(R.id.btnTakeOrder)
    private Button btnTakeOrder;
    @ViewInject(R.id.tvCar)
    private TextView tvCar;

    @ViewInject(R.id.addressList)
    private ListView addressList;

    private CustomProgressDialog dialog;
    private SharedPreferences sp;
    /**
     * 出车状态:1 出车，0 收车
     */
    private String carStatus;

    /**
     * 标志从哪里启动该界面
     */
    private String from;
    private String orderNo;
    private String isUegent;
    private String userMsgID;
    private Bundle bundle;
    private String orderState = "0";

    private List<PointInfo> list = new ArrayList<PointInfo>();
    private AddressAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_message);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);

        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        dialog = new CommonTools(context).getProgressDialog(context, "获取消息详情...");

        bundle = getIntent().getExtras();
        from = getIntent().getExtras().getString("From");
        if ("Main".equals(from)) { // 从MainFragment启动
            setView(bundle);
            userMsgID = getIntent().getExtras().getString("USERMESSAGE_ID");
            orderNo = bundle.getString("WAYBILL_NUMBER");
        } else if ("OrderList".equals(from)) { // 来自订单列表
            orderNo = getIntent().getExtras().getString("ORDER_NO");
            getOrderDetail();
        } else {
            orderNo = bundle.getString("ORDERNO");
            userMsgID = bundle.getString("userMessageID");
            getOrderDetail();
        }

    }

    @SuppressLint("NewApi")
    private void setView(Bundle bundle) {
        tvTime.setText(bundle.getString("CREATE_DATE", CommonTools.getDateTime()));
        tvCar.setText(bundle.getString("CarNumber",""));
        try {
            JSONArray jArray = new JSONArray(bundle.getString("deliverList"));
            JSONArray orderListArray = new JSONArray(bundle.getString("orderList"));
            list.clear();
            list.size();
            List<PointInfo> listEnd = new ArrayList<PointInfo>();
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject obj = jArray.getJSONObject(i);
                PointInfo pInfo = new PointInfo();
                pInfo.setContactAddress(obj.getString("CONTACT_ADDRESS"));
                pInfo.setProvince(obj.getString("PROVINCE"));
                pInfo.setCity(obj.getString("CITY"));
                pInfo.setDistrict(obj.getString("DISTRICT"));
                String alias = CommonTools.judgeNull(obj,"ADDR_ALIAS","");
                pInfo.setAlias(alias);
                pInfo.setIsFirst(obj.getInt("IS_FIRST"));
                pInfo.setIsLast(obj.getInt("IS_LAST"));
                pInfo.setSort(obj.getInt("SORT"));
                pInfo.setType(obj.getInt("TYPE"));
                pInfo.setWaybillid(bundle.getString("WAYBILL_ID"));
                String Orderno = judgeNull(obj, "ORDER_NO", "");

                //IsFirst()  是否为起点，等于1为起点
                if (pInfo.getIsFirst() == 1) {
                    list.add(pInfo);
//                    if (i < 1) {
//                    }
                } else {
                    for (int k=0;k<orderListArray.length();k++){
                        JSONObject orderObject = orderListArray.getJSONObject(k);
                        if (Orderno.equals(judgeNull(orderObject, "order_no", ""))){
                            pInfo.setPayway(judgeNull(orderObject, "pay_way", ""));
                            pInfo.setTotalcost(judgeNull(orderObject, "total_cost", ""));
                            pInfo.setRemarks(judgeNull(orderObject, "remarks", ""));
                            pInfo.setTransportway(judgeNull(orderObject, "TRANSPORT_WAY", ""));
                            pInfo.setGoodsname(judgeNull(orderObject, "GOODS_NAME", ""));
                            String goodstype = judgeNull(orderObject, "GOODS_TYPE", "");
                            String goodstypeSecond = judgeNull(orderObject, "GOODS_TYPE_SECOND", "");
                            String goodstypeThird = judgeNull(orderObject, "GOODS_TYPE_THIRD", "");
                            pInfo.setGoodstype(goodstype);
                            pInfo.setGoodsweight(judgeNull(orderObject, "WEIGHT", ""));
                            pInfo.setWeightunit(judgeNull(orderObject, "WEIGHT_UNIT", "吨"));
                            pInfo.setGoodsvolume(judgeNull(orderObject, "VOLUMN", ""));
                            pInfo.setVolumeunit(judgeNull(orderObject, "VOLUMN_UNIT", "方"));
                            pInfo.setWorth(judgeNull(orderObject, "WORTH", ""));
                            pInfo.setPieceno(judgeNull(orderObject, "PIECE_NO", ""));
                            pInfo.setPackagetype(judgeNull(orderObject, "PACKAGE_TYPE", "件"));

                            pInfo.setIsInsuredtransport(judgeNull(orderObject, "is_insuredtransport", ""));
                            pInfo.setInsuredtransportCost(judgeNull(orderObject, "insuredtransport_cost", ""));
                            pInfo.setIsPaymentcollection(judgeNull(orderObject, "is_paymentcollection", ""));
                            pInfo.setPaymentcollectionCost(judgeNull(orderObject, "paymentcollection_cost", ""));
                            pInfo.setIsDelivery(judgeNull(orderObject, "is_delivery", ""));
                            pInfo.setDeliveryCost(judgeNull(orderObject, "delivery_cost", ""));
                            pInfo.setIsUnloading(judgeNull(orderObject, "is_unloading", ""));
                            pInfo.setIsUpstairs(judgeNull(orderObject, "is_upstairs", ""));
                            pInfo.setIsLoading(judgeNull(orderObject, "is_loading", ""));
                        }
                    }
                    listEnd.add(pInfo);
                }
            }
            list.addAll(listEnd);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter = new AddressAdapter(context, list);
        addressList.setAdapter(adapter);
        tvGoodsName.setText(bundle.getString("GOODS_NAME", ""));
//        Bundle[{NUMBER=32, STATUS=0, VOLUME=42.0, WEIGHT=32.0
        tvGoodsNumber.setText(bundle.getString("NUMBER", "0") + "件");
        tvGoodsSize.setText(bundle.getString("VOLUME", "0") + bundle.getString("VOLUMN_UNIT", "方"));
        tvGoodsWeight.setText(bundle.getString("WEIGHT", "0") + bundle.getString("WEIGHT_UNIT", "吨"));
//        tvGoodsWorth.setText(bundle.getString("TOTAL_COST", "0") + "元");
        tvGoodsWorth.setText(bundle.getString("DRIVERTOTALCOST", "0") + "元");
        if ("1".equals(bundle.getString("ISURGENT", "0"))){
            mIvUrgent.setVisibility(View.VISIBLE);
        }
        orderState = bundle.getString("STATUS", "0");
        String size = bundle.getString("VOLUMN", "1");
        if (orderState.equals("-1")) {
            btnTakeOrder.setText("已取消");
        } else if (orderState.equals("-7")) {
            btnTakeOrder.setText("已放空");
        } else if (orderState.equals("-3")) {
            btnTakeOrder.setText("已作废");
        } else if (orderState.equals("-5")) {
            btnTakeOrder.setText("已撤单");
        } else if (!orderState.equals("1")) {
            btnTakeOrder.setText("已接单");
        }
    }


    /*
     * 判断服务是否启动,context上下文对象 ，className服务的name
     */
//    public static boolean isServiceRunning(Context mContext, String className) {
//
//        boolean isRunning = false;
//        ActivityManager activityManager = (ActivityManager) mContext
//                .getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningServiceInfo> serviceList = activityManager
//                .getRunningServices(30);
//
//        if (!(serviceList.size() > 0)) {
//            return false;
//        }
//
//        for (int i = 0; i < serviceList.size(); i++) {
//            if (serviceList.get(i).service.getClassName().equals(className) == true) {
//                isRunning = true;
//                break;
//            }
//        }
//        return isRunning;
//    }
    private boolean isServiceRunning(Context mContext, String className) {
        ActivityManager myManager = (ActivityManager) mContext
                .getApplicationContext().getSystemService(
                        Context.ACTIVITY_SERVICE);
        ArrayList<ActivityManager.RunningServiceInfo> runningService = (ArrayList<ActivityManager.RunningServiceInfo>) myManager
                .getRunningServices(30);
        for (int i = 0; i < runningService.size(); i++) {
            if (runningService.get(i).service.getClassName().toString()
                    .equals(className)) {
                return true;
            }
        }
        return false;
    }
    /**
     * 获取订单详细信息
     */
    private void getOrderDetail() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);

        String url = getString(R.string.server_url) + URLMap.CDSORDER_DETAIL;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                dialog.dismiss();
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(arg0.result);

                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        Iterator<String> it = data.keys();
                        Bundle bundle = new Bundle();
                        while (it.hasNext()) {
                            String key = it.next();
                            String value = data.getString(key);
                            bundle.putString(key, value);
                        }


                        setView(bundle);
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            }
        });
    }

    @OnClick({R.id.btnTakeOrder})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTakeOrder:
                if (orderState.equals("-5")) {
                    startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", orderNo));
                    finish();
                } else if (orderState.equals("-7")) {
                    startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", orderNo));
                    finish();
                } else if (orderState.equals("-1")) {
                    Toast.makeText(context, "订单已取消！", Toast.LENGTH_LONG).show();
                    finish();
                } else if (orderState.equals("-3")) {
                    startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", orderNo));
                    finish();
                } else if (!orderState.equals("1")) {
                    startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", orderNo));
                    finish();
                } else {
                    carStatus = sp.getString(UserEntity.CAR_STATUS, "0");
                    String issocialuser = sp.getString(UserEntity.ISSOCIALUSER, "0");
                    if("0".equals(issocialuser)){
                        btnTakeOrder.setClickable(false);
                        takeOrder();
                    }else {
                        if (carStatus.equals("1")) {
                            btnTakeOrder.setClickable(false);
                            takeOrder();
                        } else {
                            Toast.makeText(context, "非出车状态不能接单！", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                break;
            default:
                break;
        }
    }

    private void takeOrder() {
        RequestParams params = new RequestParams();
        String waybillId = list.get(0).getWaybillid();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
        params.addQueryStringParameter("RESULTTYPE", "1");
        params.addQueryStringParameter("WAYBILL_ID", waybillId);

        params.addQueryStringParameter("TRACE_LON", sp.getString(Constants.LONGTITUDE, ""));//上传位置
        params.addQueryStringParameter("TRACE_LAT", sp.getString(Constants.LATITUDE, ""));//上传位置
        params.addQueryStringParameter("TRACE_PROVINCE", sp.getString(UserEntity.CURRENT_PROVINCE, ""));//上传位置
        params.addQueryStringParameter("TRACE_CITY", sp.getString(UserEntity.CURRENT_CITY, ""));//上传位置
        params.addQueryStringParameter("TRACE_DISTRICT", sp.getString(UserEntity.CURRENT_DISTRICT, ""));//上传位置
        params.addQueryStringParameter("TRACE_ADDRESS", sp.getString(UserEntity.CURRENT_STREET, "") + sp.getString(UserEntity.CURRENT_STREETNUMBER, ""));//上传位置

        String url = getString(R.string.server_url) + URLMap.PROCESS_CDSORDER;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                // Toast.makeText(context, "发送请求接单失败！",
                // Toast.LENGTH_SHORT).show();
                btnTakeOrder.setClickable(true);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        if (!TextUtils.isEmpty(userMsgID)) { // 通知存在，清除
                            // doDeleteMessage();
                        }
//                        startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", orderNo).putExtra("ISSMS", "1"));
                        startActivity(new Intent(context, DoneListActivity.class));
                        // 更新按钮状态
                        Editor edit = sp.edit();
                        edit.putString(UserEntity.CAR_STATUS, MainFragment.OUT_CAR);
                        edit.putString(UserEntity.LOAD_STATUS, MainFragment.FULL_LOAD);
                        edit.commit();

                        // 自有车辆，如果没有上传定位信息则开始上传定位信息
                        String issocialuser = sp.getString(UserEntity.ISSOCIALUSER, "0");
                        if ("0".equals(issocialuser)) {
                            if (!isServiceRunning(context, "cn.com.dkl.logistics.service.LocationService")) {
                                context.startService(new Intent(context, LocationService.class));
                            }
                        }
                        if ("OrderList".equals(from)) { // 来自订单列表
                            sendBroadcast(new Intent(Constant.WAYBILLTAKEORDER).putExtra(ArroundCarFragment.WAYBILLTAKEORDER,orderNo));
                            finish();
                        } else {
                            isReadNews();
                            finish();
                        }

                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        btnTakeOrder.setClickable(true);
                        finish();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                    btnTakeOrder.setClickable(true);
                }
            }
        });
    }

    /**
     * 弹窗已读消息
     */
    private void isReadNews(){
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("USERMESSAGE_ID", userMsgID);

        String url = getString(R.string.server_url) + URLMap.IS_READ_MESSAGE;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                    Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
                }
            }

        });

    }



    private void doDeleteMessage() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("USERMESSAGE_ID", userMsgID);

        String url = getString(R.string.server_url) + URLMap.DELETE_MY_MESSAGE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i(arg0.toString() + arg1);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);

                    LogUtils.i(obj.toString());
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }

    /**
     * 内部类
     */
    public class AddressAdapter extends BaseAdapter {
        private Context context;
        private List<PointInfo> lists;
        private LayoutInflater inflater;

        public AddressAdapter(Context contexts, List<PointInfo> lists) {
            this.context = contexts;
            this.lists = lists;
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<PointInfo> lists) {
            this.lists = lists;
        }

        @Override
        public int getCount() {
            return lists.size();
        }

        @Override
        public Object getItem(int position) {
            return lists.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressWarnings("deprecation")
        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (null == convertView) {
                convertView = inflater.inflate(R.layout.list_view_address, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            PointInfo pointInfo = lists.get(position);
            if (pointInfo.getIsFirst() == 1) {
                holder.llAddService.setVisibility(View.GONE);
                if (getCount()>2){
                    int point = position + 1;
                    holder.tvPoint.setText("起点" + point + "：");
                }else {
                    holder.tvPoint.setText("起点：");
                }
            } else if (pointInfo.getIsLast() == 1) {
                holder.llAddService.setVisibility(View.VISIBLE);
                if (getCount()>2){
                    int point = (position - getCount()/2 + 1);
                    holder.tvPoint.setText("终点"+ point +"：");
                }else {
                    holder.tvPoint.setText("终点：");
                }
            }
            String addService = "";
            if (pointInfo.getIsInsuredtransport().equals("1")){
                addService = isAddService(addService);
                addService += getString(R.string.is_insuredtransport);
            }
            if (pointInfo.getIsPaymentcollection().equals("1")){
                addService = isAddService(addService);
                addService += getString(R.string.is_paymentcollection);
            }
            if (pointInfo.getIsDelivery().equals("1")){
                addService = isAddService(addService);
                addService += getString(R.string.is_delivery);
            }
            if (pointInfo.getIsUnloading().equals("1")){
                addService = isAddService(addService);
                addService += getString(R.string.is_unloading);
            }
            if (pointInfo.getIsUpstairs().equals("1")){
                addService = isAddService(addService);
                addService += getString(R.string.is_upstairs);
            }
            if (pointInfo.getIsLoading().equals("1")){
                addService = isAddService(addService);
                addService += getString(R.string.is_loading);
            }
            /*else if (pointInfo.getType() == 1) {
                holder.tvPoint.setText("提货：");
            } else if (pointInfo.getType() == 2) {
                holder.tvPoint.setText("卸货：");
            }*/
            if (addService.equals("")){
                holder.llAddService.setVisibility(View.GONE);
            }else {
                holder.llAddService.setVisibility(View.VISIBLE);
            }
            holder.tvAddService.setText(addService);
            if (!pointInfo.getAlias().equals("") && pointInfo.getAlias()!=null) {
//                holder.tvAddress.setText(pointInfo.getAlias());
                holder.tvAddress.setText(pointInfo.getProvince() + pointInfo.getCity() + pointInfo.getDistrict() + pointInfo.getContactAddress());
            } else {
//                holder.tvAddress.setText(pointInfo.getContactAddress());
                holder.tvAddress.setText(pointInfo.getProvince() + pointInfo.getCity() + pointInfo.getDistrict() + pointInfo.getContactAddress());
            }
            return convertView;
        }

        private String isAddService(String str){
            if (!str.equals("")){
                str += "/";
            }
            return str;
        }

        class ViewHolder {
            private TextView tvAddress, tvPoint, tvAddService;
            private LinearLayout llAddService;

            public ViewHolder(View v) {
                tvPoint = (TextView) v.findViewById(R.id.tvPoint);
                tvAddress = (TextView) v.findViewById(R.id.tvAddress);
                llAddService = (LinearLayout) v.findViewById(R.id.llAddService);
                tvAddService = (TextView) v.findViewById(R.id.tvAddService);
            }
        }
    }

}
