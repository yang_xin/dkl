package cn.com.dkl.logistics.driver;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONObject;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;

public class NewsDetailsActivity extends BaseActivity {
    private Context mContext = NewsDetailsActivity.this;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;

    @ViewInject(R.id.llDelete)
    private LinearLayout llDelete;
    @ViewInject(R.id.btnDelete)
    private Button btnDelete;

    @ViewInject(R.id.tvNewsDetail)
    private TextView tvNewsDetail;
    private Dialog progressDialog;
    private CommonTools tools = new CommonTools(this);
    private String callBackCode = "";
    private String callBackMessage = "";
    private String callBackState = "";
    private String userMessageID = "";
    private boolean isDelete = false;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void initView() {
        ViewUtils.inject(this);
        progressDialog = tools.getProgressDialog(mContext, getString(R.string.loading));
        Intent intent = getIntent();
        // if (null != intent && intent.getExtras() != null &&
        // getIntent().getExtras().getString(JPushInterface.EXTRA_ALERT) !=
        // null) {
        // Bundle bundle = getIntent().getExtras();
        // String title =
        // bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
        // String content = bundle.getString(JPushInterface.EXTRA_ALERT);
        // tvTitle.setText(title);
        // tvNewsDetail.setText(content);
        // btnDelete.setVisibility(View.INVISIBLE);
        // }else{}
        if (intent.getExtras() != null) {
            if (intent.getStringExtra("jpush") != null && !intent.getStringExtra("jpush").equals("")) {
                llDelete.setVisibility(View.GONE);
            }
            tvTitle.setText(R.string.title_news_detail);
            userMessageID = getIntent().getStringExtra("userMessageID");
            requestData();
        }

    }

    @OnClick({R.id.btnLeft})
    public void View(View v) {
        finish();
    }

    /***
     * 提交数据
     * 获取我的消息详情
     */
    public void requestData() {

        progressDialog.show();
        String userName = sp.getString(UserEntity.PHONE, "");
        String password = sp.getString(UserEntity.PASSWORD, "");
        // 传入需要的参数
        final RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERMESSAGE_ID", userMessageID);
        params.addQueryStringParameter("USERNAME", userName);
        params.addQueryStringParameter("PASSWORD", password);

        String url = "";
        if (isDelete) {
            url = getString(R.string.server_url) + URLMap.DELETE_MY_MESSAGE;
        } else {
            url = getString(R.string.server_url) + URLMap.MY_MESSAGE_RESULT_DETAILS;
            params.addQueryStringParameter("SCANTYPE", getString(R.string.SCANTYPE));
        }
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);// 设置超时

        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                JSONObject jsonObject = null;

                try {
                    if (TextUtils.isEmpty(responseInfo.result)) {
                        Toast.makeText(mContext, getString(R.string.server_connected_failed), Toast.LENGTH_SHORT);
                        return;
                    } else {
                        jsonObject = new JSONObject(responseInfo.result);
                        LogUtils.i("responseInfo= " + responseInfo.result);
                        callBackMessage = jsonObject.getString("message");
                        callBackState = jsonObject.getString("state");
                        callBackCode = jsonObject.getString("code");
                        LogUtils.i("callBackMessage= " + callBackMessage + "\n" + "callBackState= " + callBackState + "\n" + "callBackCode= " + callBackCode);
                        if (isDelete) {
                            if (callBackState.equals("1")) {
                                Toast.makeText(mContext, "删除成功！", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                if (callBackMessage.isEmpty()) {
                                    Toast.makeText(mContext, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(mContext, callBackMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            if (!TextUtils.isEmpty(callBackMessage)) {
                                Toast.makeText(mContext, getString(R.string.loading_failed), Toast.LENGTH_SHORT).show();
                            } else {
                                JSONObject jbData = jsonObject.optJSONObject("data");
                                String content = CommonTools.judgeNull(jbData, "CONTENT", "");
                                if (content.equals("")) {
                                    tvNewsDetail.setText(Html.fromHtml(jbData.getString("TITLE") + "<br>" + "<br>" + jbData.getString("SUMMARY")));
                                } else {
                                    tvNewsDetail.setText(Html.fromHtml(jbData.getString("TITLE") + "<br>" + "<br>" + jbData.getString("CONTENT")));
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(mContext, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(HttpException error, String msg) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CommonTools.failedToast(mContext);
            }
        });
    }

    @OnClick(value = {R.id.btnLeft, R.id.btnDelete})
    public void onClicked(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnDelete:
                warningDelete();
                break;
            default:
                break;
        }
    }

    /**
     * 删除警告弹出框
     *
     * @author guochaohui
     */
    public void warningDelete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("警告");
        builder.setMessage("请确认是否删除该消息？");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDelete = true;
                requestData();

            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        builder.create().show();
    }

}
