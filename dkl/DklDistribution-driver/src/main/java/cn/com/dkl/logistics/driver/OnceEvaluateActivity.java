package cn.com.dkl.logistics.driver;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.view.CircleImageView;

/**
 * 单独订单的评价
 *
 * @author hzq
 */
public class OnceEvaluateActivity extends BaseActivity {

    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.imgUserImg)
    private CircleImageView imgUserImg;
    @ViewInject(R.id.tvUserName)
    private TextView tvUserName;
    @ViewInject(R.id.tvRatingNum)
    private TextView tvRatingNum;
    @ViewInject(R.id.tvEvaContent)
    private TextView tvEvaContent;
    @ViewInject(R.id.tvEvaDate)
    private TextView tvEvaDate;
    @ViewInject(R.id.tvOrderNumber)

    private TextView tvOrderNumber;
    private SharedPreferences sp;
    private Context context = OnceEvaluateActivity.this;
    private String orderNo;
    private String user_headPhotouURL;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onec_evaluate);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText("查看评价");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        orderNo = getIntent().getExtras().getString("OrderNo");
        progressDialog = new CommonTools(context).getProgressDialog(context, "正在查询中...");
        requestData();

    }

    protected void downLoadHeadImg(final String imageName) {
        if (TextUtils.isEmpty(imageName)) {
            return;
        }
        String url = getString(R.string.server_imgurl) + "upload/" + imageName;
        //String url = getString(R.string.server_url) + "upload/" + imageName;
        final String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.download(url, imagePath, true, true, new RequestCallBack<File>() {

            @Override
            public void onSuccess(ResponseInfo<File> arg0) {
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                if (null != bitmap) {
                    imgUserImg.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("下载用户头像失败：" + arg0.getMessage() + arg1);
            }
        });
    }

    private void requestData() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);

        String url = getString(R.string.server_url) + URLMap.CDSORDER_EVALUATE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");

                    if ("1".equals(status)) {
                        JSONObject data = obj.getJSONObject("data");
                        tvUserName.setText(CommonTools.judgeNull(data, "SHIPPER_USERAME", ""));
                        String type = CommonTools.judgeNull(data, "SHIPPER_COMMENT_LEVEL", "0");
                        String typeName = "好评";
                        if ("1".equals(type)) {
                            typeName = "好评";
                        } else if ("2".equals(type)) {
                            typeName = "中评";
                        } else if ("3".equals(type)) {
                            typeName = "差评";
                        }
                        tvRatingNum.setText(("给我" + (int) Double.parseDouble(CommonTools.judgeNull(data, "SERVICE_ATTITUDE_POINT", "5")) + "星" + typeName));
                        tvEvaContent.setText(CommonTools.judgeNull(data, "SHIPPER_COMMENT_CONTENT", ""));
                        tvEvaDate.setText(CommonTools.judgeNull(data, "SHIPPER_COMMENT_TIME", ""));
                        tvOrderNumber.setText("订单编号：" + CommonTools.judgeNull(data, "ORDER_NO", ""));
                        LogUtils.i(data.toString());

                        user_headPhotouURL = CommonTools.judgeNull(data, "SHIPPER_HEADPHOTOURL", "");
                        if (!TextUtils.isEmpty(user_headPhotouURL)) {
                            String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + user_headPhotouURL;
                            File file = new File(imagePath);
                            if (file.exists()) {
                                Bitmap bitmap = BitmapFactory.decodeFile(file.toString());
                                if (null != bitmap) {
                                    imgUserImg.setImageBitmap(bitmap);
                                }
                            } else {
                                downLoadHeadImg(user_headPhotouURL);
                            }
                        }
                    } else {
                        Toast.makeText(context, "获取信息失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                progressDialog.dismiss();
            }
        });

    }

    @OnClick({R.id.btnLeft})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            default:
                break;
        }
    }

}
