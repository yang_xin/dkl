package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tencent.stat.StatService;

import java.util.HashMap;
import java.util.Map;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.fragment.ArroundCarFragment;
import cn.com.dkl.logistics.fragment.OrderDetailFragment;
import cn.com.dkl.logistics.fragment.ReceiptVoucherFragment;
import cn.com.dkl.logistics.fragment.TransportPathFragment;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.Constant;
import cn.com.dkl.logistics.utils.ImageUtils;

/**
 * 订单详情
 * @author txw
 */
@SuppressLint("NewApi")
public class   OrderDetailActivity extends FragmentActivity {

    private Context context = OrderDetailActivity.this;
    @ViewInject(R.id.ivLeft)
    private ImageView ivLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;

    @ViewInject(R.id.rgFragment)
    private RadioGroup rgFragment;
    @ViewInject(R.id.rbOderInfo)
    private RadioButton rbOderInfo;
    @ViewInject(R.id.rbTransportPath)
    private RadioButton rbTransportPath;
    @ViewInject(R.id.rbReceiptVoucher)
    private RadioButton rbReceiptVoucher;

    @ViewInject(R.id.llOrderInfo)
    private LinearLayout llOrderInfo;
    @ViewInject(R.id.llTransportPath)
    private LinearLayout llTransportPath;
    @ViewInject(R.id.llReceiptVoucher)
    private LinearLayout llReceiptVoucher;
    private boolean isFirst = true;
    private boolean isFirstSms = true;
    public static boolean isSelectDetail = true;

    public Map<String, Fragment> mapFragment = null;
    private int checkedId;

    private SharedPreferences sp;
    private String orderNo;

    OnUpLoadTrstImageListener mOnUpLoadTrstImageListener;
    public interface OnUpLoadTrstImageListener{
        /**
         *
         * @param orderNo
         */
        void uploadImage(String orderNo);
    }

    public OnUpLoadTrstImageListener getmOnUpLoadTrstImageListener() {
        return mOnUpLoadTrstImageListener;
    }

    public void setmOnUpLoadTrstImageListener(OnUpLoadTrstImageListener mOnUpLoadTrstImageListener) {
        this.mOnUpLoadTrstImageListener = mOnUpLoadTrstImageListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        IntentFilter filter = new IntentFilter();
        filter.addAction("finish");
        registerReceiver(receiver, filter);
        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        StatService.onResume(this);
        if (getIntent().getStringExtra("ISSMS") != null && isFirstSms) {
            isFirstSms = false;
            orderNo = getIntent().getStringExtra("OrderNo");
            isSms();
        }

        FlowerCollector.onResume(this);
        if (mOnUpLoadTrstImageListener != null && getIntent().getIntExtra("flag", Constant.NON) ==
                Constant.UPLOADTRSTIMAGE){
            mOnUpLoadTrstImageListener.uploadImage(getIntent().getStringExtra("ORDERNO"));
        }
        if (mOnUpLoadTrstImageListener != null && getIntent().getIntExtra("flag", Constant.NON) ==
                Constant.UPLOADTRSTIMAGE){
            mOnUpLoadTrstImageListener.uploadImage(getIntent().getStringExtra("ORDERNO"));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        StatService.onPause(this);
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        ImageUtils.picPath = null;
    }

    private static final String ORDER_INFO = "OrderDetailFragment";
    private static final String TRANSPORT_PATH = "TransportPathFragment";
    private static final String RECEIPT_VOUCHER = "ReceiptVoucherFragment";

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("finish")) {
                finish();
            }
        }
    };

    private void init() {
        ViewUtils.inject(this);

        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        mapFragment = new HashMap<String, Fragment>();
        mapFragment.put(ORDER_INFO, new OrderDetailFragment());
        mapFragment.put(TRANSPORT_PATH, new TransportPathFragment());
        mapFragment.put(RECEIPT_VOUCHER, new ReceiptVoucherFragment());

        String to = getIntent().getExtras().getString("ToPage");
        if (RECEIPT_VOUCHER.equals(to)) {
            rbReceiptVoucher.setChecked(true);
            setFragment();
        } else {
            setFragment();
        }
    }

    public void setFragment() {
        int fragmentId = 0;
        Fragment fragment = null;
        checkedId = rgFragment.getCheckedRadioButtonId();
        switch (checkedId) {
            case R.id.rbOderInfo:
                fragmentId = R.id.llOrderInfo;
                fragment = mapFragment.get(ORDER_INFO);
                //ImageUtils.picPath = null;
                viewVisible();
                break;
            case R.id.rbTransportPath:
                fragmentId = R.id.llTransportPath;
                fragment = mapFragment.get(TRANSPORT_PATH);
                viewVisible();
                break;
            case R.id.rbReceiptVoucher:
                fragmentId = R.id.llReceiptVoucher;
                fragment = mapFragment.get(RECEIPT_VOUCHER);
                //ImageUtils.picPath = null;
                if (isFirst) {
                    isFirst = false;
                } else {
                    ((ReceiptVoucherFragment) fragment).getReceitImageName();
                }
                viewVisible();
                break;
            default:
                break;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(fragmentId, fragment);
        ft.commit();
    }

    private void viewVisible() {
        switch (checkedId) {
            case R.id.rbOderInfo:
                llOrderInfo.setVisibility(View.VISIBLE);
                llTransportPath.setVisibility(View.GONE);
                llReceiptVoucher.setVisibility(View.GONE);
                break;
            case R.id.rbTransportPath:
                llOrderInfo.setVisibility(View.GONE);
                llTransportPath.setVisibility(View.VISIBLE);
                llReceiptVoucher.setVisibility(View.GONE);
                break;
            case R.id.rbReceiptVoucher:
                llOrderInfo.setVisibility(View.GONE);
                llTransportPath.setVisibility(View.GONE);
                llReceiptVoucher.setVisibility(View.VISIBLE);

                break;
            default:
                break;
        }
    }

    @OnClick({R.id.ivLeft, R.id.rbOderInfo, R.id.rbTransportPath, R.id.rbReceiptVoucher})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft: // 返回
                endFinish();
                break;
            case R.id.rbOderInfo: // 订单信息
                isSelectDetail = true;
                setFragment();
                break;
            case R.id.rbTransportPath: // 运输轨迹
                setFragment();
                break;
            case R.id.rbReceiptVoucher: // 回单凭证
                isSelectDetail = false;
                setFragment();
                break;
            default:
                break;
        }
    }

    private void isSms() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
        params.addQueryStringParameter("ISSMS", "1");

        String url = getString(R.string.server_url) + URLMap.SEND_PROCESS_CDSORDER;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageUtils.isHeadImage = true;
        ImageUtils.uploadImageName = "IMG_receipt";
        switch (requestCode) {
            case ImageUtils.GET_IMAGE_BY_CAMERA: // 拍照获取图片
                if (null != ImageUtils.imageUriFromCamera && resultCode != RESULT_CANCELED) {
                    boolean converResult = ImageUtils.converImagePath(OrderDetailActivity.this,ImageUtils.imageUriFromCamera);
                    if (converResult){
                        OrderDetailFragment.setImageInfo(ImageUtils.imageUriFromCamera);
                    }
                   /* LogUtils.e("isSelectDetail" + isSelectDetail);
                    ImageUtils.uriToPath(this, ImageUtils.imageUriFromCamera);
                    if (isSelectDetail) {
                        OrderDetailFragment.setImageInfo();
                    } else {
                        ReceiptVoucherFragment.setImageInfo();
                    }*/
                }
                break;
            case ImageUtils.GET_IMAGE_FROM_PHONE: // 手机相册获取图片
                if (null != data && null != data.getData()) {
                    if (null != data && null != data.getData()) {
                        boolean converResult = ImageUtils.converImagePath(OrderDetailActivity.this,data.getData());
                        if (converResult){
                            OrderDetailFragment.setImageInfo(data.getData());
                        }
                    }
                    /*ImageUtils.uriToPath(this, data.getData());
                    if (isSelectDetail) {
                        OrderDetailFragment.setImageInfo();
                    } else {
                        ReceiptVoucherFragment.setImageInfo();
                    }*/
                }
                break;
            default:
                break;
        }
    }

   void endFinish(){
       //告诉主页面，我是运单详情，不要更改我的状态
       Intent intent = new Intent();
       intent.putExtra("flag", ArroundCarFragment.ORDERDEAIL);
       setResult(RESULT_OK,intent);
       finish();
   }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        endFinish();
    }
}
