package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.com.dkl.logistics.adapter.OrderListAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.view.CustomProgressDialog;

/**
 * 订单列表
 *
 * @author txw
 */
@SuppressLint("NewApi")
public class  OrderListActivity extends FragmentActivity {
    private static final String TAG = "OrderListActivity";
    @ViewInject(R.id.ivLeft)
    private ImageView ivLeft;
    @ViewInject(R.id.rbTransaction)
    private RadioButton rbTransaction;
    @ViewInject(R.id.rbFinished)
    private RadioButton rbFinished;
    @ViewInject(R.id.ptrListView)
    private PullToRefreshListView ptrListView;

    private Context context = OrderListActivity.this;
    private SharedPreferences sp;
    private int currentPage = 1;
    private int pageCount = 0;
    /**
     * 一页显示数量
     */
    private static final int SHOW_COUNT = 50;
    /**
     * 1买家，2卖家
     */
    private static final String DATA_TYPE = "1";
    /**
     * 已完成订单
     */
    private final String FINISHED = "1";
    /***
     * 交易中订单
     */
    private final String UNFINISHED = "2";
    /**
     * 订单完成状态
     */
    private String isFinished = UNFINISHED;

    private List<Map<String, String>> listOrder = new ArrayList<Map<String, String>>();
    private OrderListAdapter adapter;
    private CustomProgressDialog dialog = null;
    private boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        IntentFilter filter = new IntentFilter();
        filter.addAction("finish");
        registerReceiver(receiver, filter);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
        currentPage = 1;
        listOrder.clear();
        adapter.notifyDataSetChanged();
        sendRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("finish")) {
                finish();
            }
        }
    };

    private void init() {
        ViewUtils.inject(this);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        dialog = new CommonTools(context).getProgressDialog(context, "获取订单中...");

        initListView();

    }

    private void initListView() {
        adapter = new OrderListAdapter(context, listOrder);
        ptrListView.setAdapter(adapter);

        ptrListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String state = listOrder.get(position-1).get("STATUS");
                if ("1".equals(state)){
                    startActivity(new Intent(context, NewMessageActivity.class).putExtra("From", "OrderList")
                            .putExtra("ORDER_NO",listOrder.get(position - 1).get("WAYBILL_NUMBER")));
                }else {
                    startActivity(new Intent(context, OrderDetailActivity.class).putExtra
                            ("OrderNo", listOrder.get(position - 1).get("WAYBILL_NUMBER")).
                            putExtra("URGENT",listOrder.get(position - 1).get("ISURGENT")));
                }



            }
        });


        ptrListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                if (flag) {
                    flag = false;
                    currentPage = 1;
                    sendRequest();
                }
            }

        });
        ptrListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (currentPage <= pageCount) {
                    if (flag) {
                        flag = false;
                        sendRequest();
                    }
                } else {
                    Toast.makeText(context, R.string.already_no_data, Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    @OnClick({R.id.ivLeft, R.id.rbTransaction, R.id.rbFinished})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft:
                finish();
                break;
            case R.id.rbTransaction: // 交易订单
                if (flag) {
                    flag = false;
                    isFinished = UNFINISHED;
                    currentPage = 1;
                    listOrder.clear();
                    adapter.notifyDataSetChanged();
                    sendRequest();
                }
                break;
            case R.id.rbFinished: // 已完成订单
                if (flag) {
                    flag = false;
                    isFinished = FINISHED;
                    currentPage = 1;
                    listOrder.clear();
                    adapter.notifyDataSetChanged();
                    sendRequest();
                }
                break;
            default:
                break;
        }
    }

    ResponseInfo<String> setTestData(ResponseInfo<String> arg0) {
        if (arg0 == null) {
            Toast.makeText(context, "ResponseInfo 为 null", Toast.LENGTH_SHORT).show();
            return null;
        }
        arg0.result = getString(R.string.testData);
        return arg0;
    }


    /**
     * 获取订单详情
     */
    private void getOrderDetail(String orderNo, final String userMsgID) {

        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);

        String url = getString(R.string.server_url) + URLMap.CDSORDER_DETAIL;

        LogUtils.i(url + CommonTools.getQuryParams(params));
        Log.d(TAG, "onStart: " + url + "USERNAME=" + sp.getString(UserEntity.PHONE, null) + "&PASSWORD=" + sp.getString(UserEntity.PASSWORD, null) + "&ORDER_NO=" +
                orderNo);

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");

                        LogUtils.w("MainFragment.getOrderDetail()");

                        LogUtils.i(data.toString());

                        Bundle bundle = new Bundle();
                        Iterator<String> it = data.keys();
                        while (it.hasNext()) {
                            String key = it.next();
                            String value = data.getString(key);
                            bundle.putString(key, value);
                        }
                        if ("1".equals(bundle.get("STATUS"))) { // 订单为已派单状态，进行接单
                            startActivity(new Intent(context, NewMessageActivity.class).putExtras(bundle).putExtra("From", "Main")
                                    .putExtra("USERMESSAGE_ID", userMsgID));
                        }  else { // 其它状态打开订单详情
                            startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", bundle.getString("WAYBILL_NUMBER")));
                        }
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(context, "此单因超时未接单，系统已转派其他车辆", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            }
        });
    }


    private void sendRequest() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("CURRENTPAGE", String.valueOf(currentPage));
        params.addQueryStringParameter("SHOWCOUNT", String.valueOf(SHOW_COUNT));
        params.addQueryStringParameter("DATATYPE", DATA_TYPE);
        params.addQueryStringParameter("ISPLAN", "0");
        params.addQueryStringParameter("ISFINISH", isFinished);

        String url = getString(R.string.server_url) + URLMap.GET_CDSORDER_LIST;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                flag = true;
                dialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                flag = true;
                dialog.dismiss();
                try {
//					setTestData(arg0);
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        if (ptrListView.isRefreshing()) { // 刷新显示中
                            ptrListView.onRefreshComplete();
                            listOrder.clear();
                        }
//                        pageCount = obj.getInt("PAGECOUNT");
                        JSONArray array = obj.getJSONArray("data");
                        Map<String, String> order = null;
                        JSONObject data = null;
                        for (int i = 0; i < array.length(); i++) {
                            order = new HashMap<String, String>();
                            data = array.getJSONObject(i);
                            Iterator<String> it = data.keys();
                            while (it.hasNext()) {
                                String key = it.next();
                                String value = CommonTools.judgeNull(data, key, "");
                                order.put(key, value);
                            }
                            JSONArray orderArray = new JSONArray(order.get("orderList"));
                            JSONObject orderObject = orderArray.getJSONObject(0);
                            order.put("is_urgent",CommonTools.judgeNull(orderObject, "is_urgent", ""));
                            JSONArray jArray = new JSONArray(order.get("deliverList"));
                            JSONObject object = jArray.getJSONObject(0);
                            order.put("FROMPROVINCE", CommonTools.judgeNull(object, "PROVINCE", ""));
                            order.put("FROMCITY", CommonTools.judgeNull(object, "CITY", ""));
                            order.put("FROMDISTRICT", CommonTools.judgeNull(object, "DISTRICT", ""));
                            order.put("FROMADDRALIAS", CommonTools.judgeNull(object, "CONTACT_ADDRESS", ""));
                            JSONObject jsonObject = jArray.getJSONObject(jArray.length() - 1);
                            order.put("TOPROVINCE", CommonTools.judgeNull(jsonObject, "PROVINCE", ""));
                            order.put("TOCITY", CommonTools.judgeNull(jsonObject, "CITY", ""));
                            order.put("TODISTRICT", CommonTools.judgeNull(jsonObject, "DISTRICT", ""));
                            order.put("TOADDRALIAS", CommonTools.judgeNull(jsonObject, "CONTACT_ADDRESS", ""));

                            listOrder.add(order);
//                            if(!order.get("ORDER_STATUS").equals("-7")) {
//                            }
                        }
                        if (listOrder.size() > 0) {
                            adapter.notifyDataSetChanged();
                        }
                        // 当前页+1
                        currentPage += 1;
                    } else {
                        Toast.makeText(context, "获取订单失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }

}
