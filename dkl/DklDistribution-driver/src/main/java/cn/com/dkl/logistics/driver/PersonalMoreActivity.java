package cn.com.dkl.logistics.driver;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.UpgradeInfo;

import org.json.JSONObject;

import java.io.File;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.SettingEntity;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.service.LocationService;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.DataCleanManager;
import cn.com.dkl.logistics.utils.JPushUtil;

/**
 * 更多
 *
 * @author hzq
 */
public class PersonalMoreActivity extends BaseActivity {
    private static Context context;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.llFeedback)
    private LinearLayout llFeedback;
    @ViewInject(R.id.llComplain)
    private LinearLayout llComplain;
    @ViewInject(R.id.llAboutUs)
    private LinearLayout llAboutUs;
    @ViewInject(R.id.llVersionUpdate)
    private LinearLayout llVersionUpdate;
    @ViewInject(R.id.btnRight)
    private Button mBtnRight;
    @ViewInject(R.id.llClearCache)
    private Button mLlClearCache;
    @ViewInject(R.id.tvClearCache)
    private TextView mTvClearCache;
    @ViewInject(R.id.llSelectStorageLocation)
    private LinearLayout llSelectStorageLocation;
    /**
     * 联系客服
     */
    @ViewInject(R.id.llContactService)
    private LinearLayout llContactService;

    @ViewInject(R.id.tvVersion)
    private TextView tvVersion;
    private String versionName = null;
    private String callBackCode = "";
    private String callBackMessage = "";
    private String callBackState = "";
    private ProgressDialog loadingDialog;
    String apkFolderUrl = "";
    private static SharedPreferences sp;
    private static SharedPreferences sps;
    private int verCode = 0;
    private int newVerCode = 0;
    private String newVer = "";
    private String versionString = "";
    private String downloadUrl = "";
    private String appName = "";
    private CommonTools tools;
    private Dialog progressDialog;
    private String description = "";
    // 0 运起来 1 同城配运司机版 2 同城配运货主版
    private final int versonType = 1;
    private AlertDialog alertdialog;

    private AlertDeleteDialogwindow window;
    private AlertClearCacheDialogWindow mClearCacheDialogWindow;

    private static String deviceID;
    private String official_url;    //正式服务器地址
    private String test_url;        //测试服务器地址
    private String CacheSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_more);
//        mBtnRight.setVisibility(View.VISIBLE);
        init();

        if (("http://www.wuetong.com/".equals(getString(R.string.server_url)))) {
            mBtnRight.setVisibility(View.VISIBLE);
            mBtnRight.setTextColor(context.getResources().getColor(R.color.full_tranparent));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        context = PersonalMoreActivity.this;
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            deviceID = tm.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tools = new CommonTools(context);
        tvTitle.setText(R.string.title_personal_more);
        try {
            versionName = context.getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_CONFIGURATIONS).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        official_url = PersonalMoreActivity.this.getResources().getString(R.string.zh_server_url);
        test_url = PersonalMoreActivity.this.getResources().getString(R.string.test_server_url);;
        String nowServer_url = PersonalMoreActivity.this.getResources().getString(R.string.server_url);
        if (official_url.equals(nowServer_url)) {
            tvVersion.setText("V" + versionName);
        } else if (test_url.equals(nowServer_url)){
            tvVersion.setText("TestV" + versionName);
        } else {
            tvVersion.setText("DevV" + versionName);
        }

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setTitle("大可龙");
        loadingDialog.setMessage("下载中...");
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        // 初始化进度框
        progressDialog = tools.getProgressDialog(context, getString(R.string.loading));

        sp = getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
        sps = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        apkFolderUrl = sp.getString(SettingEntity.APK_URL, "");

        try {
            verCode = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_CONFIGURATIONS).versionCode;
            mTvClearCache.setText(DataCleanManager.getTotalCacheSize(context));
            CacheSize = DataCleanManager.getTotalCacheSize(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent in = getIntent();
        if (in.getExtras() != null) {
            logout();
        }


    }

    @OnClick({R.id.btnLeft, R.id.llFeedback, R.id.llAboutUs, R.id.llVersionUpdate, R.id.llContactService, R.id.btnExitLogin,
            R.id.btnRight, R.id.llClearCache, R.id.llComplain, R.id.llSelectStorageLocation})
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.llFeedback:  // 意见反馈
                intent = new Intent(context, FeedbackActivity.class);
                startActivity(intent);
                break;
            case R.id.llComplain:  // 投诉
                intent = new Intent(context, ComplainActivity.class);
                startActivity(intent);
                break;
            case R.id.llAboutUs:   // 关于我们
                intent = new Intent(context, AboutUsActivity.class);
                startActivity(intent);
                break;
            case R.id.llVersionUpdate:
//                Toast.makeText(context, "已经是最新版本！", Toast.LENGTH_SHORT).show();
                Beta.checkUpgrade();
//                if (!progressDialog.isShowing()) {
//                    progressDialog.show();
//                }
//                checkUpdate();

                loadUpgradeInfo();
                break;
            case R.id.llContactService:
                CommonTools tools = new CommonTools(context);
                String phoneString = this.getResources().getString(R.string.contact_service_hotline);
                tools.alertDialog(phoneString);
                break;

            case R.id.llClearCache:
                clearCacheWindow();
//                if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
//                    if (!TextUtils.isEmpty(Constants.STORAGEPATH)){
//                        DataCleanManager.cleanCustomCache(Constants.STORAGEPATH);
//                    }else {
//                        DataCleanManager.deleteFolderFile(Constants.STORAGEPATH, true);
//                    }
//                }else if(Environment.getExternalStorageState().equals(Environment.MEDIA_REMOVED)){
//                    if (!TextUtils.isEmpty(Constants.STORAGEPATH)){
//                        DataCleanManager.cleanCustomCache(Constants.STORAGEPATH);
//                    }else {
//                        DataCleanManager.deleteFolderFile(Constants.STORAGEPATH, true);
//                    }
//                }
                break;

            case R.id.btnExitLogin:
                // 清除用户数据
                windows();
                break;
            case R.id.btnRight:
                if (official_url.equals(getString(R.string.server_url))) {
                    mBtnRight.setVisibility(View.GONE);
                } else {
                    Toast.makeText(context, getString(R.string.server_url), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.llSelectStorageLocation:
                if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){

                    Constants.ISSDLOCATION = "1";
                }else if(Environment.getExternalStorageState().equals(Environment.MEDIA_REMOVED)){

                    Constants.ISSDLOCATION = "0";
                }
                break;
            default:
                break;
        }
    }

    private void loadUpgradeInfo() {
//        if (upgradeInfoTv == null)
//            return;

        /***** 获取升级信息 *****/
        UpgradeInfo upgradeInfo = Beta.getUpgradeInfo();

        if (upgradeInfo == null) {
//            upgradeInfoTv.setText("无升级信息");
            LogUtils.d("无升级信息");
            return;
        }
        StringBuilder info = new StringBuilder();
        info.append("id: ").append(upgradeInfo.id).append("\n");
        info.append("标题: ").append(upgradeInfo.title).append("\n");
        info.append("升级说明: ").append(upgradeInfo.newFeature).append("\n");
        info.append("versionCode: ").append(upgradeInfo.versionCode).append("\n");
        info.append("versionName: ").append(upgradeInfo.versionName).append("\n");
        info.append("发布时间: ").append(upgradeInfo.publishTime).append("\n");
        info.append("安装包Md5: ").append(upgradeInfo.apkMd5).append("\n");
        info.append("安装包下载地址: ").append(upgradeInfo.apkUrl).append("\n");
        info.append("安装包大小: ").append(upgradeInfo.fileSize).append("\n");
        info.append("弹窗间隔（ms）: ").append(upgradeInfo.popInterval).append("\n");
        info.append("弹窗次数: ").append(upgradeInfo.popTimes).append("\n");
        info.append("发布类型（0:测试 1:正式）: ").append(upgradeInfo.publishType).append("\n");
        info.append("弹窗类型（1:建议 2:强制 3:手工）: ").append(upgradeInfo.upgradeType).append("\n");
        info.append("图片地址：").append(upgradeInfo.imageUrl);

        LogUtils.d("info is " + info);


//        upgradeInfoTv.setText(info);
    }


    /**
     * 清除缓存弹窗
     */
    private void clearCacheWindow() {
        mClearCacheDialogWindow = new AlertClearCacheDialogWindow(PersonalMoreActivity.this, CacheSize);
        mClearCacheDialogWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mClearCacheDialogWindow.showAtLocation(this.findViewById(R.id.llFeedback), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }


    private void windows() {
        String tDelete = "确认是否退出登录？";
        String tCancel = "退出登录";
        ClickListener listener = new ClickListener();
        window = new AlertDeleteDialogwindow(PersonalMoreActivity.this, listener, tDelete, tCancel);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        window.showAtLocation(this.findViewById(R.id.llFeedback), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }


    class ClickListener implements OnClickListener {

        private ClickListener() {
        }

        @Override
        public void onClick(View arg0) {
            logout();
            window.dismiss();
        }

    }

    public static void logout() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sps.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sps.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("DEVICEID", deviceID);

        String url = context.getString(R.string.server_url) + URLMap.LOGOUT;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onStart() {
                super.onStart();

            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                // Toast.makeText(context, "请稍候再试!", Toast.LENGTH_SHORT).show();
                LogUtils.e("退出失败");
                JPushUtil.clearJPushAliasAndTags(context);
                Editor edit = sps.edit().clear();
                edit.commit();
                context.startActivity(new Intent(context, LoginActivity.class));

                context.stopService(new Intent(context, LocationService.class));
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                JPushUtil.clearJPushAliasAndTags(context);
                Editor edit = sps.edit().clear();
                edit.commit();
                context.startActivity(new Intent(context, LoginActivity.class));
                context.sendBroadcast(new Intent("finish"));
                LogUtils.i("退出成功");
                context.stopService(new Intent(context, LocationService.class));
            }
        });
    }


    public void checkUpdate() {
        String url = getString(R.string.server_url) + URLMap.VERSION_UPDATE;
        RequestParams params = new RequestParams(url);
        params.addQueryStringParameter("Version", String.valueOf(verCode));
        params.addQueryStringParameter("VERSIONTYPE", String.valueOf(versonType));

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                LogUtils.i("responseInfo.result= " + arg0.result);
                versionString = arg0.result;
                try {
                    if (!versionString.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(versionString);
                        callBackMessage = jsonObject.getString("message");
                        callBackState = jsonObject.getString("state");
                        callBackCode = jsonObject.getString("code");
                        if (callBackState.equals("1")) {
                            JSONObject dataObject = jsonObject.optJSONObject("data");
                            newVerCode = dataObject.getInt("VERSION");
                            newVer = CommonTools.judgeNull(dataObject, "VERSION", "");
                            downloadUrl = CommonTools.judgeNull(dataObject, "LINK", "");
                            appName = CommonTools.judgeNull(dataObject, "NAME", "");
                            description = CommonTools.judgeNull(dataObject, "DESCRIPTION", "");
                            if (verCode < newVerCode) {
                                alertDialog();
                            } else {
                                Toast.makeText(context, "已经是最新版本！", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (callBackMessage.isEmpty()) {
                                Toast.makeText(context, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, callBackMessage, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Toast.makeText(context, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * 下载apk
     */
    public void downloadApk() {
        final String fileName = apkFolderUrl + "/" + appName;
        LogUtils.i(fileName);
        File file = new File(fileName);
        if (file.exists()) {
            install(fileName);
        } else {
            HttpUtils httpUtils = new HttpUtils(60 * 1000);
            httpUtils.configCurrentHttpCacheExpiry(1000 * 10);
            httpUtils.download(downloadUrl, fileName, false, false, new RequestCallBack<File>() {
                @Override
                public void onLoading(long total, long current, boolean isUploading) {
                    super.onLoading(total, current, isUploading);
                    /*
                     * loadingDialog.setMax(100);
					 * loadingDialog.setProgress((int) (current * 100 / total));
					 */
                }

                @Override
                public void onSuccess(ResponseInfo<File> arg0) {
                    alertdialog.dismiss();
                    install(fileName);
                    Toast.makeText(context, "下载完成", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(HttpException arg0, String arg1) {
                    alertdialog.dismiss();
                    CommonTools.failedToast(context);
                }
            });
        }
    }

    /**
     * 安装apk
     */
    public void install(String path) {
        alertdialog.dismiss();
        loadingDialog.dismiss();
        File file = new File(path);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(file);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        startActivity(intent);
    }

    public void alertDialog() {
        alertdialog = new AlertDialog.Builder(context).create();
        alertdialog.show();
        Window window = alertdialog.getWindow();
        window.setContentView(R.layout.dialog_main_info);
        TextView tvDialogVersonCode = (TextView) window.findViewById(R.id.tvDialogVersonCode);
        final ImageView ivLoading = (ImageView) window.findViewById(R.id.ivLoading);
        RotateAnimation animation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(900);
        animation.setRepeatCount(-1);
        ivLoading.startAnimation(animation);
        int start = appName.lastIndexOf("-") + 1;
        int end = appName.lastIndexOf(".");
        String versonCode = appName.substring(start, end);
        tvDialogVersonCode.setText(versonCode);
        final Button btnUpdate = (Button) window.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // loadingDialog.show();
                ivLoading.setVisibility(View.VISIBLE);
                btnUpdate.setVisibility(View.GONE);
                downloadApk();
            }
        });

		/*
         * builder.setTitle("版本更新"); builder.setMessage("V" + newVer + "主要更新内容："
		 * + "\n" + "\n" + description + "\n"); builder.setPositiveButton("确定",
		 * new DialogInterface.OnClickListener() {
		 * 
		 * @Override public void onClick(DialogInterface dialog, int which) {
		 * loadingDialog.show(); downloadApk(); } });
		 */

		/*
         * builder.setNegativeButton("取消", new DialogInterface.OnClickListener()
		 * {
		 * 
		 * @Override public void onClick(DialogInterface dialog, int which) {
		 * dialog.dismiss(); } });
		 */
        // builder.create().show();
    }

}
