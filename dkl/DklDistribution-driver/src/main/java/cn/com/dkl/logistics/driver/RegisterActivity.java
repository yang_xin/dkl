package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.EncryptTools;
import cn.com.dkl.logistics.view.CustomProgressDialog;

/**
 * 注册用户
 *
 * @author txw
 */
public class RegisterActivity extends BaseActivity {
    private Context context = RegisterActivity.this;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.etPhoneNumber)
    private EditText etPhoneNumber;
    @ViewInject(R.id.etPassword)
    private EditText etPassword;
    @ViewInject(R.id.etConfirmPwd)
    private EditText etConfirmPwd;
    @ViewInject(R.id.etSmsCode)
    private EditText etSmsCode;
    @ViewInject(R.id.btnGetSmsCode)
    private Button btnGetSmsCode;
    @ViewInject(R.id.btnRegister)
    private Button btnRegister;
    @ViewInject(R.id.tvServiceAgreement)
    private TextView tvServiceAgreement;
    private String userType = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText(R.string.title_register);
        userType = getIntent().getStringExtra("RoleID");
    }

    @OnClick({R.id.btnLeft, R.id.btnGetSmsCode, R.id.btnRegister, R.id.tvServiceAgreement})
    public void onclick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft: // 返回
                finish();
                break;
            case R.id.btnGetSmsCode: // 获取验证码
                String phoneNumber = etPhoneNumber.getText().toString().trim();
                if (TextUtils.isEmpty(phoneNumber)) {
                    Toast.makeText(context, R.string.hint_input_phone_number, Toast.LENGTH_SHORT).show();
                } else {
                    boolean result = CommonTools.checkPhoneNumber(phoneNumber); // 验证手机号码是否正确
                    if (result) {
                        sendSmsCode(phoneNumber);
                    } else {
                        Toast.makeText(context, "请输入正确的手机号码！", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.btnRegister: // 注册
                btnRegister.setClickable(false);
                doRegister();
                break;
            case R.id.tvServiceAgreement: // 服务协议
                startActivity(new Intent(context, ServiceAgreementActivity.class).putExtra("Code", "tyRegister"));
                break;
        }
    }

    /**
     * 执行注册操作
     */
    private void doRegister() {
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String confirmPwd = etConfirmPwd.getText().toString().trim();
        String smsCode = etSmsCode.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNumber)) {
            Toast.makeText(context, R.string.hint_input_phone_number, Toast.LENGTH_SHORT).show();
            etPhoneNumber.requestFocus();
            btnRegister.setClickable(true);
            return;
        } else if (!CommonTools.checkPhoneNumber(phoneNumber)) { // 号码验证
            Toast.makeText(context, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
            etPhoneNumber.requestFocus();
            btnRegister.setClickable(true);
            return;
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(context, R.string.hint_input_password, Toast.LENGTH_SHORT).show();
            etPassword.requestFocus();
            btnRegister.setClickable(true);
            return;
        } else if (6 > password.length()) {
            Toast.makeText(context, "密码长度不小于6位", Toast.LENGTH_SHORT).show();
            etPassword.requestFocus();
            btnRegister.setClickable(true);
            return;
        } else if (TextUtils.isEmpty(confirmPwd)) {
            Toast.makeText(context, R.string.hint_input_confirm_pwd, Toast.LENGTH_SHORT).show();
            etConfirmPwd.requestFocus();
            btnRegister.setClickable(true);
            return;
        } else if (!password.equals(confirmPwd)) {
            Toast.makeText(context, "两次密码输入不一致", Toast.LENGTH_SHORT).show();
            etConfirmPwd.requestFocus();
            btnRegister.setClickable(true);
            return;
        } else if (TextUtils.isEmpty(smsCode)) {
            Toast.makeText(context, R.string.hint_input_smscode, Toast.LENGTH_SHORT).show();
            etSmsCode.requestFocus();
            btnRegister.setClickable(true);
            return;
        } else if (6 != smsCode.length()) {
            Toast.makeText(context, "请输入正确的验证码", Toast.LENGTH_SHORT).show();
            etSmsCode.requestFocus();
            btnRegister.setClickable(true);
            return;
        }

        sendRequest(phoneNumber, EncryptTools.getBASE64Str(password).toString(), EncryptTools.getBASE64Str(confirmPwd).toString(), smsCode);
    }

    /**
     * 发送注册验证码
     *
     * @param phoneNumber 手机号码
     */
    private void sendSmsCode(String phoneNumber) {
        btnGetSmsCode.setClickable(false);
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("PHONE", phoneNumber);
        params.addQueryStringParameter("CLIENT", getString(R.string.SCANTYPE));
        params.addQueryStringParameter("SCANTYPE", getString(R.string.SCANTYPE));

        String url = getString(R.string.server_url) + URLMap.SEND_REGISTER_SMS_CODE;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            CustomProgressDialog smsDialog = null;
            @Override
            public void onStart() {
                super.onStart();
                smsDialog = new CommonTools(context).getProgressDialog(context, "提交注册中...");
                smsDialog.show();
            }

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                smsDialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(result.result);
                    String status = obj.getString("state");
                    String code = obj.getString("code");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        setButtonState(); // 更新短信发送按钮状态
                    } else if ("0".equals(status)) {
                        btnGetSmsCode.setClickable(true);
                        Toast.makeText(context, "验证码发送失败", Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        btnGetSmsCode.setClickable(true);
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                smsDialog.dismiss();
                btnGetSmsCode.setClickable(true);
                CommonTools.failedToast(context);
            }
        });

    }

    /**
     * 设置发送短信按钮状态
     */
    private void setButtonState() {
        btnGetSmsCode.setClickable(false);
        final Timer timer = new Timer(); // 定时器对象
        TimerTask task = new TimerTask() {
            int i = 60;

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        i--;
                        btnGetSmsCode.setText(i + "秒");
                        if (i == 0) {
                            timer.cancel();
                            btnGetSmsCode.setText("点击重发");
                            btnGetSmsCode.setClickable(true);
                        }
                    }
                });
            }
        };
        timer.schedule(task, 0, 1000);
    }

    /**
     * 发送请求
     *
     * @param phoneNumber 手机号码
     * @param password    密码
     * @param confirmPwd  确认密码
     * @param smsCode     验证码
     */
    private void sendRequest(String phoneNumber, String password, String confirmPwd, String smsCode) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("PHONE", phoneNumber);
        params.addQueryStringParameter("PASSWORD", password);
        params.addQueryStringParameter("CONFIRMPASSWORD", confirmPwd);
        params.addQueryStringParameter("VALIDCODE", smsCode);
        params.addQueryStringParameter("REGISTERTYPE", userType);
        params.addQueryStringParameter("CLIENT", getString(R.string.SCANTYPE));
        params.addQueryStringParameter("ISSPECIAL", Constants.ISSPECIAL);
        params.addQueryStringParameter("TEAM_ID", Constants.TEAMID);

        String url = getString(R.string.server_url) + URLMap.REGISTER;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            CustomProgressDialog dialog = null;

            @Override
            public void onStart() {
                dialog = new CommonTools(context).getProgressDialog(context, "提交注册中...");
                dialog.show();
                super.onStart();
            }

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                dialog.dismiss();
                btnRegister.setClickable(true);
                try {
                    JSONObject obj = new JSONObject(result.result);
                    String status = obj.getString("status");
                    String code = obj.getString("code");
                    String message = obj.getString("message");
                    if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("1".equals(status)) {
                        if ("1".equals(code)) {
                            Toast.makeText(context, "注册成功，请登录", Toast.LENGTH_SHORT).show();
                            SystemClock.sleep(1000);
                            finish();
                        }
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException e, String result) {
                dialog.dismiss();
                CommonTools.failedToast(context);
                btnRegister.setClickable(true);
            }

        });
    }

}
