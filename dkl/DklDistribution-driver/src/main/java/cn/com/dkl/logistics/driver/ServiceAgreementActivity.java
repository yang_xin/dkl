package cn.com.dkl.logistics.driver;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONException;
import org.json.JSONObject;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.utils.CommonTools;

/**
 * 注册协议
 *
 * @author hzq
 */
public class ServiceAgreementActivity extends BaseActivity {
    private Context mContext = ServiceAgreementActivity.this;
    @ViewInject(R.id.webView)
    private WebView webView;

    private CommonTools tools = new CommonTools(this);
    private Dialog progressDialog;
    private Context context = ServiceAgreementActivity.this;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_agreement);
        ViewUtils.inject(this);
        // 初始化进度框
        progressDialog = tools.getProgressDialog(mContext, getString(R.string.loading));
        webView.getSettings().setSupportZoom(true);// 支持缩放
        webView.getSettings().setBuiltInZoomControls(true);// 设置出现缩放工具（支持手指缩放）
        webView.getSettings().setUseWideViewPort(true);// 扩大比例的缩放
        // 自适应屏幕8
        webView.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        RequestParams params = new RequestParams();
        params.addBodyParameter("SCANTYPE", "2");
        params.addBodyParameter("CODE", getIntent().getExtras().getString("Code", "tyRegister"));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        String url = getString(R.string.server_url) + URLMap.SERVICE_AGREEMENT;
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        String name = CommonTools.judgeNull(data, "NAME", "");
                        LogUtils.i(CommonTools.judgeNull(data, "CONTENT", ""));
                        webView.loadDataWithBaseURL(null, (CommonTools.judgeNull(data, "CONTENT", "")), "text/html", "UTF-8", null);
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

}
