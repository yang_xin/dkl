package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.widget.TextView;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;

/**
 * 开始界面
 *
 * @author Dao
 */
public class StartPageActivity extends BaseActivity {
    private Context mContext = StartPageActivity.this;

    @ViewInject(R.id.tvVersionName)
    private TextView tvVersionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);
        ViewUtils.inject(this);
        if((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0){
            finish();
            return;
        }
        try {
            String versionName = mContext.getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_CONFIGURATIONS).versionName;
            tvVersionName.setText(versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                SharedPreferences sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
                Intent intent = null;
                if (sp.getBoolean(UserEntity.IS_lOGIN, false)) {
                    intent = new Intent(StartPageActivity.this, MainActivity.class);
                } else {
                    intent = new Intent(StartPageActivity.this, LoginActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, 1000 * 1);
//        initTingYun();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    void initTingYun(){
//        NBSAppAgent.setLicenseKey("c89377dd1cfe49a99320bad7d24d248d").withLocationServiceEnabled(true).start(this.getApplicationContext());
//        NBSAppAgent.setLicenseKey("a49bebb9d4cf4c7286243bd571067262").withLocationServiceEnabled(true).setX5Enable(true).start(this.getApplicationContext());
    }

}
