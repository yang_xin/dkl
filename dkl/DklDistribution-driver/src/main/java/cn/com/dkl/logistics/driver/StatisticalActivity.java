package cn.com.dkl.logistics.driver;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.util;

public class StatisticalActivity extends BaseActivity {

    @Bind(R.id.ivLeft)
    ImageView ivLeft;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.rlWeek)
    RelativeLayout rlWeek;
    @Bind(R.id.tvWeek)
    TextView tvWeek;
    @Bind(R.id.tvYear)
    TextView tvYear;
    @Bind(R.id.rlMonth)
    RelativeLayout rlMonth;
    @Bind(R.id.llMonth)
    LinearLayout llMonth;
    @Bind(R.id.tvMonth)
    TextView tvMonth;
    @Bind(R.id.tvDriverTotalcost)
    TextView tvDriverTotalcost;
    @Bind(R.id.llWeek)
    LinearLayout llWeek;
    @Bind(R.id.rbLast)
    RadioButton rbLast;
    @Bind(R.id.rbNext)
    RadioButton rbNext;
    @Bind(R.id.tvWeekRange)
    TextView tvWeekRange;
    @Bind(R.id.tvWayCount)
    TextView tvWayCount;
    @Bind(R.id.tvOrderCount)
    TextView tvOrderCount;
    @Bind(R.id.tvWeightCount)
    TextView tvWeightCount;
    @Bind(R.id.tvVolumeCount)
    TextView tvVolumeCount;
    @Bind(R.id.tvPieceCount)
    TextView tvPieceCount;


    private Context context = StatisticalActivity.this;
    private SharedPreferences sp;
    private int currentYear;
    private int currentMonth;
    private int currentDay;
    private int currentWeek;
    AlertTimeDialogWindow window;
    private String year;
    private String month;
    private String weekStart;
    private String weekEnd;
    private int week = 0;
    //按月查询
    boolean flag = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistical);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        ButterKnife.bind(this);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        Calendar calendar = Calendar.getInstance();
        currentYear = calendar.get(Calendar.YEAR);
        currentMonth = calendar.get(Calendar.MONTH);
        currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        currentWeek = calendar.get(Calendar.DAY_OF_WEEK);
        year = String.valueOf(currentYear);
        month = String.valueOf(currentMonth + 1);
        tvYear.setText(currentYear + "年");
        tvMonth.setText(month);
        requestData();
    }

    @OnClick(value = {R.id.ivLeft, R.id.rlWeek, R.id.rlMonth, R.id.rbLast, R.id.rbNext })
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft:
                finish();
                break;
            case R.id.rlWeek:
                flag = !flag;
                if (!flag){
                    tvWeek.setText("月");
                    llMonth.setVisibility(View.GONE);
                    llWeek.setVisibility(View.VISIBLE);
                    week = 0;
                    setWeekRange();
                }else {
                    tvWeek.setText("周");
                    llMonth.setVisibility(View.VISIBLE);
                    llWeek.setVisibility(View.GONE);
                    year = String.valueOf(currentYear);
                    month = String.valueOf(currentMonth + 1);
                    requestData();
                }
                break;
            case R.id.rlMonth:
                window = new AlertTimeDialogWindow((Activity) context, listener, null);
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                window.showAtLocation(this.findViewById(R.id.llMain), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                break;
            case R.id.rbLast:
                week++;
                if (week>0) {
                    rbNext.setText("下一周");
                }
                setWeekRange();
                break;
            case R.id.rbNext:
                if (week>0){
                    week--;
                }
                if (week==0){
                    rbNext.setText("本周");
                }
                setWeekRange();
                break;

            default:
                break;
        }
    }

    private void setWeekRange(){
        Date d = new Date();
        SimpleDateFormat df=new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String startWeek = "";
        String endWeek = "";
        int startNum = 0;
        int endNum = 0;
        if (week == 0){
            startNum = currentWeek - 1;
            endWeek = df.format(d);
            weekEnd = sdf.format(d);
            if (startNum == 0){
                startWeek = endWeek;
                weekStart = weekEnd;
            }else {
                startWeek = df.format(new Date(d.getTime() - (long)startNum * 24 * 60 * 60 * 1000));
                weekStart = sdf.format(new Date(d.getTime() - (long)startNum * 24 * 60 * 60 * 1000));
            }
        }else {
            startNum = week * 7 + currentWeek - 1;
            endNum = startNum - 6;
            startWeek = df.format(new Date(d.getTime() - (long)startNum * 24 * 60 * 60 * 1000));
            endWeek = df.format(new Date(d.getTime() - (long)endNum * 24 * 60 * 60 * 1000));
            weekStart = sdf.format(new Date(d.getTime() - (long)startNum * 24 * 60 * 60 * 1000));
            weekEnd = sdf.format(new Date(d.getTime() - (long)endNum * 24 * 60 * 60 * 1000));
        }
        tvWeekRange.setText(startWeek + "-" + endWeek);
        requestData();
    }

    /**
     * 获取订单信息
     */
    private void requestData() {
        rlMonth.setClickable(false);
        rlWeek.setClickable(false);
        rbLast.setClickable(false);
        rbNext.setClickable(false);
        String yearMonth = year + "-" + month;
        if (Integer.parseInt(month)<10){
            yearMonth = year + "-0" + month;
        }
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        if (flag){
            params.addQueryStringParameter("yearMonth", yearMonth);
        }else {
            params.addQueryStringParameter("weekStart", weekStart);
            params.addQueryStringParameter("weekEnd", weekEnd);
        }

        String url = getString(R.string.server_url) + URLMap.GET_DRIVER_STATISTICAL;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpRequest.HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                rlMonth.setClickable(true);
                rlWeek.setClickable(true);
                rbLast.setClickable(true);
                rbNext.setClickable(true);
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject datas = obj.getJSONObject("data");
                        tvDriverTotalcost.setText(CommonTools.judgeNull(datas, "driverTotalCount", "0.00"));
                        tvWayCount.setText(CommonTools.judgeNull(datas, "wayCount", "0") + " 单");
                        tvOrderCount.setText(CommonTools.judgeNull(datas, "orderCount", "0") + " 单");
                        tvWeightCount.setText(util.getTwoDecimal(Double.valueOf(CommonTools.judgeNull(datas, "weightCount", "0.00"))) + " 吨");
                        tvVolumeCount.setText(util.getTwoDecimal(Double.valueOf(CommonTools.judgeNull(datas, "volumeCount", "0.00"))) + " 方");
                        tvPieceCount.setText(util.getTwoDecimal(Double.valueOf(CommonTools.judgeNull(datas, "numberCount", "0.00"))) + " 件");
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }
        });
    }


    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            year = AlertTimeDialogWindow.selectYear;
            month = AlertTimeDialogWindow.selectMonth;
            LogUtils.i("year = " + year + "month = " + month);
            tvYear.setText(year + "年");
            tvMonth.setText(String.valueOf(month));
            window.dismiss();
            requestData();
        }
    };

}
