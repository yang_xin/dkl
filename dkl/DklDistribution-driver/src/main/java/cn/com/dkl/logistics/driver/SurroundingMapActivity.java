package cn.com.dkl.logistics.driver;

import android.os.Bundle;
/*import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.dkl.logistics.driver.R;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;*/

/**
 * 周边情况地图界面
 * @author Dao
 *
 */
/*public class SurroundingMapActivity extends BaseActivity implements OnGetGeoCoderResultListener {*/
public class SurroundingMapActivity extends BaseActivity {
	/*private Context mContext = SurroundingMapActivity.this;
	@ViewInject(R.id.btnLeft)
	private Button btnLeft;
	@ViewInject(R.id.tvTitle)
	private TextView tvTitle;
	@ViewInject(R.id.btnResetLocation)
	private Button btnResetLocation;
	@ViewInject(R.id.btnResetTraffic)
	private Button btnResetTraffic;

	@ViewInject(R.id.mMapView)
	private MapView mMapView;
	GeoCoder mSearch = null; // 搜索模块
	private BaiduMap mBaiduMap;
	boolean isFirstLoc = true;// 是否首次定位
	boolean isFirstShow = true;
	// 定位相关
	private LocationClient mLocClient;
	public MyLocationListenner myListener = new MyLocationListenner();
	private LocationMode mCurrentMode;
	BitmapDescriptor mCurrentMarker;
	private String mcurrentAddress;
	private InfoWindow mInfoWindow;
	private LatLng ll;*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_surrounding_map);
		/*init();*/
	}

	/*@Override
	protected void onResume() {
		mMapView.onResume();
		super.onResume();
		FlowerCollector.onResume(this);
	}

	@Override
	protected void onPause() {
		mMapView.onPause();
		super.onPause();
		FlowerCollector.onPause(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		// 退出时销毁定位
		mLocClient.stop();
		// 关闭定位图层
		mBaiduMap.setMyLocationEnabled(false);
		mMapView.onDestroy();
		mMapView = null;
		super.onDestroy();
	}

	public void init() {
		ViewUtils.inject(this);

		tvTitle.setText(getString(R.string.title_surround_map));

		// 地图初始化
		mBaiduMap = mMapView.getMap();
		// 初始化搜索模块，注册事件监听
		mSearch = GeoCoder.newInstance();
		mSearch.setOnGetGeoCodeResultListener(this);
		// 显示或隐藏缩放控件
		mMapView.showZoomControls(true);
		// 开启交通图
		mBaiduMap.setTrafficEnabled(true);
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);
		// 定位初始化
		mCurrentMode = LocationMode.NORMAL;

		this.locationMode();
		mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			public boolean onMarkerClick(final Marker marker) {
				Button button = new Button(getApplicationContext());
				button.setBackgroundResource(R.drawable.popup);
				OnInfoWindowClickListener listener = null;
				button.setText(mcurrentAddress);
				listener = new OnInfoWindowClickListener() {
					public void onInfoWindowClick() {
						mBaiduMap.hideInfoWindow();
					}
				};
				ll = marker.getPosition();
				mInfoWindow = new InfoWindow(BitmapDescriptorFactory.fromView(button), ll, -47, listener);
				mBaiduMap.showInfoWindow(mInfoWindow);
				return true;
			}
		});
	}

	@OnClick(value = { R.id.btnLeft, R.id.btnResetLocation, R.id.btnResetTraffic })
	public void onClicked(View v) {
		switch (v.getId()) {
		case R.id.btnLeft:
			finish();
			break;
		case R.id.btnResetLocation:
			locateMyself();
			break;
		case R.id.btnResetTraffic:
			resetTraffic();
			break;
		default:
			break;
		}

	}

	*//***
	 * 重置实时交通图
	 *//*
	private void resetTraffic() {
		boolean isTrafficEnabled = mBaiduMap.isTrafficEnabled();
		mBaiduMap.setTrafficEnabled(!isTrafficEnabled);
		if (isTrafficEnabled) {
			btnResetTraffic.setBackgroundResource(R.drawable.img_traffic_disselected);
			Toast.makeText(mContext, "实时路况已关闭", Toast.LENGTH_SHORT).show();
		} else {
			btnResetTraffic.setBackgroundResource(R.drawable.img_traffic_selected);
			Toast.makeText(mContext, "实时路况已开启", Toast.LENGTH_SHORT).show();
		}
	}

	*//**
	 * 定位SDK监听函数
	 *//*
	public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// Toast.makeText(mContext, "onReceiveLocation()",
			// Toast.LENGTH_SHORT).show();
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null)
				return;
			MyLocationData locData = new MyLocationData.Builder().accuracy(location.getRadius())
			// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(location.getLatitude()).longitude(location.getLongitude()).build();
			mBaiduMap.setMyLocationData(locData);

			// 定义Maker坐标点
			ll = new LatLng(location.getLatitude(), location.getLongitude());
			if (isFirstLoc) {
				isFirstLoc = false;
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				mBaiduMap.animateMapStatus(u);
			} else {
				// Toast.makeText(mContext, "clearmap()",
				// Toast.LENGTH_SHORT).show();
				mMapView.getMap().clear();
			}

			// 得到精度维度后获取地址描述，反Geo搜索
			mSearch.reverseGeoCode(new ReverseGeoCodeOption().location(new LatLng(location.getLatitude(), location.getLongitude())));
			// 构建MarkerOption，用于在地图上添加Marker
			OverlayOptions option1 = new MarkerOptions().position(ll).icon(mCurrentMarker);
			// 在地图上添加Marker，并显示
			mBaiduMap.addOverlay(option1);
		}

		public void onReceivePoi(BDLocation poiLocation) {
		}
	}

	*//**
	 * 更新地图信息
	 * 
	 * @author guochaohui
	 * 
	 *//*
	public void updateMap() {
		try {
			mMapView.getMap().clear();
			MapStatus mapStatus = new MapStatus.Builder().build();
			MapStatusUpdate mapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mapStatus);
			mBaiduMap.setMapStatus(mapStatusUpdate);

		} catch (Exception e) {
		}
	}

	// 设置定位图标
	public boolean locationMode() {
		mLocClient = new LocationClient(this);
		mLocClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(60000); // 设置定位频率,小于1000只定位一次，1分钟一次
		mLocClient.setLocOption(option);
		try {
			mLocClient.start();
		} catch (Exception e) {
			Toast.makeText(mContext, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
			return false;
		}
		mCurrentMarker = BitmapDescriptorFactory.fromResource(R.drawable.img_car);
		mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(mCurrentMode, false, null));
		// 当不需要定位图层时关闭定位图层
		// mBaiduMap.setMyLocationEnabled(false);

		return true;
	}

	private void locateMyself() {

		if (mCurrentMode == LocationMode.NORMAL) {
			mCurrentMode = LocationMode.FOLLOWING;
			btnResetLocation.setBackgroundResource(R.drawable.img_reset_location_pressed);
			if (locationMode())
				Toast.makeText(mContext, "地图处于跟随态", Toast.LENGTH_SHORT).show();
		} else {
			mCurrentMode = LocationMode.NORMAL;
			btnResetLocation.setBackgroundResource(R.drawable.img_reset_location);
			if (locationMode())
				Toast.makeText(mContext, "地图处于普通态", Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void onGetGeoCodeResult(GeoCodeResult arg0) {

	}

	@Override
	public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
		if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
			Toast.makeText(mContext, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
			return;
		}
		mcurrentAddress = result.getAddress() + "\n" + "更新时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new java.util.Date());
		*//*
		 * if(isFirstShow){ isFirstShow=false;
		 *//*
		View view = View.inflate(mContext, R.layout.map_info_window_layout, null);
		TextView tvInfo = (TextView) view.findViewById(R.id.tvInfoWindow);
		tvInfo.setText(mcurrentAddress);
		tvInfo.setBackgroundResource(R.drawable.popup);
		OnInfoWindowClickListener listener = null;
		listener = new OnInfoWindowClickListener() {
			public void onInfoWindowClick() {
				mBaiduMap.hideInfoWindow();
			}
		};
		mInfoWindow = new InfoWindow(BitmapDescriptorFactory.fromView(tvInfo), ll, -47, listener);
		mBaiduMap.showInfoWindow(mInfoWindow);
	}*/

}
