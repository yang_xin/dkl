package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;

/**
 * 运单评价
 * @author txw
 */
public class WayBillEvaluateActivity extends BaseActivity {
    private Context context = WayBillEvaluateActivity.this;
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;
    @ViewInject(R.id.rgEvaluateType)
    private RadioGroup rgEvaluateType;
    @ViewInject(R.id.rbGood)
    private RadioButton rbGood;
    @ViewInject(R.id.rbMedium)
    private RadioButton rbMedium;
    @ViewInject(R.id.rbBad)
    private RadioButton rbBad;
    @ViewInject(R.id.etEvaluate)
    private EditText etEvaluate;
    @ViewInject(R.id.rBarServiceAttitude)
    private RatingBar rBarServiceAttitude;
    @ViewInject(R.id.rBarTransportCosts)
    private RatingBar rBarTransportCosts;
    @ViewInject(R.id.rBarTransportLimitation)
    private RatingBar rBarTransportLimitation;
    @ViewInject(R.id.rBarGoodsSafety)
    private RatingBar rBarGoodsSafety;
    @ViewInject(R.id.btnApllyEvaluate)
    private Button btnApllyEvaluate;
    private String orderNum;
    private String supplierID; // 发布者ID
    private String buyerIsComment = "0"; // 评价状态
    private String sellerCommentResultType;
    private float serviceAttitudePoint;
    private float transportationCostPoint;
    private float transportLimitationPoint;
    private float goodsSafetyPoint;
    private String commentContent;
    private SharedPreferences sp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waybill_evaluate);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText(R.string.title_waybill_evaluate);
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        orderNum = getIntent().getExtras().getString("OrderNum");
        supplierID = getIntent().getExtras().getString("SupplierID");
        buyerIsComment = getIntent().getExtras().getString("BuyerIsComment");
        if ("0".equals(buyerIsComment)) { // 未评价
            btnApllyEvaluate.setVisibility(View.VISIBLE);
        } else { // 已评价，初始化评价内容
            sellerCommentResultType = getIntent().getExtras().getString("SellerCommentResultType");
            serviceAttitudePoint = Float.parseFloat(getIntent().getExtras().getString("ServiceAttitudePoint"));
            transportationCostPoint = Float.parseFloat(getIntent().getExtras().getString("TransportationCostPoint"));
            transportLimitationPoint = Float.parseFloat(getIntent().getExtras().getString("TransportLimitationPoint"));
            goodsSafetyPoint = Float.parseFloat(getIntent().getExtras().getString("GoodsSafetyPoint"));
            commentContent = getIntent().getExtras().getString("CommentContent");

            if ("0".equals(sellerCommentResultType)) {
                rbGood.setChecked(true);
            } else if ("1".equals(sellerCommentResultType)) {
                rbMedium.setChecked(true);
            } else if ("2".equals(sellerCommentResultType)) {
                rbBad.setChecked(true);
            } else {
                rbGood.setChecked(false);
                rbMedium.setChecked(false);
                rbBad.setChecked(false);
            }
            etEvaluate.setText(commentContent);
            rBarServiceAttitude.setRating(serviceAttitudePoint);
            rBarTransportCosts.setRating(transportationCostPoint);
            rBarTransportLimitation.setRating(transportLimitationPoint);
            rBarGoodsSafety.setRating(goodsSafetyPoint);
            viewDisable();
        }
    }

    private void viewDisable() {
        // 设置控件状态
        rbGood.setClickable(false);
        rbMedium.setClickable(false);
        rbBad.setClickable(false);
        etEvaluate.setEnabled(false);
        rBarServiceAttitude.setIsIndicator(true);
        rBarTransportCosts.setIsIndicator(true);
        rBarTransportLimitation.setIsIndicator(true);
        rBarGoodsSafety.setIsIndicator(true);
        btnApllyEvaluate.setVisibility(View.GONE);
    }

    @OnClick({R.id.btnLeft, R.id.btnApllyEvaluate})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnApllyEvaluate:
                checkDatas();
                break;
            default:
                break;
        }
    }

    private void checkDatas() {
        if (TextUtils.isEmpty(orderNum)) {
            Toast.makeText(context, "订单号为空！", Toast.LENGTH_SHORT).show();
            return;
        }
        String commentLevel = "";
        switch (rgEvaluateType.getCheckedRadioButtonId()) {
            case R.id.rbGood:
                commentLevel = "0";
                break;
            case R.id.rbMedium:
                commentLevel = "1";
                break;
            case R.id.rbBad:
                commentLevel = "2";
                break;
            default:
                commentLevel = "0";
                break;
        }
        String comment = etEvaluate.getText().toString().trim();
        if (TextUtils.isEmpty(comment)) {
            Toast.makeText(context, "评价内容不能为空！", Toast.LENGTH_SHORT).show();
            etEvaluate.requestFocus();
            return;
        } else {
            if (comment.length() < 10) {
                Toast.makeText(context, "评价内容不少于10个字符！", Toast.LENGTH_SHORT).show();
                etEvaluate.requestFocus();
                return;
            }
        }

        String data = commentLevel + "\t服务态度：" + rBarServiceAttitude.getRating() + "运输费用：" + rBarTransportCosts.getRating() + "运输时效："
                + rBarTransportLimitation.getRating() + "货物安全：" + rBarGoodsSafety.getRating();
        LogUtils.i("评价内容：" + data);

        sendRequest(comment, commentLevel, String.valueOf((int) rBarServiceAttitude.getRating()), String.valueOf((int) rBarTransportCosts.getRating()),
                String.valueOf((int) rBarTransportLimitation.getRating()), String.valueOf((int) rBarGoodsSafety.getRating()));
    }

    private void sendRequest(String comment, String commentLevel, String serviceAttitude, String transportationCost, String transportLimitation,
                             String goodsSafety) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("ORDER_NO", orderNum); // 运单号
        params.addQueryStringParameter("COMMENT", comment); // 评价内容
        params.addQueryStringParameter("COMMENTLV", commentLevel); // 评价等级：好、中、差
        params.addQueryStringParameter("SERVICEATTITUDEPOINT", serviceAttitude); // 服务态度
        params.addQueryStringParameter("TRANSPORTATIONCOSTPOINT", transportationCost); // 运输费用
        params.addQueryStringParameter("TRANSPORTLIMITATIONPOINT", transportLimitation); // 运输效率
        params.addQueryStringParameter("GOODSSAFETYPOINT", goodsSafety); // 货物安全评分
        params.addQueryStringParameter("SCANTYPE", getString(R.string.SCANTYPE));

        String url = getString(R.string.server_url) + URLMap.ORDER_COMMENT;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        Toast.makeText(context, "评价成功！", Toast.LENGTH_SHORT).show();
                        finish();
                    } else if ("0".equals(state)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(state)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }
        });
    }

}
