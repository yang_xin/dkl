package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.com.dkl.logistics.adapter.WelcomePagerAdapter;
import cn.com.dkl.logistics.constant.SettingEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.DataInit;
import cn.com.dkl.logistics.view.MyViewPager;
import cn.jpush.android.api.JPushInterface;

public class WelcomePageActivity extends BaseActivity {
    private Context mContext = WelcomePageActivity.this;
    private final int DISPLAY_LENGHT = 3; // 延迟三秒
    private int[] imgs = {R.drawable.img_welcome1, R.drawable.img_welcome2, R.drawable.img_welcome3};
    //	private int[] imgs = { R.drawable.img_welcome2 };
    private List<View> views = new ArrayList<View>();
    @ViewInject(R.id.viewPager)
    private MyViewPager viewPager;
    @ViewInject(R.id.curDot)
    private ImageView curDot;
    @ViewInject(R.id.llLoading)
    private LinearLayout llLoading;
    private int offset;
    private int curPosition = 0;
    private boolean isFirstUse = true;
    private SharedPreferences spSetting;
    private int verCode;
    private int oldVersionCode = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        spSetting = getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
        isFirstUse = spSetting.getBoolean(SettingEntity.IS_FIRST_USE, true);
        oldVersionCode = spSetting.getInt(SettingEntity.VERSION_CODE, -1);
        try {
            verCode = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), PackageManager.GET_CONFIGURATIONS).versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        // if (!isFirstUse) {// 如果不是第一次使用则直接跳到主界面
        if (oldVersionCode == verCode) {
            Intent intent = new Intent(WelcomePageActivity.this, StartPageActivity.class);
            WelcomePageActivity.this.startActivity(intent);
            WelcomePageActivity.this.finish();
        } else {// 如果第一次登陆就加载登陆界面
            Editor editor = spSetting.edit();
            editor.putBoolean(SettingEntity.IS_FIRST_USE, false);
            editor.putInt(SettingEntity.VERSION_CODE, verCode);
            editor.commit();

            setContentView(R.layout.activity_welcome_page);
            initWelcomeView();
        }
        FlowerCollector.setCaptureUncaughtException(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
        JPushInterface.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
        JPushInterface.onPause(this);
    }

    /**
     * 初始化
     *
     * @author guochaohui
     */
    @SuppressWarnings("deprecation")
    public void initWelcomeView() {
        // 初始化viewinject
        ViewUtils.inject(this);
        // 将图片绑定到view里面
        for (int i = 0; i < imgs.length; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(imgs[i]);
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(params);
            imageView.setScaleType(ScaleType.FIT_XY);
            views.add(imageView);
        }
        ;
        // 设置指针移动
        curDot.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public boolean onPreDraw() {
                // 指针每次移动的距离
                offset = curDot.getWidth();
                return true;
            }
        });

        WelcomePagerAdapter adapter = new WelcomePagerAdapter(views);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                moveCursorTo(arg0);
                if (arg0 == imgs.length - 1) {// 到最后一页了
                    llLoading.setVisibility(View.VISIBLE);
                    viewPager.setScrollable(false);// 滑动到最后一张时禁止滑动
                    startMainActivity();// 跳转到主页面
                }
                curPosition = arg0;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

        //startMainActivity();

    }

    /**
     * 移动指针到相邻的位置
     *
     * @param position 指针的索引值
     */
    private void moveCursorTo(int position) {
        TranslateAnimation anim = new TranslateAnimation(offset * curPosition, offset * position, 0, 0);
        anim.setDuration(300);
        anim.setFillAfter(true);
        curDot.startAnimation(anim);
    }

    public void startMainActivity() {
        new Thread() {

            @Override
            public void run() {
                super.run();
                try {
                    // 初始化数据库
                    DataInit.initSqlte(WelcomePageActivity.this);
                } catch (IOException e) {
                    LogUtils.e("IOException= " + e.toString());
                }
            }
        }.start();

        // 延迟启动页面
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // Intent intent = new Intent(WelcomePageActivity.this,
                // IndexFragmentActivity.class);
                Intent intent = new Intent(WelcomePageActivity.this, LoginActivity.class);
                WelcomePageActivity.this.startActivity(intent);
                WelcomePageActivity.this.finish();
            }
        }, DISPLAY_LENGHT * 1000);
    }

}
