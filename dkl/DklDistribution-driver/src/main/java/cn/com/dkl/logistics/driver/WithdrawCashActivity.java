package cn.com.dkl.logistics.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.sunflower.FlowerCollector;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.EncryptTools;
import cn.com.dkl.logistics.view.CustomProgressDialog;

/**
 * 提现
 * @author hzq
 */
public class WithdrawCashActivity extends BaseActivity {
    @ViewInject(R.id.btnLeft)
    private Button btnLeft;
    @ViewInject(R.id.tvTitle)
    private TextView tvTitle;

    @ViewInject(R.id.btnWithdraw)
    private Button btnWithdraw;

    @ViewInject(R.id.etCashNum)
    private EditText etCashNum;

    @ViewInject(R.id.etPayPaw)
    private EditText etPayPaw;

    private Context context = WithdrawCashActivity.this;
    private SharedPreferences sp;
    private float balance;
    private CustomProgressDialog dialog = null;
    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_cash);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowerCollector.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowerCollector.onPause(this);
    }

    private void init() {
        ViewUtils.inject(this);
        tvTitle.setText("提现");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        dialog = new CommonTools(context).getProgressDialog(context, "提交数据中...");
        setPoint(etCashNum);
        getWebBalance();

    }

    private void sendRequest(final String cashNum, String payPassword) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        params.addQueryStringParameter("CHASHMONEY", String.valueOf(CommonTools.subDouble(cashNum)));
        params.addQueryStringParameter("PAYMENTPASSWORD", payPassword);

        String url = getString(R.string.server_url) + URLMap.WITHDRAWCASH;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            public void onStart() {
                super.onStart();
                dialog = new CommonTools(context).getProgressDialog(context, "提交数据中...");
                dialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                dialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        Toast.makeText(context, "申请提现成功！", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        startActivity(new Intent(context, MyWalletActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @OnClick({R.id.btnLeft, R.id.btnWithdraw})
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();

        switch (v.getId()) {
            case R.id.btnLeft:
                finish();
                break;
            case R.id.btnWithdraw:
                if (currentTime - lastClickTime > CommonTools.MIN_CLICK_DELAY_TIME) {
                    lastClickTime = currentTime;
                    String cashNum = etCashNum.getText().toString();
                    int index = cashNum.indexOf(".");
                    String payPwd = etPayPaw.getText().toString();
                    if (payPwd.length() != 6 || TextUtils.isEmpty(payPwd)) {
                        Toast.makeText(context, "请输入六位数的支付密码!", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(cashNum) || cashNum.equals("0")) {
                        Toast.makeText(context, "请输入提现金额", Toast.LENGTH_SHORT).show();
                        etCashNum.setText("");
                        etPayPaw.setText("");
                    } else {
                        if (isExceedBalance(cashNum)) {
                            sendRequest(cashNum, EncryptTools.getBASE64Str(payPwd).toString());
                        } else {
                            Toast.makeText(context, "提现金额大于余额，请重新输入!", Toast.LENGTH_SHORT).show();
                            etCashNum.setText("");
                            etPayPaw.setText("");
                        }
                    }
                }
                break;

            default:
                break;
        }
    }

    private boolean isExceedBalance(String cashNum) {

        double withdrawCash = CommonTools.subDouble(cashNum);
        if (balance >= withdrawCash) {
            LogUtils.i("balance = " + balance + "withdrawCash = " + withdrawCash);
            return true;
        } else {
            LogUtils.i("balance = " + balance + "withdrawCash = " + withdrawCash);
            return false;
        }
    }

    private void getWebBalance() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_DRIVER_USERINFO;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onStart() {
                super.onStart();
                dialog = new CommonTools(context).getProgressDialog(context, "获取数据中...");
                dialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                dialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        JSONObject userInfo = data.getJSONObject("userInfo");
                        String cashNum = CommonTools.judgeNull(userInfo, "availableBalance", "0");
                        balance = Float.parseFloat(cashNum);
                        etCashNum.setHint("可提现金额为" + cashNum + "元");
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
    }


    public void setPoint(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0,
                                s.toString().indexOf(".") + 3);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }

                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        editText.setText(s.subSequence(0, 1));
                        editText.setSelection(1);
                        return;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
