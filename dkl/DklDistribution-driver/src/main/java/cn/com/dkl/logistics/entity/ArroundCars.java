package cn.com.dkl.logistics.entity;

import java.io.Serializable;

/**
 * 周边车辆
 * Created by LiuXing on 2016/12/12.
 */
public class ArroundCars implements Serializable{
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        /**
         * 车id
         */
        private String carId = "";
        /**
         * 车长
         */
        private String carLength = "";
        /**
         * 车长单位
         */
        private String lengthUnit = "";
        /**
         * 车牌号
         */
        private String carNumber = "";
        /**
         * 车辆型号
         */
        private String carType = "";

        /**
         * 车辆载重
         */
        private String carLoad = "";
        /**
         * 重量单位
         */
        private String weightUnit = "";
        /**
         * 位置经度
         */
        private String longitude = "";
        /**
         *
         */
        private String volumnunt = "";
        /**
         * 范围半径
         */
        private String radius = "";
        /**
         * 街道号码
         */
        private String streetNumber = "";
        /**
         * 司机真实姓名
         */
        private String driverRealName = "";
        /**
         * 地区
         */
        private String district= "";
        /**
         * 状态
         */
        private String status = "";
        /**
         * 用户名
         */
        private String userName = "";
        /**
         *
         */
        private String volumn = "";
        /**
         * 默认司机
         */
        private String defaultDriver = "";
        /**
         * 司机手机号码
         */
        private String driverMobile = "";
        /**
         * 街道
         */
        private String street = "";
        /**
         *
         */
        private String dispathStatus = "";
        /**
         * 定位时间
         */
        private String locationTime = "";
        /**
         * 城市
         */
        private String city = "";
        /**
         * 地址
         */
        private String address = "";
        /**
         *
         */
        private String loadStatus = "";
        /**
         * 公司名称
         */
        private String companyName = "";
        /**
         * 省份
         */
        private String province = "";
        /**
         * 位置纬度
         */
        private String latitude = "";
        /**
         * 车主电话
         */
        private String ownerMobile = "";
        /**
         * 车主姓名
         */
        private String ownerName = "";

        public ArroundCars() {
            super();
        }

        public ArroundCars(String carId, String carLength, String lengthUnit, String carNumber, String carType,
                           String carLoad, String weightUnit, String longitude, String volumnunt, String radius,
                           String streetNumber, String driverRealName, String district, String status, String userName, String volumn,
                           String defaultDriver, String driverMobile, String street, String dispathStatus, String locationTime, String city,
                           String address, String loadStatus, String companyName, String province, String latitude, String ownerMobile, String ownerName) {
            super();

            this.carId = carId;
            this.carLength = carLength;
            this.lengthUnit = lengthUnit;
            this.carNumber = carNumber;
            this.carType = carType;
            this.carLoad = carLoad;
            this.weightUnit = weightUnit;
            this.longitude = longitude;
            this.volumnunt = volumnunt;
            this.radius = radius;
            this.streetNumber = streetNumber;
            this.driverRealName = driverRealName;
            this.district = district;
            this.status = status;
            this.userName = userName;
            this.volumn = volumn;
            this.defaultDriver = defaultDriver;
            this.driverMobile = driverMobile;
            this.street = street;
            this.dispathStatus = dispathStatus;
            this.locationTime = locationTime;
            this.city = city;
            this.address = address;
            this.loadStatus = loadStatus;
            this.companyName = companyName;
            this.province = province;
            this.latitude = latitude;
            this.ownerMobile = ownerMobile;
            this.ownerName = ownerName;

        }

        public void setCarId(String carId) {
            this.carId = carId;
        }

        public String getCarId() {
            return carId;
        }

        public void setCarNumber(String carNumber) {
            this.carNumber = carNumber;
        }

        public String getCarNumber() {
            return carNumber;
        }

        public void setCarType(String carType) {
            this.carType = carType;
        }

        public String getCarType() {
            return carType;
        }

        public void setCarLength(String carLength) {
            this.carLength = carLength;
        }

        public String getCarLength() {
            return carLength;
        }

        public void setLengthUnit(String lengthUnit) {
            this.lengthUnit = lengthUnit;
        }

        public String getLengthUnit() {
            return lengthUnit;
        }

        public void setCarLoad(String carLoad) {
            this.carLoad = carLoad;
        }

        public String getCarLoad() {
            return carLoad;
        }

        public void setWeightUnit(String weightUnit) {
            this.weightUnit = weightUnit;
        }

        public String getWeightUnit() {
            return weightUnit;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setVolumnunt(String volumnunt) {
            this.volumnunt = volumnunt;
        }

        public String getVolumnunt() {
            return volumnunt;
        }

        public void setRadius(String radius) {
            this.radius = radius;
        }

        public String getRadius() {
            return radius;
        }

        public void setStreetNumber(String streetNumber) {
            this.streetNumber = streetNumber;
        }

        public String getStreetNumber() {
            return streetNumber;
        }

        public void setDriverRealName(String driverRealName) {
            this.driverRealName = driverRealName;
        }

        public String getDriverRealName() {
            return driverRealName;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getDistrict() {
            return district;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserName() {
            return userName;
        }

        public void setVolumn(String volumn) {
            this.volumn = volumn;
        }

        public String getVolumn() {
            return volumn;
        }



        public void setDefaultDriver(String defaultDriver) {
            this.defaultDriver = defaultDriver;
        }

        public String getDefaultDriver() {
            return defaultDriver;
        }

        public void setDriverMobile(String driverMobile) {
            this.driverMobile = driverMobile;
        }

        public String getDriverMobile() {
            return driverMobile;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getStreet() {
            return street;
        }

        public void setDispathStatus(String dispathStatus) {
            this.dispathStatus = dispathStatus;
        }

        public String getDispathStatus() {
            return dispathStatus;
        }

        public void setLocationTime(String locationTime) {
            this.locationTime = locationTime;
        }

        public String getLocationTime() {
            return locationTime;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCity() {
            return city;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddress() {
            return address;
        }

        public void setLoadStatus(String loadStatus) {
            this.loadStatus = loadStatus;
        }

        public String getLoadStatus() {
            return loadStatus;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getProvince() {
            return province;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setOwnerMobile(String ownerMobile) {
            this.ownerMobile = ownerMobile;
        }

        public String getOwnerMobile() {
            return ownerMobile;
        }

        public void setOwnerName(String ownerName) {
            this.ownerName = ownerName;
        }

        public String getOwnerName() {
            return ownerName;
        }


        @Override
        public String toString() {
            return "ArroundCars [carId=" + carId + ", carLength=" + carLength + ", lengthUnit=" + lengthUnit + ", carNumber="
                    + carNumber + ", carType=" + carType + ", carLoad=" + carLoad + ", weightUnit=" + weightUnit
                    + ", longitude=" + longitude+ ", volumnunt=" + volumnunt + ", radius=" + radius + ", streetNumber=" + streetNumber + ", driverRealName="
                    + driverRealName + ", district=" + district + ", status=" + status + ", userName=" + userName + ", volumn=" + volumn + ", defaultDriver="
                    + defaultDriver + ", driverMobile=" + driverMobile + ", street=" + street
                    + "dispathStatus=" + dispathStatus + ", locationTime=" + locationTime + ", city=" + city + ", address=" + address + ", loadStatus="
                    + loadStatus + ", companyName=" + companyName + ", province=" + province + ", latitude=" + latitude + ", ownerMobile=" + ownerMobile
                    + ", ownerName=" + ownerName + "]";
        }



}
