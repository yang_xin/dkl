package cn.com.dkl.logistics.entity;

import java.io.Serializable;

/**
 * 周边商超
 * Created by LiuXing on 2017/2/14.
 */

public class ArroundSupermarket implements Serializable {

    /**
     * SUPER_NUMBER : P0003
     * CREATE_USER : admin
     * UPDATE_DATE : 1481856241000
     * SUPER_ADDRESS : 广州天河元岗路308号
     * SUPER_LINKMOBILE : 13975245994
     * UPDATE_USER : admin
     * SUPER_STATUS : 0
     * DEL_FLAG : 1
     * REMARKS : null
     * SUPER_NAME : 万福隆
     * SUPER_LAT : 23.5152695
     * CREATE_DATE : 1481795815000
     * SUPER_LON : 117.025852
     * SUPER_LINKNAME : 剑川
     * SUPER_ID : 4f6b7bafffe24617aad36b39b18fc494
     */


    private String SUPER_NUMBER;
    private String CREATE_USER;
    private long UPDATE_DATE;
    private String SUPER_ADDRESS;
    private String SUPER_LINKMOBILE;
    private String UPDATE_USER;
    private String SUPER_STATUS;
    private String DEL_FLAG;
    private Object REMARKS;
    private String SUPER_NAME;
    private String SUPER_LAT;
    private long CREATE_DATE;
    private String SUPER_LON;
    private String SUPER_LINKNAME;
    private String SUPER_ID;

    public String getSUPER_NUMBER() {
        return SUPER_NUMBER;
    }

    public void setSUPER_NUMBER(String SUPER_NUMBER) {
        this.SUPER_NUMBER = SUPER_NUMBER;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public void setCREATE_USER(String CREATE_USER) {
        this.CREATE_USER = CREATE_USER;
    }

    public long getUPDATE_DATE() {
        return UPDATE_DATE;
    }

    public void setUPDATE_DATE(long UPDATE_DATE) {
        this.UPDATE_DATE = UPDATE_DATE;
    }

    public String getSUPER_ADDRESS() {
        return SUPER_ADDRESS;
    }

    public void setSUPER_ADDRESS(String SUPER_ADDRESS) {
        this.SUPER_ADDRESS = SUPER_ADDRESS;
    }

    public String getSUPER_LINKMOBILE() {
        return SUPER_LINKMOBILE;
    }

    public void setSUPER_LINKMOBILE(String SUPER_LINKMOBILE) {
        this.SUPER_LINKMOBILE = SUPER_LINKMOBILE;
    }

    public String getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(String UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public String getSUPER_STATUS() {
        return SUPER_STATUS;
    }

    public void setSUPER_STATUS(String SUPER_STATUS) {
        this.SUPER_STATUS = SUPER_STATUS;
    }

    public String getDEL_FLAG() {
        return DEL_FLAG;
    }

    public void setDEL_FLAG(String DEL_FLAG) {
        this.DEL_FLAG = DEL_FLAG;
    }

    public Object getREMARKS() {
        return REMARKS;
    }

    public void setREMARKS(Object REMARKS) {
        this.REMARKS = REMARKS;
    }

    public String getSUPER_NAME() {
        return SUPER_NAME;
    }

    public void setSUPER_NAME(String SUPER_NAME) {
        this.SUPER_NAME = SUPER_NAME;
    }

    public String getSUPER_LAT() {
        return SUPER_LAT;
    }

    public void setSUPER_LAT(String SUPER_LAT) {
        this.SUPER_LAT = SUPER_LAT;
    }

    public long getCREATE_DATE() {
        return CREATE_DATE;
    }

    public void setCREATE_DATE(long CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public String getSUPER_LON() {
        return SUPER_LON;
    }

    public void setSUPER_LON(String SUPER_LON) {
        this.SUPER_LON = SUPER_LON;
    }

    public String getSUPER_LINKNAME() {
        return SUPER_LINKNAME;
    }

    public void setSUPER_LINKNAME(String SUPER_LINKNAME) {
        this.SUPER_LINKNAME = SUPER_LINKNAME;
    }

    public String getSUPER_ID() {
        return SUPER_ID;
    }

    public void setSUPER_ID(String SUPER_ID) {
        this.SUPER_ID = SUPER_ID;
    }
}
