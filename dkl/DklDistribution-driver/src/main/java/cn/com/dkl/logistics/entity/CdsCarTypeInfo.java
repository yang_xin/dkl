package cn.com.dkl.logistics.entity;

import java.io.Serializable;

public class CdsCarTypeInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3026640381217847237L;
    /**
     * 名称
     */
    public String NAME = "";
    /**
     *
     */
    public String CREATE_TIME = "";
    /**
     *
     */
    public String REMARKS = "";
    /**
     *
     */
    public String HEIGHT = "";
    /**
     *
     */
    public String IS_DELETE = "";
    /**
     *
     */
    public String UPDATE_TIME = "";
    /**
     *
     */
    public String WIDTH = "";
    /**
     *
     */
    public String ICON = "";
    /**
     *
     */
    public String SORT = "";
    /**
     *
     */
    public String WEIGHT = "";
    /**
     *
     */
    public String VOLUMN = "";
    /**
     *
     */
    public String WEIGHT_UNIT = "";
    /**
     *
     */
    public String BRAND = "";
    /**
     *
     */
    public String LENGTH = "";
    /**
     *
     */
    public String IS_FORBID = "";
    /**
     *
     */
    public String CAR_TYPE_ID = "";
}
