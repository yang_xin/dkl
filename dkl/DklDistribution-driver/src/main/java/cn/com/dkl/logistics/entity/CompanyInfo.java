package cn.com.dkl.logistics.entity;

import java.io.Serializable;

/**
 * Created by Xillen on 2017/9/5.
 */

public class CompanyInfo implements Serializable {

    /**
     * 公司ID
     */
    public String COMPANY_ID = "";

    /**
     * 公司编号
     */
    public String COMPANY_NO = "";

    /**
     * 公司名称
     */
    public String COMPANY_NAME = "";

    /**
     * logo
     */
    public String LOGO_NAME = "";

    /**
     * 所在省
     */
    public String COMPANY_PROVINCE = "";

    /**
     * 所在市
     */
    public String COMPANY_CITY = "";

    /**
     * 公司详细地址
     */
    public String COMPANY_ADDRESS = "";

    /**
     * 联系人
     */
    public String LINK_MAN = "";

    /**
     * 联系手机
     */
    public String LINK_MOBILE = "";

    /**
     * 联系电话
     */
    public String LINK_TELEPHONE = "";

    /**
     * 联系QQ
     */
    public String LINK_QQ = "";

    /**
     * 公司介绍
     */
    public String COMPANY_INTRODUCTION = "";

    /**
     * 公司状态
     */
    public String STATUS = "";
}
