package cn.com.dkl.logistics.entity;

public class ConditionType {
    /**
     * 车型
     */
    public static final int TYPE_CAR_TYPE = 1001;
    /**
     * 车长
     */
    public static final int TYPE_CAR_LENGTH = 1002;
    /**
     * 车辆载重
     */
    public static final int TYPE_CAR_LOAD = 1003;
    /**
     * 货物重量
     */
    public static final int TYPE_GOOD_WEIGHT = 1004;
    /**
     * 找车页面
     */
    public static final int TYPE_FIND_CARS = 1005;
    /**
     * 找货页面
     */
    public static final int TYPE_FIND_GOODS = 1006;
    /**
     * 找专线页面
     */
    public static final int TYPE_FIND_LINES = 1007;
    /**
     * 我的页面
     */
    public static final int TYPE_MINE = 1008;
    /**
     * 运输方式
     */
    public static final int TYPE_TRANSPORT_WAY = 1009;
    /**
     * 货物类型
     */
    public static final int TYPE_GOODS_TYPE = 10010;
    /**
     * 车牌号类型
     */
    public static final int TYPE_CARS_NUMBER = 10011;
    /**
     * 参考时效(小)类型
     */
    public static final int TYPE_MIN_EFFECTIVE_DAY = 10012;
    /**
     * 参考时效(大)类型
     */
    public static final int TYPE_MAX_EFFECTIVE_DAY = 10013;
    /**
     * 车牌开头类型
     */
    public static final int TYPE_NUMBER_HEAD = 10014;
    /**
     * 司机列表
     */
    public static final int TYPE_DRIVER = 10015;
    /**
     * 车辆来源
     */
    public static final int TYPE_CAR_FROM = 10016;
    /**
     * 频率单位
     */
    public static final int TYPE_FREQUENCY_UNIT = 10017;
    /**
     * 定位时间单位
     */
    public static final int TYPE_PERIOD_UNIT = 10018;
    /**
     * 园区
     */
    public static final int TYPE_PARK = 10019;
    /**
     * 企业
     */
    public static final int TYPE_COMPANY = 10020;
}
