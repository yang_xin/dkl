package cn.com.dkl.logistics.entity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;

public class DataManager {
    /**
     * the instance of DataManager
     */
    private static DataManager DATAMANAGER;
    /**
     * the context the instance running in ,through which it can access the current resources
     */
    private Context mContext;
    /**
     * the preference use for access the user preference
     */
    private SharedPreferences mPreferences;
    /**
     * preference editor
     */
    private Editor mEditor;
    /**
     * the name of user info preference
     */
    public static String PREFERENCE_USER_INFO = "hexway.logistics.userinfo";
    /**
     * the name of user setting preference
     */
    public static String PREFERENCE_SETTING = "hexway.logistics.setting";
    /**
     * the root path use for copy the application data to
     */
    private String mCopyDestinationPathPrefix;
    private String dbDirPath;

    public static void initialize(Context context) {
        if (DATAMANAGER == null) {
            DATAMANAGER = new DataManager(context);
        }
    }

    /**
     * @return the instance of DataManger
     * @Author hxd
     */
    public static DataManager getInstance() {
        return DATAMANAGER;
    }

    private DataManager(Context context) {
        mContext = context;
        mPreferences = mContext.getSharedPreferences(PREFERENCE_SETTING, Activity.MODE_PRIVATE);
        mEditor = mPreferences.edit();

        boolean sdCardExist = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (sdCardExist) {

            mCopyDestinationPathPrefix = Environment.getExternalStorageDirectory().getPath() + "/" + mContext.getPackageName();
        } else {

            mCopyDestinationPathPrefix = mContext.getFilesDir().getPath();
        }
    }

    /**
     * the root path use for copy the application data to
     *
     * @author guochaohui
     */
    public String getPackagePathPrefix() {
        return mCopyDestinationPathPrefix;
    }

}
