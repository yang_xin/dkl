package cn.com.dkl.logistics.entity;

import java.io.Serializable;

/** 配送点信息
 * Created by magic on 2016/12/15.
 */

public class DeliverBean implements Serializable{
    /**
     * 省
     */
    String PROVINCE;
    /**
     * 市
     */
    String CITY;
    /**
     * 区
     */
    String DISTRICT;
    /**
     * 详细地址
     */
    String CONTACT_ADDRESS;
    /**
     * 纬度
     */
    String LATITUDE;
    /**
     * 经度
     */
    String LONGITUDE;

    public String getPROVINCE() {
        return PROVINCE;
    }

    public void setPROVINCE(String PROVINCE) {
        this.PROVINCE = PROVINCE;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getDISTRICT() {
        return DISTRICT;
    }

    public void setDISTRICT(String DISTRICT) {
        this.DISTRICT = DISTRICT;
    }

    public String getCONTACT_ADDRESS() {
        return CONTACT_ADDRESS;
    }

    public void setCONTACT_ADDRESS(String CONTACT_ADDRESS) {
        this.CONTACT_ADDRESS = CONTACT_ADDRESS;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }
}
