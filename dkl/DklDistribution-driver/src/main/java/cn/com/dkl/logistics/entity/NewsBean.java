package cn.com.dkl.logistics.entity;

import java.io.Serializable;

/** 消息模型
 * Created by magic on 2017/12/1.
 */

public class NewsBean implements Serializable {
    String STATUS;
    String SOURCEID;
    String USERMESSAGE_ID;
    String SUMMARY;

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSOURCEID() {
        return SOURCEID;
    }

    public void setSOURCEID(String SOURCEID) {
        this.SOURCEID = SOURCEID;
    }

    public String getUSERMESSAGE_ID() {
        return USERMESSAGE_ID;
    }

    public void setUSERMESSAGE_ID(String USERMESSAGE_ID) {
        this.USERMESSAGE_ID = USERMESSAGE_ID;
    }

    public String getSUMMARY() {
        return SUMMARY;
    }

    public void setSUMMARY(String SUMMARY) {
        this.SUMMARY = SUMMARY;
    }
}
