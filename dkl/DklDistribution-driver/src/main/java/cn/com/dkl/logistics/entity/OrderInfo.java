package cn.com.dkl.logistics.entity;

/**
 * 订单信息
 *
 * @author Dao
 */
public class OrderInfo {
    /**
     * 货物名称
     */
    private String goodsName;
    /**
     * 货物重量
     */
    private String goodsLoadWeight;
    /**
     * 总费用
     */
    private Integer totalCost;
    /**
     * 接货地点
     */
    private String goodsFromAddress;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 是否要回单
     */
    private String isReceipt;
    /**
     * 开发票
     */
    private String invoiceTitle;
    /**
     * 提货单地址
     */
    private String deliveryAddress;
    /**
     * 担保金
     */
    private String insuranceCost;
    /**
     * 用户头像
     */
    private String userHeadPhotoUrl;
    /**
     * 件数
     */
    private String pieceNumber;
    /**
     * 附件运费
     */
    private String additionalFreight;
    /**
     * 里程
     */
    private String distance;
    /**
     * 用户电话
     */
    private String userPhone;
    /**
     * 特殊要求
     */
    private String specialeRquirement;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 体积
     */
    private String volume;
    /**
     * 体积单位
     */
    private String volumeUnit;
    /**
     * 运输费用
     */
    private String transportationCost;
    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 回单图片
     */
    private String receiptImg;
    /**
     * 卸货点
     */
    private String goodsToAddress;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 货物重量单位
     */
    private String goodsWeightUnit;
    /**
     * 支付金额
     */
    private String payment;
    /**
     * 更新时间
     */
    private String updateTime;

    public OrderInfo() {
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsLoadWeight() {
        return goodsLoadWeight;
    }

    public void setGoodsLoadWeight(String goodsLoadWeight) {
        this.goodsLoadWeight = goodsLoadWeight;
    }

    public Integer getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Integer totalCost) {
        this.totalCost = totalCost;
    }

    public String getGoodsFromAddress() {
        return goodsFromAddress;
    }

    public void setGoodsFromAddress(String goodsFromAddress) {
        this.goodsFromAddress = goodsFromAddress;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getIsReceipt() {
        return isReceipt;
    }

    public void setIsReceipt(String isReceipt) {
        this.isReceipt = isReceipt;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getInsuranceCost() {
        return insuranceCost;
    }

    public void setInsuranceCost(String insuranceCost) {
        this.insuranceCost = insuranceCost;
    }

    public String getUserHeadPhotoUrl() {
        return userHeadPhotoUrl;
    }

    public void setUserHeadPhotoUrl(String userHeadPhotoUrl) {
        this.userHeadPhotoUrl = userHeadPhotoUrl;
    }

    public String getPieceNumber() {
        return pieceNumber;
    }

    public void setPieceNumber(String pieceNumber) {
        this.pieceNumber = pieceNumber;
    }

    public String getAdditionalFreight() {
        return additionalFreight;
    }

    public void setAdditionalFreight(String additionalFreight) {
        this.additionalFreight = additionalFreight;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getSpecialeRquirement() {
        return specialeRquirement;
    }

    public void setSpecialeRquirement(String specialeRquirement) {
        this.specialeRquirement = specialeRquirement;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getVolumeUnit() {
        return volumeUnit;
    }

    public void setVolumeUnit(String volumeUnit) {
        this.volumeUnit = volumeUnit;
    }

    public String getTransportationCost() {
        return transportationCost;
    }

    public void setTransportationCost(String transportationCost) {
        this.transportationCost = transportationCost;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getReceiptImg() {
        return receiptImg;
    }

    public void setReceiptImg(String receiptImg) {
        this.receiptImg = receiptImg;
    }

    public String getGoodsToAddress() {
        return goodsToAddress;
    }

    public void setGoodsToAddress(String goodsToAddress) {
        this.goodsToAddress = goodsToAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGoodsWeightUnit() {
        return goodsWeightUnit;
    }

    public void setGoodsWeightUnit(String goodsWeightUnit) {
        this.goodsWeightUnit = goodsWeightUnit;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "OrderInfo [goodsName=" + goodsName + ", goodsLoadWeight=" + goodsLoadWeight + ", totalCost=" + totalCost + ", goodsFromAddress="
                + goodsFromAddress + ", createTime=" + createTime + ", isReceipt=" + isReceipt + ", invoiceTitle=" + invoiceTitle + ", deliveryAddress="
                + deliveryAddress + ", insuranceCost=" + insuranceCost + ", userHeadPhotoUrl=" + userHeadPhotoUrl + ", pieceNumber=" + pieceNumber
                + ", additionalFreight=" + additionalFreight + ", distance=" + distance + ", userPhone=" + userPhone + ", specialeRquirement="
                + specialeRquirement + ", orderNo=" + orderNo + ", volume=" + volume + ", volumeUnit=" + volumeUnit + ", transportationCost="
                + transportationCost + ", orderStatus=" + orderStatus + ", receiptImg=" + receiptImg + ", goodsToAddress=" + goodsToAddress + ", userName="
                + userName + ", goodsWeightUnit=" + goodsWeightUnit + ", payment=" + payment + ", updateTime=" + updateTime + "]";
    }

}
