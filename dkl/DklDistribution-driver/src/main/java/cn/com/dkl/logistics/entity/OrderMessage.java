package cn.com.dkl.logistics.entity;

import java.io.Serializable;

public class OrderMessage implements Serializable {
    private static final long serialVersionUID = -353003881346623857L;
    /**
     * 消息概要说明
     */
    private String summary;
    /**
     * 消息内容类型：1.新鲜资讯 2.活动福利 3.物流小秘书 4.积分提醒 5.客服助手
     */
    private String contentType;
    /**
     * 用户消息id
     */
    private String userMessageID;
    /**
     * 消息创建时间
     */
    private String createTime;
    /**
     * 状态
     */
    private String status;
    /**
     * 消息标题
     */
    private String title;
    /**
     * 订单Id
     */
    private String sourceID;
    /**
     * 消息类型
     */
    private String sourceType;

    public OrderMessage() {
        super();
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getUserMessageID() {
        return userMessageID;
    }

    public void setUserMessageID(String userMessageID) {
        this.userMessageID = userMessageID;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    @Override
    public String toString() {
        return "OrderMessage [summary=" + summary + ", contentType=" + contentType + ", userMessageID=" + userMessageID + ", createTime=" + createTime
                + ", status=" + status + ", title=" + title + ", sourceID=" + sourceID + ", sourceType=" + sourceType + "]";
    }

}
