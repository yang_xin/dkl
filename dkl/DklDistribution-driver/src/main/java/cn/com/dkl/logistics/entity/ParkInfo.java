package cn.com.dkl.logistics.entity;

import java.io.Serializable;

/**
 * Created by Xillen on 2017/8/19.
 */

public class ParkInfo implements Serializable {
    private static final long serialVersionUID = 0L;

    /**
     * 园区编号
     */
    public String PARK_NO = "";

    /**
     * 园区名称
     */
    public String PARK_NAME = "";

    /**
     * 公司名称
     */
    public String GROUP_NAME = "";

    /**
     * logo
     */
    public String LOGO_NAME = "";

    /**
     * 园区所在省
     */
    public String CAPITAL = "";

    /**
     * 园区所在市
     */
    public String DOWNTOWN = "";

    /**
     * 拥有物流公司数目
     */
    public String HASCOMPANY_NUM = "";

    /**
     * 拥有专线数目
     */
    public String HASLINE_NUM = "";

    /**
     * 联系人姓名
     */
    public String LINK_MAN = "";

    /**
     * 联系手机
     */
    public String LINK_MOBILE = "";

    /**
     * 联系电话
     */
    public String LINK_TELEPHONE = "";

    /**
     * 联系QQ
     */
    public String LINK_QQ = "";

    /**
     * 联系FAX
     */
    public String LINK_FAX = "";

}
