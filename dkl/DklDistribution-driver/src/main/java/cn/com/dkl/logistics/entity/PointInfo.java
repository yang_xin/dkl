package cn.com.dkl.logistics.entity;

import java.io.Serializable;

/**
 * 提货点、到货点信息
 *
 * @author Xillen
 */

public class PointInfo implements Serializable {
    private static final long serialVersionUID = 4781191795238639246L;
    /**
     * 省
     */
    private String province = "";
    /**
     * 市
     */
    private String city = "";
    /**
     * 区
     */
    private String district = "";
    /**
     * 别名
     */
    private String alias = "";
    /**
     * 联系电话
     */
    private String linkPhone = "";
    /**
     * 详细地址
     */
    private String detailsAddress = "";
    /**
     * 具体地址(省+市+区+详细地址)
     */
    private String contactAddress = "";
    /**
     * 联系人
     */
    private String linkMan = "";
    /**
     * 提货、回单图片路径
     */
    private String imgpath = "";
    /**
     * 真实路径
     */
    private String picPath = "";
    /**
     * 是否提货,到达,回单
     */
    private int pointStatus = 0;
    /**
     * 类型：1为装货点，2为卸货点
     */
    private int type = 0;
    /**
     * 装货点或卸货点排序
     */
    private int sort = 0;
    /**
     * 起点标识:1：是；0：不是
     */
    private int isFirst = 0;
    /**
     * 终点标识:1：是；0：不是
     */
    private int isLast = 0;
    /**
     * 地址id
     */
    private String deliverId = "";
    /**
     * 位置纬度
     */
    private String latitude = "";
    /**
     * 位置经度
     */
    private String longitude = "";
    /**
     * 运单号
     */
    private String waybillnum = "";
    /**
     * 订单号
     */
    private String orderno = "";
    /**
     * 运单ID
     */
    private String waybillid = "";
    /**
     * 提货回单时间
     */
    private String operatetime = "";
    /**
     * 订单状态
     * @return
     */
    private String waybillstatus = "";
    /**
     * 支付类型
     */
    private String payway = "";
    /**
     * 支付费用
     */
    private String totalcost = "";
    /**
     * 备注
     */
    private String remarks = "";
    /**
     * 运输方式
     */
    private String transportway = "";
    /**
     * 货物名称
     */
    private String goodsname = "";
    /**
     * 货物类型
     */
    private String goodstype = "";
    /**
     * 货物重量
     */
    private String goodsweight = "";
    /**
     * 货物重量单位
     */
    private String weightunit = "";
    /**
     * 货物体积
     */
    private String goodsvolume = "";
    /**
     * 货物体积单位
     */
    private String volumeunit = "";
    /**
     * 货物价值
     */
    private String worth = "";
    /**
     * 货物数量
     */
    private String pieceno = "";
    /**
     * 货物包装类型
     */
    private String packagetype = "";
    /**
     * 增值服务-保价运输
     */
    private String isInsuredtransport = "";
    /**
     * 增值服务-保价运输费用
     */
    private String insuredtransportCost = "";
    /**
     * 增值服务-代收货款
     */
    private String isPaymentcollection = "";
    /**
     * 增值服务-代收货款费用
     */
    private String paymentcollectionCost = "";
    /**
     * 增值服务-送货上门
     */
    private String isDelivery = "";
    /**
     * 增值服务-送货上门费用
     */
    private String deliveryCost = "";
    /**
     * 增值服务-卸车服务
     */
    private String isUnloading = "";
    /**
     * 增值服务-上楼服务
     */
    private String isUpstairs = "";

    /**
     * 增值服务-装车服务
     */
    private String isLoading = "";

    public String getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(String isLoading) {
        this.isLoading = isLoading;
    }

    public String getIsInsuredtransport() {
        return isInsuredtransport;
    }

    public void setIsInsuredtransport(String isInsuredtransport) {
        this.isInsuredtransport = isInsuredtransport;
    }

    public String getInsuredtransportCost() {
        return insuredtransportCost;
    }

    public void setInsuredtransportCost(String insuredtransportCost) {
        this.insuredtransportCost = insuredtransportCost;
    }

    public String getIsPaymentcollection() {
        return isPaymentcollection;
    }

    public void setIsPaymentcollection(String isPaymentcollection) {
        this.isPaymentcollection = isPaymentcollection;
    }

    public String getPaymentcollectionCost() {
        return paymentcollectionCost;
    }

    public void setPaymentcollectionCost(String paymentcollectionCost) {
        this.paymentcollectionCost = paymentcollectionCost;
    }

    public String getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(String isDelivery) {
        this.isDelivery = isDelivery;
    }

    public String getDeliveryCost() {
        return deliveryCost;
    }

    public void setDeliveryCost(String deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    public String getIsUnloading() {
        return isUnloading;
    }

    public void setIsUnloading(String isUnloading) {
        this.isUnloading = isUnloading;
    }

    public String getIsUpstairs() {
        return isUpstairs;
    }

    public void setIsUpstairs(String isUpstairs) {
        this.isUpstairs = isUpstairs;
    }

    public String getPackagetype() {
        return packagetype;
    }

    public void setPackagetype(String packagetype) {
        this.packagetype = packagetype;
    }

    public String getPieceno() {
        return pieceno;
    }

    public void setPieceno(String pieceno) {
        this.pieceno = pieceno;
    }

    public String getWorth() {
        return worth;
    }

    public void setWorth(String worth) {
        this.worth = worth;
    }

    public String getVolumeunit() {
        return volumeunit;
    }

    public void setVolumeunit(String volumeunit) {
        this.volumeunit = volumeunit;
    }

    public String getGoodsweight() {
        return goodsweight;
    }

    public void setGoodsweight(String goodsweight) {
        this.goodsweight = goodsweight;
    }

    public String getWeightunit() {
        return weightunit;
    }

    public void setWeightunit(String weightunit) {
        this.weightunit = weightunit;
    }

    public String getGoodsvolume() {
        return goodsvolume;
    }

    public void setGoodsvolume(String goodsvolume) {
        this.goodsvolume = goodsvolume;
    }

    public String getTransportway() {
        return transportway;
    }

    public void setTransportway(String transportway) {
        this.transportway = transportway;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getGoodstype() {
        return goodstype;
    }

    public void setGoodstype(String goodstype) {
        this.goodstype = goodstype;
    }

    public String getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(String totalcost) {
        this.totalcost = totalcost;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPayway() {
        return payway;
    }

    public void setPayway(String payway) {
        this.payway = payway;
    }

    public String getWaybillstatus() {
        return waybillstatus;
    }

    public void setWaybillstatus(String waybillstatus) {
        this.waybillstatus = waybillstatus;
    }

    public String getOperatetime() {
        return operatetime;
    }

    public void setOperatetime(String operatetime) {
        this.operatetime = operatetime;
    }

    public PointInfo() {
        super();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public String getDetailsAddress() {
        return detailsAddress;
    }

    public void setDetailsAddress(String detailsAddress) {
        this.detailsAddress = detailsAddress;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public int getPointStatus() {
        return pointStatus;
    }

    public void setPointStatus(int pointStatus) {
        this.pointStatus = pointStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(int isFirst) {
        this.isFirst = isFirst;
    }

    public int getIsLast() {
        return isLast;
    }

    public void setIsLast(int isLast) {
        this.isLast = isLast;
    }

    public String getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(String deliverId) {
        this.deliverId = deliverId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getWaybillnum() {
        return waybillnum;
    }

    public void setWaybillnum(String waybillnum) {
        this.waybillnum = waybillnum;
    }

    public String getWaybillid() {
        return waybillid;
    }

    public void setWaybillid(String waybillid) {
        this.waybillid = waybillid;
    }
}
