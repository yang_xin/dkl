package cn.com.dkl.logistics.entity;

/**
 * @author guochaohui
 */
public class RoleType {
    /**
     * 物流企业id
     */
    public final static String LOGISTICS_ENTERPRISE = "4e42ec1f537f4644a3bfc820eddeb5f5";
    /**
     * 个人货主id
     */
    public final static String GOODS_OWNER = "4a35c2f270cb4e4b909891be77d66403";
    /**
     * 个人车主id
     */
    public final static String CARS_OWNER = "c15936986fe34dc18d64ff4aef92e201";

}
