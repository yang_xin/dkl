package cn.com.dkl.logistics.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 配送点列表
 * Created by magic on 2016/12/15.
 */

public class TstOrderBean implements Serializable{
    /**
     * 订单号
     */
    String ORDERNO;
    /**
     * 运单状态
     */
    String ORDER_STATUS;
    /**
     * 配送点列表
     */
    List<DeliverBean> deliverList;

    public String getORDER_STATUS() {
        return ORDER_STATUS;
    }

    public void setORDER_STATUS(String ORDER_STATUS) {
        this.ORDER_STATUS = ORDER_STATUS;
    }

    public List<DeliverBean> getDeliverList() {
        return deliverList;
    }

    public void setDeliverList(List<DeliverBean> deliverList) {
        this.deliverList = deliverList;
    }

    public String getORDERNO() {
        return ORDERNO;
    }

    public void setORDERNO(String ORDERNO) {
        this.ORDERNO = ORDERNO;
    }
}
