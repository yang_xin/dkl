package cn.com.dkl.logistics.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.overlayutil.DrivingRouteOverlay;
import com.baidu.mapapi.overlayutil.OverlayManager;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.baidu.mapapi.search.route.BikingRouteResult;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.google.gson.Gson;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.adapter.OrderListAdapter;
import cn.com.dkl.logistics.adapter.RouteLine2Adapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.DoneListActivity;
import cn.com.dkl.logistics.driver.MainActivity;
import cn.com.dkl.logistics.driver.MyNewsActivity;
import cn.com.dkl.logistics.driver.OrderDetailActivity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.ArroundCars;
import cn.com.dkl.logistics.entity.ArroundSupermarket;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.DeliverBean;
import cn.com.dkl.logistics.entity.NewsBean;
import cn.com.dkl.logistics.entity.OrderMessage;
import cn.com.dkl.logistics.entity.PointInfo;
import cn.com.dkl.logistics.entity.RoleType;
import cn.com.dkl.logistics.entity.TstOrderBean;
import cn.com.dkl.logistics.service.LocationService;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.Constant;
import cn.com.dkl.logistics.utils.MapNavigationUtil;
import cn.com.dkl.logistics.utils.MapUtil;
import cn.com.dkl.logistics.utils.NetworkCallBack;
import cn.com.dkl.logistics.view.CustomProgressDialog;
import cn.com.dkl.logistics.view.MyDrivingRouteOverlay;
import cn.com.dkl.logistics.view.RouteLineView;

import static cn.com.dkl.logistics.driver.R.id.tvDestination;


/**
 * 地图首页 Created by LiuXing on 2016/12/12.
 */
public class ArroundCarFragment extends BasicFragment implements SeekBar.OnSeekBarChangeListener, MainActivity.OrderBackListener, MainActivity.workListener, MainActivity.showWorkPlan, MainActivity.orderDetailListener, MainActivity.AuthChangeListener, RouteLineView.RouteLineViewListener {
    @Bind(R.id.llLastNews)
    LinearLayout mLlLastNews;
    @Bind(R.id.routeLineView)
    RouteLineView routeLineView;
    @Bind(R.id.tvNewsCount)
    TextView mTvNewsCount;
    @Bind(tvDestination)
    TextView mTvDestination;
    @Bind(R.id.rbEmptyLoad)
    RadioButton mRbEmptyLoad;
    @Bind(R.id.rbHalfLoad)
    RadioButton mRbHalfLoad;
    @Bind(R.id.rbFullLoad)
    RadioButton mRbFullLoad;
    @Bind(R.id.rgLoadState)
    RadioGroup mRgLoadState;
    @Bind(R.id.btnReleaseGoods)
    Button mBtnReleaseGoods;
    @Bind(R.id.tvOrderCount)
    TextView tvOrderCount;
    @Bind(R.id.btnLeft)
    Button mBtnLeft;
    @Bind(R.id.mMapView)
    MapView mMapView;
    @Bind(R.id.btnLocate)
    Button mBtnLocate;
    @Bind(R.id.btnOrder1)
    LinearLayout mBtnOrder1;
    @Bind(R.id.seekBar)
    SeekBar mSeekBar;
    @Bind(R.id.tvProgress)
    TextView mTvProgress;
    @Bind(R.id.btnVoice)
    Button mBtnVoice;
    @Bind(R.id.tvLastNews)
    TextView mTvLastNews;
    @Bind(R.id.minus)
    Button mMinus;
    @Bind(R.id.plus)
    Button mPlus;

    private static final String TAG = "ArroundCarFragment";
    private Context mContext;
    private View view;

    RouteLine route = null;
    OverlayManager routeOverlay = null;
    RoutePlanSearch mRoutePlanSearch = null;    // 搜索模块，也可去掉地图模块独立使用

    public static List<Activity> activityList = new LinkedList<Activity>();
    public static final String ROUTE_PLAN_NODE = "routePlanNode";

    private BaiduMap mBaiduMap;
    boolean isFirstLoc = true;// 是否首次定位
    // 定位相关
    private LocationClient mLocClient;
    public MyLocationListenner myListener = new MyLocationListenner();
    private LocationMode mCurrentMode;
    BitmapDescriptor mCurrentMarker;
    OverlayOptions mMarkerOption = null;
    BitmapDescriptor mMarker = BitmapDescriptorFactory.fromResource(R.drawable.pin_car);
    private List<OverlayOptions> optionsList = new ArrayList<OverlayOptions>();
    private OverlayManager manager;
    private InfoWindow mInfoWindow;
    private SharedPreferences sp;
    public static String mLatitude = "23.027707";//当前位置纬度
    public static String mLongitude = "113.128576";//当前位置经度
    private String mRadius = "100";//查询半径
    private CommonTools tools = new CommonTools(getActivity());
    private Dialog progressDialog, mCarstatusDialog;
    private List<ArroundCars> carList;
    private List<ArroundSupermarket> mSupermarketList;
    private int num = 18;    //地图级别

    private CustomProgressDialog dialog = null;

    private float MaxLevel;
    private float MinLevel;
    private MapStatus mapStatus;

    private GeoCoder mSearch = GeoCoder.newInstance();
    private LatLng centerPoint;
    private OnGetGeoCoderResultListener listener;
    private Boolean isGetAddress = false; //获取当前位置地址信息
    private PointInfo startInfo = new PointInfo();//起点信息
    private Boolean isShowAllCars = true; //是否尽可能多的显示车辆
    private Boolean isFristDrawPoint = false;//是否点击定位
    private Boolean isFristDrawPointTwo = true;//是否第一次操作
    MapNavigationUtil mNavigation;

    private String roleId;
    String mOrderStatus;//订单状态
    public static String mOrderStatus2;//订单状态
    String mOrderNo;//订单号
    TstOrderBean mOrderBean;
    Thread mLocThread;
    private List<Overlay> mMarkList = new ArrayList<>();
    /**
     * 出车状态:出车1，收车2
     */
    private String loadStatus = EMPTY_LOAD;
    public static final String EMPTY_LOAD = "0"; // 空载
    public static final String HALF_LOAD = "1"; // 半载
    public static final String FULL_LOAD = "2"; // 满载
    /**
     * 车辆状态
     */
    private String carStatus;
    public static final String OUT_CAR = "1";
    public static final String CLOSE_CAR = "0";
    /**
     * 快捷按钮状态
     */
    int btnStatus;
    int NOAUTH = 0;//未认证
    int NOCAR = 1; //未出车
    final int NOORDER = 2; //未设置工作单
    int WORKORDER = 3;//已设置工作单

    /**
     * 实名认证次数
     */
    int mAuthNumber;
    /**
     * 跳转运单详情标识
     */
    public static final int ORDERDEAIL = 1;
    public static final int DoneList = 2;

    private final static int BINDBACK = 101;
    private final static int PAYPWD = 102;

    String startTitle = "起点";
    String endTitle = "终站";
    private static boolean isRountPlan = false; // 是否规则路线
    private boolean isAddCurrent = Constant.isAddCurrent; // 规则路线时，是否增加当前位置
    MainActivity mainActivity;
    public static final int YUNDANYIBEIJIEDAN = 6001;   //运单已被接单
    public static final String WAYBILLTAKEORDER = "waybilltake";   //运单已被接单
    NewsReadRecive mNewsRecive = new NewsReadRecive();
    private boolean isShowRouteLine = false;  // 显示规划路线
    NewsBean mNewsBean; //消息数据

    private List<OrderMessage> listNews = new ArrayList<OrderMessage>();
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == YUNDANYIBEIJIEDAN){
                /*设置消息已读*/
                //运单ID一致
                if (msg.obj.toString().trim().equals(mNewsBean.getSOURCEID().trim())){
                    readNews();
                    mTvLastNews.setEllipsize(null);
                }
            }
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (MainActivity) activity;
        mainActivity.setmWork(this);
        mainActivity.setmShowPlan(this);
        mainActivity.setOnorderDetailListener(this);
        mainActivity.setAuthChangeListener(this);
        mainActivity.setOrderBackListener(this);
        mAuthNumber = mainActivity.getmAuthNumber();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityList.add(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_arround, null);
        ButterKnife.bind(this, view);
        initView();
        IntentFilter filter = new IntentFilter();
        filter.addAction("clearRountOverlay");
        filter.addAction("StartNavigation");
        mContext.registerReceiver(receiver, filter);
        IntentFilter filterNews = new IntentFilter();
        filterNews.addAction(Constant.WAYBILLTAKEORDER);
        mContext.registerReceiver(mNewsRecive,filterNews);
//        mContext.registerReceiver(naviReceiver, filter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        if (isAdded()) {
            getNews();
            sendRequest();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null) {
            mMapView.onPause();
            mBaiduMap.hideInfoWindow();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mContext.unregisterReceiver(receiver);
        mContext.unregisterReceiver(mNewsRecive);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mContext.unregisterReceiver(naviReceiver);
        mLocClient.stop();
        // 关闭定位图层
        mBaiduMap.setMyLocationEnabled(false);
//        mMapView.onDestroy();
        mMapView = null;
    }


    /**
     * 清除规划路线通知
     */
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("clearRountOverlay")) {
                boolean flag = intent.getBooleanExtra("Flag", false);
                clearRountOverlay(flag);
            } else if (intent.getAction().equals("StartNavigation")) {
                if (!isShowRouteLine) {
                    return;
                }

                int index = intent.getIntExtra("RountIndex", -1);
                List<PlanNode> nodeList = getNodeList(mOrderBean);

                if (index >= 0 && index < nodeList.size()) {
                    PlanNode node = nodeList.get(index);
                    double longitude = node.getLocation().longitude;
                    double latitude = node.getLocation().latitude;

                    MapNavigationUtil.StartOtherNavi(context, longitude, latitude);
                    LogUtils.d("经纬度： " + longitude + ", " + latitude);
                }

            }
        }
    };


    /**
     * 初始化view
     */
    private void initView() {
        mContext = getActivity();// 获取上下文对象

        routeLineView.setListener(this);

        mSeekBar.setOnSeekBarChangeListener(this);
        sp = mContext.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        dialog = new CommonTools(mContext).getProgressDialog(mContext, "正在为您加载数据...");
        dialog.setCancelable(false);

        // 地图初始化
        mBaiduMap = mMapView.getMap();
        // 显示或隐藏缩放控件
        mMapView.showZoomControls(false);
        // 显示或隐藏比例尺
        mMapView.showScaleControl(false);
        // 开启定位图层
        mBaiduMap.setMyLocationEnabled(true);
        // 关闭旋转手势
        mBaiduMap.getUiSettings().setRotateGesturesEnabled(false);
        //打开路况
//        mBaiduMap.setTrafficEnabled(true);
        mBaiduMap.setMapStatus(MapStatusUpdateFactory
                .newMapStatus(new MapStatus.Builder().zoom(num).build()));
        // 定位初始化
        mLocClient = new LocationClient(mContext);
        mLocClient.registerLocationListener(myListener);
        LocationClientOption option = new LocationClientOption();
        // 打开gps
        option.setOpenGps(true);
        // 设置坐标类型
        option.setCoorType("bd09ll");
        option.setScanSpan(2000);
        mLocClient.setLocOption(option);

        MaxLevel = mBaiduMap.getMaxZoomLevel();
        MinLevel = mBaiduMap.getMinZoomLevel();

        roleId = sp.getString(UserEntity.ROLE_ID, null);

        if (mBtnReleaseGoods.getText().toString().equals("出车")){
            carStatus = CLOSE_CAR;
//            mRgLoadState.setVisibility(View.GONE);
            updateCarStatus();
        }else if (mBtnReleaseGoods.getText().toString().equals("提货") || mBtnReleaseGoods.getText().toString().equals("收车") || mBtnReleaseGoods.getText().toString().equals("回单")){
            carStatus = OUT_CAR;
            updateCarStatus();
//            mRgLoadState.setVisibility(View.VISIBLE);
        }


        /**
         * 覆盖物点击事件
         */
        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                if (manager != null) {
                    manager.onMarkerClick(marker);
                }

                return true;
            }
        });

        /**
         * 点击地图隐藏信息窗口
         */
        mBaiduMap.setOnMapClickListener(new BaiduMap.OnMapClickListener() {

            @Override
            public boolean onMapPoiClick(MapPoi arg0) {
                return false;
            }

            @Override
            public void onMapClick(LatLng arg0) {
                mBaiduMap.hideInfoWindow();

            }
        });

        /**
         * 地图移动监听
         */
        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {

            @Override
            public void onMapStatusChangeStart(MapStatus arg0) {

            }

            @Override
            public void onMapStatusChangeFinish(MapStatus arg0) {
                if (isFristDrawPointTwo) {
                    addManagerToMapTwo();
                    isFristDrawPointTwo = false;
                }
            }

            @Override
            public void onMapStatusChange(MapStatus arg0) {

            }
        });

        listener = new OnGetGeoCoderResultListener() {
            public void onGetGeoCodeResult(GeoCodeResult result) {
                if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    //没有检索到结果
                } else {
                    mLatitude = "23.027707";
                    mLongitude = "113.128576";
                    LogUtils.i("************" + mLatitude + "****" + mLongitude);
                    centerPoint = result.getLocation();
                }
            }

            @Override
            public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
                if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    //没有找到检索结果
                } else {
                    try {
                        String provinceName = result.getAddressDetail().province;
                        String cityName = result.getAddressDetail().city;
                        String districtName = result.getAddressDetail().district;

                        String detailsAddress = "";
                        String street = result.getAddressDetail().street;
                        String streetNumber = result.getAddressDetail().streetNumber;
                        String str = result.getAddress();
                        LogUtils.i(str);
                        detailsAddress = street + streetNumber;

                        startInfo.setProvince(provinceName);
                        startInfo.setCity(cityName);
                        startInfo.setDistrict(districtName);
                        startInfo.setDetailsAddress(detailsAddress);
                        startInfo.setAlias("");
                        startInfo.setLongitude(mLongitude);
                        startInfo.setLatitude(mLatitude);
                        startInfo.setType(1);
                        startInfo.setSort(1);
                        startInfo.setIsFirst(1);
                        startInfo.setIsLast(0);

                        String linkman = sp.getString(UserEntity.NAME, "");
                        String phone = sp.getString(UserEntity.PHONE, "");

                        startInfo.setLinkMan(linkman);
                        startInfo.setLinkPhone(phone);

                        //isGetAddress = true;
                    } catch (Exception e) {
                        Toast.makeText(mContext, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        initRoutePlan();
//        setTestMapMarkers();
//        locateMyself();
        initData();

        getPersonal();

//        mNavigation = new MapNavigationUtil(getActivity(),activityList);
//        mNavigation.initNavi(new BNRoutePlanNode(113.3472970000,23.1808080000,"创意园","广州海豚科技"));
//        mNavigation.routeplanToNavi();
//        List<PlanNode> nodeList = new ArrayList<>();
//        PlanNode n1 = PlanNode.withLocation(new LatLng(23.180861,113.347982));
//        PlanNode n2 = PlanNode.withLocation(new LatLng(23.0249285546,113.2435545889));
//        PlanNode n3 = PlanNode.withLocation(new LatLng(23.2076090321,113.3467833665));
//        PlanNode n4 = PlanNode.withLocation(new LatLng(23.1370348983,113.3194299638));
//        nodeList.add(n1);
//        nodeList.add(n2);
//        nodeList.add(n3);
//        nodeList.add(n4);
//        MapUtil.makeRoutePlan(getActivity(), mRoutePlanSearch, nodeList, DrivingRoutePlanOption.DrivingPolicy.ECAR_AVOID_JAM);
//        MapUtil.makeRoutePlan(getActivity(), mRoutePlanSearch, getTestStroeList(), DrivingRoutePlanOption.DrivingPolicy.ECAR_AVOID_JAM);
    }

    /**
     * 根据配送点信息，生成地图识别的位置列表
     */
    List<PlanNode> getNodeList(TstOrderBean tstOrderBean) {
        List<PlanNode> passList = new ArrayList<>();
        List<DeliverBean> devrlist = tstOrderBean.getDeliverList();
        int devrSize = devrlist.size();
        for (int i = 0; i < devrSize; i++) {
            if ((i - 1) % 2 == 0) {
                continue;
            }
            DeliverBean bean = devrlist.get(i);
            if (bean.getLONGITUDE().equals("") || bean.getLATITUDE().equals("")) {
                Log.d(TAG, "getNodeList: " + null);
            } else {
                PlanNode node = PlanNode.withLocation(MapUtil.converter(new LatLng(Double.valueOf(bean.getLATITUDE()), Double.valueOf(bean.getLONGITUDE()))));
                Log.d(TAG, "get" + "第" + i + "个地址，纬度：" + bean.getLATITUDE() + "，经度：" + bean.getLONGITUDE());
                passList.add(node);
            }
        }
        for (int i = 0; i < devrSize; i++) {
            if (i > 1) {
                if ((i - 1) % 2 == 1) {
                    continue;
                }
            }
            DeliverBean bean = devrlist.get(i);
            if (bean.getLONGITUDE().equals("") || bean.getLATITUDE().equals("")) {
                Log.d(TAG, "getNodeList: " + null);
            } else {
                PlanNode node = PlanNode.withLocation(MapUtil.converter(new LatLng(Double.valueOf(bean.getLATITUDE()), Double.valueOf(bean.getLONGITUDE()))));
                Log.d(TAG, "get" + "第" + i + "个地址，纬度：" + bean.getLATITUDE() + "，经度：" + bean.getLONGITUDE());
                passList.add(node);
            }
        }

        return passList;
    }

    /**
     * 初始化路线规划
     */
    void initRoutePlan() {
        // 初始化路线规划
        mRoutePlanSearch = RoutePlanSearch.newInstance();
        mRoutePlanSearch.setOnGetRoutePlanResultListener(new OnGetRoutePlanResultListener() {
            @Override
            public void onGetWalkingRouteResult(WalkingRouteResult walkingRouteResult) {
                dialog.dismiss();
            }

            @Override
            public void onGetTransitRouteResult(TransitRouteResult transitRouteResult) {
                dialog.dismiss();
            }

            @Override
            public void onGetDrivingRouteResult(final DrivingRouteResult result) {
                dialog.dismiss();

                if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    Toast.makeText(getActivity(), "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
                }
                if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
                    // 起终点或途经点地址有岐义，通过以下接口获取建议查询信息
                    // result.getSuggestAddrInfo()
                    return;
                }
                if (result.error == SearchResult.ERRORNO.NO_ERROR) {
//                    nodeIndex = -1;
                    if (mOrderBean != null) {
                        setMarker(mOrderBean);
                        startTitle = getStartInfo(mOrderBean);
                        endTitle = getEndInfo(mOrderBean);
                    }


                    if (result.getRouteLines().size() >= 1) {

                        routeLineView.setVisibility(View.VISIBLE);
                        routeLineView.setRouteLine(
                                result.getRouteLines(),
                                RouteLine2Adapter.Type.DRIVING_ROUTE);
                        routeLineView.selectPolicy();

                        route = result.getRouteLines().get(0);
                        DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(mBaiduMap, startTitle, endTitle);
                        routeOverlay = overlay;
                        mBaiduMap.setOnMarkerClickListener(overlay);
                        overlay.setData(result.getRouteLines().get(0));
                        overlay.addToMap();
                        overlay.zoomToSpan();

                        DrivingRouteLine drivingRouteLine = (DrivingRouteLine) route;
                        String jamDistance = "" + drivingRouteLine.getCongestionDistance();

                        double distance = drivingRouteLine.getDistance();
                        DecimalFormat df = new DecimalFormat("###.00");
                        String distanceString = "" + df.format(distance / 1000);

                        int time = drivingRouteLine.getDuration();
                        String timeSting = "";
                        if (time / 3600 == 0) {
                            timeSting = "大约需要：" + time / 60 + "分钟";
                        } else {
                            timeSting = "大约需要：" + time / 3600 + "小时" + (time % 3600) / 60 + "分钟";
                        }

                        routeLineView.configShow(timeSting, distanceString, jamDistance);

//                        MyTransitDlg myTransitDlg = new MyTransitDlg(getActivity(),
//                                result.getRouteLines(),
//                                RouteLineAdapter.Type.DRIVING_ROUTE);
//                        myTransitDlg.setOnItemInDlgClickLinster(new MyTransitDlg.OnItemInDlgClickListener() {
//                            @Override
//                            public void onItemClick(int position) {
//                                route = result.getRouteLines().get(position);
//                                DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(mBaiduMap, startTitle, endTitle);
//                                mBaiduMap.setOnMarkerClickListener(overlay);
//                                routeOverlay = overlay;
//                                overlay.setData(result.getRouteLines().get(position));
//                                overlay.addToMap();
//                                overlay.zoomToSpan();
//                            }
//                        });
//                        myTransitDlg.show();

                    } else if (result.getRouteLines().size() == 1) {
                        route = result.getRouteLines().get(0);
                        DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(mBaiduMap, startTitle, endTitle);
                        routeOverlay = overlay;
                        mBaiduMap.setOnMarkerClickListener(overlay);
                        overlay.setData(result.getRouteLines().get(0));
                        overlay.addToMap();
//                        mBtnPre.setVisibility(View.VISIBLE);
//                        mBtnNext.setVisibility(View.VISIBLE);
                    } else {
                        routeLineView.configShow("大约需要 分钟", " ", " ");
                        Log.d("route result", "结果数<0");
                        return;
                    }

                }
            }

            @Override
            public void onGetBikingRouteResult(BikingRouteResult bikingRouteResult) {

            }
        });
    }

    void setMarker(TstOrderBean tstOrderBean) {
        List<DeliverBean> list = tstOrderBean.getDeliverList();
        int size = list.size();
        if (size == 0) {
            Toast.makeText(mContext, "没有起点或终点，请检查后重试！", Toast.LENGTH_SHORT).show();
            return;
        }

        MapUtil.clearMarkList();
        mMarkList.clear();
//        startInfo = list.get(0).getCONTACT_ADDRESS();
//        endInfo = list.get(size - 1).getCONTACT_ADDRESS();

        if (isAddCurrent) {
            DeliverBean bean = list.get(0);
            Overlay overlay = MapUtil.setMarkers(mBaiduMap, R.drawable.icon_st, Double.valueOf(bean.getLATITUDE()), Double.valueOf(bean.getLONGITUDE()), bean.getCONTACT_ADDRESS(), optionsList, 1);
            MapUtil.addOverlay(overlay);
            mMarkList.add(overlay);
        }

        for (int i = 1; i < size - 1; i++) {
            if (i > 1) {
                if ((i - 1) % 2 == 1) {
                    DeliverBean bean = list.get(i);
                    Overlay overlay = MapUtil.setMarkers(mBaiduMap, R.drawable.icon_st, Double.valueOf(bean.getLATITUDE()), Double.valueOf(bean.getLONGITUDE()), bean.getCONTACT_ADDRESS(), optionsList, 1);
                    MapUtil.addOverlay(overlay);
                    mMarkList.add(overlay);
                }
            }

        }
        for (int i = 1; i < size - 1; i++) {
            if ((i - 1) % 2 == 0) {
                DeliverBean bean = list.get(i);
                Overlay overlay = MapUtil.setMarkers(mBaiduMap, R.drawable.icon_en, Double.valueOf(bean.getLATITUDE()), Double.valueOf(bean.getLONGITUDE()), bean.getCONTACT_ADDRESS(), optionsList, 1);
                MapUtil.addOverlay(overlay);
                mMarkList.add(overlay);
            }
        }

    }

    String getStartInfo(TstOrderBean tstOrderBean) {
        List<DeliverBean> list = tstOrderBean.getDeliverList();
        int size = list.size();
        if (size == 0) {
            Toast.makeText(mContext, "没有起点或终点，请检查后重试！", Toast.LENGTH_SHORT).show();
            return "";
        }
        return list.get(0).getCONTACT_ADDRESS();
//        endInfo = list.get(size - 1).getCONTACT_ADDRESS();

    }

    String getEndInfo(TstOrderBean tstOrderBean) {
        List<DeliverBean> list = tstOrderBean.getDeliverList();
        int size = list.size();
        if (size == 0) {
            Toast.makeText(mContext, "没有起点或终点，请检查后重试！", Toast.LENGTH_SHORT).show();
            return "";
        }
        return list.get(size - 1).getCONTACT_ADDRESS();

    }

    /**
     * @param isShowPolicy 是否显示策略
     */
    public void clearRountOverlay(boolean isShowPolicy) {
//        MapUtil.clearMapOverlay();
        if (routeOverlay != null) {
            routeOverlay.removeFromMap();
        }

        for (int i = 0; i < mMarkList.size(); i++) {
            mMarkList.get(i).remove();
        }
        mMarkList.clear();

        if (!isShowPolicy) {
            routeLineView.setVisibility(View.GONE);
            isShowRouteLine = false;
            mTvDestination.setText("");
        }
    }

    /**
     * 初始化数据
     */
    private void initData() {
        // 初始化进度框
        progressDialog = tools.getProgressDialog(mContext, getString(R.string.loading));
        carList = new ArrayList<ArroundCars>();
        mSupermarketList = new ArrayList<ArroundSupermarket>();
        if (!isRountPlan) {
            locateMyself();
        }
        isRountPlan = false;

        //设置默认选中距离
        mSeekBar.setProgress(20);
        mRadius = "100";


    }


    @OnClick({R.id.btnReleaseGoods, R.id.minus, R.id.plus, R.id.btnLocate, R.id.btnOrder1, R.id.btnVoice, R.id.btnLeft, R.id.tvLastNews, R.id.llLastNews, R.id.rbEmptyLoad, R.id.rbHalfLoad, R.id.rbFullLoad})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLeft:
                MainActivity.menu.toggle();
                getPersonal();
                break;
            case R.id.btnReleaseGoods:

//                mContext.sendBroadcast(new Intent("clearRountOverlay"));

//                clearRountOverlay();
                if (mBtnReleaseGoods.getText().equals("点击出车")) {
                    if (sp.getString(UserEntity.STATUS, "").equals("2")) {
                        //认证通过
                        mTvDestination.setText("");
                        carStatus = OUT_CAR;
                        modifyCarStatus();
                    } else {
                        Toast.makeText(mContext, "您尚未通过认证!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                if (!mBtnReleaseGoods.getText().equals("点击收车")) {
                    if (carStatus == OUT_CAR && btnStatus == NOORDER) {
                        Toast.makeText(mContext, "您还未选择运单！", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                if (mBtnReleaseGoods.getText().equals("提货") && mOrderStatus == null) {

                    Toast.makeText(mContext, "您还未选择运单！", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (mBtnReleaseGoods.getText().equals("提货") || mBtnReleaseGoods.getText().equals("回单")) {
                    if (mOrderNo == null || mOrderNo.equals("")) {
                        Toast.makeText(mContext, "您还未选择运单！", Toast.LENGTH_SHORT).show();
                    } else {
                        getActivity().startActivityForResult(new Intent(mContext, OrderDetailActivity.class).putExtra("OrderNo", mOrderNo), ORDERDEAIL);
                    }
//                    runUploadTransprotImage(mOrderStatus);
                    break;
                }
                if (mBtnReleaseGoods.getText().equals("点击收车")) {
                    if (mRgLoadState.getVisibility() == View.GONE) {
                        //当前为收车状态
                        carStatus = OUT_CAR;
                    } else {
                        carStatus = CLOSE_CAR;
                        mTvDestination.setText("");
                    }
                    modifyCarStatus();
                    break;
                }

                break;
            case R.id.minus:
                mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomIn());
                if (isFristDrawPointTwo) {
                    addManagerToMapTwo();
                    isFristDrawPointTwo = false;
                }
                break;
            case R.id.plus:
                mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomOut());//缩小
                if (isFristDrawPointTwo) {
                    addManagerToMapTwo();
                    isFristDrawPointTwo = false;
                }
                break;
            case R.id.btnVoice://声音
                if (sp.getString(UserEntity.IS_SET_MUTING, "0").equals("0")) {
                    mBtnVoice.setBackground(getResources().getDrawable(R.drawable.img_btn_mute));
                    Editor edit = sp.edit();
                    edit.putString(UserEntity.IS_SET_MUTING, "1");
                    edit.commit();
                } else {
                    mBtnVoice.setBackground(getResources().getDrawable(R.drawable.img_btn_phonic));
                    Editor edit = sp.edit();
                    edit.putString(UserEntity.IS_SET_MUTING, "0");
                    edit.commit();
                }
                break;
            case R.id.btnLocate://定位自己
                locateMyself();
                break;
            case R.id.btnOrder1://配送计划
                if (tvOrderCount.getText().equals("") || tvOrderCount.getText().equals("   [" + 0 + "]")) {
                    Toast.makeText(mContext, "您尚无配送计划！", Toast.LENGTH_SHORT).show();
                } else {
                    getActivity().startActivityForResult(new Intent(mContext, DoneListActivity.class), ArroundCarFragment.DoneList);
                }
                break;
            case R.id.tvLastNews://消息列表
            case R.id.llLastNews:
                startActivity(new Intent(mContext, MyNewsActivity.class));
                break;
            case R.id.rbEmptyLoad://空载
                loadStatus = EMPTY_LOAD;
                modifyCarStatus();
                break;
            case R.id.rbHalfLoad://半载
                loadStatus = HALF_LOAD;
                modifyCarStatus();
                break;
            case R.id.rbFullLoad://满载
                loadStatus = FULL_LOAD;
                modifyCarStatus();
                break;
        }
        mapStatus = mMapView.getMap().getMapStatus();
        refreshZoomControlView();
    }


    private void refreshZoomControlView() {
        float zoom = mapStatus.zoom;

        if (zoom > MinLevel && zoom < MaxLevel) {

            if (!mMinus.isEnabled()) {
                mMinus.setEnabled(true); //设置为可点击
            }

            if (!mPlus.isEnabled()) {
                mPlus.setEnabled(true);
            }

        } else if (zoom == MinLevel) {
            mPlus.setEnabled(false);
            mMinus.setEnabled(true);

        } else {

            mMinus.setEnabled(false);
            mPlus.setEnabled(true);
        }

    }

    /**
     * 添加泡泡
     *
     * @param info 泡泡显示的信息
     * @param ll   标记物的经纬度
     * @author guochaohui
     */
    public void addBubble(String info, LatLng ll, int offset) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.info_window, null);
        TextView tvInfo = (TextView) view.findViewById(R.id.tvInfo);
        tvInfo.setText(info);
        MapStatus mapStatus = new MapStatus.Builder().target(ll).build();
        MapStatusUpdate mapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mapStatus);
        mBaiduMap.setMapStatus(mapStatusUpdate);

        mInfoWindow = new InfoWindow(view, ll, offset);
        mBaiduMap.showInfoWindow(mInfoWindow);
    }


    /**
     * 定位SDK监听函数
     */
    public class MyLocationListenner implements BDLocationListener {

        @Override
        public void onReceiveLocation(final BDLocation location) {

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
//                    mLongitude = "";
//                    mLatitude = "";
                    locationMode(LocationMode.NORMAL);
                    // map view 销毁后不在处理新接收的位置
                    if (location == null || mMapView == null)
                        return;
//			if (!CommonTools.latLngIsOK(String.valueOf(location.getLongitude()), String.valueOf(location.getLatitude()))) {
//				return;
//			}
                    MyLocationData locData = new MyLocationData.Builder().accuracy(location.getRadius())
                            // 此处设置开发者获取到的方向信息，顺时针0-360
                            .direction(100).latitude(location.getLatitude()).longitude(location.getLongitude()).build();
                    mBaiduMap.setMyLocationData(locData);

                    mLongitude = String.valueOf(location.getLongitude());
                    mLatitude = String.valueOf(location.getLatitude());

                    LogUtils.i("mLongitude= " + mLongitude);
                    LogUtils.i("mLatitude= " + mLatitude);

                    if (isFirstLoc) {
                        isFirstLoc = false;
                        LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
                        MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
                        mBaiduMap.animateMapStatus(u);

                        if (validateCondition()) {
//                        mLocClient.stop();
                            isShowAllCars = true;
                            requestData();
                            getSupermarketPoint();
                        }
                    }


//                    if (!isGetAddress) {
//                        centerPoint = new LatLng(location.getLatitude(), location.getLongitude());
//                        mSearch.setOnGetGeoCodeResultListener(listener);
//                        mSearch.reverseGeoCode(new ReverseGeoCodeOption().location(centerPoint));
//                    }
                }
            };

            if (mLocThread == null) {
                mLocThread = new Thread(runnable);
                mLocThread.start();
            }
        }

    }

    /**
     * 更新地图信息
     *
     * @author guochaohui
     */
    public void updateMap() {
        mMapView.getMap().clear();
        MapStatus mapStatus = new MapStatus.Builder().build();
        MapStatusUpdate mapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mapStatus);
        mBaiduMap.setMapStatus(mapStatusUpdate);
    }

    /**
     * @param mCurrentMode 定位模式
     * @Description 设置定位当前位置的图标和定位模式
     * @Author guochaohui
     */
    public void locationMode(LocationMode mCurrentMode) {
//        mCurrentMarker = BitmapDescriptorFactory.fromResource(R.drawable.custom_user_location);
        mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(mCurrentMode, false, null));
    }

    /**
     * @Description 定位自己
     * @Author guochaohui
     */
    private void locateMyself() {
        // 定位初始化
        isShowAllCars = false;
        isFristDrawPoint = true;
        mLocClient = new LocationClient(getActivity());
        mLocClient.registerLocationListener(myListener);
        LocationClientOption option = new LocationClientOption();
//        Toast.makeText(mContext, "定位自己位置", Toast.LENGTH_SHORT).show();
        option.setOpenGps(true);// 打开gps
        option.setCoorType("bd09ll"); // 设置坐标类型
//        option.setCoorType("gcj02"); // 设置坐标类型
        option.setScanSpan(2000);
        mLocClient.setLocOption(option);
        mLocClient.start();
        locationMode(LocationMode.NORMAL);//普通模式

        if (!mLatitude.equals("") && !mLongitude.equals("")) {
            //设定中心点坐标
            LatLng cenpt = new LatLng(Double.valueOf(mLatitude), Double.valueOf(mLongitude));
            //定义地图状态
            MapStatus mMapStatus = new MapStatus.Builder()
                    .target(cenpt)
                    .zoom(num)
                    .build();
            //定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
            MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
            //改变地图状态
            mBaiduMap.setMapStatus(mMapStatusUpdate);
        }

    }

    /**
     * 在地图上显示车辆位置
     *
     * @param list      数据
     * @param zoomLevel 放大级别
     * @author guochaohui
     */
    public void drawPoint(List<ArroundCars> list, int zoomLevel) {
        MapStatus mapStatus = null;
        String info = "";
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ArroundCars car = new ArroundCars();
                car = list.get(i);
                LogUtils.i(car.toString());
                if (CommonTools.latLngIsOK(car.getLongitude(), car.getLatitude())) {
                    // 设置中心点
                    mapStatus = new MapStatus.Builder()
                            .target(new LatLng(Double.parseDouble(car.getLatitude()), Double
                                    .parseDouble(car.getLongitude()))).zoom(zoomLevel).build();
                    // 要显示在窗口的信息
                    info = "司机： " + car.getDriverRealName() + "\n" + "手机号： " + car.getUserName()
                            + "\n" + "车牌号： " + car.getCarNumber();
                    //+ "\n" + "地址： " + car.getAddress() + "\n" + "定位时间： " + car.getLocationTime();

                    mMarker = BitmapDescriptorFactory.fromResource(R.drawable.pin_car);

                    // 设置点的属性
                    mMarkerOption = new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(car.getLatitude()), Double
                                    .parseDouble(car.getLongitude()))).perspective(false)
                            .icon(mMarker).zIndex(i).title(info);

                    optionsList.add(mMarkerOption);
                    // 将点添加到地图
                    mBaiduMap.addOverlay(mMarkerOption);

                    //添加点到地图
                    MapUtil.setMarkers(mBaiduMap, R.drawable.pin_car, Double.parseDouble(car.getLatitude()), Double
                            .parseDouble(car.getLongitude()), info, optionsList, i);
                }

            }

        }
        if (isShowAllCars) {
            addManagerToMap();
        }

    }

    /**
     * 在地图上显示商超配送点
     * @param list      周边商超
     * @param zoomLevel 地图放大级别
     */
    public void drawSupermarketPoint(List<ArroundSupermarket> list, int zoomLevel){
        MapStatus mapStatus = null;
        String info = "";
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ArroundSupermarket supermarket = new ArroundSupermarket();
                supermarket = list.get(i);
                LogUtils.i(supermarket.toString());
                if (CommonTools.latLngIsOK(supermarket.getSUPER_LON(), supermarket.getSUPER_LAT())) {
                    // 设置中心点
                    mapStatus = new MapStatus.Builder()
                            .target(new LatLng(Double.parseDouble(supermarket.getSUPER_LAT()), Double
                                    .parseDouble(supermarket.getSUPER_LON()))).zoom(zoomLevel).build();
                    // 要显示在窗口的信息
                    info = "店名： " + supermarket.getSUPER_NAME() + "\n" + "联系人： " + supermarket.getSUPER_LINKNAME()
                            + "\n" + "电话： " + supermarket.getSUPER_LINKMOBILE();
                    //+ "\n" + "地址： " + supermarket.getSUPER_ADDRESS() + "\n" + "定位时间： " + supermarket.getUPDATE_DATE();

                    mMarker = BitmapDescriptorFactory.fromResource(R.drawable.marker_home);

                    // 设置点的属性
                    mMarkerOption = new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(supermarket.getSUPER_LAT()), Double
                                    .parseDouble(supermarket.getSUPER_LON()))).perspective(false)
                            .icon(mMarker).zIndex(i).title(info);

                    optionsList.add(mMarkerOption);
                    // 将点添加到地图
                    mBaiduMap.addOverlay(mMarkerOption);

                    //添加点到地图
                    MapUtil.setMarkers(mBaiduMap, R.drawable.marker_home, Double.parseDouble(supermarket.getSUPER_LAT()), Double
                            .parseDouble(supermarket.getSUPER_LON()), info, optionsList, i);
                }
            }
        }
        if (isShowAllCars) {
            addManagerToMap();
        }
    }

    /**
     * 添加地图OverlayManager
     *
     * @Description
     * @Author guochaohui
     */
    public void addManagerToMap() {
        manager = new OverlayManager(mBaiduMap) {

            @Override
            public boolean onPolylineClick(Polyline arg0) {
                return false;
            }

            @Override
            public boolean onMarkerClick(Marker marker) {

                addBubble(marker.getTitle(), marker.getPosition(), -100);

                return false;
            }

            @Override
            public List<OverlayOptions> getOverlayOptions() {
                return optionsList;
            }
        };

        manager.addToMap();
        if (isFristDrawPoint) {
            return;
        }
        manager.zoomToSpan();//缩放地图，使所有Overlay都在合适的视野内
    }

    /**
     * 缩放地图
     */
    public void addManagerToMapTwo() {
        manager = new OverlayManager(mBaiduMap) {

            @Override
            public boolean onPolylineClick(Polyline arg0) {
                return false;
            }

            @Override
            public boolean onMarkerClick(Marker marker) {

                addBubble(marker.getTitle(), marker.getPosition(), -100);

                return false;
            }

            @Override
            public List<OverlayOptions> getOverlayOptions() {
                return optionsList;
            }
        };

        manager.addToMap();
        manager.zoomToSpan();//缩放地图，使所有Overlay都在合适的视野内
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mTvProgress.setText(Integer.toString(progress * 5) + "km");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        LogUtils.i("progress= " + mTvProgress.getText());
        mRadius = String.valueOf(seekBar.getProgress());
        isShowAllCars = false;
        isFristDrawPoint = false;
        if (validateCondition()) {
            isShowAllCars = true;
//            requestData();
        }
    }

    /**
     * 获取周边车辆
     */
    private void requestData() {
//        progressDialog.show();
        //清除数据
//		updateMap();
        optionsList.clear();

        RequestParams params = new RequestParams();
        params.addQueryStringParameter("LNG", mLongitude);
        params.addQueryStringParameter("LAT", mLatitude);
        params.addQueryStringParameter("RAIDUS", mRadius);

        String url = getString(R.string.server_url) + URLMap.GET_ARROUND_CAR;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(CommonTools.CONNECT_TIMEOUT);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CommonTools.failedToast(mContext);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i("result= " + arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONArray list = obj.getJSONArray("data");
                        for (int i = 0; i < list.length(); i++) {
                            JSONObject item = list.getJSONObject(i);
                            ArroundCars cars = new ArroundCars();

                            cars.setCarId(CommonTools.judgeNull(item, "CARID", ""));                //车id
                            cars.setCarLength(CommonTools.judgeNull(item, "CARLENGTH", ""));        //车长
                            cars.setLengthUnit(CommonTools.judgeNull(item, "LENGTHUNIT", ""));      //车长单位
                            cars.setLongitude(CommonTools.judgeNull(item, "LNG", ""));              //位置经度
                            cars.setLatitude(CommonTools.judgeNull(item, "LAT", ""));               //位置纬度
                            cars.setOwnerMobile(CommonTools.judgeNull(item, "OWNERMOBILE", ""));    //车主电话
                            cars.setVolumnunt(CommonTools.judgeNull(item, "VOLUMNUNIT", ""));
                            cars.setRadius(CommonTools.judgeNull(item, "RAIDUS", ""));
                            cars.setStreetNumber(CommonTools.judgeNull(item, "STREETNUMBER", ""));
                            cars.setDriverRealName(CommonTools.judgeNull(item, "DRIVERREALNAME", ""));
                            cars.setDistrict(CommonTools.judgeNull(item, "DISTRICT", ""));
                            cars.setStatus(CommonTools.judgeNull(item, "STATUS", ""));
                            cars.setUserName(CommonTools.judgeNull(item, "USERNAME", ""));
                            cars.setVolumn(CommonTools.judgeNull(item, "VOLUMN", ""));
                            cars.setWeightUnit(CommonTools.judgeNull(item, "WEIGHTUNIT", ""));
                            cars.setDefaultDriver(CommonTools.judgeNull(item, "DEFAULTDRIVER", ""));
                            cars.setDriverMobile(CommonTools.judgeNull(item, "DRIVERMOBILE", ""));
                            cars.setStreet(CommonTools.judgeNull(item, "STREET", ""));
                            cars.setDispathStatus(CommonTools.judgeNull(item, "DISPATCHSTATUS", ""));
                            cars.setOwnerName(CommonTools.judgeNull(item, "OWNERNAME", ""));        //车主姓名
                            cars.setLocationTime(CommonTools.judgeNull(item, "LOCATIONTIME", ""));  //定位时间
                            cars.setCity(CommonTools.judgeNull(item, "CITY", ""));
                            cars.setAddress(CommonTools.judgeNull(item, "ADDRESS", ""));
                            cars.setLoadStatus(CommonTools.judgeNull(item, "LOADSTATUS", ""));
                            cars.setCompanyName(CommonTools.judgeNull(item, "COMPANYNAME", ""));
                            cars.setCarType(CommonTools.judgeNull(item, "CARTYPE", ""));
                            cars.setCarLoad(CommonTools.judgeNull(item, "LOADWEIGHT", ""));
                            cars.setProvince(CommonTools.judgeNull(item, "PROVINCE", ""));
                            cars.setCarNumber(CommonTools.judgeNull(item, "CARNUMBER", ""));


                            carList.add(cars);
                        }
                        drawPoint(carList, 18);

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                    Toast.makeText(mContext, "请求失败！", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * 获取周边商超配送点信息
     */
    private void getSupermarketPoint(){
        optionsList.clear();

        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
        String url = getString(R.string.server_url) + URLMap.GET_ARROUND_SUPERMARKET;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(CommonTools.CONNECT_TIMEOUT);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CommonTools.failedToast(mContext);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i("result= " + arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONArray list = obj.getJSONArray("data");
                        for (int i = 0; i < list.length(); i++) {
                            JSONObject item = list.getJSONObject(i);
                            ArroundSupermarket supermarket = new ArroundSupermarket();

                            supermarket.setSUPER_NAME(CommonTools.judgeNull(item, "SUPER_NAME", ""));
                            supermarket.setSUPER_LINKNAME(CommonTools.judgeNull(item, "SUPER_LINKNAME", ""));
                            supermarket.setSUPER_LINKMOBILE(CommonTools.judgeNull(item, "SUPER_LINKMOBILE", ""));
                            supermarket.setSUPER_LON(CommonTools.judgeNull(item, "SUPER_LON", ""));
                            supermarket.setSUPER_LAT(CommonTools.judgeNull(item, "SUPER_LAT", ""));
                            supermarket.setSUPER_ADDRESS(CommonTools.judgeNull(item, "SUPER_ADDRESS", ""));

                            mSupermarketList.add(supermarket);
                        }
                        drawSupermarketPoint(mSupermarketList, 18);

                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                    Toast.makeText(mContext, "请求失败！", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }





    /**
     * 获取认证信息
     */
    private void getAuthInfo() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_AUTHEN_INFO;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(CommonTools.CONNECT_TIMEOUT);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        dialog.show();
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    if ("1".equals(status)) {
                        JSONObject data = obj.getJSONObject("data");
                        Editor edit = sp.edit();
                        if (RoleType.GOODS_OWNER.equals(roleId)) {
                            edit.putString(UserEntity.IDCARD_IMG, CommonTools.judgeNull(data, "SLOCALPHOTO", null));
                            edit.putString(UserEntity.PHONE, CommonTools.judgeNull(data, "PHONE", null));
                            edit.putString(UserEntity.NAME, CommonTools.judgeNull(data, "NAME", null));
                            edit.putString(UserEntity.STATUS, CommonTools.judgeNull(data, "STATUS", null));
                            edit.putString(UserEntity.IDCARD_NUM, CommonTools.judgeNull(data, "IdentityCard", null));
                        } else if (RoleType.LOGISTICS_ENTERPRISE.equals(roleId)) {
                            edit.putString(UserEntity.BUSINESS_LICENCE_IMG, CommonTools.judgeNull(data, "BUSINESSLICENCENUMIMG", null));
                            edit.putString(UserEntity.COMPANY_NAME, CommonTools.judgeNull(data, "COMPANYNAME", null));
                            edit.putString(UserEntity.COMPANY_ADDRESS, CommonTools.judgeNull(data, "COMPANYADDRESS", null));
                            edit.putString(UserEntity.STATUS, CommonTools.judgeNull(data, "STATUS", null));
                            edit.putString(UserEntity.BUSINESS_LICENCE_NUM, CommonTools.judgeNull(data, "BUSINESSLICENCENUM", null));
                            edit.putString(UserEntity.LEGAL_PERSON, CommonTools.judgeNull(data, "LEGALPERSON", null));
                            edit.putString(UserEntity.HEADER_MOBILE, CommonTools.judgeNull(data, "HEADERMOBILE", null));
                        }
                        edit.commit();
                        setAuthStatus(Integer.valueOf(CommonTools.judgeNull(data, "STATUS", null)));
                        initData();

//                        relaseGoods();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                dialog.dismiss();
            }

        });
    }

    /**
     * 获取我的消息
     */
    private void getNews() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("CURRENTPAGE", "0");//默认第0页
        params.addQueryStringParameter("SHOWCOUNT", "10000");//默认10000条

        String url = getString(R.string.server_url) + URLMap.GET_My_MESSAGE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(CommonTools.CONNECT_TIMEOUT);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("获取消息请求失败！");
                CommonTools.failedToast(mContext);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                LogUtils.i("result= " + arg0.result);
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        LogUtils.i(data.toString());
                        JSONArray list = data.getJSONArray("LIST");
                        if (list.length() > 0) {
                            mTvNewsCount.setText("[" + data.getInt("UNREADCOUNT") + "]");
                            obj = list.getJSONObject(0);
                            Gson gson = new Gson();
                            mNewsBean = gson.fromJson(obj.toString(),NewsBean.class);
                            mTvLastNews.setText(mNewsBean.getSUMMARY());
                            if (Double.valueOf(mNewsBean.getSTATUS()) == 1) {
                                //消息已读，暂停滚动
                                mTvLastNews.setEllipsize(null);
                            }
                        } else {
                            mTvNewsCount.setText("[0]");
                            mTvLastNews.setText("暂无消息");
                            mTvLastNews.setEllipsize(null);
                        }
                    } else {
                        LogUtils.i(message);
                        if (message.equals("")) {
                            Toast.makeText(mContext, "请求失败！", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "请求失败！", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * 更新车辆状态
     */
    private void modifyCarStatus() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("DISPATCHSTATUS", carStatus);
        params.addQueryStringParameter("LOADSTATUS", loadStatus);

        String url = getString(R.string.server_url) + URLMap.UPDATE_CAR_STATE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(CommonTools.CONNECT_TIMEOUT);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onStart() {
                super.onStart();
                if (mCarstatusDialog == null) {
                    mCarstatusDialog = new CommonTools(mContext).getProgressDialog(mContext, "加载中...");
                    mCarstatusDialog.setCancelable(false);
                }
                mCarstatusDialog.show();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                if (mCarstatusDialog.isShowing()) {
                    mCarstatusDialog.dismiss();
                }
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String state = obj.getString("state");
                    String message = obj.getString("message");

                    if ("1".equals(state)) {
                        LogUtils.i("更新车辆状态成功！");
//                        Toast.makeText(mContext, "设置成功", Toast.LENGTH_SHORT).show();
                        Editor edit = sp.edit();
                        edit.putString(UserEntity.CAR_STATUS, carStatus);
                        edit.putString(UserEntity.LOAD_STATUS, loadStatus);
                        edit.commit();
                        if (OUT_CAR.equals(carStatus)) { // 出车成功
                            carStatus = OUT_CAR;
                            mRgLoadState.setVisibility(View.VISIBLE);
//                            onDelivery();
                            sendRequest();
                        } else if (CLOSE_CAR.equals(carStatus)) { // 收车成功
                            mBtnReleaseGoods.setText("点击出车");
                            mRgLoadState.setVisibility(View.GONE);
                            carStatus = CLOSE_CAR;
                            loadStatus = EMPTY_LOAD;
                        }
                    } else {
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    }
                    //出车收车定位服务
                    updateCarStatus();
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
                if (mCarstatusDialog.isShowing()) {
                    mCarstatusDialog.dismiss();
                }
                CommonTools.failedToast(mContext);
                //出车收车定位服务
                updateCarStatus();
            }
        });
    }


    /**
     * 根据出车状态开启定位
     */
    private void updateCarStatus() {
//        carStatus = sp.getString(UserEntity.CAR_STATUS, CLOSE_CAR);
        if (OUT_CAR.equals(carStatus)) {                 //出车
            mContext.startService(new Intent(mContext, LocationService.class)); //开启定位
        } else if (CLOSE_CAR.equals(carStatus)) {         //收车
            mContext.stopService(new Intent(mContext, LocationService.class));  //关闭定位

        }
        updateLoadStatus();
    }

    /**
     * 更新载重状态
     */
    private void updateLoadStatus() {
        loadStatus = sp.getString(UserEntity.LOAD_STATUS, null);
        if (EMPTY_LOAD.equals(loadStatus)) {
            mRbEmptyLoad.setChecked(true);
            mRbEmptyLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            mRbHalfLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            mRbFullLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        } else if (HALF_LOAD.equals(loadStatus)) {
            mRbHalfLoad.setChecked(true);
            mRbEmptyLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            mRbHalfLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            mRbFullLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        } else if (FULL_LOAD.equals(loadStatus)) {
            mRbFullLoad.setChecked(true);
            mRbEmptyLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            mRbHalfLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            mRbFullLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        }
    }


    /**
     * 校验实名认证
     */
//    private void relaseGoods(){
//        String isBandBankCard = sp.getString(UserEntity.IS_BAND_BANKCARD, "0");
//        String name = sp.getString(UserEntity.NAME, "");
//        int status = Integer.valueOf(sp.getString(UserEntity.STATUS, "0")) ;
//        if (status == 0) {
//            Toast.makeText(mContext, "请先实名认证！", Toast.LENGTH_SHORT).show();
//            startActivity(new Intent(mContext, AuthActivity.class));
//            return;
//        }else if(status == 1){
//            Toast.makeText(mContext, "认证审核中，请耐心等候！", Toast.LENGTH_SHORT).show();
//        } else if(status == 3){
//            Toast.makeText(mContext, "认证未通过，请重新认证！", Toast.LENGTH_SHORT).show();
//            startActivity(new Intent(mContext, AuthActivity.class));
//        } else if(status == 2){
//            if (!isBandBankCard.equals("1")) {
//                Toast.makeText(mContext, "请先绑定银行卡！", Toast.LENGTH_SHORT).show();
//                startActivityForResult(new Intent(mContext, BindBankCardAcitivity.class),BINDBACK);
//                return;
//            }else if (!sp.getString(UserEntity.IS_SET_PAYMENT_PWD, "0").equals("1")) {
//                Toast.makeText(mContext, "请先设置支付密码", Toast.LENGTH_SHORT).show();
//                startActivityForResult(new Intent(mContext, ModifypaymentPawActivity.class),PAYPWD);
//                return;
//            }else {
//                Intent iStart = new Intent();
//                Bundle sbundle = new Bundle();
//                sbundle.putSerializable("pointInfo", startInfo);
//                iStart.putExtras(sbundle);
//                iStart.setClass(mContext, DeliverGoodsActivity.class);
//                startActivity(iStart);
//            }
//        }
//    }

    /**
     * 验证条件
     */
    public boolean validateCondition() {
        if (!CommonTools.latLngIsOK(mLongitude, mLatitude)) {
            Toast.makeText(mContext, "坐标无效，请重新定位再请求！", Toast.LENGTH_SHORT).show();
            return false;
        } else if (Integer.valueOf(mRadius).intValue() == 0) {
            Toast.makeText(mContext, "半径必须大于0！", Toast.LENGTH_SHORT).show();
            return false;
        } else if (isShowAllCars) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * 显示目的地
     */
    @Override
    public void showPlan(String endddress) {
        Log.d(TAG, "showEndddress: ");
        if (TextUtils.isEmpty(endddress)) {
            Toast.makeText(mContext, "没有配送计划", Toast.LENGTH_SHORT).show();
        } else {
            mTvDestination.setText(endddress);
        }
    }

    /**
     * 工作单回调
     */
    @Override
    public void work(TstOrderBean tstOrderBean) {
        Log.d(TAG, "work: ");
        if (tstOrderBean == null) {
            Toast.makeText(mContext, "缺少起点或终点，无法规划路线", Toast.LENGTH_SHORT).show();
            return;
        }
        mOrderBean = tstOrderBean;
        List<PlanNode> nodeList = getNodeList(tstOrderBean);
        mOrderStatus = tstOrderBean.getORDER_STATUS();
        mOrderNo = tstOrderBean.getORDERNO();
        InitStatus(mOrderStatus2);
        carStatus = OUT_CAR;
        mRgLoadState.setVisibility(View.VISIBLE);
        isRountPlan = false;
//        modifyCarStatus();
        if (mOrderStatus2 != null && !mOrderStatus2.equals("")) {
            InitStatus(mOrderStatus2);
        }
        if (OrderListAdapter.ARRIVED.equals(mOrderStatus2) || OrderListAdapter.CALCULATED.equals(mOrderStatus)) { // 已确认到达
            noneOrder();
        } else {
            btnStatus = WORKORDER;
            isRountPlan = true;

            String drivingPolicy = sp.getString(UserEntity.Driving_Policy, "0");
            DrivingRoutePlanOption.DrivingPolicy policy = DrivingRoutePlanOption.DrivingPolicy.ECAR_AVOID_JAM;
            if (drivingPolicy.equals("0")) {
                policy = DrivingRoutePlanOption.DrivingPolicy.ECAR_AVOID_JAM;
            } else if (drivingPolicy.equals("1")) {
                policy = DrivingRoutePlanOption.DrivingPolicy.ECAR_DIS_FIRST;
            } else if (drivingPolicy.equals("2")) {
                policy = DrivingRoutePlanOption.DrivingPolicy.ECAR_FEE_FIRST;
            } else if (drivingPolicy.equals("3")) {
                policy = DrivingRoutePlanOption.DrivingPolicy.ECAR_TIME_FIRST;
            }

            if (nodeList == null || nodeList.size() < 2) {
                locateMyself();
            }

            MapUtil.makeRoutePlan(getActivity(), mRoutePlanSearch, nodeList, policy, isAddCurrent, mLongitude, mLatitude);
            isShowRouteLine = true;
        }

    }

    /**
     * 设置为无单状态
     */
    void noneOrder() {
        mOrderNo = null;
        mOrderStatus2 = "";
        btnStatus = NOORDER;
        mainActivity.setWorkSize(0);
    }

    /**
     * 根据订单状态设置车辆状态
     */
    void InitStatus(String ordercode) {
        if (carStatus == CLOSE_CAR) {
            return;
        }
        if (OrderListAdapter.NO_ORDER.equals(ordercode)) {// 未接单
            onDelivery();
        } else if (OrderListAdapter.ORDERED.equals(ordercode)) { // 已接单
            onDelivery();
        } else if (OrderListAdapter.PORTIONTAKEGOODS.equals(ordercode)) {//已部分提货
            onDelivery();
        } else if (OrderListAdapter.TAKEGOODS.equals(ordercode)) { //已提货
            onReceipt();
        } else if (OrderListAdapter.PORTIONARRIVED.equals(ordercode)) { // 已部分到达
            onReceipt();
        } else if (OrderListAdapter.ARRIVED.equals(ordercode)) { // 已确认到达
            noneOrder();
            onReceiveCar();
        } else if (OrderListAdapter.CALCULATED.equals(ordercode)) { // 已结算
            noneOrder();
            onReceiveCar();
        }
    }

    void onDelivery() {
        mBtnReleaseGoods.setText("提货");
        mBtnReleaseGoods.setBackground(getResources().getDrawable(R.drawable.img_shipping_btn));
    }

    void onReceipt() {
        mBtnReleaseGoods.setText("回单");
        mBtnReleaseGoods.setBackground(getResources().getDrawable(R.drawable.img_shipping_btn));
    }

    void onReceiveCar() {
        mBtnReleaseGoods.setText("点击收车");
        mBtnReleaseGoods.setBackground(getResources().getDrawable(R.drawable.img_shipping_btn));
    }

    /**
     * 根据出车状态上传运输凭据
     */
    void runUploadTransprotImage(String ordercode) {
        /*if (OrderListAdapter.NO_ORDER.equals(ordercode)) {// 未接单
            mBtnReleaseGoods.setText("未接单");
        } else */
        if (OrderListAdapter.NO_LEAFLETS.equals(ordercode) || OrderListAdapter.ORDERED.equals(ordercode)) { // 已接单
            //提货
            logsGoods();
        } else if (OrderListAdapter.PORTIONTAKEGOODS.equals(ordercode)) {//已部分提货
            //提货
            logsGoods();
        } else if (OrderListAdapter.TAKEGOODS.equals(ordercode)) { //已提货
            //回单
            logsGoods();
        } else if (OrderListAdapter.PORTIONARRIVED.equals(ordercode)) { // 已部分到达
            //回单
            logsGoods();
        }
    }

    /**
     * 上传运输凭据
     */
    void logsGoods() {
        if (mOrderNo == null || mOrderNo.equals("")) {
            Toast.makeText(mContext, "您还没有选择运单！", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
        intent.putExtra("flag", Constant.UPLOADTRSTIMAGE);
        //传入订单号
        intent.putExtra("ORDERNO", mOrderNo);
        getActivity().startActivityForResult(intent, ORDERDEAIL);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == MainActivity.RESULT_OK) {
//            if (requestCode == BINDBACK){
//                relaseGoods();
//            }else if (requestCode == PAYPWD){
//                relaseGoods();
//            }
        }

    }

    /**
     * 获取订单列表
     */
    private void sendRequest() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("CURRENTPAGE", "1");
        params.addQueryStringParameter("SHOWCOUNT", "50");
        params.addQueryStringParameter("DATATYPE", "1");
        params.addQueryStringParameter("ISPLAN", "1");
        params.addQueryStringParameter("ISFINISH", "2");

        if (!isAdded()) {
            return;
        }

        String url = getString(R.string.server_url) + URLMap.GET_CDSORDER_LIST;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(CommonTools.CONNECT_TIMEOUT);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                dialog.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());

                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONArray array = obj.getJSONArray("data");
                        if (tvOrderCount != null) {
                            tvOrderCount.setText("   [" + array.length() + "]");
                        }
                        if ("0".equals(array.length())) {
                            mTvDestination.setText("");
                            mTvDestination.setEllipsize(null);
                        }
                        if (array.length() == 0) {
                            if (carStatus == OUT_CAR) {
                                onReceiveCar();
                            }
                        } else {
                            if (carStatus == OUT_CAR) {
                                int flag = getActivity().getIntent().getIntExtra("flag", 0);
                                if (flag == ORDERDEAIL) {
                                    //判断有没有运单状态，如果运单状态为已回单，就显示提货，否则
                                    //当车辆从配送计划返回时，显示原来的文字
                                    if (mOrderStatus2.equals(OrderListAdapter.CALCULATED)) {
                                        onDelivery();
                                    } else {
                                        if (mOrderNo == null || mOrderNo.equals("")) {
                                            onDelivery();
                                        } else {
                                            InitStatus(mOrderStatus2);
                                        }
                                    }
                                } else {
                                    //登录的时候，如果有配送任务，显示提货
                                    if (mOrderNo == null || mOrderNo.equals("")) {
                                        onDelivery();
                                    } else {
                                        InitStatus(mOrderStatus2);
                                    }
                                }

                            }
                        }
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }

    private void getPersonal() {
//        dialog.show();
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_DRIVER_USERINFO;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(CommonTools.CONNECT_TIMEOUT);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                dialog.dismiss();
                CommonTools.failedToast(mContext);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        JSONObject data = obj.getJSONObject("data");
                        JSONObject userInfo = data.getJSONObject("userInfo");

                        Editor edit = sp.edit();
                        edit.putString(UserEntity.HIGH_PRAISE_RATE, CommonTools.judgeNull(userInfo, "HIGHPRAISERATE", null)); // 好评率
                        edit.putString(UserEntity.EVALUATION_NUMBER, CommonTools.judgeNull(userInfo, "EVALUATIONCOUNT", null)); // 评价数
                        edit.putString(UserEntity.BALANCE, CommonTools.judgeNull(userInfo, "BALANCE", null)); // 账户
                        edit.putString(UserEntity.NAME, CommonTools.judgeNull(userInfo, "NAME", null)); // 用户姓名
                        edit.putString(UserEntity.USER_ID, CommonTools.judgeNull(userInfo, "USER_ID", null)); // 用户id
                        edit.putString(UserEntity.ONLINE_TIME_TODAY, CommonTools.judgeNull(userInfo, "ONLINETIMECOUNT", null)); // 在线小时数
                        edit.putString(UserEntity.POINT_LEVEL, CommonTools.judgeNull(userInfo, "LEVEL", null)); // 信誉
                        edit.putString(UserEntity.TRANSACTION_COUNT, CommonTools.judgeNull(userInfo, "TRANSACTIONCOUNT", null)); // 交易数量
                        String authStatus = CommonTools.judgeNull(userInfo, "STATUS", null);
                        edit.putString(UserEntity.STATUS, authStatus);

                        edit.putString(UserEntity.WEB_BALANCE, CommonTools.judgeNull(userInfo, "WEBBALANCE", null)); // 钱包余额
                        edit.putString(UserEntity.SERIVICE_ATTITUDE_POINT, CommonTools.judgeNull(userInfo, "SERVICEATTITUDEPOINT", null)); // 服务态度
                        edit.putString(UserEntity.CREDIT, CommonTools.judgeNull(userInfo, "CREDIT", null)); // 授信额度
                        edit.putString(UserEntity.TRANSACTION_MONEY, CommonTools.judgeNull(userInfo, "TRANSACTIONMONEY", null)); // 流水账金额
                        String headImageName = CommonTools.judgeNull(userInfo, "HEADPHOTOURL", null);
                        edit.putString(UserEntity.HEAD_PHOTO_URL, headImageName); // 用户头像
                        edit.putString(UserEntity.GUARANTEE_MONEY, CommonTools.judgeNull(userInfo, "GUARANTEEMONEY", null)); // 担保金
                        edit.putString(UserEntity.TURNOVER_TODAY, CommonTools.judgeNull(userInfo, "TRANSACTIONCOUNTTODATE", null)); // 今日接单数

                        JSONArray carArr = data.getJSONArray("carList");
                        if (carArr.length() > 0) {
                            edit.putString(UserEntity.CAR_ID, CommonTools.judgeNull(carArr.getJSONObject(0), "CARID", null));
                            edit.putString(UserEntity.CAR_NUMBER, CommonTools.judgeNull(carArr.getJSONObject(0), "CARNUMBER", null));
                            edit.putString(UserEntity.CAR_STATUS, CommonTools.judgeNull(carArr.getJSONObject(0), "DISPATCHSTATUS", null));
                            edit.putString(UserEntity.LOAD_STATUS, CommonTools.judgeNull(carArr.getJSONObject(0), "LOADSTATUS", null));
                        }
                        edit.commit();
                        if (authStatus.equals("2")) {
                            if (mBtnReleaseGoods != null) {
                                mBtnReleaseGoods.setVisibility(View.VISIBLE);
                            }


                            //获取出车状态
                            if (CommonTools.judgeNull(carArr.getJSONObject(0), "DISPATCHSTATUS", "0").equals(OUT_CAR)) {
                                if (mBtnReleaseGoods != null) {
                                    mBtnReleaseGoods.setText("点击出车");
                                }
                                mRgLoadState.setVisibility(View.VISIBLE);
                                carStatus = OUT_CAR;
                                //出车收车定位服务
                                updateCarStatus();
                                //判断是否有配送计划，设置文字
                            } else if (CommonTools.judgeNull(carArr.getJSONObject(0), "DISPATCHSTATUS", "0").equals(CLOSE_CAR)) {
                                if (mBtnReleaseGoods != null) {
                                    mBtnReleaseGoods.setText("点击出车");
                                }
                                mRgLoadState.setVisibility(View.GONE);
                                carStatus = CLOSE_CAR;
                            }
                            //根据出车状态设置页面

//                            if (mBtnReleaseGoods.getText().equals("出车")) {
//                                mBtnReleaseGoods.setText("提货");
//                                mRgLoadState.setVisibility(View.VISIBLE);
//                                carStatus = OUT_CAR;
//                            }
                        } else {
                            btnStatus = NOAUTH;
                            if (mBtnReleaseGoods != null) {
                                mBtnReleaseGoods.setVisibility(View.GONE);
                            }
                            if(authStatus.equals("0")){
                                Toast.makeText(mContext, "您还未认证！", Toast.LENGTH_SHORT).show();
                            }
                            if(authStatus.equals("3")){
                                Toast.makeText(mContext, "您认证未通过，请重新认证", Toast.LENGTH_SHORT).show();
                            }
                        }

                        setAuthStatus(Integer.valueOf(authStatus));
                        sendRequest();
                        sendDataUpdateBroadcast();
                    } else {
//                        Toast.makeText(mContext, "获取用户信息失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            private void sendDataUpdateBroadcast() {
                //Intent intent = new Intent();
                //intent.setAction("cn.com.dkl.logistics.driver.MainDataUpdate");
                mContext.sendBroadcast(new Intent("cn.com.dkl.logistics.driver.MainDataUpdate"));
            }

        });
    }

    @Override
    public void orderDerail(int type) {
        sendRequest();
    }

    /**
     * 设置认证状态，并记录。 如果第一次为为认证，以后n次为已认证，则需要调个人信息接口
     */
    void setAuthStatus(int authStatus) {
        Editor editor = sp.edit();
        if (mAuthNumber == 0) {
            editor.putInt("AUTHSTATUSINIT", authStatus);
        } else {
            editor.putInt("AUTHSTATUSTHEN", authStatus);
        }
        editor.commit();
        mAuthNumber++;
        mainActivity.setmAuthNumber(mAuthNumber);
    }

    @Override
    public void authChange(boolean isNeed) {
        if (isNeed) {
            getPersonal();
        }
    }

    /**
     * 选择策略
     */
    @Override
    public void onClickRountPolicy() {
        if (mOrderBean != null) {
            clearRountOverlay(true);
            dialog.show();
            work(mOrderBean);
        }
    }

    @Override
    public void orderBack() {
        if (mOrderStatus2 != null && !mOrderStatus2.equals("")) {
            InitStatus(mOrderStatus2);
        }
        if (OrderListAdapter.ARRIVED.equals(mOrderStatus2) || OrderListAdapter.CALCULATED.equals(mOrderStatus)) { // 已确认到达
            noneOrder();
        }
    }

    @Override
    public void onItemClick(int position, RouteLine route) {
        clearRountOverlay(true);
        DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(mBaiduMap, startTitle, endTitle);
        mBaiduMap.setOnMarkerClickListener(overlay);
        routeOverlay = overlay;
        overlay.setData((DrivingRouteLine) route);
        overlay.addToMap();
        overlay.zoomToSpan();
    }

    /**
     * 消息已读反馈
     */
    private void readNews() {
        if (mNewsBean == null){
            return;
        }
        String url = getString(R.string.server_url) + URLMap.IS_READ_MESSAGE;
        org.xutils.http.RequestParams params = new org.xutils.http.RequestParams(url);
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("USERMESSAGE_ID", mNewsBean.getUSERMESSAGE_ID());

        NetworkCallBack callBack = new NetworkCallBack(mContext);
        callBack.networkRequest(params, true, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state.trim())) {
                        Log.e(TAG, "readNews success: ");
                        getNews();
                    } else {
                        Log.e(TAG, "readNews failed: " + message );
                    }
                } catch (JSONException e) {
                   e.printStackTrace();
                }
            }
        });
    }

    class NewsReadRecive extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            Message message = mHandler.obtainMessage();
            message.what = YUNDANYIBEIJIEDAN;
            message.obj = intent.getStringExtra(WAYBILLTAKEORDER);
            mHandler.sendMessage(message);
        }
    }

}

