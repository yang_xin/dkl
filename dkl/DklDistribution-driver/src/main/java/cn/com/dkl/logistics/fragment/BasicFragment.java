package cn.com.dkl.logistics.fragment;

import android.support.v4.app.Fragment;

/**
 * fragment的基类
 * Created by LiuXing on 2016/12/12.
 */

public class BasicFragment extends Fragment {
    private Object mDataObject = null;

    /**
     * 将数据传到此fragment上
     *
     * @Author hexiaodong
     * @param dataObject 数据对象
     */
    public void setData(Object dataObject) {
        mDataObject = dataObject;
    }

    /**
     * 获取此fragmnet的数据对象
     *@Author hexiaodong
     *@return 数据对象
     */
    public Object getData() {
        return mDataObject;
    }

}
