package cn.com.dkl.logistics.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.com.dkl.logistics.adapter.MessageAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.AuthActivity;
import cn.com.dkl.logistics.driver.MainActivity;
import cn.com.dkl.logistics.driver.NewMessageActivity;
import cn.com.dkl.logistics.driver.NewsDetailsActivity;
import cn.com.dkl.logistics.driver.OrderDetailActivity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.OrderMessage;
import cn.com.dkl.logistics.service.LocationService;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.view.CircleImageView;
import cn.com.dkl.logistics.view.CustomProgressDialog;

@SuppressLint("NewApi")
public class MainFragment extends Fragment {
    @ViewInject(R.id.civHeadImg)
    private CircleImageView civHeadImg;
    @ViewInject(R.id.tvTurnover)
    private TextView tvTurnover;
    @ViewInject(R.id.tvWaterToday)
    private TextView tvWaterToday;
    // @ViewInject(R.id.tvAssignTurnoverRate)
    // private TextView tvAssignTurnoverRate;
    @ViewInject(R.id.tvOnlineTime)
    private TextView tvOnlineTime;
    @ViewInject(R.id.tvTime)
    private TextView tvTime;
    /* 主界面消息 */
    @ViewInject(R.id.listView)
    private SwipeMenuListView listView;
    @ViewInject(R.id.tvCarNumber)
    private TextView tvCarNumber;
    @ViewInject(R.id.btnOutCar)
    private Button btnOutCar;
    @ViewInject(R.id.rlOutCar)
    private RelativeLayout rlOutCar;
    @ViewInject(R.id.btnVoice)
    private Button btnVoice;
    @ViewInject(R.id.rgLoadState)
    private RadioGroup rgLoadState;
    @ViewInject(R.id.rbEmptyLoad)
    private RadioButton rbEmptyLoad;
    @ViewInject(R.id.rbHalfLoad)
    private RadioButton rbHalfLoad;
    @ViewInject(R.id.rbFullLoad)
    private RadioButton rbFullLoad;
    @ViewInject(R.id.btnCloseCar)
    private Button btnCloseCar;

    private Context context;
    private SharedPreferences sp;
    private MessageAdapter adapter = null;
    private List<OrderMessage> listMsg = new ArrayList<OrderMessage>();
    public static final String OUT_CAR = "1";
    public static final String CLOSE_CAR = "0";
    private CustomProgressDialog dialog = null;
    /**
     * 出车状态:出车1，收车2
     */
    private String carStatus;
    public static final String EMPTY_LOAD = "0"; // 空载
    public static final String HALF_LOAD = "1"; // 半载
    public static final String FULL_LOAD = "2"; // 满载
    /**
     * 车辆状态
     */
    private String loadStatus = EMPTY_LOAD;
    private DataUpdateReceiver receiver;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ViewUtils.inject(this, v);
        init();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        setView();
        getMessage();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        context.unregisterReceiver(receiver);
    }

    private void init() {
        context = getActivity();
        dialog = new CommonTools(context).getProgressDialog(context, "加载中...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        if (sp.getString(UserEntity.IS_SET_MUTING, "0").equals("0")) {
            btnVoice.setBackground(getResources().getDrawable(R.drawable.img_btn_phonic));
        } else {
            btnVoice.setBackground(getResources().getDrawable(R.drawable.img_btn_mute));
        }

        if (OUT_CAR.equals(carStatus)) { // 出车状态显示指派成交率
            // tvAssignTurnoverRate.setVisibility(View.GONE);
        }

        registerReceiver();
    }

    /**
     * 注册广播接收数据更新通知
     */
    private void registerReceiver() {
        receiver = new DataUpdateReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("cn.com.dkl.logistics.driver.MainDataUpdate");
        context.registerReceiver(receiver, filter);
    }

    private void setView() {
        String headImageName = sp.getString(UserEntity.HEAD_PHOTO_URL, null);
        if (TextUtils.isEmpty(headImageName)) {
            civHeadImg.setImageResource(R.drawable.img_default_head_image);
        } else {
            ImageUtils.downLoadImage(context, civHeadImg, headImageName);
        }
        tvTurnover.setText(sp.getString(UserEntity.TURNOVER_TODAY, "0"));
        tvWaterToday.setText(sp.getString(UserEntity.TRANSACTION_MONEY, "0"));
        tvCarNumber.setText(sp.getString(UserEntity.CAR_NUMBER, ""));
        tvTime.setText(CommonTools.getDateTime());

        updateCarStatus();

        initListView();

    }

    private void initListView() {
        adapter = new MessageAdapter(context, listMsg);
        listView.setAdapter(adapter);

        // 创建滑动菜单
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(context);
                openItem.setBackground(new ColorDrawable(Color.RED));
                openItem.setWidth(CommonTools.dp2px(context, 80));
                openItem.setTitle("删除");
                openItem.setTitleSize(18);
                openItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(openItem);
            }
        };
        // 添加菜单到listview
        listView.setMenuCreator(creator);
        // 添加listview item菜单点击事件
        listView.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            @Override
            public void onMenuItemClick(int position, SwipeMenu menu, int menuIndex) {
                switch (menuIndex) {
                    case 0:
                        doDeleteMessage(position);
                        break;
                    default:
                        break;
                }

            }
        });

        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //getOrderDetail(listMsg.get(position).getSourceID(), listMsg.get(position).getUserMessageID());

                // 0运起来自定义推送,1车源,2货源,3专线,4.同城配送司机订单通知,5同城配送货主订单通知,6同城配送司机普通通知,7同城配送货主普通通知
                String sourceType = listMsg.get(position).getSourceType();
                // 将资源id传到详细页面
                String userMessageID = listMsg.get(position).getUserMessageID();

                if (sourceType.equals("4")) { // 打开订单详情
                    getOrderDetail(listMsg.get(position).getSourceID(), listMsg.get(position).getUserMessageID());
                } else { // 打开普通消息详情
                    context.startActivity(new Intent(context, NewsDetailsActivity.class).putExtra("userMessageID", userMessageID));
                }
            }

        });
    }

    @OnClick({R.id.civHeadImg, R.id.btnOutCar, R.id.btnCloseCar, R.id.btnVoice, R.id.rbEmptyLoad, R.id.rbHalfLoad, R.id.rbFullLoad})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.civHeadImg:
                SlidingMenu slidingmenu = new MainActivity().getSlidingMenu();
                slidingmenu.showMenu();
                break;
            case R.id.btnOutCar:
                if (sp.getString(UserEntity.STATUS, "0").equals("2")) {
                    carStatus = OUT_CAR;
                    modifyCarStatus();
                } else if (sp.getString(UserEntity.STATUS, "0").equals("0")) {
                    carOut();
                } else {
                    getAuthInfo();
                }
                break;
            case R.id.btnCloseCar:
                carStatus = CLOSE_CAR;
                modifyCarStatus();
                break;
            case R.id.btnVoice:
                if (sp.getString(UserEntity.IS_SET_MUTING, "0").equals("0")) {
                    btnVoice.setBackground(getResources().getDrawable(R.drawable.img_btn_mute));
                    Editor edit = sp.edit();
                    edit.putString(UserEntity.IS_SET_MUTING, "1");
                    edit.commit();
                } else {
                    btnVoice.setBackground(getResources().getDrawable(R.drawable.img_btn_phonic));
                    Editor edit = sp.edit();
                    edit.putString(UserEntity.IS_SET_MUTING, "0");
                    edit.commit();
                }
                break;
            case R.id.rbEmptyLoad:
                loadStatus = EMPTY_LOAD;
                modifyCarStatus();
                break;
            case R.id.rbHalfLoad:
                loadStatus = HALF_LOAD;
                modifyCarStatus();
                break;
            case R.id.rbFullLoad:
                loadStatus = FULL_LOAD;
                modifyCarStatus();
                break;
            default:
                break;
        }
    }

    private void carOut() {
        String status = sp.getString(UserEntity.STATUS, "0");
        LogUtils.i("STATUS = " + status);
        if (status.equals("0")) {
            Toast.makeText(context, "请先实名认证！", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(context, AuthActivity.class));
        } else if (status.equals("1")) {
            Toast.makeText(context, "认证审核中，请耐心等候！", Toast.LENGTH_SHORT).show();
        } else if (status.equals("3")) {
            Toast.makeText(context, "认证未通过，请重新认证！", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(context, AuthActivity.class));
        } else if (status.equals("2")) {
            carStatus = OUT_CAR;
            modifyCarStatus();
        }
    }

    private void getOrderDetail(String orderNo, final String userMsgID) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);

        String url = getString(R.string.server_url) + URLMap.CDSORDER_DETAIL;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");

                        LogUtils.w("MainFragment.getOrderDetail()");

                        LogUtils.i(data.toString());

                        Bundle bundle = new Bundle();
                        Iterator<String> it = data.keys();
                        while (it.hasNext()) {
                            String key = it.next();
                            String value = data.getString(key);
                            bundle.putString(key, value);
                        }
                        if ("1".equals(bundle.get("ORDER_STATUS"))) { // 订单为已派单状态，进行接单
                            startActivity(new Intent(context, NewMessageActivity.class).putExtras(bundle).putExtra("From", "Main")
                                    .putExtra("USERMESSAGE_ID", userMsgID));
                        } else { // 其它状态打开订单详情
                            startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", bundle.getString("ORDER_NO")));
                        }
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(context, "此单因超时未接单，系统已转派其他车辆", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }

            }
        });
    }

    private void doDeleteMessage(final int position) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("USERMESSAGE_ID", listMsg.get(position).getUserMessageID());

        String url = getString(R.string.server_url) + URLMap.DELETE_MY_MESSAGE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        if (position > listMsg.size() - 1) {
                            listMsg.remove(listMsg.size() - 1);
                        } else {
                            listMsg.remove(position);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "删除消息失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }

    private void getMessage() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("SCANTYPE", getString(R.string.SCANTYPE));
        params.addQueryStringParameter("ISSPECIAL", "1");// 默认1
        //params.addBodyParameter("SOURCETYPE", "4");

        String url = getString(R.string.server_url) + URLMap.MY_MESSAGE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("获取消息请求失败！");
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        LogUtils.i(data.toString());
                        JSONArray list = data.getJSONArray("LIST");
                        listMsg.clear();
                        OrderMessage msg = null;
                        for (int i = 0; i < list.length(); i++) {
                            obj = list.getJSONObject(i);
                            msg = new OrderMessage();
                            msg.setSummary(CommonTools.judgeNull(obj, "SUMMARY", ""));
                            msg.setContentType(CommonTools.judgeNull(obj, "CONTENTTYPE", ""));
                            msg.setUserMessageID(CommonTools.judgeNull(obj, "USERMESSAGE_ID", ""));
                            msg.setCreateTime(CommonTools.judgeNull(obj, "CREATETIME", ""));
                            msg.setStatus(CommonTools.judgeNull(obj, "STATUS", ""));
                            msg.setTitle(CommonTools.judgeNull(obj, "TITLE", ""));
                            msg.setSourceID(CommonTools.judgeNull(obj, "SOURCEID", ""));
                            msg.setSourceType(CommonTools.judgeNull(obj, "SOURCETYPE", ""));
                            listMsg.add(msg);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        LogUtils.i(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    /**
     * 更新车辆状态
     */
    private void modifyCarStatus() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("DISPATCHSTATUS", carStatus);
        params.addQueryStringParameter("LOADSTATUS", loadStatus);

        String url = getString(R.string.server_url) + URLMap.UPDATE_CAR_STATE;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                updateCarStatus();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        LogUtils.i("更新车辆状态成功！");
                        Editor edit = sp.edit();
                        if (OUT_CAR.equals(carStatus)) { // 出车成功
                            // tvAssignTurnoverRate.setVisibility(View.GONE);
                            carStatus = OUT_CAR;
                        } else if (CLOSE_CAR.equals(carStatus)) { // 收车成功
                            // tvAssignTurnoverRate.setVisibility(View.GONE);
                            carStatus = CLOSE_CAR;
                            loadStatus = EMPTY_LOAD;
                        }
                        edit.putString(UserEntity.CAR_STATUS, carStatus);
                        edit.putString(UserEntity.LOAD_STATUS, loadStatus);
                        edit.commit();
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                    updateCarStatus();
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }

        });
    }

    /**
     * 上传车辆位置
     */
    private void updateCarStatus() {
        carStatus = sp.getString(UserEntity.CAR_STATUS, CLOSE_CAR);
        if (OUT_CAR.equals(carStatus)) {
            btnOutCar.setVisibility(View.GONE);
            //rlOutCar.setVisibility(View.VISIBLE);
            rgLoadState.setVisibility(View.VISIBLE);
            btnCloseCar.setVisibility(View.VISIBLE);
            // 启动定位服务
            context.startService(new Intent(context, LocationService.class));
        } else if (CLOSE_CAR.equals(carStatus)) {
            btnOutCar.setVisibility(View.VISIBLE);
            //rlOutCar.setVisibility(View.GONE);
            rgLoadState.setVisibility(View.GONE);
            btnCloseCar.setVisibility(View.GONE);
            // 关闭定位
            context.stopService(new Intent(context, LocationService.class));
        }
        updateLoadStatus();
    }

    /**
     * 更新载重状态显示
     */
    private void updateLoadStatus() {
        loadStatus = sp.getString(UserEntity.LOAD_STATUS, null);
        if (EMPTY_LOAD.equals(loadStatus)) {
            rbEmptyLoad.setChecked(true);
            rbEmptyLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            rbHalfLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            rbFullLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        } else if (HALF_LOAD.equals(loadStatus)) {
            rbHalfLoad.setChecked(true);
            rbEmptyLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            rbHalfLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            rbFullLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        } else if (FULL_LOAD.equals(loadStatus)) {
            rbFullLoad.setChecked(true);
            rbEmptyLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            rbHalfLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            rbFullLoad.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        }

    }

    private class DataUpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            setView();
        }
    }

    /**
     * 获取用户信息
     */
    private void getAuthInfo() {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));

        String url = getString(R.string.server_url) + URLMap.GET_DRIVER_AUTHEN_INFO;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        dialog.show();
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                dialog.dismiss();
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    if ("1".equals(status)) {
                        JSONArray datas = obj.getJSONArray("data");
                        JSONObject data = datas.getJSONObject(0);

                        Editor edit = sp.edit();
                        edit.putString(UserEntity.NAME, CommonTools.judgeNull(data, "NAME", ""));
                        edit.putString(UserEntity.IDCARD_NUM, CommonTools.judgeNull(data, "IDENTITYCARD", ""));
                        edit.putString(UserEntity.CAR_NUMBER, CommonTools.judgeNull(data, "CARNUMBER", ""));
                        edit.putString(UserEntity.CAR_TYPE, CommonTools.judgeNull(data, "CARTYPE", ""));
                        edit.putString(UserEntity.CAR_LENGTH, CommonTools.judgeNull(data, "CARLENGTH", ""));
                        edit.putString(UserEntity.CAR_LOAD_WEIGHT, CommonTools.judgeNull(data, "LOADWEIGHT", ""));
                        edit.putString(UserEntity.IDCARD_IMG, CommonTools.judgeNull(data, "SLOCALPHOTO", ""));
                        edit.putString(UserEntity.DRIVER_IMG, CommonTools.judgeNull(data, "DRIVERLICIMG", ""));
                        edit.putString(UserEntity.DRIVING_IMG, CommonTools.judgeNull(data, "TRAVELLICIMG", ""));
                        edit.putString(UserEntity.CAR_DRIVER_IMG, CommonTools.judgeNull(data, "CARDRIVERIMG", ""));
                        edit.putString(UserEntity.STATUS, CommonTools.judgeNull(data, "USER_STATUS", ""));
                        edit.putString(UserEntity.CAR_VOLUME, CommonTools.judgeNull(data, "VOLUMN", ""));
                        edit.commit();

                        carOut();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                }
            }

            @Override
            public void onFailure(HttpException arg0, String result) {
                dialog.dismiss();
            }

        });
    }

}
