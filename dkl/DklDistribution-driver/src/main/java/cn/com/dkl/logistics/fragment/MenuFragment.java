package cn.com.dkl.logistics.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.haitun.xillen.tools.ToastTool;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.text.DecimalFormat;

import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.AuthActivity;
import cn.com.dkl.logistics.driver.BindBankCardAcitivity;
import cn.com.dkl.logistics.driver.CarListActivity;
import cn.com.dkl.logistics.driver.EditPersonalInfoActivity;
import cn.com.dkl.logistics.driver.GrabSingleActivity;
import cn.com.dkl.logistics.driver.ModifyPasswordActivity;
import cn.com.dkl.logistics.driver.ModifypaymentPawActivity;
import cn.com.dkl.logistics.driver.MyWalletActivity;
import cn.com.dkl.logistics.driver.OrderListActivity;
import cn.com.dkl.logistics.driver.PersonalMoreActivity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.driver.StatisticalActivity;
import cn.com.dkl.logistics.driver.SurroundingMapActivity;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.view.CircleImageView;

public class MenuFragment extends Fragment {

    private View v;
    @ViewInject(R.id.civHeadImg)
    private CircleImageView civHeadImg;
    @ViewInject(R.id.tvUserName)
    private TextView tvUserName;
    @ViewInject(R.id.rBarPointLevel)
    private RatingBar rBarPointLevel;
    @ViewInject(R.id.tvEvaluationNumber)
    private TextView tvEvaluationNumber;
    @ViewInject(R.id.tvHighPraiseRate)
    private TextView tvHighPraiseRate;
    @ViewInject(R.id.tvRealNameAuthState)
    private TextView tvRealNameAuthState;
    @ViewInject(R.id.tvAccountBalance)
    private TextView tvAccountBalance;

    private static Activity activity;
    /**
     * 编辑个人信息
     */
    @ViewInject(R.id.rlEditPersonalInfo)
    private RelativeLayout rlEditPersonalInfo;
    /**
     * 实名认证
     */
    @ViewInject(R.id.rlRealNameAuth)
    private RelativeLayout rlRealNameAuth;
    /**
     * 我的钱包
     */
    @ViewInject(R.id.llMyPurse)
    private LinearLayout llMyPurse;
    /**
     * 我的车辆
     */
    @ViewInject(R.id.llMyCar)
    private LinearLayout llMyCar;
    /**
     * 我的订单
     */
    @ViewInject(R.id.llMyOrder)
    private LinearLayout llMyOrder;
    /**
     * 抢单列表
     */
    @ViewInject(R.id.llGrabSingle)
    private LinearLayout llGrabSingle;
    /**
     * 我的统计
     */
    @ViewInject(R.id.llMyStatistical)
    private LinearLayout llMyStatistical;
    /**
     * 安全设置
     */
    @ViewInject(R.id.llSecuritySetting)
    private LinearLayout llSecuritySetting;
    /**
     * 周边情况
     */
    @ViewInject(R.id.llPeripheralSituation)
    private LinearLayout llPeripheralSituation;
    /**
     * 绑定银行卡
     */
    @ViewInject(R.id.llBindBankCard)
    private LinearLayout llBindBankCard;
    /**
     * 设置支付密码
     */
    @ViewInject(R.id.llModifypaymentPaw)
    private LinearLayout llModifypaymentPaw;
    /**
     * 更多
     */
    @ViewInject(R.id.llMore)
    private LinearLayout llMore;
    private String state = "0";// 未认证状态
    private static Context context;
    private static SharedPreferences sp;
    private DataUpdateReceiver receiver;
    private DecimalFormat def = new DecimalFormat("###0.00");

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        registerReceiver();
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_menu, container, false);
        ViewUtils.inject(this, v);
        init();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        setView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiver != null && isAdded()) {
            context.unregisterReceiver(receiver);
        }
    }

    private void init() {
        llMyPurse.setVisibility(View.GONE);
        llMyCar.setVisibility(View.GONE);
        llGrabSingle.setVisibility(View.GONE);
        llBindBankCard.setVisibility(View.GONE);
        llModifypaymentPaw.setVisibility(View.GONE);
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        state = sp.getString(UserEntity.STATUS, "0");
        activity = getActivity();
    }

    /**
     * 注册广播接收数据更新通知
     */
    private void registerReceiver() {
        receiver = new DataUpdateReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("cn.com.dkl.logistics.driver.MainDataUpdate");
        context.registerReceiver(receiver, filter);
    }

    private void setView() {
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        String headImageName = sp.getString(UserEntity.HEAD_PHOTO_URL, null);
        if (TextUtils.isEmpty(headImageName)) {
            civHeadImg.setImageResource(R.drawable.img_default_head_image);
        } else {
            ImageUtils.downLoadImage(context, civHeadImg, headImageName);
        }

        tvUserName.setText(sp.getString(UserEntity.NAME, "--"));
        int pointLevel = 0;
        try {
            pointLevel = (int) Float.parseFloat(sp.getString(UserEntity.POINT_LEVEL, "1"));
        } catch (NumberFormatException e) {
            LogUtils.i(e.toString());
        }
        rBarPointLevel.setNumStars(pointLevel);
//		tvEvaluationNumber.setText("--");
//		tvHighPraiseRate.setText("100%");
        tvEvaluationNumber.setText(sp.getString(UserEntity.EVALUATION_NUMBER, "0") + "条");
        tvHighPraiseRate.setText(def.format(Double.parseDouble(sp.getString(UserEntity.HIGH_PRAISE_RATE, "0")) * 100) + "%");
        state = sp.getString(UserEntity.STATUS, "0");
        if ("1".equals(state)) {
            tvRealNameAuthState.setText("认证中");
        } else if ("2".equals(state)) {
            tvRealNameAuthState.setText("已认证");
        } else if ("3".equals(state)) {
            tvRealNameAuthState.setText("认证失败");
        }
        tvAccountBalance.setText(sp.getString(UserEntity.WEB_BALANCE, "0.00") + "元");
    }

    @OnClick({R.id.rlEditPersonalInfo, R.id.rlRealNameAuth, R.id.llMyPurse, R.id.llMyCar, R.id.llMyOrder, R.id.llGrabSingle, R.id.llMyStatistical, R.id.llSecuritySetting, R.id.llPeripheralSituation,
            R.id.llBindBankCard, R.id.llModifypaymentPaw, R.id.tvEvaluationNumber, R.id.llMore})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMore:
                break;
            case R.id.rlEditPersonalInfo: // 编辑个人信息
                startActivity(new Intent(context, EditPersonalInfoActivity.class));//编辑个人信息
                break;
            case R.id.rlRealNameAuth:
//                 if (state.equals("0")){
//                    startActivity(new Intent(context, AuthTypeActivity.class));//实名认证
//                }else {
                startActivity(new Intent(context, AuthActivity.class));//实名认证
//                }
                break;
            case R.id.llMyPurse:
                startActivity(new Intent(context, MyWalletActivity.class));//我的钱包界面
                break;
            case R.id.llMyCar:
                if (state.equals("2")){
                    startActivity(new Intent(context, CarListActivity.class));//车辆管理
                }else {
                    ToastTool.toastByString(context,"您的实名认证未通过");
                }
                break;
            case R.id.llMyOrder:
                startActivity(new Intent(context, OrderListActivity.class));//运单列表
                break;
            case R.id.llGrabSingle:
                startActivity(new Intent(context, GrabSingleActivity.class));//抢单列表
                break;
            case R.id.llMyStatistical:
                startActivity(new Intent(context, StatisticalActivity.class));//运力统计
                break;
            case R.id.llSecuritySetting:
                startActivity(new Intent(context, ModifyPasswordActivity.class));//修改密码
                break;
            case R.id.llPeripheralSituation:
                startActivity(new Intent(context, SurroundingMapActivity.class));//周边情况地图界面
                break;
            case R.id.llBindBankCard:
                getAuthInfo();
                break;
            case R.id.llModifypaymentPaw:
                //startActivity(new Intent(context, ModifypaymentPawActivity.class));
                getIsBandBankCard();
                break;
            case R.id.tvEvaluationNumber:
//                startActivity(new Intent(context, MyEvaluateActivity.class));//评论列表
                break;
            case R.id.llMore:
                Intent intent = new Intent();
                intent.setClass(context, PersonalMoreActivity.class);//更多
                startActivity(intent);
            default:
                break;
        }
    }

    private class DataUpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            setView();
        }
    }

    /**
     * 获取认证状态
     */
    private void getAuthInfo() {
        String status = sp.getString(UserEntity.STATUS, "0");
        LogUtils.i("STATUS = " + status);
        if (status.equals("0")) {
            Toast.makeText(context, "请先实名认证！", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(context, AuthActivity.class));
        } else if (status.equals("1")) {
            Toast.makeText(context, "认证审核中，请耐心等候！", Toast.LENGTH_SHORT).show();
        } else if (status.equals("3")) {
            Toast.makeText(context, "认证未通过，请重新认证！", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(context, AuthActivity.class));
        } else if (status.equals("2")) {
            startActivity(new Intent(context, BindBankCardAcitivity.class));
        }
    }

    /**
     * 获取绑定银行卡信息
     */
    private void getIsBandBankCard() {
        String isBandBankCard = sp.getString(UserEntity.IS_BAND_BANKCARD, "0");
        String status = sp.getString(UserEntity.STATUS, "0");
        LogUtils.i("STATUS = " + status);
        LogUtils.i("isBandBankCard = " + isBandBankCard);
        if (status.equals("0")) {
            Toast.makeText(context, "请先实名认证！", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(context, AuthActivity.class));
        } else if (status.equals("1")) {
            Toast.makeText(context, "认证审核中，请耐心等候！", Toast.LENGTH_SHORT).show();
        } else if (status.equals("3")) {
            Toast.makeText(context, "认证未通过，请重新认证！", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(context, AuthActivity.class));
        } else if (status.equals("2")) {
            if (isBandBankCard.equals("0")) {
                Toast.makeText(context, "请先绑定银行卡！", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, BindBankCardAcitivity.class));
            } else {
                startActivity(new Intent(context, ModifypaymentPawActivity.class));
            }
        }
    }
}