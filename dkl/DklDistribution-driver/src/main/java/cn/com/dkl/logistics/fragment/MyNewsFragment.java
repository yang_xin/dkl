package cn.com.dkl.logistics.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dkl.logistics.adapter.MyNewsAdapter;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.GrabSingleActivity;
import cn.com.dkl.logistics.driver.NewMessageActivity;
import cn.com.dkl.logistics.driver.NewsDetailsActivity;
import cn.com.dkl.logistics.driver.OrderDetailActivity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.OrderMessage;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.NetworkCallBack;

import static cn.com.dkl.logistics.driver.R.id.listView;

/**
 * 消息列表 Created by LiuXing on 2016/12/12.
 */

public class MyNewsFragment extends BasicFragment {
    private static final String TAG = "MyNewsFragment";
    @Bind(R.id.tvTime)
    TextView mTvTime;
    @Bind(listView)
    SwipeMenuListView mListView;

    private View mView;
    private Context context;
    private SharedPreferences sp;
    private MyNewsAdapter adapter = null;
    private List<OrderMessage> listNews = new ArrayList<OrderMessage>();
    private Callback.Cancelable mCancel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_mynews, null);
        ButterKnife.bind(this, mView);
        initData();
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isAdded()){
            getNews();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCancel != null){
            mCancel.cancel();
        }
//        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        context = getActivity();
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        String date = sdf.format(new Date());
        LogUtils.i(date);
        mTvTime.setText(date);

        adapter = new MyNewsAdapter(context, listNews);
        mListView.setAdapter(adapter);

        // 创建滑动菜单
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(context);
                openItem.setBackground(new ColorDrawable(Color.RED));
                openItem.setWidth(CommonTools.dp2px(context, 80));
                openItem.setTitle("删除");
                openItem.setTitleSize(18);
                openItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(openItem);
            }
        };

        // 添加菜单到listview
        mListView.setMenuCreator(creator);
        // 添加listview item菜单点击事件
        mListView.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            @Override
            public void onMenuItemClick(int position, SwipeMenu menu, int menuIndex) {
                switch (menuIndex) {
                    case 0:
                        doDeleteNews(position);
                        break;
                    default:
                        break;
                }

            }

        });

        mListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // 0运起来自定义推送 1车源 2货源 3专线 4同城配送司机订单通知 5同城配送货主订单通知 6同城配送司机普通通知 7同城配送货主普通通知
                String sourceType = listNews.get(position).getSourceType();
                // 将资源id传到详细页面
                String userMessageID = listNews.get(position).getUserMessageID();

                if (sourceType.equals("4")) { // 打开订单详情
                    getOrderDetail(listNews.get(position).getSourceID(), listNews.get(position).getUserMessageID());
                    isReadNews(position);
                } else if (sourceType.equals("11")) { // 打开抢单列表
                    context.startActivity(new Intent(context, GrabSingleActivity.class));
                    isReadNews(position);
                } else { // 打开普通消息详情
                    context.startActivity(new Intent(context, NewsDetailsActivity.class).putExtra("userMessageID", userMessageID));
                    isReadNews(position);
                }

            }
        });

    }

    /**
     * 删除消息
     */
    private void doDeleteNews(final int position) {
        String url = getString(R.string.server_url) + URLMap.DELETE_MY_MESSAGE;
        RequestParams params = new RequestParams(url);
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("USERMESSAGE_ID", listNews.get(position).getUserMessageID());


//        LogUtils.i(url + CommonTools.getQuryParams(params));
        NetworkCallBack callBack = new NetworkCallBack(context);
        callBack.networkRequest(params, true, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        if (position > listNews.size() - 1) {
                            listNews.remove(listNews.size() - 1);
                        } else {
                            listNews.remove(position);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        if (message.equals("")) {
                            Toast.makeText(context, "删除消息失败！", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                    Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        HttpUtils http = new HttpUtils(60 * 1000);
//        http.configCurrentHttpCacheExpiry(1000 * 10);
//        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
//
//            @Override
//            public void onFailure(HttpException arg0, String arg1) {
//                CommonTools.failedToast(context);
//            }
//
//            @Override
//            public void onSuccess(ResponseInfo<String> arg0) {cancelable
//                try {
//                    JSONObject obj = new JSONObject(arg0.result);
//                    String message = obj.getString("message");
//                    String state = obj.getString("state");
//                    if ("1".equals(state)) {
//                        if (position > listNews.size() - 1) {
//                            listNews.remove(listNews.size() - 1);
//                        } else {
//                            listNews.remove(position);
//                        }
//                        adapter.notifyDataSetChanged();
//                    } else {
//                        if (message.equals("")) {
//                            Toast.makeText(context, "删除消息失败！", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                } catch (JSONException e) {
//                    LogUtils.i(e.toString());
//                    Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//        });
    }


    /**
     * 消息已读反馈
     */
    private void isReadNews(final int position) {
        String url = getString(R.string.server_url) + URLMap.IS_READ_MESSAGE;
        RequestParams params = new RequestParams(url);
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("USERMESSAGE_ID", listNews.get(position).getUserMessageID());

//        LogUtils.i(url + CommonTools.getQuryParams(params));

        NetworkCallBack callBack = new NetworkCallBack(context);
        callBack.networkRequest(params, true, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                    Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        HttpUtils http = new HttpUtils(60 * 1000);
//        http.configCurrentHttpCacheExpiry(1000 * 10);
//        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
//
//            @Override
//            public void onFailure(HttpException arg0, String arg1) {
//                CommonTools.failedToast(context);
//            }
//
//            @Override
//            public void onSuccess(ResponseInfo<String> arg0) {
//                try {
//                    JSONObject obj = new JSONObject(arg0.result);
//                    String message = obj.getString("message");
//                    String state = obj.getString("state");
//                    if ("1".equals(state)) {
//                        adapter.notifyDataSetChanged();
//                    } else {
//                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    LogUtils.i(e.toString());
//                    Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//        });
    }

    /**
     * 获取消息
     */
    private void getNews() {
        final String url = getString(R.string.server_url) + URLMap.GET_My_MESSAGE;
        RequestParams params = new RequestParams(url);
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("CURRENTPAGE", "0");//默认第0页
        params.addQueryStringParameter("SHOWCOUNT", "30");//默认10000条

//        LogUtils.i(url + CommonTools.getQuryParams(params));
        NetworkCallBack callBack = new NetworkCallBack(context);
        mCancel = callBack.networkRequest(params, true, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        LogUtils.i(data.toString());
                        JSONArray list = data.getJSONArray("LIST");
                        listNews.clear();
                        OrderMessage msg = null;
                        if (list.length() > 0) {
                            for (int i = 0; i < list.length(); i++) {
                                obj = list.getJSONObject(i);
                                msg = new OrderMessage();
                                msg.setSummary(CommonTools.judgeNull(obj, "SUMMARY", ""));
                                msg.setContentType(CommonTools.judgeNull(obj, "CONTENTTYPE", ""));
                                msg.setUserMessageID(CommonTools.judgeNull(obj, "USERMESSAGE_ID", ""));
                                msg.setCreateTime(CommonTools.judgeNull(obj, "CREATETIME", ""));
                                msg.setStatus(CommonTools.judgeNull(obj, "STATUS", ""));
                                msg.setTitle(CommonTools.judgeNull(obj, "TITLE", ""));
                                msg.setSourceID(CommonTools.judgeNull(obj, "SOURCEID", ""));
                                msg.setSourceType(CommonTools.judgeNull(obj, "SOURCETYPE", ""));
                                listNews.add(msg);
                            }
                        } else {
                            Toast.makeText(context, "暂无数据！", Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        LogUtils.i(message);
                        if (message.equals("")) {
                            Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        HttpUtils http = new HttpUtils(60 * 1000);
//        http.configCurrentHttpCacheExpiry(1000 * 10);
//        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
//            @Override
//            public void onStart() {
//                super.onStart();
//                Log.d(TAG, "onStart: " + url + "USERNAME=" + sp.getString(UserEntity.PHONE, null) + "&PASSWORD=" + sp.getString(UserEntity.PASSWORD, null) + "&CURRENTPAGE=" +
//                        "0" + "&SHOWCOUNT=" + "30" + "&ISSPECIAL=" + "1");
//            }
//
//            @Override
//            public void onFailure(HttpException arg0, String arg1) {
//                LogUtils.i("获取消息请求失败！");
//                CommonTools.failedToast(context);
//            }
//
//            @Override
//            public void onSuccess(ResponseInfo<String> arg0) {
//                LogUtils.i("result= " + arg0.result);
//                try {
//                    JSONObject obj = new JSONObject(arg0.result);
//                    String state = obj.getString("state");
//                    String message = obj.getString("message");
//                    if ("1".equals(state)) {
//                        JSONObject data = obj.getJSONObject("data");
//                        LogUtils.i(data.toString());
//                        JSONArray list = data.getJSONArray("LIST");
//                        listNews.clear();
//                        OrderMessage msg = null;
//                        if (list.length() > 0) {
//                            for (int i = 0; i < list.length(); i++) {
//                                obj = list.getJSONObject(i);
//                                msg = new OrderMessage();
//                                msg.setSummary(CommonTools.judgeNull(obj, "SUMMARY", ""));
//                                msg.setContentType(CommonTools.judgeNull(obj, "CONTENTTYPE", ""));
//                                msg.setUserMessageID(CommonTools.judgeNull(obj, "USERMESSAGE_ID", ""));
//                                msg.setCreateTime(CommonTools.judgeNull(obj, "CREATETIME", ""));
//                                msg.setStatus(CommonTools.judgeNull(obj, "STATUS", ""));
//                                msg.setTitle(CommonTools.judgeNull(obj, "TITLE", ""));
//                                msg.setSourceID(CommonTools.judgeNull(obj, "SOURCEID", ""));
//                                msg.setSourceType(CommonTools.judgeNull(obj, "SOURCETYPE", ""));
//                                listNews.add(msg);
//                            }
//                        } else {
//                            Toast.makeText(context, "暂无数据！", Toast.LENGTH_SHORT).show();
//                        }
//                        adapter.notifyDataSetChanged();
//                    } else {
//                        LogUtils.i(message);
//                        if (message.equals("")) {
//                            Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(context, "请求失败！", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }

    /**
     * 获取订单详情
     */
    private void getOrderDetail(String orderNo, final String userMsgID) {

        String url = getString(R.string.server_url) + URLMap.CDSORDER_DETAIL;
        RequestParams params = new RequestParams(url);
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
//        LogUtils.i(url + CommonTools.getQuryParams(params));
        Log.d(TAG, "onStart: " + url + "USERNAME=" + sp.getString(UserEntity.PHONE, null) + "&PASSWORD=" + sp.getString(UserEntity.PASSWORD, null) + "&ORDER_NO=" +
                orderNo);

        NetworkCallBack callBack = new NetworkCallBack(context);
        callBack.networkRequest(params, true, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                try {
                    JSONObject obj = new JSONObject(result);
                    String message = obj.getString("message");
                    String state = obj.getString("state");
                    if ("1".equals(state)) {
                        JSONObject data = obj.getJSONObject("data");
                        LogUtils.w("MainFragment.getOrderDetail()");
                        LogUtils.i(data.toString());
                        Bundle bundle = new Bundle();
                        Iterator<String> it = data.keys();
                        while (it.hasNext()) {
                            String key = it.next();
                            String value = data.getString(key);
                            bundle.putString(key, value);
                        }

                        String userId = sp.getString(UserEntity.USER_ID, null);
                        String driverId = bundle.getString("DRIVERUSERID");
                        if (!userId.equals(driverId)) {
                            String showMessage = "该运单已经重新派指给他人";
                            Toast.makeText(context, showMessage, Toast.LENGTH_SHORT).show();
                        } else if ("1".equals(bundle.get("STATUS"))) { // 订单为已派单状态，进行接单
                            startActivity(new Intent(context, NewMessageActivity.class).putExtras(bundle).putExtra("From", "Main")
                                    .putExtra("USERMESSAGE_ID", userMsgID));
                        } else { // 其它状态打开订单详情
                            startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", bundle.getString("WAYBILL_NUMBER")));
                        }
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(context, "此单因超时未接单，系统已转派其他车辆", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    LogUtils.i(e.toString());
                }
            }
        });

//        HttpUtils http = new HttpUtils(60 * 1000);
//        http.configCurrentHttpCacheExpiry(1000 * 10);
//        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
//
//            @Override
//            public void onFailure(HttpException arg0, String arg1) {
//                CommonTools.failedToast(context);
//            }
//
//            @Override
//            public void onSuccess(ResponseInfo<String> arg0) {
//                try {
//                    JSONObject obj = new JSONObject(arg0.result);
//                    String message = obj.getString("message");
//                    String state = obj.getString("state");
//                    if ("1".equals(state)) {
//                        JSONObject data = obj.getJSONObject("data");
//                        LogUtils.w("MainFragment.getOrderDetail()");
//                        LogUtils.i(data.toString());
//                        Bundle bundle = new Bundle();
//                        Iterator<String> it = data.keys();
//                        while (it.hasNext()) {
//                            String key = it.next();
//                            String value = data.getString(key);
//                            bundle.putString(key, value);
//                        }
//                        if ("1".equals(bundle.get("STATUS"))) { // 订单为已派单状态，进行接单
//                            startActivity(new Intent(context, NewMessageActivity.class).putExtras(bundle).putExtra("From", "Main")
//                                    .putExtra("USERMESSAGE_ID", userMsgID));
//                        } else { // 其它状态打开订单详情
//                            startActivity(new Intent(context, OrderDetailActivity.class).putExtra("OrderNo", bundle.getString("WAYBILL_NUMBER")));
//                        }
//                    } else {
//                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                    }
//                    //Toast.makeText(context, "此单因超时未接单，系统已转派其他车辆", Toast.LENGTH_SHORT).show();
//                } catch (JSONException e) {
//                    LogUtils.i(e.toString());
//                }
//
//            }
//        });
    }

}
