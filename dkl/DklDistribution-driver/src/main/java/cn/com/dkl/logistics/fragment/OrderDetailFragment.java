package cn.com.dkl.logistics.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import cn.com.dkl.logistics.adapter.OrderListAdapter;
import cn.com.dkl.logistics.adapter.OrderPointAdapter;
import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.AlertDeleteDialogwindow;
import cn.com.dkl.logistics.driver.AllEvaluateActivity;
import cn.com.dkl.logistics.driver.ImageChooseDialogWindow;
import cn.com.dkl.logistics.driver.MainActivity;
import cn.com.dkl.logistics.driver.NewMessageActivity;
import cn.com.dkl.logistics.driver.OrderDetailActivity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.entity.DeliverBean;
import cn.com.dkl.logistics.entity.PointInfo;
import cn.com.dkl.logistics.entity.TstOrderBean;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.Constant;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.view.CircleImageView;
import cn.com.dkl.logistics.view.CustomProgressDialog;
import cn.com.dkl.logistics.view.IconButton;
import cn.com.dkl.logistics.view.RoundImageView;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

import static cn.com.dkl.logistics.utils.CommonTools.judgeNull;

public class OrderDetailFragment extends Fragment implements OrderDetailActivity.OnUpLoadTrstImageListener {

    @ViewInject(R.id.iv_HeadOrderDetail)
    private static CircleImageView userHeadImg;
    @ViewInject(R.id.tv_name)
    private static TextView tvname;
    @ViewInject(R.id.tv_phone)
    private static TextView tvphone;
    @ViewInject(R.id.tv_totalcost)
    private static TextView totalCost;
    @ViewInject(R.id.tv_weightnum)
    private static TextView weightNum;
    @ViewInject(R.id.tv_piecenumber)
    private static TextView pieceNumber;
    @ViewInject(R.id.tv_startingprices)
    private TextView startingPrices;
    @ViewInject(R.id.tv_distance)
    private static TextView distance;
    @ViewInject(R.id.tv_mileageprices)
    private static TextView mileagePrices;
    @ViewInject(R.id.btnTakeOrder)
    private static Button btnTakeOrder;
    @ViewInject(R.id.listView)
    private static ListView listView;
    // 订单状态
    @ViewInject(R.id.tv_order_status)
    private TextView tv_order_status;
    @ViewInject(R.id.tv_status)
    private static TextView tv_status;
    // 保险费
    @ViewInject(R.id.tv_insurancecost)
    private TextView tv_insurancecost;
    // 代收货款
    @ViewInject(R.id.tv_payment)
    private TextView tv_payment;
    // 发票费用
    @ViewInject(R.id.tv_invoicecost)
    private TextView tv_invoicecost;
    // 追加运费用
    @ViewInject(R.id.tv_additionalfreight)
    private TextView tv_additionalfreight;
    // 提货单地址
    @ViewInject(R.id.tv_content_deliveryaddress)
    private static TextView tv_content_deliveryaddress;
    @ViewInject(R.id.tv_deliveryaddress)
    private static TextView tv_deliveryaddress;
    // 发票抬头
    @ViewInject(R.id.tv_content_invocetitle)
    private static TextView tv_content_invocetitle;
    @ViewInject(R.id.tv_invocetitle)
    private static TextView tv_invocetitle;
    // 延时时间
    @ViewInject(R.id.tv_delaytime)
    private TextView tv_delaytime;
    // 延时费用
    @ViewInject(R.id.tv_delaytimecost)
    private static TextView tv_delaytimecost;
    // 特殊要求
    @ViewInject(R.id.tv_content_specialrequirement)
    private static TextView tv_content_specialrequirement;
    @ViewInject(R.id.tv_specialrequirement)
    private static TextView tv_specialrequirement;
    // 回单签收
    @ViewInject(R.id.tv_isreceipt)
    private TextView tv_isreceipt;
    // 提货开始时间
    @ViewInject(R.id.tvSendTimeStart)
    private static TextView tvSendTimeStart;
    // 提货结束时间
    @ViewInject(R.id.tvSendTimeEnd)
    private static TextView tvSendTimeEnd;
    // 体积
    @ViewInject(R.id.tvVolume)
    private static TextView tvVolume;
    // 订单号
    @ViewInject(R.id.tvOrderNumber)
    private static TextView tvOrderNumber;
    // 装车时间
    @ViewInject(R.id.tv_loding_time)
    private static TextView tv_loding_time;
    // 承运车辆
    @ViewInject(R.id.tvCarNo)
    private static TextView tvCarNo;
    // 备注
    @ViewInject(R.id.tv_remark)
    private static TextView tv_remark;
    // 选择图片
    @ViewInject(R.id.btnSelectImg)
    private static IconButton btnSelectImg;
    //设为工作单
    @ViewInject(R.id.btnSetWork)
    Button btnSetWork;
    // 提货图片
    @ViewInject(R.id.ivReceiptimg)
    private static RoundImageView ivReceiptimg;

    static TstOrderBean mOrderBean;             //配送点列表
    private static Context context;
    private static SharedPreferences sp;
    private static String orderNo;
    public static String ordercode;
    private static String user_headPhotouURL;
    private static String loding_time;
    private static String remark;
    private static String isUrgent;
    private String dateTime;
    private static CustomProgressDialog dialog = null;

    private ImageChooseDialogWindow menuWindow;

    private static OrderPointAdapter adapter;
    private static List<PointInfo> pList;

    public static final String TAKE_ORDER = "接    单";
    public static final String TAKE_GOODS = "提    货";
    public static final String CONFIRM_ARRIVED = "确认到达";
    public static final String UPLOAD_RECEIPT = "上传回单";
    public static final String FINISHED = "已完成";
    public static final String VIEW_EVALIATION = "查看评价";
    public static final String SUBMITAUDIT = "提交审核";

    public static int checkPoint = -1;          //选中item下标
    private static final String TAG = "OrderDetailFragment";
    private static TextView mTvIsUrgent;        //是否加急
    private static TextView mTvMileag;          //里程
    private static TextView mTvTotalTime;       //总配送时间
    private static TextView mTvApprovalState;   //审批状态
    private static TextView mTvApprovalOpinion; //审批意见
    private static LinearLayout mLlTotalTime;

    private static String sWaybillnum;
    private static AlertDeleteDialogwindow wiDialogwindow;
    private static LinearLayout mLlApproval;
    private static CustomProgressDialog mDialog = null;
    static OrderDetailActivity orderDetailActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        orderDetailActivity = (OrderDetailActivity) activity;
        orderDetailActivity.setmOnUpLoadTrstImageListener(this);
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_info, container, false);
        mTvIsUrgent = (TextView) v.findViewById(R.id.tvIsUrgent);
        mTvMileag = (TextView) v.findViewById(R.id.tvMileag);
        mTvTotalTime = (TextView) v.findViewById(R.id.tvTotalTime);
        mTvApprovalState = (TextView) v.findViewById(R.id.tvApprovalState);
        mTvApprovalOpinion = (TextView) v.findViewById(R.id.tvApprovalOpinion);
        mLlTotalTime = (LinearLayout) v.findViewById(R.id.llTotalTime);
        mLlApproval = (LinearLayout) v.findViewById(R.id.llApproval);
        context = getActivity();
        ViewUtils.inject(this, v);
        init();
        LogUtils.w("OrderDetailFragment.onCreateView()");
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtils.w("OrderDetailFragment.onResume()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        orderNo = getActivity().getIntent().getExtras().getString("OrderNo");
        ImageUtils.picPath = null;
        OrderDetailActivity.isSelectDetail = true;
        dialog = new CommonTools(context).getProgressDialog(context, "加载中...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        mDialog = new CommonTools(context).getProgressDialog(context, "请稍候...");
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        initListView();
    }

    private void initListView() {
        requestData();
    }

    private static void showDialog() {
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private static void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @OnClick({R.id.btnTakeOrder, R.id.btnSelectImg, R.id.tv_phone, R.id.btnSetWork})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_phone:
                String phoneNumber = tvphone.getText().toString().trim();
                //点击手机号码拨打电话(跳转拨号界面)
                startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + phoneNumber)));
                break;
            case R.id.btnTakeOrder:
                String toDo = btnTakeOrder.getText().toString();
                if (TAKE_ORDER.equals(toDo)) { // 接单
                    context.startActivity(new Intent(context, NewMessageActivity.class).putExtra("From", "OrderList"));
                } else if (VIEW_EVALIATION.equals(toDo)) { // 查看评价
                    context.startActivity(new Intent(context, AllEvaluateActivity.class).putExtra("OrderNo", orderNo));
                } else if (SUBMITAUDIT.equals(toDo)){
                    submiteAudit();
                }
                    break;
            case R.id.btnSelectImg:
                menuWindow = new ImageChooseDialogWindow((Activity) context);
                menuWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                menuWindow.showAtLocation(((Activity) context).findViewById(R.id.orderLinear), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                break;
            case R.id.btnSetWork://设为工作单
                //1,传递运单数据
                if (mOrderBean == null) {
                    Toast.makeText(context, "此运单没有起点或终点，请检查后再试！", Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("flag", Constant.WORKORDER);
                intent.putExtra("data", mOrderBean);
                startActivity(intent);
                getActivity().finish();
                break;
            default:
                break;
        }
    }

    /**
     * listView 高度
     */
    private static void listHeight() {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            //如果item内容有多行中文
            int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
            listItem.measure(desiredWidth, 0);
            //listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static void submiteAudit(){
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("WAYBILL_NUMBER", sWaybillnum);

        String url = context.getString(R.string.server_url) + URLMap.SUBMITE_ADDIT;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                try {
                    JSONObject obj = new JSONObject(responseInfo.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        requestData();
                        ImageUtils.picPath = null;
                        Toast.makeText(context, "提交审批成功！", Toast.LENGTH_SHORT).show();
                        requestData();
                        btnTakeOrder.setVisibility(View.GONE);
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                    dismissDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dismissDialog();
                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
                CommonTools.failedToast(context);
                dismissDialog();
            }
        });
    }




    /**
     * 上传回单
     */
    public static void doReceipt(String imgFileName, final int position, String validata, final boolean isQuery, final String waybillnum, final String ordernum) {
//        showNavi();

        showDialog();
        String deliverId = pList.get(position).getDeliverId();
        String orderId = pList.get(position).getOrderno();
        String waybillId = pList.get(position).getWaybillid();
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
        params.addQueryStringParameter("DELIVER_ID", deliverId);
        params.addQueryStringParameter("EVIDENCE_IMG", imgFileName);
        params.addQueryStringParameter("VALIDATE_CODE", validata);//服务码
        params.addQueryStringParameter("ORDERID", orderId);
        params.addQueryStringParameter("WAYBILL_ID", waybillId);

        params.addQueryStringParameter("TRACE_LON", sp.getString(Constants.LONGTITUDE, ""));//上传位置
        params.addQueryStringParameter("TRACE_LAT", sp.getString(Constants.LATITUDE, ""));//上传位置
        params.addQueryStringParameter("TRACE_PROVINCE", sp.getString(UserEntity.CURRENT_PROVINCE, ""));//上传位置
        params.addQueryStringParameter("TRACE_CITY", sp.getString(UserEntity.CURRENT_CITY, ""));//上传位置
        params.addQueryStringParameter("TRACE_DISTRICT", sp.getString(UserEntity.CURRENT_DISTRICT, ""));//上传位置
        params.addQueryStringParameter("TRACE_ADDRESS", sp.getString(UserEntity.CURRENT_STREET, "") + sp.getString(UserEntity.CURRENT_STREETNUMBER, ""));//上传位置


        String url = context.getString(R.string.server_url) + URLMap.CDSUPLOAD_RECEIPTIMG;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String result) {
                CommonTools.failedToast(context);
                dismissDialog();
            }

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        if (isQuery) {
                            requestData();
                        }else {
                            listView.getChildAt(position).findViewById(R.id.btnTakeGoods).setVisibility(View.GONE);
                        }
                        ImageUtils.picPath = null;
                        Toast.makeText(context, "上传回单成功！", Toast.LENGTH_SHORT).show();
                        SaveLocation(waybillnum, ordernum);
                        if (position < listView.getChildCount()-1) {
                            showNavi(position);
                        }
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                    dismissDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dismissDialog();
                }

            }
        });
        LogUtils.i(http.toString());
    }

    /**
     * 确认到达
     */
    public static void doConfirmArrived(int position) {
//        showNavi(position);

        showDialog();
        String deliverId = pList.get(position).getDeliverId();
        String orderId = pList.get(position).getOrderno();
        String waybillId = pList.get(position).getWaybillid();
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
        params.addQueryStringParameter("DELIVER_ID", deliverId);
        params.addQueryStringParameter("ORDERID", orderId);
        params.addQueryStringParameter("WAYBILL_ID", waybillId);

        String url = context.getResources().getString(R.string.server_url) + URLMap.CDSARRIVED_DESTINATION;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                dismissDialog();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                Log.i(TAG, "onSuccess: " + arg0.result);
                try {
                    dismissDialog();
                    JSONObject obj = new JSONObject(arg0.result);
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        requestData();
                        Toast.makeText(context, "确认到达目的地成功！", Toast.LENGTH_SHORT).show();
                        Editor edit = sp.edit();
                        edit.putString(UserEntity.LOAD_STATUS, MainFragment.EMPTY_LOAD);
                        edit.commit();
                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    dismissDialog();
                    LogUtils.i(e.toString());
                }
            }
        });
    }

    /**
     * 确认提货
     */
    public static void doTakeGoods(String imgFileName, final int position, final String waybillnum, final String ordernum) {

//        showNavi(position);

        showDialog();
        String deliverId = pList.get(position).getDeliverId();
        String orderId = pList.get(position).getOrderno();
        String waybillId = pList.get(position).getWaybillid();
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
        params.addQueryStringParameter("DELIVER_ID", deliverId);
        params.addQueryStringParameter("EVIDENCE_IMG", imgFileName);
        params.addQueryStringParameter("ORDERID", orderId);
        params.addQueryStringParameter("WAYBILL_ID", waybillId);

        params.addQueryStringParameter("TRACE_LON", sp.getString(Constants.LONGTITUDE, ""));//上传位置
        params.addQueryStringParameter("TRACE_LAT", sp.getString(Constants.LATITUDE, ""));//上传位置
        params.addQueryStringParameter("TRACE_PROVINCE", sp.getString(UserEntity.CURRENT_PROVINCE, ""));//上传位置
        params.addQueryStringParameter("TRACE_CITY", sp.getString(UserEntity.CURRENT_CITY, ""));//上传位置
        params.addQueryStringParameter("TRACE_DISTRICT", sp.getString(UserEntity.CURRENT_DISTRICT, ""));//上传位置
        params.addQueryStringParameter("TRACE_ADDRESS", sp.getString(UserEntity.CURRENT_STREET, "") + sp.getString(UserEntity.CURRENT_STREETNUMBER, ""));//上传位置

        String url = context.getResources().getString(R.string.server_url) + URLMap.CDSTAKE_GOODS;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                dismissDialog();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    dismissDialog();
                    JSONObject obj = new JSONObject(arg0.result);
                    String state = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(state)) {
                        requestData();
                        ImageUtils.picPath = null;
                        Toast.makeText(context, "确认提货成功！", Toast.LENGTH_SHORT).show();
                        SaveLocation(waybillnum, ordernum);
                        showNavi(position);

                    } else {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    dismissDialog();
                    LogUtils.i(e.toString());
                }
            }
        });
    }

    /**
     * 获取订单详情
     */
    private static void requestData() {
        showDialog();
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);
        String url = context.getResources().getString(R.string.server_url) + URLMap.CDSORDER_DETAIL;
        LogUtils.i(url + CommonTools.getQuryParams(params));
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        Log.d(TAG, "onStart: " + url + "USERNAME=" + sp.getString(UserEntity.PHONE, null) + "&PASSWORD=" + sp.getString(UserEntity.PASSWORD, null) + "&ORDER_NO=" +
                orderNo);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    JSONObject data = obj.getJSONObject("data");
                    dismissDialog();
                    LogUtils.i(data.toString());
                    if ("1".equals(status)) {
                        mOrderBean = getDeverList(data);
                        tvname.setText(judgeNull(data, "SHIPPER_USERAME", "个人货主"));
                        tvphone.setText(judgeNull(data, "SHIPPER_PHONE", ""));
                        tvOrderNumber.setText(judgeNull(data, "WAYBILL_NUMBER", ""));
                        user_headPhotouURL = judgeNull(data, "SHIPPER_HEADPHOTOURL", "");
                        tvSendTimeStart.setText(judgeNull(data, "LOADTIME", ""));
                        tvCarNo.setText(judgeNull(data, "CarNumber", "未指派理想车辆"));
                        // 提货开始时间
                        tvSendTimeEnd.setText(judgeNull(data, "SHIP_TIME_END", ""));
//                        totalCost.setText(judgeNull(data, "TOTAL_COST", "0.00"));
                        totalCost.setText(judgeNull(data, "DRIVERTOTALCOST", "0.00"));
                        weightNum.setText((judgeNull(data, "WEIGHT", "0") + judgeNull(data, "WEIGHT_UNIT", "吨")));
                        pieceNumber.setText((judgeNull(data, "NUMBER", "") + "件"));
                        tvVolume.setText(judgeNull(data, "VOLUME", "0") + judgeNull(data, "VOLUMN_UNIT", "方"));
//						distance.setText((CommonTools.judgeNull(data, "TRANSPORT_MILE", "") + "公里"));
                        if (!TextUtils.isEmpty((judgeNull(data, "REAL_TRANSPORT_COST", "")))) {
                            mileagePrices.setText((judgeNull(data, "REAL_TRANSPORT_COST", "") + "元"));
                        }
                        /*
                        tv_insurancecost.setText((CommonTools.judgeNull(data, "INSURANCECOST", "0.00")) + "元");
						tv_payment.setText((CommonTools.judgeNull(data, "PAYMENT", "0.00")) + "元");
						tv_invoicecost.setText((CommonTools.judgeNull(data, "INVOICECOST", "0.00")) + "元");
						tv_additionalfreight.setText((CommonTools.judgeNull(data, "ADDITIONALFREIGHT", "0.00")) + "元");
						tv_deliveryaddress.setText(CommonTools.judgeNull(data, "DELIVERYADDRESS", ""));
						tv_invocetitle.setText(CommonTools.judgeNull(data, "INVOICETITLE", ""));
						tv_delaytime.setText((CommonTools.judgeNull(data, "DELAYTIME", "0") + "分钟"));
						tv_delaytimecost.setText((CommonTools.judgeNull(data, "DELAYTIMECOST", "0") + "元"));
						// 显示回单签收
						if (CommonTools.judgeNull(data, "ISRECEIPT", "0").equals("1")) {
							tv_isreceipt.setVisibility(View.VISIBLE);
						}
						*/
                        tv_specialrequirement.setText(judgeNull(data, "REMARKS", ""));
                        //if (CommonTools.judgeNull(data, "DELAYTIMECOST", "0").equals("0")){
                        tv_delaytimecost.setVisibility(View.GONE);
                        //}
                        // 隐藏提货单地址
                        //if (TextUtils.isEmpty(CommonTools.judgeNull(data, "DELIVERYADDRESS", ""))) {
                        tv_content_deliveryaddress.setVisibility(View.GONE);
                        tv_deliveryaddress.setVisibility(View.GONE);
                        //}
                        // 隐藏发票抬头
                        //if (TextUtils.isEmpty(CommonTools.judgeNull(data, "INVOICETITLE", ""))) {
                        tv_content_invocetitle.setVisibility(View.GONE);
                        tv_invocetitle.setVisibility(View.GONE);
                        //}
                        // 隐藏特殊要求
                        if (TextUtils.isEmpty(judgeNull(data, "REMARKS", ""))) {
                            tv_content_specialrequirement.setVisibility(View.GONE);
                            tv_specialrequirement.setVisibility(View.GONE);
                        }
                        if (!TextUtils.isEmpty(user_headPhotouURL)) {
                            String url = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + user_headPhotouURL;
                            File file = new File(url);
                            if (file.exists()) {
                                Bitmap bitmap = BitmapFactory.decodeFile(file.toString());
                                if (null != bitmap) {
                                    userHeadImg.setImageBitmap(bitmap);
                                }
                            } else {
                                downLoadHeadImg(user_headPhotouURL);
                            }
                        }
                        ordercode = judgeNull(data, "STATUS", "");
                        ArroundCarFragment.mOrderStatus2 = ordercode;
                        JSONArray orderListArray = data.getJSONArray("orderList");
                        String commented = "";
                        for (int i = 0; i < orderListArray.length(); i++){
                            JSONObject object = orderListArray.getJSONObject(i);
                            commented = judgeNull(object, "shipper_is_commented", "0");
                            if ("1".equals(commented)) {
                                break;
                            }
                        }
                        if (OrderListAdapter.NO_LEAFLETS.equals(ordercode)) {// 未派单
                            tv_status.setText("未派单");
//                            tv_status.setText("已接单");
                        } else if (OrderListAdapter.NO_ORDER.equals(ordercode)) {// 未接单
                            tv_status.setText("未接单");
                        } else if (OrderListAdapter.ORDERED.equals(ordercode)) { // 已接单
                            tv_status.setText("已接单");
                        } else if (OrderListAdapter.PORTIONTAKEGOODS.equals(ordercode)) {//已部分提货
                            tv_status.setText("已部分提货");
                        } else if (OrderListAdapter.TAKEGOODS.equals(ordercode)) { //已提货
                            tv_status.setText("已提货");
                        } else if (OrderListAdapter.PORTIONARRIVED.equals(ordercode)) { // 已部分到达
                            tv_status.setText("已部分到达");
                        } else if (OrderListAdapter.ARRIVED.equals(ordercode)) { // 已确认到达
                            tv_status.setText("已确认到达");
                            context.sendBroadcast(new Intent("clearRountOverlay"));
                        } else if (OrderListAdapter.CALCULATED.equals(ordercode)) { // 已结算
                            tv_status.setText("已结算");

//                            context.sendBroadcast(new Intent("clearRountOverlay"));
                        } else if (OrderListAdapter.BEEN_CANCELED.equals(ordercode)) {// 订单取消
                            tv_status.setText("已取消");
                        } else if (OrderListAdapter.VENT.equals(ordercode)) { // 订单放空
                            tv_status.setText("放空");
                        } else if (OrderListAdapter.REVOKED.equals(ordercode)) {// 订单撤单
                            tv_status.setText("已撤单");
                        } else if (OrderListAdapter.INVALID.equals(ordercode)) { // 订单作废
                            tv_status.setText("已作废");
                        }
                        loding_time = judgeNull(data, "LOADTIME", "");
                        tv_loding_time.setText(loding_time);
                        remark = judgeNull(data, "REMARKS_SECTION", "");
                        if ("".equals(remark)) {
                            tv_remark.setText("无");
                        } else {
                            tv_remark.setText(remark);
                        }
                        isUrgent = judgeNull(data, "ISURGENT", "");
                        if ("1".equals(isUrgent)) {
                            mTvIsUrgent.setText("是");
                        } else {
                            mTvIsUrgent.setText("否");
                        }
                        mTvMileag.setText(judgeNull(data, "TRANSPORT", "") + "公里");
                        String ApprovalState = judgeNull(data, "REVIEWED_STATUS", "");
                        if ("1".equals(ApprovalState)) {
                            mTvApprovalState.setText("审批通过");
                            btnTakeOrder.setVisibility(View.GONE);
                            mLlApproval.setVisibility(View.VISIBLE);
                        } else if ("2".equals(ApprovalState)) {
                            mTvApprovalState.setText("审批未通过");
                            btnTakeOrder.setVisibility(View.VISIBLE);
                            mLlApproval.setVisibility(View.VISIBLE);
                        } else if ("0".equals(ApprovalState)) {
                            mTvApprovalState.setText("审批中");
                            btnTakeOrder.setVisibility(View.GONE);
                        }

                        mTvApprovalOpinion.setText(judgeNull(data, "REMARKS", ""));

                        JSONArray traceArray = data.getJSONArray("tracelist");
                        JSONArray jArray = data.getJSONArray("deliverList");
                        ArrayList<String> waybillStatusList = new ArrayList<String>();
                        String GetGoodsTime = "";
                        String receiptTime = "";
                        for (int a = 0; a < traceArray.length(); a++) {
                            JSONObject traceObject = traceArray.getJSONObject(a);
                            waybillStatusList.add(judgeNull(traceObject, "WAYBILL_STATUS", ""));
                            if ("2".equals(judgeNull(traceObject, "WAYBILL_STATUS", ""))) {
                                GetGoodsTime = CommonTools.judgeNull(traceObject, "OPERATE_TIME", "");
                            }
//                            2 + jArray.length();
                            if ("11".equals(judgeNull(traceObject, "WAYBILL_STATUS", ""))) {
                                receiptTime = CommonTools.judgeNull(traceObject, "OPERATE_TIME", "");

                            }
                            if (!"".equals(receiptTime)) {
                                mLlTotalTime.setVisibility(View.VISIBLE);
                                mTvTotalTime.setText(CommonTools.formatSeconds(CommonTools.compareTime(receiptTime, GetGoodsTime)));
                                break;
                            }
                        }

                        if (commented.equals("1")) { // 已评价
                            btnTakeOrder.setText(VIEW_EVALIATION);
                            btnTakeOrder.setVisibility(View.VISIBLE);
                        }
                        String orderNos = judgeNull(data, "ORDERNOS", "");
                        if (jArray.length() > 0) {
                            pList = new ArrayList<PointInfo>();

                            String takeGoodsTime = "";
                            for (int a = 0; a < traceArray.length(); a++) {
                                JSONObject traceObject = traceArray.getJSONObject(a);
                                if ("2".equals(judgeNull(traceObject, "WAYBILL_STATUS", ""))) {
                                    takeGoodsTime = CommonTools.judgeNull(traceObject, "OPERATE_TIME", "");
                                }
                            }

                            List<PointInfo> pListEnd = new ArrayList<PointInfo>();//订单的终点信息集合
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject jObject = jArray.getJSONObject(i);
                                PointInfo pointInfo = new PointInfo();

                                pointInfo.setProvince(judgeNull(jObject, "PROVINCE", ""));
                                pointInfo.setCity(judgeNull(jObject, "CITY", ""));
                                pointInfo.setDistrict(judgeNull(jObject, "DISTRICT", ""));
                                pointInfo.setDetailsAddress(judgeNull(jObject, "CONTACT_ADDRESS", ""));
                                pointInfo.setImgpath(judgeNull(jObject, "EVIDENCE_IMG", ""));
                                pointInfo.setLinkMan(judgeNull(jObject, "CONTACT_NAME", ""));
                                pointInfo.setLinkPhone(judgeNull(jObject, "CONTACT_MOBILE", ""));
                                pointInfo.setAlias(judgeNull(jObject, "ADDR_ALIAS", ""));
                                pointInfo.setType(CommonTools.intNull(jObject, "TYPE", 0));
                                pointInfo.setSort(CommonTools.intNull(jObject, "SORT", 0));
                                pointInfo.setIsFirst(CommonTools.intNull(jObject, "IS_FIRST", 0));
                                pointInfo.setIsLast(CommonTools.intNull(jObject, "IS_LAST", 0));
                                pointInfo.setPointStatus(CommonTools.intNull(jObject, "POINT_STATUS", 0));
                                pointInfo.setDeliverId(judgeNull(jObject, "DELIVER_ID", ""));
                                pointInfo.setWaybillnum(judgeNull(jObject, "WAYBILL_NUMBER", ""));//运单号
                                sWaybillnum = judgeNull(jObject, "WAYBILL_NUMBER", "");
                                pointInfo.setOrderno(judgeNull(jObject, "ORDER_NO", ""));//订单号
                                String Orderno = judgeNull(jObject, "ORDER_NO", "");
                                pointInfo.setWaybillid(judgeNull(data, "WAYBILL_ID", ""));//

                                for (int a = 0; a < traceArray.length(); a++) {
                                    JSONObject traceObject = traceArray.getJSONObject(a);
                                    if (i == 0) {
                                        if ("7".equals(judgeNull(traceObject, "WAYBILL_STATUS", ""))) {
                                            pointInfo.setOperatetime(judgeNull(traceObject, "OPERATE_TIME", ""));
                                            break;
                                        }
                                    } else {
                                        if ("9".equals(judgeNull(traceObject, "WAYBILL_STATUS", "")) &&
                                                Orderno.equals(judgeNull(traceObject, "ORDER_NO", ""))) {
                                            pointInfo.setOperatetime(judgeNull(traceObject, "OPERATE_TIME", ""));
                                            break;
                                        }
                                        if ("11".equals(judgeNull(traceObject, "WAYBILL_STATUS", "")) &&
                                                Orderno.equals(judgeNull(traceObject, "ORDER_NO", ""))) {
                                            pointInfo.setOperatetime(judgeNull(traceObject, "OPERATE_TIME", ""));
                                            break;
                                        }
                                    }
                                    pointInfo.setWaybillstatus(judgeNull(traceObject, "WAYBILL_STATUS", ""));
                                }

                                for (int k=0;k<orderListArray.length();k++){
                                    JSONObject orderObject = orderListArray.getJSONObject(k);
                                    if (Orderno.equals(judgeNull(orderObject, "order_no", ""))){
                                        pointInfo.setPayway(judgeNull(orderObject, "pay_way", ""));
                                        pointInfo.setTotalcost(judgeNull(orderObject, "total_cost", ""));
                                        pointInfo.setRemarks(judgeNull(orderObject, "remarks", ""));
                                        pointInfo.setTransportway(judgeNull(orderObject, "TRANSPORT_WAY", ""));
                                        pointInfo.setGoodsname(judgeNull(orderObject, "GOODS_NAME", ""));
                                        String goodstype = judgeNull(orderObject, "GOODS_TYPE", "");
                                        String goodstypeSecond = judgeNull(orderObject, "GOODS_TYPE_SECOND", "");
                                        String goodstypeThird = judgeNull(orderObject, "GOODS_TYPE_THIRD", "");
                                        pointInfo.setGoodstype(goodstype);
                                        pointInfo.setGoodsweight(judgeNull(orderObject, "WEIGHT", ""));
                                        pointInfo.setWeightunit(judgeNull(orderObject, "WEIGHT_UNIT", "吨"));
                                        pointInfo.setGoodsvolume(judgeNull(orderObject, "VOLUMN", ""));
                                        pointInfo.setVolumeunit(judgeNull(orderObject, "VOLUMN_UNIT", "方"));
                                        pointInfo.setWorth(judgeNull(orderObject, "WORTH", "0"));
                                        pointInfo.setPieceno(judgeNull(orderObject, "PIECE_NO", ""));
                                        pointInfo.setPackagetype(judgeNull(orderObject, "PACKAGE_TYPE", "其他"));

                                        pointInfo.setIsInsuredtransport(judgeNull(orderObject, "is_insuredtransport", ""));
                                        pointInfo.setInsuredtransportCost(judgeNull(orderObject, "insuredtransport_cost", ""));
                                        pointInfo.setIsPaymentcollection(judgeNull(orderObject, "is_paymentcollection", ""));
                                        pointInfo.setPaymentcollectionCost(judgeNull(orderObject, "paymentcollection_cost", ""));
                                        pointInfo.setIsDelivery(judgeNull(orderObject, "is_delivery", ""));
                                        pointInfo.setDeliveryCost(judgeNull(orderObject, "delivery_cost", ""));
                                        pointInfo.setIsUnloading(judgeNull(orderObject, "is_unloading", ""));
                                        pointInfo.setIsUpstairs(judgeNull(orderObject, "is_upstairs", ""));
                                        pointInfo.setIsLoading(judgeNull(orderObject, "is_loading", ""));

                                    }
                                }

                                //IsFirst()  是否为起点，等于1为起点
                                if (pointInfo.getIsFirst() == 1) {
                                    pList.add(pointInfo);
                                } else {
                                    pListEnd.add(pointInfo);
                                }
                            }
                            pList.addAll(pListEnd);

                            adapter = new OrderPointAdapter(context, pList, takeGoodsTime, orderNos, ApprovalState, mOrderBean, waybillStatusList, sWaybillnum);
                            listView.setAdapter(adapter);
                            listHeight();
                        }
                    } else {
                        Toast.makeText(context, "获取信息失败！", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    LogUtils.e(e.toString());
                    dismissDialog();
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                CommonTools.failedToast(context);
                dismissDialog();
            }
        });
    }


    /**
     * 获取所有的配送点
     */
    static TstOrderBean getDeverList(JSONObject data) {
        TstOrderBean orderBean = new TstOrderBean();
        orderBean.setORDER_STATUS(judgeNull(data, "ORDER_STATUS", ""));
        orderBean.setORDERNO(judgeNull(data, "ORDER_NO", ""));
        List<DeliverBean> deliverList = new ArrayList<>();
        JSONArray jArray = null;
        try {
            jArray = data.getJSONArray("deliverList");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (jArray == null) {
            Toast.makeText(context, "没有找到起点或终点！", Toast.LENGTH_SHORT).show();
            return null;
        }
        for (int i = 0; i < jArray.length(); i++) {
            DeliverBean deliver = new DeliverBean();
            JSONObject jObject = null;
            try {
                jObject = jArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (jObject == null) {
                Toast.makeText(context, "没有找到起点或终点！", Toast.LENGTH_SHORT).show();
                return null;
            }
            deliver.setPROVINCE(judgeNull(jObject, "PROVINCE", ""));
            deliver.setCITY(judgeNull(jObject, "CITY", ""));
            deliver.setDISTRICT(judgeNull(jObject, "DISTRICT", ""));
            deliver.setCONTACT_ADDRESS(judgeNull(jObject, "CONTACT_ADDRESS", ""));
            deliver.setLATITUDE(judgeNull(jObject, "LATITUDE", ""));
            deliver.setLONGITUDE(judgeNull(jObject, "LONGITUDE", ""));
            deliverList.add(deliver);
        }
        orderBean.setDeliverList(deliverList);
        return orderBean;
    }

    /**
     * 下载头像并保存
     */
    protected static void downLoadHeadImg(final String imageName) {
        if (TextUtils.isEmpty(user_headPhotouURL)) {
            return;
        }
        String url = context.getResources().getString(R.string.server_imgurl) + "upload/" + user_headPhotouURL;
        //String url = context.getResources().getString(R.string.server_url) + "upload/" + user_headPhotouURL;
        final String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + user_headPhotouURL;
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.download(url, imagePath, true, true, new RequestCallBack<File>() {

            @Override
            public void onSuccess(ResponseInfo<File> arg0) {
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                if (null != bitmap) {
                    userHeadImg.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("下载用户头像失败：" + arg0.getMessage() + arg1);
            }
        });
    }

   /* *//**
     * 显示选择的图片
     *//*
    public static void setImageInfo() {
        File file = new File(ImageUtils.picPath);
        if (!(file.length() > 0)){
            Toast.makeText(context, context.getResources().getString(R.string.image_no_legitimacy), Toast.LENGTH_SHORT).show();
            return;
        }

        PointInfo pInfo = pList.get(checkPoint);
        pInfo.setPicPath(ImageUtils.picPath);
        try {

            ImageView btnImg = (ImageView)listView.getChildAt(checkPoint).findViewById(R.id.btnImg);

            btnImg.setImageBitmap(BitmapFactory.decodeStream(context.getContentResolver()
                    .openInputStream(Uri.parse("file://" + "" + pInfo.getPicPath()))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        adapter.notifyDataSetChanged();
        listHeight();
        try {
            Log.i("TAG", Uri.parse("file://" + "/" + ImageUtils.picPath).toString());
            ivReceiptimg.setImageBitmap(BitmapFactory.decodeStream(context.getContentResolver()
                    .openInputStream(Uri.parse("file://" + "/" + ImageUtils.picPath))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }*/
    /**
     * 显示选择的图片
     */
    public static void setImageInfo(Uri uri) {
        File file = new File(ImageUtils.getPath(context, uri));
        Luban.with(context).load(file).ignoreBy(100).
                setTargetDir(ImageUtils.getCompressImageFolder(orderDetailActivity)).
                setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                        mDialog.show();
                    }

                    @Override
                    public void onSuccess(File file) {
                        // TODO 压缩成功后调用，返回压缩后的图片文件
                        mDialog.dismiss();
                        PointInfo pInfo = pList.get(checkPoint);
                        pInfo.setPicPath(file.getPath());
                        Log.d(TAG, "photo1: " + file.getPath());
                        ImageView btnImg = (ImageView) listView.getChildAt(checkPoint).findViewById(R.id.btnImg);
                        Glide.with(context).load(file).into(btnImg);
                        listHeight();
//                        mImgFile = file;s
                    }

                    @Override
                    public void onError(Throwable e) {
                        // TODO 当压缩过程出现问题时调用
                        e.printStackTrace();
                        mDialog.dismiss();
                        Toast.makeText(context, "处理失败，请重新选择图片!", Toast.LENGTH_SHORT).show();
                    }
                }).launch();
    }
    @Override
    public void uploadImage(String orderNo) {
        if (orderNo == null || orderNo.equals("")) {
            Toast.makeText(getActivity(), "您还没有选择运单！", Toast.LENGTH_SHORT).show();
            return;
        }
        this.orderNo = orderNo;
        requestData();
    }

    private static void showNavi(final int position) {
        Constant.LastPosition = position+1;

        wiDialogwindow = new AlertDeleteDialogwindow((Activity) context, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d("正式开启导航");

                Intent intent = new Intent("StartNavigation");  //Itent就是我们要发送的内容
                intent.putExtra("RountIndex", position+1);
                context.sendBroadcast(intent);   //发送广播

                wiDialogwindow.dismiss();
            }
        }, "是否开启导航？", "开启导航");
        wiDialogwindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        wiDialogwindow.showAtLocation(listView, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }
    /**
     * 上传位置
     * @param waybillnum 运单号
     * @param ordernum   订单号
     */
    public static void SaveLocation(String waybillnum, String ordernum) {
        Intent intent = new Intent("SaveLocation");  //Itent就是我们要发送的内容
        intent.putExtra("WAYBILLNUMBER", waybillnum);
        intent.putExtra("ORDERNUMBER", ordernum);
        context.sendBroadcast(intent);   //发送广播
    }
//    /**
//     * 上传位置
//     * @param waybillnum 运单号
//     * @param ordernum   订单号
//     */
//    public static void reportLocation(String waybillnum, String ordernum) {
//        BDLocation location = new BDLocation();
//        double latitude = Double.parseDouble(sp.getString(Constants.LATITUDE, "0"));
//        double longtitude = Double.parseDouble(sp.getString(Constants.LONGTITUDE, "0"));
//        LatLng oldLoc = new LatLng(latitude, longtitude);
//        LatLng currentLoc = new LatLng(location.getLatitude(), location.getLongitude());
//        double distance = DistanceUtil.getDistance(oldLoc, currentLoc);
//
//        RequestParams params = new RequestParams();
//        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
//        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
//        params.addQueryStringParameter("CARID", sp.getString(UserEntity.CAR_NUMBER, null));
//        params.addQueryStringParameter("LNG", String.valueOf(longtitude)); // 经度
//        params.addQueryStringParameter("LAT", String.valueOf(latitude)); // 纬度
//        params.addQueryStringParameter("LASTLOCATIONDISTANCE", String.valueOf(distance));
//        params.addQueryStringParameter("WAYBILLNUMBER", waybillnum);
//        params.addQueryStringParameter("ORDERNUMBER", ordernum);
//
//        String url = context.getString(R.string.server_url) + Constants.REPORT_LOCATION;
//        LogUtils.i(url + CommonTools.getQuryParams(params));
//
//        HttpUtils http = new HttpUtils(30 * 1000);
//        http.configCurrentHttpCacheExpiry(1000 * 10);
//        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
//            @Override
//            public void onSuccess(ResponseInfo<String> responseInfo) {
//                try {
//                    JSONObject obj = new JSONObject(responseInfo.result);
//                    LogUtils.i(obj.toString());
//                    String status = obj.getString("state");
//                    String message = obj.getString("message");
//                    if ("1".equals(status)) {
//                        message = context.getString(R.string.report_location_success);
//                    }
//                    LogUtils.i(message);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(HttpException e, String s) {
//                LogUtils.i(context.getString(R.string.request_failed));
//            }
//        });


    }







    /**
     * 上传图片
     */
    /*
    private void doUpload() {
		dialog.show();
		final RequestParams params = new RequestParams();
		File file = new File(ImageUtils.picPath);
		if (file.exists()) {
			params.addBodyParameter("Filedata", file);
			String url = context.getString(R.string.server_url) + URLMap.UPLOAD_IMAGE;
			
			LogUtils.i(url + CommonTools.getParams(params));

			HttpUtils http = new HttpUtils(60 * 1000);
			http.configCurrentHttpCacheExpiry(1000 * 10);
			http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
				@Override
				public void onSuccess(ResponseInfo<String> result) {
					try {
						LogUtils.i("result= " + result.result);
						JSONObject obj = new JSONObject(result.result);
						String message = obj.getString("message");
						String status = obj.getString("status");
						if ("1".equals(status)) {
							JSONArray datas = obj.getJSONArray("data");
							String imgFileName = null;
							for (int i = 0; i < datas.length(); i++) {
								JSONObject data = datas.getJSONObject(i);
								imgFileName = CommonTools.judgeNull(data, "FILENAME", "");
							}
							doTakeGoods(imgFileName);
						} else if ("0".equals(status)) {
							Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
						} else if ("-1".equals(status)) {
							Toast.makeText(context, "服务器异常", Toast.LENGTH_SHORT).show();
						}
					} catch (JSONException e) {
						Toast.makeText(context, "图片上传失败！", Toast.LENGTH_SHORT).show();
						e.printStackTrace();
						dialog.dismiss();
					}
				}

				@Override
				public void onFailure(HttpException arg0, String arg1) {
					CommonTools.failedToast(context);
					dialog.dismiss();
				}
			});
		}
	}
	*/

//}
