package cn.com.dkl.logistics.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.ImageChooseDialogWindow;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;
import cn.com.dkl.logistics.utils.ImageUtils;
import cn.com.dkl.logistics.view.CustomProgressDialog;
import cn.com.dkl.logistics.view.IconButton;

@SuppressLint("NewApi")
public class ReceiptVoucherFragment extends Fragment {
    @ViewInject(R.id.receiptimg)
    private static ImageView receiptimg;

    @ViewInject(R.id.btn_uploadreceiptimg)
    private static Button btn_uploadreceiptimg;

    @ViewInject(R.id.txUploadTime)
    private static TextView txUploadTime;

    @ViewInject(R.id.btn_selectImg)
    private static IconButton btn_selectImg;

    @ViewInject(R.id.tvValiDate)
    private TextView tvValiDate;

    @ViewInject(R.id.llValiDate)
    private LinearLayout llValiDate;

    private static Context context;
    private static SharedPreferences sp;
    private String receiptImageName;
    private String orderStatus;
    private String receiptUploadTime;
    private static String orderNo;
    private static CustomProgressDialog dialog = null;
    private static CustomProgressDialog dialogTips = null;
    private static String valiDate;
    private static Activity activity;

    private ImageChooseDialogWindow menuWindow;

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_receipt_voucher, null);
        context = getActivity();
        ViewUtils.inject(this, v);
        init();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        activity = getActivity();
        orderNo = getActivity().getIntent().getExtras().getString("OrderNo");
        dialog = new CommonTools(context).getProgressDialog(context, "上传回单中...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialogTips = new CommonTools(context).getProgressDialog(context, "加载中...");
        getReceitImageName();
    }

    @OnClick({R.id.btn_uploadreceiptimg, R.id.btn_selectImg})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_selectImg:
                menuWindow = new ImageChooseDialogWindow(getActivity());
                menuWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                menuWindow.showAtLocation(getActivity().findViewById(R.id.orderLinear), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                break;
            case R.id.btn_uploadreceiptimg:
                valiDate = tvValiDate.getText().toString().trim();
                if (TextUtils.isEmpty(valiDate)) {
                    Toast.makeText(context, "请输入服务码!", Toast.LENGTH_SHORT).show();
                } else if (valiDate.length() != 6) {
                    Toast.makeText(context, "请输入6位数的服务码!", Toast.LENGTH_SHORT).show();
                } else if (ImageUtils.picPath == null) {
                    Toast.makeText(context, "请选择图片!", Toast.LENGTH_SHORT).show();
                } else {
                    doUpload();
                    btn_uploadreceiptimg.setClickable(false);
                }
                break;
            default:
                break;
        }
    }

    public static void setImageInfo() {
        File file = new File(ImageUtils.picPath);
        if (!(file.length() > 0)){
            Toast.makeText(context, context.getResources().getString(R.string.image_no_legitimacy), Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            Log.i("TAG", Uri.parse("file://" + "/" + ImageUtils.picPath).toString());
            receiptimg
                    .setImageBitmap(BitmapFactory.decodeStream(context.getContentResolver().openInputStream(Uri.parse("file://" + "/" + ImageUtils.picPath))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void doUpload() {
        dialog.show();
        final RequestParams params = new RequestParams();
        File file = new File(ImageUtils.picPath);
        if (file.exists()) {
            params.addBodyParameter("Filedata", file);
            String url = context.getString(R.string.server_url) + URLMap.UPLOAD_IMAGE;

            HttpUtils http = new HttpUtils(60 * 1000);
            http.configCurrentHttpCacheExpiry(1000 * 10);
            http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {
                @Override
                public void onSuccess(ResponseInfo<String> result) {
                    try {
                        LogUtils.i("result= " + result.result);
                        JSONObject obj = new JSONObject(result.result);
                        String message = obj.getString("message");
                        String status = obj.getString("status");
                        if ("1".equals(status)) {
                            JSONArray datas = obj.getJSONArray("data");
                            String imgFileName = null;
                            for (int i = 0; i < datas.length(); i++) {
                                JSONObject data = datas.getJSONObject(i);
                                imgFileName = CommonTools.judgeNull(data, "FILENAME", "");
                            }
                            sendRequest(imgFileName);
                        } else if ("0".equals(status)) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            btn_uploadreceiptimg.setClickable(true);
                        } else if ("-1".equals(status)) {
                            Toast.makeText(context, "服务器异常", Toast.LENGTH_SHORT).show();
                            btn_uploadreceiptimg.setClickable(true);
                        }
                    } catch (JSONException e) {
                        Toast.makeText(context, "图片上传失败！", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        btn_uploadreceiptimg.setClickable(true);
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(HttpException arg0, String arg1) {
                    CommonTools.failedToast(context);
                    //Toast.makeText(context, "图片上传失败！", Toast.LENGTH_SHORT).show();
                    btn_uploadreceiptimg.setClickable(true);
                    dialog.dismiss();
                }
            });
        }
    }

    private static void sendRequest(String imgFileName) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDERNO", orderNo);
        params.addQueryStringParameter("RECEIPTIMG", imgFileName);
        params.addQueryStringParameter("VALIDATE", valiDate);
        params.addQueryStringParameter("ORDERID", orderNo);

        String url = context.getString(R.string.server_url) + URLMap.UPLOAD_RECEIPTIMG;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String result) {
                CommonTools.failedToast(context);
                dialog.dismiss();
                btn_uploadreceiptimg.setClickable(true);
            }

            @Override
            public void onSuccess(ResponseInfo<String> result) {
                try {
                    JSONObject obj = new JSONObject(result.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        ImageUtils.picPath = null;
                        Toast.makeText(context, "上传回单成功！", Toast.LENGTH_SHORT).show();
                        txUploadTime.setText("");
                        activity.finish();
                    } else if ("0".equals(status)) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else if ("-1".equals(status)) {
                        Toast.makeText(context, "服务器异常！", Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                    btn_uploadreceiptimg.setClickable(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    btn_uploadreceiptimg.setClickable(true);
                }

            }
        });
        LogUtils.i(http.toString());
    }

    private void downLoadHeadImg(String imageName) {
        if (TextUtils.isEmpty(receiptImageName)) {
            return;
        }
        String url = getString(R.string.server_imgurl) + "upload/" + receiptImageName;
        //String url = getString(R.string.server_url) + "upload/" + receiptImageName;
        LogUtils.i(url);
        final String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + receiptImageName;
        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.download(url, imagePath, true, true, new RequestCallBack<File>() { // true1代表允许断点续传，true2//
            // 如果从请求返回信息中获取到文件名，下载完成后自动重命名。

            @Override
            public void onSuccess(ResponseInfo<File> arg0) {
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                if (null != bitmap) {
                    receiptimg.setImageBitmap(bitmap);
                }
            }

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i("下载图片失败：" + arg0.getMessage() + arg1);
            }
        });

    }

    public void getReceitImageName() {
        dialogTips.show();
        btn_uploadreceiptimg.setClickable(true);
        btn_selectImg.setClickable(true);
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("ORDER_NO", orderNo);

        String url = getString(R.string.server_url) + URLMap.CDSORDER_DETAIL;

        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(60 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            @Override
            public void onFailure(HttpException arg0, String arg1) {
                Toast.makeText(context, "获取图片失败！", Toast.LENGTH_SHORT).show();
                dialogTips.dismiss();
            }

            @Override
            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    JSONObject data = obj.getJSONObject("data");

                    if (status.equals("1")) {
                        LogUtils.i(data.toString());
                        receiptImageName = CommonTools.judgeNull(data, "RECEIPTIMG", "");
                        orderStatus = CommonTools.judgeNull(data, "ORDERSTATUS", "");
                        receiptUploadTime = CommonTools.judgeNull(data, "RECEIPTIMGTIME", "");
                        if (!TextUtils.isEmpty(receiptImageName)) {
                            LogUtils.i(receiptImageName);
                            txUploadTime.setText(receiptUploadTime);
                            String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + receiptImageName;
                            File file = new File(imagePath);
                            if (file.exists()) {
                                Bitmap bitmap = BitmapFactory.decodeFile(file.toString());
                                if (null != bitmap) {
                                    receiptimg.setImageBitmap(bitmap);
                                }
                            } else {
                                downLoadHeadImg(receiptImageName);
                            }
                        }
                        if (orderStatus.equals("6")) {
                            Toast.makeText(context, "已确认收货，不能更改回单！", Toast.LENGTH_SHORT).show();
                            llValiDate.setVisibility(View.GONE);
                            btn_uploadreceiptimg.setClickable(false);
                            btn_selectImg.setClickable(false);
                        } else if (orderStatus.equals("7")) {
                            llValiDate.setVisibility(View.GONE);
                            btn_uploadreceiptimg.setClickable(false);
                            btn_selectImg.setClickable(false);
                        } else if (orderStatus.equals("-1")) {
                            Toast.makeText(context, "订单已取消，不能上传回单！", Toast.LENGTH_SHORT).show();
                            llValiDate.setVisibility(View.GONE);
                            btn_uploadreceiptimg.setClickable(false);
                            btn_selectImg.setClickable(false);
                        } else if (orderStatus.equals("-3")) {
                            Toast.makeText(context, "订单已放空，不能上传回单！", Toast.LENGTH_SHORT).show();
                            llValiDate.setVisibility(View.GONE);
                            btn_uploadreceiptimg.setClickable(false);
                            btn_selectImg.setClickable(false);
                        } else if (!orderStatus.equals("5")) {
                            Toast.makeText(context, R.string.receipt_voucher_warning, Toast.LENGTH_SHORT).show();
                            btn_uploadreceiptimg.setClickable(false);
                            btn_selectImg.setClickable(false);
                        }

                    } else {
                        Toast.makeText(context, "获取图片失败！", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    dialogTips.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogTips.dismiss();
                }
            }
        });

    }
}
