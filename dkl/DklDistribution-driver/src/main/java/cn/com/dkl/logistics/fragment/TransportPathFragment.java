package cn.com.dkl.logistics.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMapClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.overlayutil.DrivingRouteOverlay;
import com.baidu.mapapi.overlayutil.OverlayManager;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.BikingRouteResult;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption.DrivingPolicy;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;

/*import com.baidu.mapapi.search.route.BikingRouteResult;*/
/*import com.baidu.mapapi.search.route.IndoorRouteResult;
import com.baidu.mapapi.search.route.MassTransitRouteResult;*/

public class TransportPathFragment extends Fragment {
	@ViewInject(R.id.mapView)
	private MapView mapView;
	@ViewInject(R.id.tvTravelTime)
	private TextView tvTravelTime;
	@ViewInject(R.id.tvTravelKilometre)
	private TextView tvTravelKilometre;

	private Context context;
	private BaiduMap baiduMap;
	private SharedPreferences sp;
	private String orderNo;

	private MapStatusUpdate myUpdata;
	private BitmapDescriptor bdStart = BitmapDescriptorFactory.fromResource(R.drawable.icon_track_navi_end);
	private BitmapDescriptor bdEnd = BitmapDescriptorFactory.fromResource(R.drawable.icon_track_navi_start);
	private BitmapDescriptor bdNormal = BitmapDescriptorFactory.fromResource(R.drawable.pin_taxi);
	private BitmapDescriptor bdNull = BitmapDescriptorFactory.fromResource(R.drawable.bdnull);

	private String status = "3";
	private boolean isFirstLocation = true;
	private boolean isShow = false;
	private double distance;
	private String nowDate;
	private String startDate;
	private LatLng endLatLng;
	private OverlayOptions overlayOptions;
	private LatLng startLatLng;
	private OverlayOptions startOverlayOptions;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_transport_path, null);
		context = getActivity();
		ViewUtils.inject(this, v);
		init();
		return v;
	}

	private RoutePlanSearch search;
	// 驾车路线
	private DrivingRouteLine route;
	/*private LocationClient locClient;*/

	private void init() {
		sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
		orderNo = getActivity().getIntent().getExtras().getString("OrderNo");

		initBaiduMap();

		sendRequest();
	}

	private void initBaiduMap() {
		baiduMap = mapView.getMap();
		// 开启定位图层
		baiduMap.setMyLocationEnabled(true);
		/*locClient = new LocationClient(context);
		locClient.registerLocationListener(new MyLocationListenner());
		locClient.start();*/
		// 创建路径规划
		search = RoutePlanSearch.newInstance();
		search.setOnGetRoutePlanResultListener(new OnGetRoutePlanResultListener() {

			@Override
			public void onGetWalkingRouteResult(WalkingRouteResult arg0) {
			}

			@Override
			public void onGetTransitRouteResult(TransitRouteResult arg0) {
			}

			@Override
			public void onGetDrivingRouteResult(DrivingRouteResult arg0) {
				isShow = true;
				if (arg0.error == SearchResult.ERRORNO.NO_ERROR) {
					route = arg0.getRouteLines().get(0);
					distance = Math.ceil((route.getDistance() / 1000.0) * 100) / 100;
					tvTravelKilometre.setText(distance + "KM");
					DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(baiduMap);
					baiduMap.setOnMarkerClickListener(overlay);
					
					overlay.setData(arg0.getRouteLines().get(0));
					overlay.addToMap();
					//overlay.zoomToSpan();
//					for (MarkerOptions marker : markerList) {
//						baiduMap.addOverlay(marker);
//					}
				} else if (arg0.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) {
					Toast.makeText(context, "路径规划失败！", Toast.LENGTH_SHORT).show();
				}
			}

			/*@Override
			public void onGetMassTransitRouteResult(MassTransitRouteResult massTransitRouteResult) {

			}

			@Override
			public void onGetIndoorRouteResult(IndoorRouteResult indoorRouteResult) {

			}
*/
			@Override
			public void onGetBikingRouteResult(BikingRouteResult bikingRouteResult) {

			}
		});
		// 地图点击
		baiduMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public boolean onMapPoiClick(MapPoi arg0) {
				baiduMap.hideInfoWindow();
				return false;
			}

			@Override
			public void onMapClick(LatLng arg0) {
				baiduMap.hideInfoWindow();
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		mapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		mapView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	private void sendRequest() {
		RequestParams params = new RequestParams();
		params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, ""));
		params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, ""));
		params.addQueryStringParameter("ORDERNO", orderNo);

		String url = getString(R.string.server_url) + URLMap.ORDER_TRACK;

		LogUtils.i(url + CommonTools.getQuryParams(params));

		HttpUtils http = new HttpUtils(60 * 1000);
		http.configCurrentHttpCacheExpiry(1000 * 10);
		http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				CommonTools.failedToast(context);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				try {
					JSONObject obj = new JSONObject(arg0.result);

					LogUtils.i(obj.toString());

					String state = obj.getString("state");
					String message = obj.getString("message");
					List<LatLng> listPoint = new ArrayList<LatLng>();
					if ("1".equals(state)) {
						JSONObject data = obj.getJSONObject("data");

						JSONObject location = data.getJSONObject("orderLocation");
						String fromLatitude = CommonTools.judgeNull(location, "FROMLATITUDE", "");
						String fromLongitude = CommonTools.judgeNull(location, "FROMLONGITUDE", "");
						String toLatitude = CommonTools.judgeNull(location, "TOLATITUDE", "");
						String toLongitude = CommonTools.judgeNull(location, "TOLONGITUDE", "");
						if (!TextUtils.isEmpty(fromLatitude)) {
							listPoint.add(new LatLng(Double.valueOf(fromLatitude), Double.valueOf(fromLongitude)));
							startLatLng = new LatLng(Double.valueOf(fromLatitude), Double.valueOf(fromLongitude));
							startOverlayOptions = new MarkerOptions().position(startLatLng).icon(bdStart).zIndex(5);
						}
						JSONArray orderTrades = data.getJSONArray("orderTrades");
						for (int i = orderTrades.length()-1; i >= 0; i--) {
							JSONObject orderInfo = orderTrades.getJSONObject(i);
							nowDate = orderTrades.getJSONObject(0).getString("LOCATIONTIME");
							startDate = orderTrades.getJSONObject(orderTrades.length() - 1).getString("LOCATIONTIME");
							listPoint.add(new LatLng(orderInfo.getDouble("LAT"), orderInfo.getDouble("LNG")));
							LogUtils.i("LAT =" + orderInfo.getDouble("LAT")+"LNG = "+orderInfo.getDouble("LNG"));
						}
						if (!TextUtils.isEmpty(toLatitude)) {
							//listPoint.add(new LatLng(Double.valueOf(toLatitude), Double.valueOf(toLongitude)));
							endLatLng = new LatLng(Double.valueOf(toLatitude), Double.valueOf(toLongitude));
							overlayOptions = new MarkerOptions().position(endLatLng).icon(bdEnd).zIndex(5);
						}
						tvTravelTime.setText(CommonTools.RegulationTiming(nowDate, startDate));
						baiduMap.addOverlay(overlayOptions);
						baiduMap.addOverlay(startOverlayOptions);
						List<OverlayOptions> li = new ArrayList<OverlayOptions>();
						li.add(overlayOptions);
						li.add(startOverlayOptions);
						LiOverlayManager manager = new LiOverlayManager(baiduMap);
						baiduMap.setOnMarkerClickListener(manager);
						manager.setData(li);
						manager.addToMap();
						manager.zoomToSpan();
						addTravalPath(listPoint);
					} else {
						Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
						LogUtils.i(message);
					}
				} catch (JSONException e) {
					LogUtils.e(e.toString());
				}
			}
		});
	}

	private List<MarkerOptions> markerList = new ArrayList<MarkerOptions>();

	private void addTravalPath(final List<LatLng> listPoint) {
		final List<PlanNode> nodeList = new ArrayList<PlanNode>();
		for (LatLng latLng : listPoint) {
			nodeList.add(PlanNode.withLocation(latLng));
			markerList.add(new MarkerOptions().position(latLng).icon(bdNormal).draggable(false).title(latLng.toString()));
		}

		new Thread(){
			public void run() {
				search.drivingSearch(new DrivingRoutePlanOption().from(PlanNode.withLocation(listPoint.get(0))).passBy(nodeList)
						.to(PlanNode.withLocation(listPoint.get(listPoint.size() - 1))).policy(DrivingPolicy.ECAR_DIS_FIRST)); // 距离优先
			};
		}.start();

	}

	private class MyDrivingRouteOverlay extends DrivingRouteOverlay {

		public MyDrivingRouteOverlay(BaiduMap baiduMap) {
			super(baiduMap);
		}

		@Override
		public BitmapDescriptor getStartMarker() {
			return bdNull;
		}

		@Override
		public BitmapDescriptor getTerminalMarker() {
			return bdNull;
		}

	}

	/*public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (null != location) {
				locClient.stop();
				MyLocationData locData = new MyLocationData.Builder().accuracy(location.getRadius()).direction(100).latitude(location.getLatitude())
						.longitude(location.getLongitude()).build();
				baiduMap.setMyLocationData(locData);
				if (isFirstLocation) {
					isFirstLocation = false;
					if (!isShow) { // 路线未显示时，更改地图位置到定位位置
						//baiduMap.animateMapStatus(MapStatusUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
					}
				}
			}
		}

	}*/
	
	
	private class LiOverlayManager extends OverlayManager {
	    private List<OverlayOptions> optionsList = new ArrayList<OverlayOptions>();

	    public LiOverlayManager(BaiduMap baiduMap) {
	        super(baiduMap);
	    }

	    @Override
	    public List<OverlayOptions> getOverlayOptions() {
	        return optionsList;
	    }

	    @Override
	    public boolean onMarkerClick(Marker marker) {
	        return false;
	    }

	    public void setData(List<OverlayOptions> optionsList) {
	        this.optionsList = optionsList;
	    }

		@Override
		public boolean onPolylineClick(Polyline arg0) {
			return false;
		}
	}

}
