package cn.com.dkl.logistics.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Binder;
import android.os.IBinder;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;
import cn.com.dkl.logistics.utils.CommonTools;


public class LocationService extends Service {
    private static final String TAG = "LocationService";
    private Context context = LocationService.this;
    public LocationClient mLocationClient;
    public MyLocationListener mMyLocationListener;
    private LocationInfo locInfo;
    private SharedPreferences sp;
    /* 上传位置距离间隔(米) */
    private double distance = 0;
    private String mOrdernum = "";
    private String mWaybillnum = "";
    private boolean saveLocal = false;

    static long index = 0;

    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.i("onBind...");
        return locInfo;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtils.i("onCreate...");
        sp = getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        // 初始化定位
        initLocation();
        // 开启定位
        mLocationClient.start();
        IntentFilter filter = new IntentFilter();
        filter.addAction("SaveLocation");
        context.registerReceiver(receiver, filter);
        locInfo = new LocationInfo();
    }

    public BroadcastReceiver receiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("SaveLocation")){
                mWaybillnum = intent.getStringExtra("WAYBILLNUMBER");
                mOrdernum = intent.getStringExtra("ORDERNUMBER");
                saveLocal = true;
                mMyLocationListener = new MyLocationListener();
                mLocationClient.registerLocationListener(mMyLocationListener);
            }
        }
    };
//    @Override
//    public void onStart(Intent intent, int startId) {
//        // 再次动态注册广播
//        IntentFilter localIntentFilter = new IntentFilter("android.intent.action.USER_PRESENT");
//        localIntentFilter.setPriority(Integer.MAX_VALUE);// 整形最大值
//        myReceiver searchReceiver = new myReceiver();
//        registerReceiver(searchReceiver, localIntentFilter);
//        super.onStart(intent, startId);
//    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.i("onStartCommand...");
//        Intent notificationIntent = new Intent(this, MainActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//        Notification noti = new Notification.Builder(this)
//                .setContentTitle("大可龙")
//                .setContentText("上传位置信息中")
//                .setSmallIcon(R.drawable.ic_launcher)
//                .setContentIntent(pendingIntent)
//                .build();
//        startForeground(12346, noti);
//        flags = START_STICKY;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) { // 只有返回true，再绑定时，onRebind()才会被回调
        LogUtils.i("onUnbind...");
        mLocationClient.start();
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        LogUtils.i("onRebind...");
    }

    @Override
    public void onDestroy() {
//        Intent localIntent = new Intent();
//        localIntent.setClass(this, LocationService.class);  //销毁时重新启动Service
//        this.startService(localIntent);
        LogUtils.i("onDestroy...");
//        stopForeground(true);
        super.onDestroy();
    }

//    public class myReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            context.startService(new Intent(context, LocationService.class));
//        }
//    }

    /**
     * 初始化定位参数
     */
    private void initLocation() {
        mLocationClient = new LocationClient(this.getApplicationContext());
        mMyLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mMyLocationListener);

        int scanSpan = sp.getInt(Constants.REPORT_FREQUENCY, 1); // 默认值1分钟

        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationMode.Hight_Accuracy);// 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");// 可选，默认gcj02，设置返回的定位结果坐标系，bd09ll(无偏移)
        option.setScanSpan(scanSpan * 60 * 1000);// 可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);// 可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);// 可选，默认false,设置是否使用gps
        option.setLocationNotify(true);// 可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);// 可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setEnableSimulateGps(false);// 可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        option.setIsNeedLocationDescribe(true);// 可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);// 可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setPriority(LocationClientOption.GpsFirst); // 不设置，默认是gps优先
        mLocationClient.setLocOption(option);
    }

    String info = null;

    /**
     * 实现实时位置回调监听
     */
    @SuppressLint("NewApi")
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) { // Receive Location
            if (null != location) {
                boolean flag = false;
                StringBuffer sb = new StringBuffer(256);
                if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                    flag = true;
                    sb.append("gps定位成功");
                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                    flag = true;
                    sb.append("网络定位成功");
                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                    flag = true;
                    sb.append("离线定位成功，离线定位结果也是有效的");
                } else if (location.getLocType() == BDLocation.TypeServerError) {
                    sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                    sb.append("网络不同导致定位失败，请检查网络是否通畅");
                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                    sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
                }
                LogUtils.i(sb.toString());

                if (flag) {
                    locInfo.setProvince(location.getProvince());
                    locInfo.setCity(location.getCity());
                    locInfo.setDistrict(location.getDistrict());
                    locInfo.setAddress(location.getAddrStr());
                    locInfo.setStreet(location.getStreet());
                    locInfo.setStreetnumber(location.getStreetNumber());
                    locInfo.setLatitude(location.getLatitude());
                    locInfo.setLongitude(location.getLongitude());
                    locInfo.setSpeed(location.getSpeed());
//                    LogUtils.i(locInfo.toString());


//                    String tmpString = "" + (++index) + "、 速度:" + location.getSpeed() +
//                            " 海拔:" + location.getAltitude() +
//                            " 经度:" + location.getLongitude() +
//                            " 纬度:" + location.getLatitude() +
//                            " 方向:" + location.getDirection();
//                    LogUtils.e(tmpString);
//
//                    if (location.hasSpeed()) {
//                        Toast.makeText(context, tmpString, Toast.LENGTH_SHORT).show();
//                    }

                    double latitude = Double.parseDouble(sp.getString(Constants.LATITUDE, "0"));
                    double longtitude = Double.parseDouble(sp.getString(Constants.LONGTITUDE, "0"));
                    LatLng oldLoc = new LatLng(latitude, longtitude);
                    LatLng currentLoc = new LatLng(location.getLatitude(), location.getLongitude());

                    distance = DistanceUtil.getDistance(oldLoc, currentLoc);
                    info = "定位：" + CommonTools.getDateTime() + "\n";
                    if (CommonTools.hadNetwork(context) && sp.getBoolean(UserEntity.IS_lOGIN, false)) {
                        int reportFrequency = sp.getInt(Constants.REPORT_FREQUENCY, 5) * 60 * 1000;
//                        reportFrequency = 10 * 1000;
                        long currentTime = System.currentTimeMillis();
                        long lastTime = sp.getLong(Constants.LAST_REPORT_LOCATION_TIME, currentTime - reportFrequency);
                        long timeInterval = currentTime - lastTime;

                        if (!location.hasSpeed()) {
                            float speed = (float) distance * 1000 / timeInterval;
                            locInfo.setSpeed(speed);
                        }



//                        String tmpString = "" + (++index) + "、 速度:" + speed +
//                                " 经度:" + location.getLongitude() +
//                                " 纬度:" + location.getLatitude() +
//                                " 距离:" + distance +
//                                " 时间:" + timeInterval;
//                        LogUtils.e(tmpString);
//                        Toast.makeText(context, tmpString, Toast.LENGTH_SHORT).show();


//                        LogUtils.e("reportFrequency = " + reportFrequency + "\t timeInterval =" + timeInterval);
                        if (saveLocal){
                            reportLocation(context, sp, locInfo, mWaybillnum, mOrdernum);
                            saveLocal = false;
                        }else {
                            if (reportFrequency <= timeInterval) { // 上报时间 <两次上传时间间隔
                                // 上报定位信息
                                reportLocation(context, sp, locInfo, "", "");
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
                                info = "上报当前时间：" + format.format(currentTime) + "\n最后上传时间：" + format.format(lastTime) + "\n上传时间间隔：" + timeInterval / 1000 / 60
                                        + "分钟\n" + distance + "\n";
                                LogUtils.e(info);
                            }
                        }
                    } else {
                        LogUtils.i("用户未登录或没有网络不上报定位");
                    }
                }
            }

        }

    }

    /**
     * 上报位置
     *
     * @param context 上下文
     * @param sp      SharedPrenfrences
     * @param locInfo 位置信息
     */
    @SuppressLint("NewApi")
    private void reportLocation(final Context context, final SharedPreferences sp, final LocationInfo locInfo, final String waybillnum, final String ordernum) {
        RequestParams params = new RequestParams();
        params.addQueryStringParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
        params.addQueryStringParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
        params.addQueryStringParameter("CARID", sp.getString(UserEntity.CAR_NUMBER, null));
        params.addQueryStringParameter("LNG", String.valueOf(locInfo.getLongitude())); // 经度
        params.addQueryStringParameter("LAT", String.valueOf(locInfo.getLatitude())); // 纬度
        params.addQueryStringParameter("SPEED", String.valueOf(locInfo.getSpeed() * 3.6f)); // 速度 km/h
        params.addQueryStringParameter("LASTLOCATIONDISTANCE", String.valueOf(distance));

        if (distance >= Constants.STATICDISTANCE) {
            params.addQueryStringParameter("ISSTOP", "0");
        } else {
            params.addQueryStringParameter("ISSTOP", "1");  // 小于20m视为静止
        }

        Editor edit = sp.edit();
        // 存储这次上报的最后时间点，如果超时请求，则会跳过下一个点的请求
        edit.putLong(Constants.LAST_REPORT_LOCATION_TIME, System.currentTimeMillis());
        edit.commit();

        // TODO: 2017/3/2 上传运单号,订单号
        params.addQueryStringParameter("ORDERNUMBER", ordernum);
        params.addQueryStringParameter("WAYBILLNUMBER", waybillnum);

        String url = context.getString(R.string.server_url) + Constants.REPORT_LOCATION;
        LogUtils.i(url + CommonTools.getQuryParams(params));

        HttpUtils http = new HttpUtils(30 * 1000);
        http.configCurrentHttpCacheExpiry(1000 * 10);
        http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

            public void onFailure(HttpException arg0, String arg1) {
                LogUtils.i(context.getString(R.string.request_failed));
            }

            public void onSuccess(ResponseInfo<String> arg0) {
                try {
                    JSONObject obj = new JSONObject(arg0.result);
                    LogUtils.i(obj.toString());
                    String status = obj.getString("state");
                    String message = obj.getString("message");
                    if ("1".equals(status)) {
                        // 保存经纬度
                        Editor edit = sp.edit();
                        edit.putString(Constants.LONGTITUDE, String.valueOf(locInfo.getLongitude()));
                        edit.putString(Constants.LATITUDE, String.valueOf(locInfo.getLatitude()));
                        edit.putString(UserEntity.CURRENT_PROVINCE, locInfo.getProvince());
                        edit.putString(UserEntity.CURRENT_CITY, locInfo.getCity());
                        edit.putString(UserEntity.CURRENT_DISTRICT, locInfo.getDistrict());
                        edit.putString(UserEntity.CURRENT_ADDRESS, locInfo.getAddress());
                        edit.putString(UserEntity.CURRENT_STREET, locInfo.getStreet());
                        edit.putString(UserEntity.CURRENT_STREETNUMBER, locInfo.getStreetnumber());
                        edit.putLong(Constants.LAST_REPORT_LOCATION_TIME, System.currentTimeMillis());
                        edit.commit();
                        message = context.getString(R.string.report_location_success);
                        mOrdernum = "";
                        LogUtils.i(info);
                    }
                    LogUtils.i(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class LocationInfo extends Binder {
        private String province = null;
        private String city = null;
        private String district = null;
        private String street = null;
        private String streetnumber = null;
        private String address = null;
        private Double longitude = null;
        private Double latitude = null;
        private float speed = 0.0f;

        public LocationInfo() {
            super();
        }

        public void startLocation() {
            mLocationClient.start();
        }

        public void stopLocation() {
            mLocationClient.stop();
        }

        public LocationInfo(String province, String city, String district, String street, String streetnumber, String address, Double longitude, Double latitude, float speed) {
            super();
            this.province = province;
            this.city = city;
            this.district = district;
            this.street = street;
            this.streetnumber = streetnumber;
            this.address = address;
            this.longitude = longitude;
            this.latitude = latitude;
            this.speed = speed;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getStreetnumber() {
            return streetnumber;
        }
        public void setStreetnumber(String streetnumber) {
            this.streetnumber = streetnumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public float getSpeed() {
            return speed;
        }

        public void setSpeed(float speed) {
            this.speed = speed;
        }

        @Override
        public String toString() {
            return "LocationInfo [province=" + province + ", city=" + city + ", district=" + district + ", street=" + street + ", address=" + address
                    + ", longitude=" + longitude + ", latitude=" + latitude + ", speed=" + speed +  "]";
        }

    }

}
