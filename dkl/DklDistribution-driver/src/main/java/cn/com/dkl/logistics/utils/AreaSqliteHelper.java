package cn.com.dkl.logistics.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 类名称：AreaSqliteHelper 类描述：
 *
 * @author 何胜圣
 * @version 1.0 创建时间：2014年6月13日 上午9:59:54 最后修改时间： 修改人：何胜圣
 */
public class AreaSqliteHelper extends SQLiteOpenHelper {

    private List<Map<String, Object>> list;
    private Map<String, Object> map;
    private String sql;

    public AreaSqliteHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * 根据查询类型与父编号查询子地点列表
     *
     * @param table  数据表名
     * @param father 父编号
     */
    public List<Map<String, Object>> query(AreaSqliteHelper myHelper, String table, String parentCode) {
        list = new ArrayList<Map<String, Object>>();
        SQLiteDatabase db = myHelper.getWritableDatabase();

        sql = "select Code, Name from " + table + " where ParentCode= " + parentCode;

        if (parentCode.equals("0")) {
            // 此处添加全部地区选项
            map = new HashMap<String, Object>();
            map.put("ID", "100000");
            map.put("Name", "全部地区");
            list.add(map);
        }

        // execute the sql
        Cursor cursor = db.rawQuery(sql, null);

        // if
        if (cursor.getCount() == 0) {
            cursor.close();
            db.close();
            return list;
        }

        cursor.moveToFirst();

        for (cursor.moveToFirst(); !(cursor.isAfterLast()); cursor.moveToNext()) {
            map = new HashMap<String, Object>();
            map.put("ID", cursor.getString(0));
            map.put("Name", cursor.getString(1));
            list.add(map);
        }
        cursor.close();
        db.close();
        return list;
    }

    /**
     * @param @param myHelper
     * @param @param table
     * @param @param input 关键字
     * @param @param father 父编号
     * @return List<Map<String,Object>>
     * @Title: search
     * @Description: TODO根据关键字搜索地点
     */
    public List<Map<String, Object>> search(AreaSqliteHelper myHelper, String table, String input, String parentCode) {
        list = new ArrayList<Map<String, Object>>();
        SQLiteDatabase db = myHelper.getWritableDatabase();

        sql = "select Code, Name from " + table + " where Name like '%" + input + "%' and ParentCode= " + parentCode;

        if (parentCode.equals("0")) {
            // 此处添加全部地区选项
            map = new HashMap<String, Object>();
            map.put("ID", "100000");
            map.put("Name", "全部地区");
            list.add(map);
        }

        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount() == 0) {
            cursor.close();
            db.close();
            return list;
        }

        cursor.moveToFirst();

        for (cursor.moveToFirst(); !(cursor.isAfterLast()); cursor.moveToNext()) {
            map = new HashMap<String, Object>();
            map.put("ID", cursor.getString(0));
            map.put("Name", cursor.getString(1));
            list.add(map);
        }
        cursor.close();
        db.close();
        return list;
    }

    public List<Map<String, Object>> getPopularCityAndProvince(AreaSqliteHelper myHelper, String table) {
        list = new ArrayList<Map<String, Object>>();
        SQLiteDatabase db = myHelper.getWritableDatabase();

        sql = "select city.name CityName, parent.name ProvinceName, city.code CityCode from " + table + " city inner join base_chinaarea parent on city.parentcode = parent.code where city.isHotCity = '1'";

        // 此处添加全部地区选项
        map = new HashMap<String, Object>();
        map.put("Name", "全部地区");
        map.put("ParentName", "");
        map.put("ID", "100000");
        list.add(map);

        // execute the sql
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount() == 0) {
            cursor.close();
            db.close();
            return list;
        }

        cursor.moveToFirst();

        for (cursor.moveToFirst(); !(cursor.isAfterLast()); cursor.moveToNext()) {
            map = new HashMap<String, Object>();
            map.put("Name", cursor.getString(0));
            map.put("ParentName", cursor.getString(1));
            map.put("ID", cursor.getString(2));
            list.add(map);
        }
        cursor.close();
        db.close();
        return list;
    }

}
