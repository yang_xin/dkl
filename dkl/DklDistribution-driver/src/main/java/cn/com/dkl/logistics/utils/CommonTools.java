package cn.com.dkl.logistics.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.fragment.ArroundCarFragment;
import cn.com.dkl.logistics.view.CustomProgressDialog;

@SuppressLint("NewApi")
public class CommonTools {
    private static final String TAG = "CommonTools";
    public static final int MIN_CLICK_DELAY_TIME = 2000;

    /**
     * 网络连接超时时间
     */
    public static final int CONNECT_TIMEOUT = 8 * 1000;
    private Context mContext;
    private CustomProgressDialog progressDialog;
    private SharedPreferences sp;

    private AlertDialog alertdialog;

    public CommonTools(Context context) {
        this.mContext = context;
    }

    /**
     * 格式化值
     *
     * @param selectedItem 需要格式化的值
     * @param type         未选中时的数据
     * @return 返回的数据
     * @author guochaohui
     */
    public List<String> formatConditionValue(String selectedItem, String type) {
        String resultString = "";
        String resultString1 = "";
        String resultString2 = "";
        String[] resultStr = new String[2];
        List<String> result = new ArrayList<String>();
        if (TextUtils.isEmpty(selectedItem) || selectedItem.equals(type)) {// 判断该值是否有效
            result.add("");
            result.add("");
            return result;
        }

        if (!selectedItem.equals(type)) {
            // 格式化选中值
            if (selectedItem.contains("米以下") || selectedItem.contains("吨以下")) {
                resultString1 = selectedItem.replaceAll("米以下", "").replaceAll("吨以下", "");
                result.add("");
                result.add(resultString1);
            } else if (selectedItem.contains("米以上") || selectedItem.contains("吨以上")) {
                resultString2 = selectedItem.replaceAll("米以上", "").replaceAll("吨以上", "");
                result.add(resultString2);
                result.add("");
            } else {
                resultString = selectedItem.replaceAll("米", "").replaceAll("吨", "");
                if (resultString.contains("-")) {
                    resultStr = resultString.split("-");// 去掉符号"-"
                    for (int i = 0; i < resultStr.length; i++) {
                        result.add(resultStr[i]);// 返回数据
                    }
                } else {
                    for (int i = 0; i < resultStr.length; i++) {
                        result.add(resultString);// 返回数据
                    }
                }
            }

        }

        return result;
    }

    /**
     * 显示或隐藏附带搜索条件
     *
     * @param totalHeight     listView距离最顶部的总高度
     * @param conditionHeight 条件高度(带按钮)
     * @param llCondition     条件控件(带按钮)
     * @param btnCondition    控制按钮
     * @author guochaohui
     */
    public void displayCondition(final int totalHeight, final int conditionHeight, final LinearLayout llCondition, final View listView, final View btnCondition) {

        if (llCondition.getVisibility() == View.INVISIBLE) {// 如果条件选项隐藏则显示
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.setMargins(0, totalHeight + 1, 0, 0);
            listView.setLayoutParams(params);
            llCondition.setVisibility(View.VISIBLE);
            btnCondition.setBackgroundResource(R.drawable.img_up_arrow);

        } else {// 如果条件选项显示则隐藏

            llCondition.setVisibility(View.INVISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.setMargins(0, totalHeight - conditionHeight + 1, 0, 0);
            listView.setLayoutParams(params);
            btnCondition.setBackgroundResource(R.drawable.img_down_arrow);

        }
    }

    /**
     * 拨打电话
     *
     * @param phoneString 电话号码
     * @param linkMan     联系人
     * @author guochaohui
     */
    public void makeCallDialog(final String phoneString, String linkMan) {
        ContextThemeWrapper contextThemewrapper = new ContextThemeWrapper(mContext, R.style.change_text_size_dialog);
        AlertDialog.Builder builder = new AlertDialog.Builder(contextThemewrapper);
        builder.setTitle("请确认是否拨打以下电话？");
        builder.setMessage(linkMan + "   " + phoneString);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // 拨打电话
                Intent callFamilyintent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneString));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mContext.startActivity(callFamilyintent);
            }
        });

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        builder.create().show();
    }

    public void alertDialog(final String phoneString) {
        alertdialog = new AlertDialog.Builder(mContext).create();
        alertdialog.show();
        Window window = alertdialog.getWindow();
        window.setContentView(R.layout.call_dialog_info);
        TextView tvMandP = (TextView) window.findViewById(R.id.tvMandP);
        tvMandP.setText(phoneString);

        final Button btnOk = (Button) window.findViewById(R.id.btnOk);
        final Button btnCancel = (Button) window.findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // 拨打电话
                Intent callFamilyintent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneString));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mContext.startActivity(callFamilyintent);
                alertdialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                alertdialog.dismiss();
            }
        });
    }

    /**
     * 初始化dialog
     *
     * @param msg 提示信息
     * @return 返回dialog
     * @author guochaohui
     */
    public CustomProgressDialog getProgressDialog(Context mContext, String msg) {
        progressDialog = new CustomProgressDialog(mContext, msg);
        progressDialog.setCancelable(true);
        return progressDialog;
    }

    /**
     * 检查手机号码是否正确
     */
    public static boolean checkPhoneNumber(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return false;
        } else {
            String reg = "(^1[3|4|5|7|8][0-9]{9}$)";
            return isCorrect(reg, phoneNumber);
        }
    }

    /**
     * 身份证号码验证
     */
    public static boolean checkIDCardNumber(String idCardNum) {
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        if (TextUtils.isEmpty(idCardNum)) {
            return false;
        } else {
            String reg = "([0-9]{17}([0-9]|X))|([0-9]{15})";
            if (isCorrect(reg, idCardNum)) {
                if (idCardNum.length() == 15) {
                    idCardNum = idCardNum.subSequence(0, 6) + "19" + idCardNum.subSequence(6, idCardNum.length());
                }
                String strYear = idCardNum.substring(6, 10);// 年份
                String strMonth = idCardNum.substring(10, 12);// 月份
                String strDay = idCardNum.substring(12, 14);// 月份
                if (isDate(strYear + "-" + strMonth + "-" + strDay) == false) {
                    return false;
                } else {
                    try {
                        if ((gc.get(Calendar.YEAR) - Integer.parseInt(strYear)) > 150
                                || (gc.getTime().getTime() - s.parse(strYear + "-" + strMonth + "-" + strDay).getTime()) < 0) {
                            return false;
                        } else {
                            return true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
    }

    /**
     * 功能：判断字符串是否为日期格式
     */
    public static boolean isDate(String strDate) {
        Pattern pattern = Pattern
                .compile("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");
        Matcher m = pattern.matcher(strDate);
        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 正则匹配
     *
     * @param reg 正则表达式
     * @param res 需要验证的资源
     * @return 匹配结果
     */
    public static boolean isCorrect(String reg, String res) {
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(res);
        return matcher.matches();
    }

    /**
     * 验证车牌号
     *
     * @param carNumber 车牌号
     */
    public static boolean checkCarNumber(String carNumber) {
        if (TextUtils.isEmpty(carNumber)) {
            return false;
        } else {
            /*
             * 车牌号格式：汉字 + A-Z + 5位A-Z或0-9 只包括了普通车牌号，教练车和部分部队车等车牌号不包括在内）
			 */
            String reg = "^[\u4e00-\u9fa5|WJ]{1}[A-Za-z]{1}[A-Za-z0-9]{5}$";
            return isCorrect(reg, carNumber);
        }
    }

    /**
     * 给json的值判空
     *
     * @param jsonObject   json对象
     * @param key          需要判空的字段
     * @param defaultValue 默认值
     * @author guochaohui
     */
    public static String judgeNull(JSONObject jsonObject, String key, String defaultValue) {
        try {
            if (!jsonObject.has(key)) {
                return defaultValue;
            } else {
                return ("null".equals(jsonObject.getString(key)) ? defaultValue : jsonObject.getString(key));

            }
        } catch (JSONException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static int intNull(JSONObject jsonObject, String key, int defaultValue) {
        try {
            if (!jsonObject.has(key)) {
                return defaultValue;
            } else {
                return ("null".equals(jsonObject.getString(key)) ? defaultValue : jsonObject.getInt(key));

            }
        } catch (JSONException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    /**
     * 验证固话号码
     *
     * @param telephoneNum 固话号码
     */
    public static boolean checkTelephoneNum(String telephoneNum) {
        if (TextUtils.isEmpty(telephoneNum)) {
            return false;
        } else {
            String reg = "[0-9]{3}[1-9][0-9]{7}";
            if (!isCorrect(reg, telephoneNum)) {
                reg = "[0-9]{4}[1-9][0-9]{6}";
            }
            return isCorrect(reg, telephoneNum);
        }
    }

    /**
     * 验证车长
     *
     * @param carLength 车长
     */
    public static boolean checkCarLength(String carLength) {
        if (TextUtils.isEmpty(carLength)) {
            return false;
        } else {
            if (carLength.contains(".")) {
                String[] cl = new String[2];
                // 小数点要转义
                cl = carLength.split("\\.");
                if (Integer.valueOf(cl[0]).intValue() > 23 || cl[0].length() > 2) {
                    return false;
                } else if (Integer.valueOf(cl[1]).intValue() > 99 || cl[1].length() > 2) {
                    return false;
                }
            } else {
                if (Integer.valueOf(carLength).intValue() > 23 || carLength.length() > 2) {
                    return false;
                }

            }
            return true;
        }
    }

    /**
     * 检查输入的文字是否为汉字
     */
    public static boolean isChinese(String res) {
        String reg = "[\u4E00-\u9FA5]";
        boolean result = false;
        for (int i = 0; i < res.length(); i++) {
            if (isCorrect(reg, res.substring(i, i + 1))) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * 时间比较
     *
     * @return true代表当前时间>dateTime
     */
    public static boolean compareDate(String dateTime) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String currentTime = df.format(System.currentTimeMillis());
            Date date1 = df.parse(dateTime);
            Date date2 = df.parse(currentTime);
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c1.setTime(date1);
            c2.setTime(date2);
            int result = c1.compareTo(c2);
            if (result > 0) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * 将ip的整数形式转换成ip形式
     */
    public static String int2IP(int ipInt) {
        StringBuilder sb = new StringBuilder();
        sb.append(ipInt & 0xFF).append(".");
        sb.append((ipInt >> 8) & 0xFF).append(".");
        sb.append((ipInt >> 16) & 0xFF).append(".");
        sb.append((ipInt >> 24) & 0xFF);
        return sb.toString();
    }

    /**
     * 获取WIFI连接IP地址
     */
    private static String getWifiIpAddress(WifiManager wifiManager) {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return int2IP(wifiInfo.getIpAddress());
    }

    /*
     * 获取数据连接的IP地址
     */
    private static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface netInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = netInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex) {
            LogUtils.i("WifiPreference IpAddress" + ex.toString());
        }
        return null;
    }

    /**
     * 获取手机IP地址
     */
    public static String getIPAddress(Context context) {
        String ip = null;
        NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (null != netInfo && netInfo.isAvailable()) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifiManager.isWifiEnabled()) { // 使用的是WiFi
                ip = getWifiIpAddress(wifiManager);
            } else {
                ip = getLocalIpAddress();
            }
        }
        return ip;
    }

    /**
     * 获取手机网络状态
     */
    public static boolean hadNetwork(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connManager.getActiveNetworkInfo();
        if (null != info && info.isAvailable()) {
            LogUtils.i("当前网络名称：" + info.getTypeName());
            return true;
        } else {
            LogUtils.i("没有可用网络");
            return false;
        }
    }

    @SuppressWarnings("deprecation")
    public static String getParams(RequestParams params) {
        String result = null;
//        int size = params.getQueryStringParams().size();
//        for (int i = 0; i < size; i++) {
//
//        }
        try {
//			result = EntityUtils.toString(params.getEntity(), "UTF-8");
        } catch (Exception e) {
            LogUtils.i(e.toString());
        }

        return result;
    }

    public static String getQuryParams(RequestParams params) {
        String result = null;
        int size = params.getQueryStringParams().size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                result = result + "&" + params.getQueryStringParams().get(i);
            } else {
                result = "" + params.getQueryStringParams().get(i);
            }

        }
//        int size = params.getQueryStringParams().size();
//        for (int i = 0; i < size; i++) {
//
//        }
        try {
//			result = EntityUtils.toString(params.getEntity(), "UTF-8");
        } catch (Exception e) {
            LogUtils.i(e.toString());
        }

        return result;
    }

    /**
     * dip转px
     */
    public static int dp2px(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int getWindowWidth(Activity activity) {
        WindowManager manager = activity.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int width = outMetrics.widthPixels;
        return width;
    }

    public static int getWindowHeight(Activity activity) {
        WindowManager manager = activity.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int height = outMetrics.heightPixels;
        return height;
    }

    /**
     * 获取当前显示的activity
     */
    public static String getTopActivity(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> runTask = manager.getRunningTasks(1000);
        return runTask.get(0).topActivity.getClassName();
    }

    public static String getDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
    }

    public static int getDaysByYearMonth(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 校验银行卡卡号
     */
    public static boolean checkBankCard(String cardId) {
        char bit = getBankCardCheckCode(cardId.substring(0, cardId.length() - 1));
        if (bit == 'N') {
            return false;
        }
        return cardId.charAt(cardId.length() - 1) == bit;
    }

    /**
     * 从不含校验位的银行卡卡号采用 Luhm 校验算法获得校验位
     */
    public static char getBankCardCheckCode(String nonCheckCodeCardId) {
        if (nonCheckCodeCardId == null || nonCheckCodeCardId.trim().length() == 0 || !nonCheckCodeCardId.matches("\\d+")) {
            // 如果传的不是数据返回N
            return 'N';
        }
        char[] chs = nonCheckCodeCardId.trim().toCharArray();
        int luhmSum = 0;
        for (int i = chs.length - 1, j = 0; i >= 0; i--, j++) {
            int k = chs[i] - '0';
            if (j % 2 == 0) {
                k *= 2;
                k = k / 10 + k % 10;
            }
            luhmSum += k;
        }
        return (luhmSum % 10 == 0) ? '0' : (char) ((10 - luhmSum % 10) + '0');
    }

    /**
     * 获取小数点后两位
     */
    public static double subDouble(String str) {
        double i = Double.parseDouble(str);
        i = Math.round(i * 100) / 100.0;
        return i;
    }

    /**
     * 请求失败Toast信息
     */
    public static void failedToast(Context context) {
        if (CommonTools.hadNetwork(context)) {
            Toast.makeText(context, R.string.request_failed, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, R.string.connection_network_faild, Toast.LENGTH_SHORT).show();
        }
    }

    // 时间相减
    public static String RegulationTiming(String startDate, String nowDate) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            Date dt1 = df.parse(nowDate);
            Date dt2 = df.parse(startDate);
            long temp = dt2.getTime() - dt1.getTime();
            long hours = temp / 1000 / 3600; // 相差小时数
            long temp2 = temp % (1000 * 3600);
            long mins = temp2 / 1000 / 60; // 相差分钟数
            if (hours == 0) {
                return (mins + "分钟");
            } else {
                return (hours + "小时" + mins + "分钟");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            return "0分钟";
        }
    }


    /**
     * @param longitude 经度
     * @param latitude  纬度
     * @return boolean
     * @Description 判断经纬度是否有效
     * @Author guochaohui
     */
    public static boolean latLngIsOK(String longitude, String latitude) {
        if (longitude == null || latitude == null || longitude.equals("") || latitude.equals("") || Double.parseDouble(longitude) == 0
                || Double.parseDouble(latitude) == 0) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * 计算两个时间差
     *
     * @param timeFormat 时间格式
     */
    public static long compareTime(String time1, String time2, String timeFormat) {
        DateFormat df = new SimpleDateFormat(timeFormat);

        try {
            Date d1 = df.parse(time1);
            Date d2 = df.parse(time2);

            long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
//            long days = diff / (1000 * 60 * 60 * 24);
//            long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
//            long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
//            System.out.println(""+days+"天"+hours+"小时"+minutes+"分");

            return diff / 1000;
        } catch (Exception e) {

        }
        return 0;
    }

    /**
     * 计算两个时间差
     */
    public static long compareTime(String time1, String time2) {
        return compareTime(time1, time2, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 格式化时间
     */
    public static String formatSeconds(long seconds) {
        String timeStr = seconds + "秒";
        if (seconds > 60) {
            long second = seconds % 60;
            long min = seconds / 60;
            timeStr = min + "分" + second + "秒";
            if (min > 60) {
                min = (seconds / 60) % 60;
                long hour = (seconds / 60) / 60;
                timeStr = hour + "小时" + min + "分" + second + "秒";
                if (hour > 24) {
                    hour = ((seconds / 60) / 60) % 24;
                    long day = (((seconds / 60) / 60) / 24);
                    timeStr = day + "天" + hour + "小时" + min + "分" + second + "秒";
                }
            }
        }
        return timeStr;
    }


    /**
     * 检查手机上是否安装了指定的软件
     */
    public static boolean isAvilible(Context context, String packageName) {
        //获取packagemanager
        final PackageManager packageManager = context.getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
        //用于存储所有已安装程序的包名
        List<String> packageNames = new ArrayList<String>();
        //从pinfo中将包名字逐一取出，压入pName list中
        if (packageInfos != null) {
            for (int i = 0; i < packageInfos.size(); i++) {
                String packName = packageInfos.get(i).packageName;
                packageNames.add(packName);
            }
        }
        //判断packageNames中是否有目标程序的包名，有TRUE，没有FALSE
        return packageNames.contains(packageName);
    }

    /**
     * 判断是否进入范围  单位:m
     */
    public static boolean isRangeInto(LatLng p2) {
        LatLng p = new LatLng(Double.valueOf(ArroundCarFragment.mLatitude),
                Double.valueOf(ArroundCarFragment.mLongitude));
        double i = DistanceUtil.getDistance(p, p2);

        if (i <= Constants.RANGE && i >= 0) {
            return true;
        } else {
            return true;
        }
    }

    /**
     * 连接收到的字节总数
     */
    public static long getMobileRxBytes(Context mContext) {
        if (isWifi(mContext)){
            return TrafficStats.getTotalRxBytes() == TrafficStats.UNSUPPORTED ? 0 : (TrafficStats
                    .getTotalRxBytes() / 1024);
        }else if (isMobileNetwork(mContext)){
            return TrafficStats.getMobileRxBytes() == TrafficStats.UNSUPPORTED ? 0 : (TrafficStats
                    .getMobileRxBytes() / 1024);
        }
        return 0;
    }

    /**
     * 连接收到的字节总数
     */
    public static long getMobileTxBytes(Context mContext) {
        if (isWifi(mContext)){
            return TrafficStats.getTotalTxBytes() == TrafficStats.UNSUPPORTED ? 0 : (TrafficStats
                    .getTotalTxBytes() / 1024);
        }else if (isMobileNetwork(mContext)){
            return TrafficStats.getMobileTxBytes() == TrafficStats.UNSUPPORTED ? 0 : (TrafficStats
                    .getMobileTxBytes() / 1024);
        }
        return 0;

    }

    /**
     * 是否WiFi
     */
    private static boolean isWifi(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetInfo != null && activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            return true;
        }
        return false;
    }

    /**
     * 是否数据流量
     * @param context
     * @return
     */
    private static boolean isMobileNetwork(Context context) {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mMobileNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);   //获取移动网络信息
        if (mMobileNetworkInfo != null) {
            return mMobileNetworkInfo.isAvailable();    //getState()方法是查询是否连接了数据网络
        }
        return false;
    }

}