package cn.com.dkl.logistics.utils;

/** 常量类
 * Created by magic on 2016/12/15.
 */

public class Constant {
    /**
     * 无
     */
    public static final int NON = 0;
    /**
     * 设为工作单
     */
    public static final int WORKORDER = 1;
    /**
     * 上传运输凭证
     */
    public static final int UPLOADTRSTIMAGE = 2;

    // 规划路线时是否增加当前位置
    public static final boolean isAddCurrent = true;
    /**
     * 认证成功发送广播
     */
    public static final String AUTHSUCCESS = "cn.com.dkl.logistics.driver.AUTHSUCCESS";
    /**
     * 设为工作单广播
     */
    public static final String AUTOMATICBROADCAST = "cn.com.dkl.logistics.driver.AUTOMATICBROADCAST";
    /**
     * 记录下个目的地
     */
    public static int LastPosition = 0;
    /**
     *  运单已接单
     */
    public static final String WAYBILLTAKEORDER = "cn.com.dkl.logistics.driver.WAYBILLTAKEORDER";

}
