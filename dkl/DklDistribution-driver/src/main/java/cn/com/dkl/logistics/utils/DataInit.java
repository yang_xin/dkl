package cn.com.dkl.logistics.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.lidroid.xutils.util.LogUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import cn.com.dkl.logistics.constant.SettingEntity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;

/**
 * @author hxd E-mail: 493579402dxh@gmail.com
 * @version 创建时间：2012-9-6 下午4:30:21
 */
public class DataInit {
    /**
     * @param context
     * @return
     * @throws IOException
     */

    private static SharedPreferences sp;

    public static boolean initSqlte(Context context) throws IOException {

        sp = context.getSharedPreferences(DataManager.PREFERENCE_SETTING, Context.MODE_PRIVATE);
        Editor editor = sp.edit();

        File apkFolder = context.getExternalFilesDir("apkfiles");
        File apkDir = new File(apkFolder.toString());
        if (!apkDir.exists()) {
            apkDir.mkdir();
        }
        File dbDirPath = context.getExternalFilesDir("db");
        editor.putString(SettingEntity.APK_URL, apkFolder.toString());
        editor.putString(SettingEntity.DATA_BASE_URL, dbDirPath + "/AreaData.db");
        editor.commit();
        LogUtils.i("dbDirPath= " + dbDirPath.toString());
        File dbDir = new File(dbDirPath.toString());
        if (!dbDir.exists()) // 如果不存在该目录则创建
        {
            dbDir.mkdir();
            // 打开静态数据库文件的输入流
            InputStream is = context.getResources().openRawResource(R.raw.china_province_city_district);
            // 打开目标数据库文件的输出流
            FileOutputStream os = new FileOutputStream(dbDirPath + "/AreaData.db");
            byte[] buffer = new byte[1024];
            int count = 0;
            // 将静态数据库文件拷贝到目的地
            while ((count = is.read(buffer)) > 0) {
                os.write(buffer, 0, count);
            }
            is.close();
            os.close();
            return true;
        } else {
            // 打开静态数据库文件的输入流
            InputStream is = context.getResources().openRawResource(R.raw.china_province_city_district);
            // 打开目标数据库文件的输出流
            FileOutputStream os = new FileOutputStream(dbDirPath + "/AreaData.db");
            byte[] buffer = new byte[1024];
            int count = 0;
            // 将静态数据库文件拷贝到目的地
            while ((count = is.read(buffer)) > 0) {
                os.write(buffer, 0, count);
            }
            is.close();
            os.close();
            return true;
        }
    }
}