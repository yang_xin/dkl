package cn.com.dkl.logistics.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.com.dkl.logistics.driver.R;

/**
 * Dialog提示框
 * Created by LiuXing on 2017/3/3.
 */

public class DialogUtil {
    private Context mContext;
    private View dialogView;
    private TextView tvDialogValue;
    private Dialog dialog;

    public DialogUtil(Context context) {
        if (mContext == null) {
            this.mContext = context;
            dialogView = View.inflate(context, R.layout.custom_progress_dialog, null);
            tvDialogValue = (TextView) dialogView.findViewById(R.id.tvLoading);
            dialog = new Dialog(mContext, R.style.viewDialog);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
        }
    }

    /**
     * 登录提示框
     *@Author hss
     *@return
     */
    public Dialog login() {
        tvDialogValue.setText("正在登录...");
        dialog.setContentView(dialogView);
        return dialog;
    }

    /**
     * 加载提示框
     *@Author hss
     *@return
     */
    public Dialog loading() {
        tvDialogValue.setText("请稍候...");
        dialog.setContentView(dialogView);
        return dialog;
    }

    /**
     * 查询提示框
     *@Author hss
     *@return
     */
    public Dialog search() {
        tvDialogValue.setText("正在查询...");
        dialog.setContentView(dialogView);
        return dialog;
    }

    /**
     * 上传提示框
     *@Author hss
     *@return
     */
    public Dialog upload() {
        tvDialogValue.setText("正在上传数据...");
        dialog.setContentView(dialogView);
        return dialog;
    }

    /**
     * 下载提示框
     *@Author hss
     *@return
     */
    public Dialog download() {
        tvDialogValue.setText("正在下载...");
        dialog.setContentView(dialogView);
        return dialog;
    }
}
