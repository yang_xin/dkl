package cn.com.dkl.logistics.utils;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import cn.com.dkl.logistics.driver.R;


/**
 * Created by Xillen on 2017/7/14.
 * 图片显示样式，原图、圆角、圆形
 */
public class DisplayOptions {

    public DisplayImageOptions options;

    public DisplayOptions() {
        this.options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_camera) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.icon_camera) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.icon_camera) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中.
                .displayer(new RoundedBitmapDisplayer(20)) // 设置成圆角图片 new Display(0)
                .build();	// 设置图片显示相关参数
    }

    public DisplayOptions(int i, int round){
        switch(i){
            case 0:
                this.options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.icon_camera) // 设置图片下载期间显示的图片
                        .showImageForEmptyUri(R.drawable.icon_camera) // 设置图片Uri为空或是错误的时候显示的图片
                        .showImageOnFail(R.drawable.icon_camera) // 设置图片加载或解码过程中发生错误显示的图片
                        .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                        .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中// 原图
                        .build();	// 设置图片显示相关参数
                break;
            case 1:
                this.options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.icon_camera) // 设置图片下载期间显示的图片
                        .showImageForEmptyUri(R.drawable.icon_camera) // 设置图片Uri为空或是错误的时候显示的图片
                        .showImageOnFail(R.drawable.icon_camera) // 设置图片加载或解码过程中发生错误显示的图片
                        .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                        .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中.
                        .displayer(new RoundedBitmapDisplayer(round)) // 设置成圆角图片 自定义圆角半径
                        .build();	// 设置图片显示相关参数
                break;
            case 2:
                this.options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.icon_camera) // 设置图片下载期间显示的图片
                        .showImageForEmptyUri(R.drawable.icon_camera) // 设置图片Uri为空或是错误的时候显示的图片
                        .showImageOnFail(R.drawable.icon_camera) // 设置图片加载或解码过程中发生错误显示的图片
                        .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                        .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中.
                        .displayer(new Display(0)) // 设置成圆形图片
                        .build();	// 设置图片显示相关参数
                break;
        }
    }

}
