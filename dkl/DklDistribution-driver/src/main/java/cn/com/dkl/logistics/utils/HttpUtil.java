package cn.com.dkl.logistics.utils;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * 网络请求公共方法
 * Created by LiuXing on 2017/3/3.
 */

public class HttpUtil {
    /**
     * HTTP GET 请求(不可取消当前请求)
     * @param params 请求参数，包含地址
     * @param callback 回调
     */
    public void HttpGet(RequestParams params, Callback.CommonCallback callback) {
        x.http().get(params, callback);
    }

    /**
     * HTTP GET 请求(可取消当前网络请求)
     * @param params 请求参数，包含地址
     * @param callback 回调
     * @return 返回Cancelable
     */
    public Callback.Cancelable HttpGetCancel(RequestParams params, Callback.CommonCallback callback) {
        return x.http().get(params, callback);
    }

    /**
     * HTTP POST 请求(不可取消当前请求)
     * @param params 请求参数，包含地址
     * @param callback 回调
     */
    public void HttpPost(RequestParams params, Callback.CommonCallback callback) {
        x.http().post(params, callback);
    }

    /**
     * HTTP POST 请求(可取消当前网络请求)
     * @param params 请求参数，包含地址
     * @param callback 回调
     * @return 返回Cancelable
     */
    public Callback.Cancelable HttpPostCancel(RequestParams params, Callback.CommonCallback callback) {
        return x.http().post(params, callback);
    }

    /**
     * HTTP DOWNLOAD 请求(不可取消当前请求)
     * @param params 请求参数，包含地址
     * @param callback 回调
     */
    public void DownloadFile(RequestParams params, Callback.CommonCallback callback) {
        x.http().post(params, callback);
    }

    /**
     * HTTP DOWNLOAD(可取消当前网络请求)
     * @param params 请求参数，包含地址
     * @param callback 回调
     * @return 返回Cancelable
     */
    public Callback.Cancelable DownloadFileCancel(RequestParams params, Callback.CommonCallback callback) {
        return x.http().post(params, callback);
    }
}
