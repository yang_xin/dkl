package cn.com.dkl.logistics.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.haitun.xillen.utils.ValueUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.util.LogUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cn.com.dkl.logistics.constant.Constants;
import cn.com.dkl.logistics.driver.R;

@SuppressLint("NewApi")
public class ImageUtils {
    public static final int GET_IMAGE_BY_CAMERA = 0;
    public static final int GET_IMAGE_FROM_PHONE = 1;
    public static final int ZIP_IMAGE = 3;
    public static final int CROP_IMAGE = 4;

    /**
     * 来自拍照的图片Uri
     */
    public static Uri imageUriFromCamera;
    /**
     * 截取图片的Uri
     */
    public static Uri cropImageUri;
    /**
     * 要上传图片的真实路径
     */
    public static String picPath = null;
    /**
     * 当前头像是否是图片是头像
     */
    public static boolean isHeadImage = false;
    /**
     * 当前要上传的图片名称
     */
    public static String uploadImageName = null;
    private static boolean isTakePhone = false;
    /**
     * 相机保存图片路径
     */
    public static String sPath;
    /**
     * 图片压缩文件目录
     */
    public static String cprsFolder;

    /**
     * 选择用户图片
     */
    public static void chooseImage(final Activity activity) {
        String[] choices = new String[]{"拍照", "手机中选择"};
        new AlertDialog.Builder(activity).setTitle("选择图片").setItems(choices, new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switch (which) {
                    case 0:
                        isTakePhone = true;
                        ImageUtils.openCameraImage(activity);
                        break;
                    case 1:
                        isTakePhone = false;
                        ImageUtils.openLocalImage(activity);
                        break;
                }
            }
        }).setNegativeButton("取消", null).show();
    }
    /**
     * 打开相机获得图片
     */
    public static void openCameraImage(final Activity activity) {
        ImageUtils.imageUriFromCamera = ImageUtils.createImagePathUri(activity);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
//            // 文件夹
//            sPath = Environment.getExternalStorageDirectory().toString()
//                    + "/" + activity.getPackageName() + "/pictures";
//        }else if(Environment.getExternalStorageState().equals(Environment.MEDIA_REMOVED)){
//            sPath = activity.getFilesDir().getPath() + "/" + activity.getPackageName() + "/pictures";
//        }
        sPath = Constants.STORAGEPATH;
        File path1 = new File(sPath);
        if (!path1.exists()) {
            path1.mkdirs();
        }
        File file = new File(path1, System.currentTimeMillis() + ".jpg");
        ImageUtils.imageUriFromCamera = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtils.imageUriFromCamera);

        if(intent.resolveActivity(activity.getPackageManager()) != null){
            activity.startActivityForResult(intent, ImageUtils.GET_IMAGE_BY_CAMERA);
        }

    }

    /**
     * 打开本地图片
     */
    @SuppressLint("InlinedApi")
    public static void openLocalImage(final Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);// ACTION_OPEN_DOCUMENT
        // intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            activity.startActivityForResult(intent, ImageUtils.GET_IMAGE_FROM_PHONE);
        } else {
            activity.startActivityForResult(intent, ImageUtils.GET_IMAGE_FROM_PHONE);
        }
    }

    /**
     * 将Uri转成路径
     *
     * @param uri 图片Uri
     */
    @SuppressLint("NewApi")
    public static void uriToPath(Activity activity, Uri uri) {
        if (null != uri) {
            try {
                String filePath = getPath(activity, uri);
                if (!TextUtils.isEmpty(filePath)) {
                    zipImage(activity, filePath);
                    copyImage(activity, filePath);
                }
            } catch (Exception e) {
                LogUtils.i(e.toString());
            }
        }
    }

    private static void copyImage(Context context, String imagePath) throws Exception {
        File goalfile = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), imagePath.substring(imagePath.lastIndexOf("/"), imagePath.length()));
        if (goalfile.exists()) {
            goalfile.delete();
        }
        FileInputStream in = context.openFileInput(imagePath);
        FileOutputStream out = context.openFileOutput(goalfile.toString(), Context.MODE_PRIVATE);
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = in.read(buffer)) != -1) {
            out.write(buffer, 0, len);
        }
        out.close();
        in.close();
    }

    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) { // ExternalStorageProvider
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) { // DownloadsProvider
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) { // MediaProvider
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) { // MediaStore(and
            if (isGooglePhotosUri(uri)) { // Return the remote address
                return uri.getLastPathSegment();
            }

            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) { // File
            return uri.getPath();
        }

        return null;
    }

    /**
     * 根据uri获取图片路径
     * @param activity
     * @param uri
     * @return true,转换成功。false，转换失败
     */
    public static boolean converImagePath (final Activity activity, Uri uri){
        boolean result = false;
        if (null != uri) {
            try {
                String filePath = getPath(activity, uri);
                File imageFile = new File(filePath);
                if (!TextUtils.isEmpty(filePath) && imageFile.exists() && imageFile.length() > 0) {
                    String imagePath = filePath.substring(0, filePath.lastIndexOf("/")) + "/";
                    /** 因为本地缓存多个回单时，图片名称不能一样，所以图片命名增加时间戳 picPath = imagePath + uploadImageName + ".jpg";*/
                    picPath = imagePath + uploadImageName + System.currentTimeMillis() +".jpg";
                    result = true;
                } else {
                    Toast.makeText(activity, "文件不存在，请选择其他图片!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 获取压缩图片目录
     * @param activity
     * @return
     */
    public static String getCompressImageFolder(final Activity activity){
        cprsFolder = "";
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            // 文件夹
            cprsFolder = Environment.getExternalStorageDirectory().toString()
                    + "/" + activity.getPackageName() + "/pictures/zipImage";
        }else if(Environment.getExternalStorageState().equals(Environment.MEDIA_REMOVED)){
            cprsFolder = activity.getFilesDir().getPath() + "/" + activity.getPackageName() + "/pictures/zipImage";
        }
        File cprsFile = new File(cprsFolder);
        if (!cprsFile.exists()){
            cprsFile.mkdirs();
        }
        return cprsFolder;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for MediaStore Uris, and other
     * file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String selectImage(Context context, Intent data) {
        Uri selectedImage = data.getData();
        if (selectedImage != null) {
            String uriStr = selectedImage.toString();
            String path = uriStr.substring(10, uriStr.length());
            if (path.startsWith("com.sec.android.gallery3d")) {
                LogUtils.i("It's auto backup pic path:" + selectedImage.toString());
                return null;
            }
        }
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }

    /**
     * 图片路径
     * @param
     */
    public static void zipImage(Activity activity, String fileFromPath) {
        try {
            boolean isNeedZip = false;
            // 压缩(上传)图片存储路径
            String imagePath = fileFromPath.substring(0, fileFromPath.lastIndexOf("/")) + "/";
            // String imageName = uploadImageName +
            // fileFromPath.substring(fileFromPath.lastIndexOf("/") + 1,
            // fileFromPath.length());
            picPath = imagePath + uploadImageName + ".jpg";

            File yuanfile = new File(fileFromPath);
            if ((yuanfile.length() / 1024) > 210){
                isNeedZip = true;
            }
            if (isNeedZip){
                Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = true;
                Bitmap bitmap = BitmapFactory.decodeFile(fileFromPath, opts);


                float width = opts.outWidth; // 原始宽高
                float height = opts.outHeight;
                int newWidth = 192; // 新宽高405
                opts.inJustDecodeBounds = false; // 真正生成图片
                if (width > height) { // 宽>高，以高为比例进行缩放
                    opts.inSampleSize = Math.round(height / newWidth); // inSampleSize的参数为2的指数才能生效，非2的指数向下指向最近的2的指数，如5，6,7的有效值为4
                } else {
                    opts.inSampleSize = Math.round(width / newWidth);
                }
                bitmap = BitmapFactory.decodeFile(fileFromPath, opts); // 压缩图片

                bitmap = rotateImage(fileFromPath, opts);
                File file = new File(picPath);
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                if (false == bitmap.isRecycled()) {
                    bitmap.recycle(); // 回收
                }
            } else {
                Bitmap bitmap = BitmapFactory.decodeFile(fileFromPath);
                File file = new File(picPath);
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            }
        } catch (Exception e) {
            LogUtils.i(e.toString());
        }
    }

    /**
     *
     *
     */
    public static Bitmap rotateImage(String fileFromPath, Options bitmapOptions) {
        bitmapOptions.inSampleSize = 8;
        File file = new File(fileFromPath);

        /**
         * 获取图片的旋转角度，有些系统把拍照的图片旋转了，有的没有旋转
         */
        int degree = readPictureDegree(file.getAbsolutePath());

        Bitmap cameraBitmap = BitmapFactory.decodeFile(fileFromPath, bitmapOptions);
        Bitmap bitmap = cameraBitmap;

        /**
         * 把图片旋转为正的方向
         */
        bitmap = rotaingImageView(degree, bitmap);

        return bitmap;
    }


    /**
     * 旋转图片
     * @param angle
     * @param bitmap
     * @return Bitmap
     */
    public static Bitmap rotaingImageView(int angle , Bitmap bitmap) {
        //旋转图片 动作
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        System.out.println("angle2=" + angle);
        // 创建新的图片
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return resizedBitmap;
    }

    /**
     * 读取图片属性：旋转的角度
     * @param path 图片绝对路径
     * @return degree旋转的角度
     */
    public static int readPictureDegree(String path) {
        int degree  = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }

            LogUtils.d("degree is " + degree);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }


    /**
     * 删除临时图片
     */
    public static void deleteImage(String imagePath) {
        if (TextUtils.isEmpty(imagePath)) {
            return;
        }
        File file = new File(imagePath);
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * 删除临时图片
     */
    public static void deleteImage(String whatImage, String imagePath) {
        if (TextUtils.isEmpty(imagePath)) {
            return;
        }
        File file = new File(imagePath);
        if (file.exists()) {
            String filePath = imagePath.substring(0, imagePath.lastIndexOf("/") + 1); // 图片所在目录
            File dir = new File(filePath);
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile()) { // 目录
                    String fileName = files[i].getAbsolutePath();
                    if (fileName.contains(whatImage)) {
                        System.out.println("删除文件：" + fileName);
                        if (files[i].exists()) {
                            files[i].delete();
                        }
                    }
                }
            }
        }
    }

    /**
     * 创建一条图片地址uri,用于保存照片
     *
     * @return 图片的uri
     */
    private static Uri createImagePathUri(Context context) {
        Uri imageFilePath = null;
        String status = Environment.getExternalStorageState();
        SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA);
        long time = System.currentTimeMillis();
        String imageName = timeFormatter.format(new Date(time));
        // ContentValues是我们希望这条记录被创建时包含的数据信息
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, imageName);
        values.put(MediaStore.Images.Media.DATE_TAKEN, time);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        if (status.equals(Environment.MEDIA_MOUNTED)) { // 判断是否有SD卡, 优先使用SD卡存储,
            // 当没有SD卡时使用手机存储
            imageFilePath = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } else {
            imageFilePath = context.getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, values);
        }
        return imageFilePath;
    }

    public static void downLoadImage(final Context context, final ImageView imageView, String imageName) {
        if (!TextUtils.isEmpty(imageName)) {
            final String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;
            File file = new File(imagePath);
            if (file.exists()) {
                imageView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            } else {
                String url = context.getString(R.string.server_imgurl) + "upload/" + imageName;
                //String url = context.getString(R.string.server_url) + "upload/" + imageName;
                HttpUtils http = new HttpUtils(60 * 1000);
                http.configCurrentHttpCacheExpiry(1000 * 10);
                http.download(url, imagePath, true, true, new RequestCallBack<File>() {

                    @Override
                    public void onSuccess(ResponseInfo<File> arg0) {
                        imageView.setImageBitmap(zipShowImage(imagePath));
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        LogUtils.i(arg0.getMessage() + arg1);
                    }
                });
            }
        }
    }

    public static void downLoadImage(final Context context, String imageName) {
        if (!TextUtils.isEmpty(imageName)) {
            final String imagePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + imageName;
            File file = new File(imagePath);
            if (!file.exists()) {
                String url = context.getString(R.string.server_imgurl) + "upload/" + imageName;
                //String url = context.getString(R.string.server_url) + "upload/" + imageName;
                HttpUtils http = new HttpUtils(60 * 1000);
                http.configCurrentHttpCacheExpiry(1000 * 10);
                http.download(url, imagePath, true, true, new RequestCallBack<File>() {

                    @Override
                    public void onSuccess(ResponseInfo<File> arg0) {
                        // zipShowImage(imagePath);
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        LogUtils.i(arg0.getMessage() + arg1);
                    }
                });
            }
        }
    }

    /**
     * 根据控件大小，缩放图片
     */
    public static Bitmap zipShowImage(String imagePath) {
        Bitmap bitmap = null;
        try {
            int size = 1000;
            Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            bitmap = BitmapFactory.decodeFile(imagePath, opts);
            float width = opts.outWidth; // 原始宽高
            float height = opts.outHeight;
            opts.inJustDecodeBounds = false; // 真正生成图片
            if (width > height) { // 宽>高，以高为比例进行缩放
                opts.inSampleSize = Math.round(height / size); // inSampleSize的参数为2的指数才能生效，非2的指数向下指向最近的2的指数，如5，6,7的有效值为4
            } else {
                opts.inSampleSize = Math.round(width / size);
            }
            if (opts.inSampleSize > 0) { // 缩放
                bitmap = BitmapFactory.decodeFile(imagePath, opts); // 压缩图片
            }
            // FileOutputStream out = new FileOutputStream(imagePath);
            // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            // out.flush();
            // out.close();
        } catch (Exception e) {
            LogUtils.e(e.toString());
        }
        return bitmap;
    }

    /**
     * 显示图片 圆角20dp
     * @param context
     * @param headImageName
     * @param imageView
     */
    public static void imageLoad(Context context,String headImageName,ImageView imageView) {
        if (ValueUtils.isStrEmpty(headImageName)) {
            return;
        }
        String url = context.getString(R.string.server_url) + "upload/" + headImageName;
        ImageLoader.getInstance().displayImage(url,imageView,new DisplayOptions(1,20).options);
    }








}
