package cn.com.dkl.logistics.utils;

import android.content.Context;

public class InitDataBase {
    public static AreaSqliteHelper getMySqliteHelper(Context context, String string) {

        // get database address
        String databaseString = string;
        // creat a sqliteOpenHelper
        return new AreaSqliteHelper(context, databaseString, null, 1);
    }
}
