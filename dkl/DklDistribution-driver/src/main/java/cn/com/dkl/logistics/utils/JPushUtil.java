package cn.com.dkl.logistics.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.lidroid.xutils.util.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;


public class JPushUtil {
    public static final String PREFS_NAME = "JPUSH_EXAMPLE";
    public static final String PREFS_DAYS = "JPUSH_EXAMPLE_DAYS";
    public static final String PREFS_START_TIME = "PREFS_START_TIME";
    public static final String PREFS_END_TIME = "PREFS_END_TIME";
    public static final String KEY_APP_KEY = "JPUSH_APPKEY";

    public static boolean isEmpty(String s) {
        if (null == s)
            return true;
        if (s.length() == 0)
            return true;
        if (s.trim().length() == 0)
            return true;
        return false;
    }

    // 校验Tag Alias 只能是数字,英文字母和中文
    public static boolean isValidTagAndAlias(String s) {
        Pattern p = Pattern.compile("^[\u4E00-\u9FA50-9a-zA-Z_-]{0,}$");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    // 取得AppKey
    public static String getAppKey(Context context) {
        Bundle metaData = null;
        String appKey = null;
        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            if (null != ai)
                metaData = ai.metaData;
            if (null != metaData) {
                appKey = metaData.getString(KEY_APP_KEY);
                if ((null == appKey) || appKey.length() != 24) {
                    appKey = null;
                }
            }
        } catch (NameNotFoundException e) {

        }
        return appKey;
    }

    // 取得版本号
    public static String GetVersion(Context context) {
        try {
            PackageInfo manager = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            return manager.versionName;
        } catch (NameNotFoundException e) {
            return "Unknown";
        }
    }

    public static void showToast(final String toast, final Context context) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        }).start();
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager conn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = conn.getActiveNetworkInfo();
        return (info != null && info.isConnected());
    }

    public static String getImei(Context context, String imei) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            imei = telephonyManager.getDeviceId();
        } catch (Exception e) {
            LogUtils.i(JPushUtil.class.getSimpleName() + ":" + e.getMessage());
            //Log.e(JPushUtil.class.getSimpleName(), e.getMessage());
        }
        return imei;
    }

    /***
     * 登陆成功后设置JPush的Tag和Alias
     */
    public static void setJPushAliasAndTags(Context context, JSONObject data) throws JSONException {
        //调用JPush API设置Alias
//          JPushInterface.setAlias(context, data.getString("Uid"), new TagAliasCallback(){
//              @Override
//              public void gotResult(int arg0, String arg1,
//                      Set<String> arg2) {
//                 LogUtils.i("set alias result is "+arg0);
//          } }); 
        //调用JPush API设置Tag 
        Set<String> sets = new HashSet<String>();
        sets.add(data.getString("Role_ID"));
//          JPushInterface.setTags(context, sets, new TagAliasCallback(){
//              @Override
//              public void gotResult(int arg0, String arg1,
//                      Set<String> arg2) {
//                 LogUtils.i("set tags result is "+arg0);
//          } }); 

        //调用JPush API 同时设置Alias 与 Tag
        JPushInterface.setAliasAndTags(context, data.getString("Uid"), sets, new TagAliasCallback() {

            @Override
            public void gotResult(int arg0, String arg1, Set<String> arg2) {
                LogUtils.i("set tags result is " + arg0);
            }
        });
    }
    /***
     * 设置JPush的Tag和Alias
     */
    public static void setJPushAliasAndTags(Context context, String role_ID,String uid) throws JSONException {
        //调用JPush API设置Tag
        Set<String> sets = new HashSet<String>();
        sets.add(role_ID);
        //调用JPush API 同时设置Alias 与 Tag
        JPushInterface.setAliasAndTags(context, uid, sets, new TagAliasCallback() {

            @Override
            public void gotResult(int arg0, String arg1, Set<String> arg2) {
                LogUtils.i("set tags result is " + arg0);
            }
        });
    }


    /***
     * 退出成功后清理JPush的Tag和Alias
     */
    public static void clearJPushAliasAndTags(Context context) {
        //调用JPush API设置Alias
//          JPushInterface.setAlias(context, "0", new TagAliasCallback(){
//              @Override
//              public void gotResult(int arg0, String arg1,
//                      Set<String> arg2) {
//                 LogUtils.i("set alias result is "+arg0);
//          } }); 
        //调用JPush API设置Tag 
        Set<String> sets = new HashSet<String>();
        sets.add("0");
//          JPushInterface.setTags(context, sets, new TagAliasCallback(){
//              @Override
//              public void gotResult(int arg0, String arg1,
//                      Set<String> arg2) {
//                 LogUtils.i("set tags result is "+arg0);
//          } }); 

        //调用JPush API 同时设置Alias 与 Tag 
        JPushInterface.setAliasAndTags(context, "0", sets, new TagAliasCallback() {

            @Override
            public void gotResult(int arg0, String arg1, Set<String> arg2) {
                LogUtils.i("set tags result is " + arg0);
            }
        });
    }
}
