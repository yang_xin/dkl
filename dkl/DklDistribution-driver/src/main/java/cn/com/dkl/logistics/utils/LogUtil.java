package cn.com.dkl.logistics.utils;

import android.content.Context;

/**
 * Created by LiuXing on 2017/3/3.
 */

public class LogUtil {
    private static Context mContext;
    private static boolean DEBUG = true;

    /**
     * init the logutil
     *
     * @Author hxd
     * @param context
     *            the application running in
     */
    public static void initialize(Context context) {
        if (null == mContext) {
            mContext = context;
        }
    }

    /**
     * log message, must run initialize(Context context) before use
     *
     * @Author hxd
     * @param  content
     */
    public static void Log(String content) {
        try {
            if (DEBUG) {
                android.util.Log.d(mContext.getPackageName(), content);
            }
        } catch (Exception e) {
        }
    }
}
