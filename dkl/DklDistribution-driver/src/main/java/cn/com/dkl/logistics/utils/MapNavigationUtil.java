package cn.com.dkl.logistics.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.baidu.navisdk.adapter.BNOuterLogUtil;
import com.baidu.navisdk.adapter.BNOuterTTSPlayerCallback;
import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BNaviSettingManager;
import com.baidu.navisdk.adapter.BaiduNaviManager;
import com.lidroid.xutils.util.LogUtils;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.fragment.ArroundCarFragment;

import static com.baidu.navisdk.adapter.PackageUtil.getSdcardDir;

/**
 * 地图导航类 Created by magic on 2016/12/14.
 */

public class MapNavigationUtil {
    private static final String TAG = "MapNavigationUtil";
    static String authinfo;
    static Activity mActivity;
    private static String mSDCardPath = null;
    private static final String APP_FOLDER_NAME = "driver";
    static List<Activity> mActivityList = new LinkedList<Activity>();

    public MapNavigationUtil(List<Activity> activityListList) {
        mActivityList = activityListList;
    }

    public static void setData(Activity activity){

        mActivity = activity;
        mActivityList.add(activity);
        if (initDirs()) {
            initNavi();
        }
    }

    /**
     * 算路设置起、终点，算路偏好，是否模拟导航等参数，然后在回调函数中设置跳转至诱导。
     */
    public static void routeplanToNavi(double longitude,double latitude/*BNRoutePlanNode stNode*/) {
        BNRoutePlanNode.CoordinateType coType = BNRoutePlanNode.CoordinateType.BD09LL;
        BNRoutePlanNode sNode = null;
        BNRoutePlanNode eNode = null;

        sNode = new BNRoutePlanNode(latitude, longitude, "", null, coType);
        eNode = new BNRoutePlanNode(latitude, longitude, "", null, coType);

        if (sNode != null && eNode != null) {
            List<BNRoutePlanNode> list = new ArrayList<BNRoutePlanNode>();
            list.add(sNode);
            list.add(eNode);
            BaiduNaviManager.getInstance().launchNavigator(mActivity, list, 1, true, new DemoRoutePlanListener(sNode));
        }
    }


    private static boolean initDirs() {
        mSDCardPath = getSdcardDir();
        if (mSDCardPath == null) {
            return false;
        }
        File f = new File(mSDCardPath, APP_FOLDER_NAME);
        if (!f.exists()) {
            try {
                f.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }


    /**
     * 初始化导航
     */
    public static void initNavi() {
        // 打开log开关
        BNOuterLogUtil.setLogSwitcher(true);
        File diverDir = StorageUtils.getOwnCacheDirectory(mActivity, "Distribution/diver");
        diverDir.mkdirs();
        Log.d(TAG, "initDirsSDCardPath: " + diverDir.getPath());
        BaiduNaviManager.getInstance().init(mActivity, mSDCardPath, APP_FOLDER_NAME,
                new BaiduNaviManager.NaviInitListener() {
                    @Override
                    public void onAuthResult(int status, String msg) {
                        Log.d(TAG, "onAuthResult: ");

                        if (0 == status) {
                            authinfo = "key校验成功!";
                        } else {
                            authinfo = "key校验失败, " + msg;
                        }
                        mActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(mActivity, authinfo, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    public void initSuccess() {
                        Log.d(TAG, "initSuccess: ");
                        Toast.makeText(mActivity, "百度导航引擎初始化成功", Toast.LENGTH_SHORT).show();
                        // 是否开启路况
                        BNaviSettingManager.setRealRoadCondition(BNaviSettingManager.RealRoadCondition.NAVI_ITS_ON);
                        if (BaiduNaviManager.isNaviInited()) {
                            initSetting();
                        }
                    }

                    public void initStart() {
                        Log.d(TAG, "initStart: ");
                        Toast.makeText(mActivity, "百度导航引擎初始化开始", Toast.LENGTH_SHORT).show();
                    }

                    public void initFailed() {
                        Log.d(TAG, "initFailed: ");
                        Toast.makeText(mActivity, "百度导航引擎初始化失败", Toast.LENGTH_SHORT).show();
                    }
                }, null);
    }

    public static class DemoRoutePlanListener implements BaiduNaviManager.RoutePlanListener {

        private BNRoutePlanNode mBNRoutePlanNode = null;

        public DemoRoutePlanListener(BNRoutePlanNode node) {
            mBNRoutePlanNode = node;
        }

        @Override
        public void onJumpToNavigator() {
        /**
         * 设置途径点以及resetEndNode会回调该接口
         */
            if (mActivityList == null) {
                Toast.makeText(mActivity, "未找到相关导航！", Toast.LENGTH_SHORT).show();
                return;
            }
            for (Activity ac : mActivityList) {

                if (ac.getClass().getName().endsWith("BNDemoGuideActivity")) {
                    Log.d(TAG, "onJumpToNavigator: ");
                    return;
                }
            }
            Intent intent = new Intent(mActivity, BNDemoGuideActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ArroundCarFragment.ROUTE_PLAN_NODE, (BNRoutePlanNode) mBNRoutePlanNode);
            intent.putExtras(bundle);
            mActivity.startActivity(intent);

        }

        @Override
        public void onRoutePlanFailed() {
            Toast.makeText(mActivity, "算路失败", Toast.LENGTH_SHORT).show();
        }

    }

    private static void initSetting() {
        // 设置是否双屏显示
        BNaviSettingManager.setShowTotalRoadConditionBar(BNaviSettingManager.PreViewRoadCondition.ROAD_CONDITION_BAR_SHOW_ON);
        // 设置导航播报模式
        BNaviSettingManager.setVoiceMode(BNaviSettingManager.VoiceMode.Veteran);
        // 是否开启路况
        BNaviSettingManager.setRealRoadCondition(BNaviSettingManager.RealRoadCondition.NAVI_ITS_ON);
    }

    private BNOuterTTSPlayerCallback mTTSPlayerCallback = new BNOuterTTSPlayerCallback() {
        @Override
        public void stopTTS() {
            Log.e("test_TTS", "stopTTS");
        }

        @Override
        public void resumeTTS() {
            Log.e("test_TTS", "resumeTTS");
        }

        @Override
        public void releaseTTSPlayer() {
            Log.e("test_TTS", "releaseTTSPlayer");
        }

        @Override
        public int playTTSText(String speech, int bPreempt) {
            Log.e("test_TTS", "playTTSText" + "_" + speech + "_" + bPreempt);
            return 1;
        }

        @Override
        public void phoneHangUp() {
            Log.e("test_TTS", "phoneHangUp");
        }

        @Override
        public void phoneCalling() {
            Log.e("test_TTS", "phoneCalling");
        }

        @Override
        public void pauseTTS() {
            Log.e("test_TTS", "pauseTTS");
        }

        @Override
        public void initTTSPlayer() {
            Log.e("test_TTS", "initTTSPlayer");
        }

        @Override
        public int getTTSState() {
            Log.e("test_TTS", "getTTSState");
            return 1;
        }
    };

    /**
     * 地图选择
     * @param context
     * @param longitude
     * @param latitude
     */
    public static void StartOtherNavi(Context context,double longitude,double latitude){

//        final AlertDialog dia=new AlertDialog.Builder(context).create();
//        View view= LayoutInflater.from(context).inflate(R.layout.dialog_map, null);
//        dia.show();
//        dia.getWindow().setContentView(view);



        if (CommonTools.isAvilible(context,"com.autonavi.minimap")){
//        if (CommonTools.isAvilible(context,"com.autonavi.amapauto")){
//            Intent intent = new Intent();

            double[] gd_lat_lon = bdToGaoDe(latitude, longitude);
            longitude = gd_lat_lon[0];
            latitude = gd_lat_lon[1];

            // 导航
            String urlString = "androidamap://navi?" +
                    "sourceApplication="+ context.getString(R.string.app_name) +
                    "&lat="+ latitude +
                    "&lon="+ longitude +
                    "&dev=0&" +
                    "style=2";

            LogUtils.d("高德url: " + urlString);

            Intent intent = new Intent("android.intent.action.VIEW", android.net.Uri.parse(urlString));
            intent.setPackage("com.autonavi.minimap");

//            // 规划路线
//            intent.setData(Uri.parse("androidauto://route?" +
//                    "sourceApplication="+ context.getString(R.string.app_name) +
//                    "&slat=%2$s&" +
//                    "slon=%3$s&" +
//                    "sname=%4$s&" +
//                    "dlat="+ latitude +
//                    "dlon="+ longitude +
//                    "dname=%7$s&" +
//                    "dev=0&" +
//                    "m=2"
//            ));
            //                    "baidumap://map/navi?location=" + latitude + "," +longitude

            context.startActivity(intent);
        } else if (CommonTools.isAvilible(context,"com.baidu.BaiduMap")){
            Intent intent = new Intent();
            intent.setData(Uri.parse("baidumap://map/navi?location=" + latitude + "," +longitude));
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "您尚未安装高德地图或者百度地图", Toast.LENGTH_SHORT).show();
//            setData((Activity) context);
//            routeplanToNavi(latitude,longitude);
        }
    }

    /**
     * 百度地图定位经纬度转高德经纬度
     * @param bd_lat
     * @param bd_lon
     * @return
     */
    public static double[] bdToGaoDe(double bd_lat, double bd_lon) {
        double[] gd_lat_lon = new double[2];
        double PI = 3.14159265358979324 * 3000.0 / 180.0;
        double x = bd_lon - 0.0065, y = bd_lat - 0.006;
        double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * PI);
        double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * PI);
        gd_lat_lon[0] = z * Math.cos(theta);
        gd_lat_lon[1] = z * Math.sin(theta);
        return gd_lat_lon;
    }

}
