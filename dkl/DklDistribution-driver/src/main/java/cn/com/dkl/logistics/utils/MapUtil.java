package cn.com.dkl.logistics.utils;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.utils.CoordinateConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by magic on 2016/12/12.
 */

public class MapUtil  {
    private static final String TAG = "MapUtil";
    private static List<Overlay> mMarkOverlayList = new ArrayList<>();

    public static void clearMapOverlay() {
        if (mMarkOverlayList != null) {
            for (int i = 0; i < mMarkOverlayList.size(); i++) {
                mMarkOverlayList.get(i).remove();
            }

            clearMarkList();
        }
    }

    public static void clearMarkList() {
        mMarkOverlayList.clear();
    }

    public static void addOverlay(Overlay overlay) {
        mMarkOverlayList.add(overlay);
    }

    /**
     * 加载一个位置到地图上，并且把该点放入一个容器
     * @param baiduMap
     * @param localResourcePic  marker图片
     * @param latitude
     * @param longitude
     * @param title  marker名称
     * @param list   容器
     * @param var1   设置 marker 覆盖物的 zIndex
     */
    public static Overlay setMarkers(BaiduMap baiduMap, int localResourcePic, double latitude, double longitude, String title, List<OverlayOptions> list,int var1){
        BitmapDescriptor mMarker = BitmapDescriptorFactory.fromResource(localResourcePic);
        OverlayOptions mMarkerOption = new MarkerOptions()
                        .position(MapUtil.converter(new LatLng(latitude, longitude))).perspective(false)
                        .icon(mMarker).zIndex(var1).title(title);
        // 将点添加到地图
        Overlay mMarkOverlay = baiduMap.addOverlay(mMarkerOption);
        if (list != null){
//            list.add(mMarkerOption);
        } else {
            Log.e(TAG, "setMarkers: list == null");
        }

        return mMarkOverlay;
    }

    /**
     * 坐标转换
     * @param sourceLatLng
     * @return
     */
    public static LatLng converter(LatLng sourceLatLng){
        return sourceLatLng;

//        // 将google地图、soso地图、aliyun地图、mapabc地图和amap地图// 所用坐标转换成百度坐标
//        CoordinateConverter converter  = new CoordinateConverter();
//        converter.from(CoordinateConverter.CoordType.COMMON);
//        // sourceLatLng待转换坐标
//        converter.coord(sourceLatLng);
//        return converter.convert();
    }

    /**
     * 坐标转换
     * @param sourceLatLng
     * @return
     */
    public static LatLng converterGPS(LatLng sourceLatLng){
        // 将GPS设备采集的原始GPS坐标转换成百度坐标
        CoordinateConverter converter  = new CoordinateConverter();
        converter.from(CoordinateConverter.CoordType.GPS);
        // sourceLatLng待转换坐标
        converter.coord(sourceLatLng);
        return converter.convert();
    }

    /**
     * 规划路线
     * @param activity
     * @param mRoutePlanSearch
     * @param pnList  路径点集合
     * @param mode  规划路线的模式(躲避拥堵、时间优先、最短距离、较少费用)
     * @param isAddCurrent  是否增加当前点显示
     * @param longitude  经度
     * @param latitude  纬度
     */
    public static void makeRoutePlan(Activity activity, RoutePlanSearch mRoutePlanSearch, List<PlanNode> pnList, DrivingRoutePlanOption.DrivingPolicy mode, boolean isAddCurrent, String longitude, String latitude){

        if (pnList == null ){
            Toast.makeText(activity, "路线规划失败，请检查起点位置或终点位置！", Toast.LENGTH_SHORT).show();
            return;
        }
        int pnSize = pnList.size();
        if (pnList.size() < 2 ){
            Toast.makeText(activity, "路线规划失败，请检查起点位置或终点位置！", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isAddCurrent && !TextUtils.isEmpty(longitude) && !TextUtils.isEmpty(latitude)) {
            PlanNode node = PlanNode.withLocation(new LatLng(Double.valueOf(latitude), Double.valueOf(longitude)));
            pnList.add(0, node);
            pnSize = pnList.size();
        }

        //设置起点和终点
        PlanNode stNode = pnList.get(0);
        PlanNode enNode = pnList.get(pnSize - 1);
        //设置途径点
        List<PlanNode> passList = new ArrayList<>();
        for (int i = 1; i < pnSize - 1; i++) {
            passList.add(pnList.get(i));
        }
        //调起线路规划
        DrivingRoutePlanOption drivingOption = new DrivingRoutePlanOption();
        drivingOption.from(stNode);
        drivingOption.to(enNode);

        //设置途经点
        if (passList.size() > 0){
            drivingOption.passBy(passList);
        }
        //设置驾车策略（躲避拥堵、时间优先、最短距离、较少费用）
        drivingOption.policy(mode);
        LatLng st = stNode.getLocation();
        Log.d("1111起点",st.toString() );
        for (int i = 0; i < passList.size() ; i++) {
            PlanNode node = passList.get(i);
            Log.d("1111中间点"+ i,node.getLocation().toString());
        }
        LatLng en = enNode.getLocation();
        Log.d("1111终点",en.toString() );
        if (mRoutePlanSearch != null){
            mRoutePlanSearch.drivingSearch(drivingOption);
        }
    }
}
