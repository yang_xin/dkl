package cn.com.dkl.logistics.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.haitun.xillen.tools.BasicTool;
import com.haitun.xillen.tools.ToastTool;
import com.haitun.xillen.utils.EnumUtil;
import com.haitun.xillen.utils.ValueUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.common.util.KeyValue;
import org.xutils.http.RequestParams;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import cn.com.dkl.logistics.constant.URLMap;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.LoginActivity;
import cn.com.dkl.logistics.entity.DataManager;

/**
 * 网络请求
 * Created by LiuXing on 2017/3/3.
 */

public class NetworkCallBack {
    CallBackListener mCallBackListener;
    private Context mContext;
    private Dialog mCallDialog;
    private SharedPreferences sp;
    public boolean isShowToast = true;
    private boolean isTest = false;

    public void setTest(boolean isTest) {
        this.isTest = isTest;
    }

    public NetworkCallBack(Context context) {
        mContext = context;
        mCallDialog = new DialogUtil(mContext).loading();
    }

    public void setListener(CallBackListener listener) {
        this.mCallBackListener = listener;
    }

    public interface CallBackListener {
        void callbackSuccess(String result);
    }

    public void networkRequest(RequestParams params, boolean isDefaultCondition) {

        if (isDefaultCondition) {
            params.addBodyParameter("userId", URLMap.DEFAULT_USER_ID);
            params.addBodyParameter("companyId", URLMap.DEFAULT_COMPANY_ID);
            params.addBodyParameter("extends", "extends");
        }

        LogUtil.Log("网址为：" + params.toString());
        List<KeyValue> bodyParams = params.getQueryStringParams();
        LogUtil.Log("参数为：");
        for (int i = 0; i < bodyParams.size(); i++) {
            KeyValue tmp = bodyParams.get(i);
            LogUtil.Log(tmp.toString());
        }
        request(params);
    }

    /**
     * 网络请求
     *
     * @param params             部分请求参数
     * @param isDefaultCondition 默认参数
     * @param listener
     */
    public Callback.Cancelable networkRequest(RequestParams params, EnumUtil.CommonParams commonParams, boolean isDefaultCondition, CallBackListener listener) {

        this.mCallBackListener = listener;
        sp = mContext.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        if (isDefaultCondition) {
            params.addBodyParameter("SCANTYPE", "2");//android 访问
            params.addBodyParameter("USERNAME", sp.getString(UserEntity.PHONE, null));
            params.addBodyParameter("PASSWORD", sp.getString(UserEntity.PASSWORD, null));
//            params.addBodyParameter("userId", UrlMap.DEFAULT_USER_ID);
//            params.addBodyParameter("companyId", UrlMap.DEFAULT_COMPANY_ID);
//            params.addBodyParameter("extends", "extends");
        } else {
            if (commonParams == EnumUtil.CommonParams.CommonParamsCompanyId) {
                params.addBodyParameter("companyId", URLMap.DEFAULT_COMPANY_ID);
                params.addBodyParameter("extends", "extends");
            } else if (commonParams == EnumUtil.CommonParams.CommonParamsUserId) {
                params.addBodyParameter("userId", URLMap.DEFAULT_USER_ID);
                params.addBodyParameter("extends", "extends");
            } else if (commonParams == EnumUtil.CommonParams.CommonParamsExtends) {
                params.addBodyParameter("extends", "extends");
            }
        }

        String str = "";
        LogUtil.Log("网址为：" + params.toString());
        List<KeyValue> bodyParams = params.getQueryStringParams();
        LogUtil.Log("参数为：");
        int count = bodyParams.size();
        for (int i = 0; i < bodyParams.size(); i++) {
            KeyValue tmp = bodyParams.get(i);
            LogUtil.Log(tmp.toString());

            if (i < count - 1) {
                str += "" + tmp.key + "=" + tmp.value + "&";
            } else {
                str += "" + tmp.key + "=" + tmp.value + "";
            }
        }

        LogUtil.Log(params.toString() + str);

        return request(params);
    }

    public Callback.Cancelable networkRequest(RequestParams params, boolean isDefaultCondition, CallBackListener listener) {
        return networkRequest(params, EnumUtil.CommonParams.CommonParamsNone, isDefaultCondition, listener);
    }

    public void networkRequest(RequestParams params, EnumUtil.CommonParams commonParamsExtends, CallBackListener listener) {

        this.mCallBackListener = listener;

        LogUtil.Log("网址为：" + params.toString());
        List<KeyValue> bodyParams = params.getQueryStringParams();
        LogUtil.Log("参数为：");
        for (int i = 0; i < bodyParams.size(); i++) {
            KeyValue tmp = bodyParams.get(i);
            LogUtil.Log(tmp.toString());
        }

        // 有上传文件时使用multipart表单, 否则上传原始文件流
        // params.setMultipart(true);
        // 上传文件方式 1
        // params.uploadFile = new File("/sdcard/test.txt");
        // 上传文件方式 2
        // params.addBodyParameter("uploadFile", new File("/sdcard/test.txt"));

        request(params);
    }

    /**
     * 下载文件
     *
     * @param params
     * @param callback
     */
    public void downloadFile(RequestParams params, Callback.CommonCallback callback) {
        download(params, callback);
    }

    /**
     * 请求数据
     * @param params
     */
    private Callback.Cancelable request(final RequestParams params) {

        if (!BasicTool.checkNetworkStatusWithToast(mContext)) {
            return null;
        }

        if (URLMap.isDebug && isTest) {
            mCallBackListener.callbackSuccess("");
            return null;
        }

        Callback.ProgressCallback<String> progressCallback = new Callback.ProgressCallback<String>() {

            private String callBackResult;

            @Override
            public void onWaiting() {
                //第一步-等待
                LogUtil.Log("onWaiting");
                if (isShowToast) {
                    mCallDialog.show();
                }
            }

            @Override
            public void onStarted() {
                //第三步-开始请求
                LogUtil.Log("onStarted");
            }

            @Override
            public void onLoading(long total, long current, boolean isDownloading) {
                //第四步-进度
                LogUtil.Log("onLoading");
            }

            @Override
            public void onSuccess(String result) {
                //第五步-请求成功
                LogUtil.Log("onSuccess:" + result);
                callBackResult = result;
//                if (mCallBackListener != null) {
//                    mCallBackListener.callbackSuccess(result);
//                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                //第五步-请求错误
                LogUtil.Log("onError:" + ex.getMessage());
                if (isShowToast) {
                    ToastTool.toastByConnectionFailure(mContext);
                }
            }

            @Override
            public void onCancelled(CancelledException cex) {
                //第五步-取消请求
                if (isShowToast) {
                    LogUtil.Log("onCancelled");
                }
            }

            @Override
            public void onFinished() {
                //最后一步-请求完成
                LogUtil.Log("onFinished");
                mCallDialog.dismiss();
                if (mCallBackListener != null) {
                    try {
                        if (!params.toString().contains("login")){
                            JSONObject obj = new JSONObject(callBackResult);
                            String message = obj.getString("message");
                            if ("账号密码错误".equals(message)){
                                JPushUtil.clearJPushAliasAndTags(mContext);
                                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                                mContext.sendBroadcast(new Intent("finish"));
                                Toast.makeText(mContext, "账号密码错误", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mCallBackListener.callbackSuccess(callBackResult);
                }
            }
        };

        //开始请求
        Callback.Cancelable cancelable = new HttpUtil().HttpPostCancel(params, progressCallback);
        return cancelable;
//        cancelable.cancel();//取消请求
    }

    /**
     * 网络下载
     *
     * @param params
     * @param backListener
     */
    private void download(RequestParams params, Callback.CommonCallback backListener) {
        //开始下载
        Callback.Cancelable cancelable = new HttpUtil().DownloadFileCancel(params, backListener);
    }

    /**
     * 上传凭证图片
     *
     * @param filePath 图片路径
     */
    public void uploadImage(RequestParams params, String filePath, String folderName, final UploadImageListener listener) {

        if (!BasicTool.checkNetworkStatusWithToast(mContext)) {
            return;
        }

        params.setMultipart(true);

        File file = new File(filePath);
        boolean isFile = file.isFile();
        LogUtil.Log("是否文件 ：" + isFile);

        LogUtil.Log("filePath is " + filePath);
        params.addBodyParameter("fileData", file);
        params.addBodyParameter("fileType", "1");
        params.addBodyParameter("folderName", folderName);

        NetworkCallBack callBack = new NetworkCallBack(mContext);
        callBack.networkRequest(params, EnumUtil.CommonParams.CommonParamsNone, false, new NetworkCallBack.CallBackListener() {
            @Override
            public void callbackSuccess(String result) {
                LogUtil.Log("result: " + result);

                if (ValueUtils.isStrNotEmpty(result)) {
                    if (listener != null) {
                        result = ValueUtils.deleteSymbolString(result, "\"");
                        listener.uploadImageSuccess(result + ",");
                    }
                } else {
                    ToastTool.toastByQueryDataFailure(mContext);
                }
            }
        });
    }


    public interface UploadImageListener {
        void uploadImageSuccess(String picName);
    }

    public interface UpdateListener {
        void callbackSuccess(boolean result);
    }

    public static byte[] File2byte(String filePath) {
        byte[] buffer = null;
        try
        {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1)
            {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return buffer;
    }
}
