package cn.com.dkl.logistics.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by magic on 2016/12/30.
 */

public class SKLUtil {
    /**
     * 根据时间戳转换时间,精确到分(适用于13位时间戳)
     * @param date
     * @return
     */
    public static String getDateSecondString(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM",
                Locale.getDefault());
        return sdf.format(Long.valueOf(date));
    }
}
