package cn.com.dkl.logistics.utils;

import android.content.Context;
import android.os.Bundle;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;
import com.lidroid.xutils.util.LogUtils;

/***
 * 讯飞语音帮助类
 *
 * @author whoami
 */
public class XFClouldUtil {
    /***
     * 根据传递的参数读出字符串
     */
    public static void speek(final Context context, String text) {
        // 1.创建SpeechSynthesizer对象, 第二个参数：本地合成时传InitListener
        SpeechSynthesizer mTts = SpeechSynthesizer.createSynthesizer(context, null);
        // 2.合成参数设置，详见《科大讯飞MSC API手册(Android)》SpeechSynthesizer 类
        mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");// 设置发音人
        mTts.setParameter(SpeechConstant.SPEED, "50");// 设置语速
        mTts.setParameter(SpeechConstant.VOLUME, "80");// 设置音量，范围0~100
        mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD); // 设置云端

        // 合成监听器
        SynthesizerListener mSynListener = new SynthesizerListener() {
            // 会话结束回调接口，没有错误时，error为null
            public void onCompleted(SpeechError error) {
                LogUtils.i("Complete...");
            }

            // 缓冲进度回调
            // percent为缓冲进度0~100，beginPos为缓冲音频在文本中开始位置，endPos表示缓冲音频在文本中结束位置，info为附加信息。
            public void onBufferProgress(int percent, int beginPos, int endPos, String info) {
            }

            // 开始播放
            public void onSpeakBegin() {
            }

            // 暂停播放
            public void onSpeakPaused() {
            }

            // 播放进度回调
            // percent为播放进度0~100,beginPos为播放音频在文本中开始位置，endPos表示播放音频在文本中结束位置.
            public void onSpeakProgress(int percent, int beginPos, int endPos) {
            }

            // 恢复播放回调接口
            public void onSpeakResumed() {
            }

            // 会话事件回调接口
            public void onEvent(int arg0, int arg1, int arg2, Bundle arg3) {
            }

        };
        // 3.开始合成
        mTts.startSpeaking(text, mSynListener);
    }
}
