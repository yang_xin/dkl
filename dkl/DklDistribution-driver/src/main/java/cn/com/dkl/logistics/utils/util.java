package cn.com.dkl.logistics.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

/**
 * Created by magic on 2017/1/10.
 */

public class util {
    private static final String TAG = "util";
    /** 剩余空间*/
    private static final long AvailableStorageCapacity = 30;

    NoStorageListener mNoStorageListener;
    public interface NoStorageListener{
        void noStorage();
    };
    public NoStorageListener getmNoStorageListener() {
        return mNoStorageListener;
    }

    public void setmNoStorageListener(NoStorageListener mNoStorageListener) {
        this.mNoStorageListener = mNoStorageListener;
    }

    /**
     * 认证成功发送广播
     * @param context
     * @param action
     */
    public static void sendBroadcast(Context context,String action,int status){
        Intent intent = new Intent(action);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        /** 可用则返回true*/
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void getStorageCapacity(){
        util mUtil = new util();
        if (mUtil.isExternalStorageWritable()){
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bytesAvailable = (long)stat.getBlockSize() * (long)stat.getAvailableBlocks();
            long megAvailable = bytesAvailable / 1048576;
            Log.d(TAG, "getAvailableStorageCapacity: " + megAvailable);
            long capacity = megAvailable - AvailableStorageCapacity;
            if (capacity <= 0){
                if (mNoStorageListener != null){
                    mNoStorageListener.noStorage();
                }
            }

        }
        
    }

    /**
     * 保留2位小数
     * @param d
     * @return
     */
    public static String getTwoDecimal(double d){
        if(d - 0 == 0){
            return "0";
        }else {
            java.text.DecimalFormat  df = new java.text.DecimalFormat("0.00");
            return df.format(d);
        }
    }
}
