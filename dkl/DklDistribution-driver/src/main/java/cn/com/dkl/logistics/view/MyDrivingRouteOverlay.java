package cn.com.dkl.logistics.view;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.overlayutil.DrivingRouteOverlay;

import cn.com.dkl.logistics.driver.R;

/**
 * Created by magic on 2016/12/14.
 */

public class MyDrivingRouteOverlay extends DrivingRouteOverlay {
    public MyDrivingRouteOverlay(BaiduMap baiduMap) {
        super(baiduMap);
    }

    public MyDrivingRouteOverlay(BaiduMap baiduMap, String startTitle, String endTitle) {
        super(baiduMap, startTitle, endTitle);
    }

    @Override
    public BitmapDescriptor getStartMarker() {

        return BitmapDescriptorFactory.fromResource(R.drawable.icon_st);

    }

    @Override
    public BitmapDescriptor getTerminalMarker() {

        return BitmapDescriptorFactory.fromResource(R.drawable.icon_en);

    }

}
