package cn.com.dkl.logistics.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Gallery;

public class MyGallery extends Gallery {

    private static final int timerAnimation = 1;

    public MyGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        int keyEvent;
        if (isScrollingLeft(e1, e2)) {
            keyEvent = KeyEvent.KEYCODE_DPAD_LEFT;
        } else {
            keyEvent = KeyEvent.KEYCODE_DPAD_RIGHT;
        }
        onKeyDown(keyEvent, null);
        Log.e("velocity", velocityX + "-----" + velocityY);
        return true;
        // return super.onFling(e1, e2, velocityX, velocityY);
    }

    public boolean isScrollingLeft(MotionEvent e1, MotionEvent e2) {
        return e2.getX() > e1.getX();

    }

    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == timerAnimation) {
                int position = getSelectedItemPosition();
                if (position >= (getCount() - 1)) {
                    onKeyDown(KeyEvent.KEYCODE_DPAD_LEFT, null);

                } else {
                    onKeyDown(KeyEvent.KEYCODE_DPAD_RIGHT, null);
                }

            }
            mHandler.sendEmptyMessageDelayed(timerAnimation, 3000);
            super.handleMessage(msg);

        }

    };

    public void startRun() {
        mHandler.sendEmptyMessageDelayed(timerAnimation, 3000);
        System.out.println("////startRun");
    }

    public void stopRun() {
        mHandler.removeMessages(timerAnimation);
        System.out.println("////stopRun");
    }

}
