package cn.com.dkl.logistics.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.baidu.mapapi.search.core.RouteLine;

import java.util.List;

import cn.com.dkl.logistics.adapter.RouteLineAdapter;
import cn.com.dkl.logistics.driver.R;

/** 选择线路
 * Created by magic on 2016/12/14.
 */

public class MyTransitDlg extends Dialog {
    private List<? extends RouteLine> mtransitRouteLines;
    private ListView transitRouteList;
    private RouteLineAdapter mTransitAdapter;

    OnItemInDlgClickListener onItemInDlgClickListener;

    public MyTransitDlg(Context context, int theme) {
        super(context, theme);
    }

    public MyTransitDlg(Context context, List< ? extends RouteLine> transitRouteLines,  RouteLineAdapter.Type
            type) {
        this( context, 0);
        mtransitRouteLines = transitRouteLines;
        mTransitAdapter = new  RouteLineAdapter( context, mtransitRouteLines , type);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transit_dialog);

        transitRouteList = (ListView) findViewById(R.id.transitList);
        transitRouteList.setAdapter(mTransitAdapter);

        transitRouteList.setOnItemClickListener( new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onItemInDlgClickListener.onItemClick( position);
//                    mBtnPre.setVisibility(View.VISIBLE);
//                    mBtnNext.setVisibility(View.VISIBLE);

            }
        });
    }

    public void setOnItemInDlgClickLinster( OnItemInDlgClickListener itemListener) {
        onItemInDlgClickListener = itemListener;
    }
    // 响应DLg中的List item 点击
    public interface OnItemInDlgClickListener {
        public void onItemClick(int position);
    }
}
