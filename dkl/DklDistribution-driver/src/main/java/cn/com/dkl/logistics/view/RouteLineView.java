package cn.com.dkl.logistics.view;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.baidu.mapapi.search.core.RouteLine;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dkl.logistics.adapter.RouteLine2Adapter;
import cn.com.dkl.logistics.constant.UserEntity;
import cn.com.dkl.logistics.driver.R;
import cn.com.dkl.logistics.entity.DataManager;

/**
 * Created by Hexway on 2017/1/10.
 */

public class RouteLineView extends FrameLayout {


    @Bind(R.id.rbAvoidJam)
    RadioButton rbAvoidJam;
    @Bind(R.id.rbDisFirst)
    RadioButton rbDisFirst;
    @Bind(R.id.rbFeeFirst)
    RadioButton rbFeeFirst;
    @Bind(R.id.rbTimeFirst)
    RadioButton rbTimeFirst;
    @Bind(R.id.tvTime)
    TextView tvTime;
    @Bind(R.id.tvDistance)
    TextView tvDistance;
    @Bind(R.id.tvJamDistance)
    TextView tvJamDistance;
    @Bind(R.id.grid)
    GridView gridView;
    @Bind(R.id.ivPrevious)
    ImageView ivPrevious;
    @Bind(R.id.ivNext)
    ImageView ivNext;
    @Bind(R.id.horizontalScrollView)
    PageView horizontalScrollView;

    private RouteLineViewListener listener;
    private SharedPreferences sp;

    private List<? extends RouteLine> mtransitRouteLines = new ArrayList<>();
    private RouteLine2Adapter mTransitAdapter;
    private Context mContext;
    private int size = 0;
    private int current = 0;
    private int width = 0;

    public RouteLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.view_route_line, this);
        ButterKnife.bind(this);

        mContext = context;

        sp = context.getSharedPreferences(DataManager.PREFERENCE_USER_INFO, Context.MODE_PRIVATE);
        selectPolicy();


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listener != null && mtransitRouteLines != null) {
                    listener.onItemClick(position, mtransitRouteLines.get(position));
//                    mBtnPre.setVisibility(View.VISIBLE);
//                    mBtnNext.setVisibility(View.VISIBLE);
                }

            }
        });

        mTransitAdapter = new RouteLine2Adapter(mContext, mtransitRouteLines, RouteLine2Adapter.Type.DRIVING_ROUTE);
        setGridView(0);
        gridView.setAdapter(mTransitAdapter);

        horizontalScrollView.setListener(new PageView.PageViewListener() {
            @Override
            public void onSmoothPage(int page) {
                showArrow(page);
                current = page;
            }
        });
    }

    public void selectPolicy() {
        String drivingPolicy = sp.getString(UserEntity.Driving_Policy, "0");
        if (drivingPolicy.equals("0")) {
            rbAvoidJam.setChecked(true);
        } else if (drivingPolicy.equals("1")) {
            rbDisFirst.setChecked(true);
        } else if (drivingPolicy.equals("2")) {
            rbFeeFirst.setChecked(true);
        } else if (drivingPolicy.equals("3")) {
            rbTimeFirst.setChecked(true);
        }
    }

    public void setListener(final RouteLineViewListener listener) {
        this.listener = listener;
    }

    public void setRouteLine(List<? extends RouteLine> transitRouteLines, RouteLine2Adapter.Type
            type) {
        mtransitRouteLines = transitRouteLines;
        size = mtransitRouteLines.size();
        setGridView(transitRouteLines.size());
        mTransitAdapter.setData(transitRouteLines, type);

        showArrow(0);
        current = 0;
        moveToPage(0);
    }

    private void setGridView(int size) {

        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);

        int length = dm.widthPixels;
        width = length;
        float density = dm.density;
        int gridviewWidth = (int) (size * length);
        int itemWidth = (int) (length);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                gridviewWidth, LinearLayout.LayoutParams.FILL_PARENT);
        gridView.setLayoutParams(params); // ???
        gridView.setColumnWidth(itemWidth); // ???
        gridView.setHorizontalSpacing(0); // ???
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setNumColumns(size); // ???
    }

    public interface RouteLineViewListener {
        void onClickRountPolicy();

        public void onItemClick(int position, RouteLine route);
    }

    @OnClick({R.id.rbAvoidJam, R.id.rbDisFirst, R.id.rbFeeFirst, R.id.rbTimeFirst, R.id.ivPrevious, R.id.ivNext})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbAvoidJam:
                doSelectPolicy("0");
                break;
            case R.id.rbDisFirst:
                doSelectPolicy("1");
                break;
            case R.id.rbFeeFirst:
                doSelectPolicy("2");
                break;
            case R.id.rbTimeFirst:
                doSelectPolicy("3");
                doWork();
                break;
            case R.id.ivPrevious:
                current -= 1;
                showArrow(current);
                moveToPage(current);
                break;
            case R.id.ivNext:
                current += 1;
                showArrow(current);
                moveToPage(current);
                break;
        }
    }

    public void doSelectPolicy(String policy) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(UserEntity.Driving_Policy, policy);
        edit.commit();
        doWork();
    }

    public void doWork() {
        if (listener != null) {
            listener.onClickRountPolicy();
        }
    }

    public void configShow(String time, String distance, String jamDistance) {
        tvTime.setText(time);
        tvDistance.setText("共" + distance + "公里");
        tvJamDistance.setText("拥堵距离" + jamDistance + "米");
    }

    public void showArrow(int position) {
        if (position == 0) {
            ivPrevious.setVisibility(GONE);
        } else {
            ivPrevious.setVisibility(VISIBLE);
        }

        if ((position == size - 1) || (size == 0)) {
            ivNext.setVisibility(GONE);
        } else {
            ivNext.setVisibility(VISIBLE);
        }
    }

    public void moveToPage(int page) {
        horizontalScrollView.smoothScrollTo(page * width, 0);
    }
}
