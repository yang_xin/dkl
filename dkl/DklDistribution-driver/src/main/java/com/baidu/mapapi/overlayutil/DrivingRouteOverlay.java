package com.baidu.mapapi.overlayutil;

import android.graphics.Color;
import android.os.Bundle;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.DrivingRouteLine.DrivingStep;
import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import cn.com.dkl.logistics.utils.Constant;

/**
 * 用于显示一条驾车路线的overlay，自3.4.0版本起可实例化多个添加在地图中显示，当数据中包含路况数据时，则默认使用路况纹理分段绘制
 */
public class DrivingRouteOverlay extends OverlayManager {
    /**
     * 驾车线路
     */
    private DrivingRouteLine mRouteLine = null;
    boolean focus = false;
    private String startTitle = "";
    private String endTitle = "";


    /**
     * 构造函数
     *
     * @param baiduMap 该DrivingRouteOvelray引用的 BaiduMap
     */
    public DrivingRouteOverlay(BaiduMap baiduMap) {
        super(baiduMap);
    }

    /**
     * 构造函数
     *
     * @param baiduMap 该DrivingRouteOvelray引用的 BaiduMap
     */
    public DrivingRouteOverlay(BaiduMap baiduMap, String startTitle, String endTitle) {
        super(baiduMap);
        this.startTitle = startTitle;
        this.endTitle = endTitle;
    }

    @Override
    public final List<OverlayOptions> getOverlayOptions() {
        if (mRouteLine == null) {
            return null;
        }
        /* 地图覆盖物选型基类 */
        List<OverlayOptions> overlayOptionses = new ArrayList<OverlayOptions>();
        // step node
        if (mRouteLine.getAllStep() != null && mRouteLine.getAllStep().size() > 0) {    // 驾车路段不为空
            for (DrivingRouteLine.DrivingStep step : mRouteLine.getAllStep()) {    // 遍历驾车路段,绘制路段标记
                Bundle b = new Bundle();
                b.putInt("index", mRouteLine.getAllStep().indexOf(step));
                if (step.getEntrance() != null) {    // 路段入口信息不为空
                    overlayOptionses.add((new MarkerOptions()).position(step.getEntrance().getLocation())
                            .anchor(0.5f, 0.5f)    // 设置 marker覆盖物的锚点比例，默认（0.5f, 1.0f）水平居中，垂直下对齐
                            .zIndex(10)    // 设置 marker 覆盖物的 zIndex
                            .rotate((360 - step.getDirection()))    // 设置 marker 覆盖物旋转角度，逆时针
                            .extraInfo(b)    // 设置 marker 覆盖物的额外信息
                            .icon(BitmapDescriptorFactory.fromAssetWithDpi("Icon_line_node.png")));
                }
                if (mRouteLine.getAllStep().indexOf(step) == (mRouteLine.getAllStep().size() - 1) && step.getExit() != null) {    // 出口信息不为空
                    overlayOptionses.add((new MarkerOptions()).position(step.getExit().getLocation()).anchor(0.5f, 0.5f).zIndex(10)
                            .icon(BitmapDescriptorFactory.fromAssetWithDpi("Icon_line_node.png")));
                }
            }
        }

        if (mRouteLine.getStarting() != null && !Constant.isAddCurrent) {    // 驾车线路起点信息不为空
            String pic = "Icon_start.png";
//            if (Constant.isAddCurrent) {
//                pic = "icon_location.png";
//            }
            overlayOptionses.add((new MarkerOptions()).position(mRouteLine.getStarting().getLocation())
                    .icon(getStartMarker() != null ? getStartMarker() : BitmapDescriptorFactory.fromAssetWithDpi(pic)).zIndex(10).title(startTitle));
        }
        if (mRouteLine.getTerminal() != null) {    // 驾车线路重点信息不为空
            overlayOptionses.add((new MarkerOptions()).position(mRouteLine.getTerminal().getLocation())
                    .icon(getTerminalMarker() != null ? getTerminalMarker() : BitmapDescriptorFactory.fromAssetWithDpi("Icon_end.png")).zIndex(10).title(endTitle));
        }
        // poly line
        if (mRouteLine.getAllStep() != null && mRouteLine.getAllStep().size() > 0) {    // 驾车线段不为空

            List<DrivingStep> steps = mRouteLine.getAllStep();     // 驾车线段
            int stepNum = steps.size();
            /* 经纬度 */
            List<LatLng> points = new ArrayList<LatLng>();    // 存储路段途经点经纬度
            ArrayList<Integer> traffics = new ArrayList<Integer>();    // 存储路况信息，0：没路况，1：畅通，2：缓慢，3：拥堵
            int totalTraffic = 0;
            for (int i = 0; i < stepNum; i++) {
                if (i == stepNum - 1) {    // 最后一个线段
                    points.addAll(steps.get(i).getWayPoints());    // 保存途径点信息到points
                } else {
                    points.addAll(steps.get(i).getWayPoints().subList(0, steps.get(i).getWayPoints().size() - 1));
                }

                totalTraffic += steps.get(i).getWayPoints().size() - 1;
                if (steps.get(i).getTrafficList() != null && steps.get(i).getTrafficList().length > 0) {    // 获取路况数据数组，个数为wayPoints个数-1
                    for (int j = 0; j < steps.get(i).getTrafficList().length; j++) {
                        traffics.add(steps.get(i).getTrafficList()[j]);    // 获取每一个路段的路况
                    }
                }
            }

            boolean isDotLine = false;

            if (traffics != null && traffics.size() > 0) {
                isDotLine = true;
            }
//			创建折线覆盖物选项类
            PolylineOptions option = new PolylineOptions().points(points)    // 设置折线坐标点列表
                    .textureIndex(traffics)    // 设置折线每个点的纹理索引，每一个点带一个索引，绘制时按照索引从customTextureList里面取，个数>= points的个数 若index越界大于纹理列表个数，则取最后一个纹理绘制
                    .width(7).dottedLine(isDotLine)    // 是否是虚线
                    .focus(true)
                    .color(getLineColor() != 0 ? getLineColor() : Color.argb(178, 0, 78, 255)).zIndex(0);
            if (isDotLine) {
                option.customTextureList(getCustomTextureList());
            }
            // 添加路线折线到地图基类
            overlayOptionses.add(option);
        }
        return overlayOptionses;
    }

    /**
     * 设置路线数据
     *
     * @param routeLine 路线数据
     */
    public void setData(DrivingRouteLine routeLine) {
        this.mRouteLine = routeLine;
    }

    /**
     * 覆写此方法以改变默认起点图标
     *
     * @return 起点图标
     */
    public BitmapDescriptor getStartMarker() {
        return null;
    }

    /**
     * 覆写此方法以改变默认绘制颜色
     *
     * @return 线颜色
     */
    public int getLineColor() {
        return 0;
    }

    public List<BitmapDescriptor> getCustomTextureList() {
        ArrayList<BitmapDescriptor> list = new ArrayList<BitmapDescriptor>();
        list.add(BitmapDescriptorFactory.fromAsset("Icon_road_blue_arrow.png"));
        list.add(BitmapDescriptorFactory.fromAsset("Icon_road_green_arrow.png"));
        list.add(BitmapDescriptorFactory.fromAsset("Icon_road_yellow_arrow.png"));
        list.add(BitmapDescriptorFactory.fromAsset("Icon_road_red_arrow.png"));
        list.add(BitmapDescriptorFactory.fromAsset("Icon_road_nofocus.png"));
        return list;
    }

    /**
     * 覆写此方法以改变默认终点图标
     *
     * @return 终点图标
     */
    public BitmapDescriptor getTerminalMarker() {
        return null;
    }

    /**
     * 覆写此方法以改变默认点击处理
     *
     * @param i 线路节点的 index
     * @return 是否处理了该点击事件
     */
    public boolean onRouteNodeClick(int i) {
        if (mRouteLine.getAllStep() != null && mRouteLine.getAllStep().get(i) != null) {
            LogUtils.i("baidumapsdk:DrivingRouteOverlay onRouteNodeClick处理线路节点");
        }
        return false;
    }

    @Override
    public final boolean onMarkerClick(Marker marker) {
        for (Overlay mMarker : mOverlayList) {
            if (mMarker instanceof Marker && mMarker.equals(marker)) {
                if (marker.getExtraInfo() != null) {
                    onRouteNodeClick(marker.getExtraInfo().getInt("index"));
                }
            }
        }
        return true;
    }

    @Override
    public boolean onPolylineClick(Polyline polyline) {
        boolean flag = false;
        for (Overlay mPolyline : mOverlayList) {
            if (mPolyline instanceof Polyline && mPolyline.equals(polyline)) {
                // 选中
                flag = true;
                break;
            }
        }
        setFocus(flag);
        return true;
    }

    public void setFocus(boolean flag) {
        focus = flag;
        for (Overlay mPolyline : mOverlayList) {
            if (mPolyline instanceof Polyline) {
                // 选中
                ((Polyline) mPolyline).setFocus(flag);

                break;
            }
        }
    }

}
