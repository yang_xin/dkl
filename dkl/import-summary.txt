ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* LICENSE
* PullToRefreshLibrary.iml
* doc\
* doc\allclasses-frame.html
* doc\allclasses-noframe.html
* doc\com\
* doc\com\handmark\
* doc\com\handmark\pulltorefresh\
* doc\com\handmark\pulltorefresh\library\
* doc\com\handmark\pulltorefresh\library\BuildConfig.html
* doc\com\handmark\pulltorefresh\library\ILoadingLayout.html
* doc\com\handmark\pulltorefresh\library\IPullToRefresh.html
* doc\com\handmark\pulltorefresh\library\LoadingLayoutProxy.html
* doc\com\handmark\pulltorefresh\library\OverscrollHelper.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshAdapterViewBase.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.AnimationStyle.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.Mode.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.OnLastItemVisibleListener.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.OnPullEventListener.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.OnRefreshListener.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.OnRefreshListener2.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.Orientation.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.State.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshBase.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshExpandableListView.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshGridView.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshHorizontalScrollView.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshListView.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshScrollView.html
* doc\com\handmark\pulltorefresh\library\PullToRefreshWebView.html
* doc\com\handmark\pulltorefresh\library\R.anim.html
* doc\com\handmark\pulltorefresh\library\R.attr.html
* doc\com\handmark\pulltorefresh\library\R.dimen.html
* doc\com\handmark\pulltorefresh\library\R.drawable.html
* doc\com\handmark\pulltorefresh\library\R.html
* doc\com\handmark\pulltorefresh\library\R.id.html
* doc\com\handmark\pulltorefresh\library\R.layout.html
* doc\com\handmark\pulltorefresh\library\R.string.html
* doc\com\handmark\pulltorefresh\library\R.styleable.html
* doc\com\handmark\pulltorefresh\library\class-use\
* doc\com\handmark\pulltorefresh\library\class-use\BuildConfig.html
* doc\com\handmark\pulltorefresh\library\class-use\ILoadingLayout.html
* doc\com\handmark\pulltorefresh\library\class-use\IPullToRefresh.html
* doc\com\handmark\pulltorefresh\library\class-use\LoadingLayoutProxy.html
* doc\com\handmark\pulltorefresh\library\class-use\OverscrollHelper.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshAdapterViewBase.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.AnimationStyle.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.Mode.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.OnLastItemVisibleListener.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.OnPullEventListener.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.OnRefreshListener.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.OnRefreshListener2.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.Orientation.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.State.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshBase.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshExpandableListView.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshGridView.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshHorizontalScrollView.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshListView.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshScrollView.html
* doc\com\handmark\pulltorefresh\library\class-use\PullToRefreshWebView.html
* doc\com\handmark\pulltorefresh\library\class-use\R.anim.html
* doc\com\handmark\pulltorefresh\library\class-use\R.attr.html
* doc\com\handmark\pulltorefresh\library\class-use\R.dimen.html
* doc\com\handmark\pulltorefresh\library\class-use\R.drawable.html
* doc\com\handmark\pulltorefresh\library\class-use\R.html
* doc\com\handmark\pulltorefresh\library\class-use\R.id.html
* doc\com\handmark\pulltorefresh\library\class-use\R.layout.html
* doc\com\handmark\pulltorefresh\library\class-use\R.string.html
* doc\com\handmark\pulltorefresh\library\class-use\R.styleable.html
* doc\com\handmark\pulltorefresh\library\extras\
* doc\com\handmark\pulltorefresh\library\extras\PullToRefreshWebView2.html
* doc\com\handmark\pulltorefresh\library\extras\SoundPullEventListener.html
* doc\com\handmark\pulltorefresh\library\extras\class-use\
* doc\com\handmark\pulltorefresh\library\extras\class-use\PullToRefreshWebView2.html
* doc\com\handmark\pulltorefresh\library\extras\class-use\SoundPullEventListener.html
* doc\com\handmark\pulltorefresh\library\extras\package-frame.html
* doc\com\handmark\pulltorefresh\library\extras\package-summary.html
* doc\com\handmark\pulltorefresh\library\extras\package-tree.html
* doc\com\handmark\pulltorefresh\library\extras\package-use.html
* doc\com\handmark\pulltorefresh\library\internal\
* doc\com\handmark\pulltorefresh\library\internal\EmptyViewMethodAccessor.html
* doc\com\handmark\pulltorefresh\library\internal\FlipLoadingLayout.html
* doc\com\handmark\pulltorefresh\library\internal\IndicatorLayout.html
* doc\com\handmark\pulltorefresh\library\internal\LoadingLayout.html
* doc\com\handmark\pulltorefresh\library\internal\RotateLoadingLayout.html
* doc\com\handmark\pulltorefresh\library\internal\Utils.html
* doc\com\handmark\pulltorefresh\library\internal\ViewCompat.html
* doc\com\handmark\pulltorefresh\library\internal\class-use\
* doc\com\handmark\pulltorefresh\library\internal\class-use\EmptyViewMethodAccessor.html
* doc\com\handmark\pulltorefresh\library\internal\class-use\FlipLoadingLayout.html
* doc\com\handmark\pulltorefresh\library\internal\class-use\IndicatorLayout.html
* doc\com\handmark\pulltorefresh\library\internal\class-use\LoadingLayout.html
* doc\com\handmark\pulltorefresh\library\internal\class-use\RotateLoadingLayout.html
* doc\com\handmark\pulltorefresh\library\internal\class-use\Utils.html
* doc\com\handmark\pulltorefresh\library\internal\class-use\ViewCompat.html
* doc\com\handmark\pulltorefresh\library\internal\package-frame.html
* doc\com\handmark\pulltorefresh\library\internal\package-summary.html
* doc\com\handmark\pulltorefresh\library\internal\package-tree.html
* doc\com\handmark\pulltorefresh\library\internal\package-use.html
* doc\com\handmark\pulltorefresh\library\package-frame.html
* doc\com\handmark\pulltorefresh\library\package-summary.html
* doc\com\handmark\pulltorefresh\library\package-tree.html
* doc\com\handmark\pulltorefresh\library\package-use.html
* doc\constant-values.html
* doc\deprecated-list.html
* doc\help-doc.html
* doc\index-files\
* doc\index-files\index-1.html
* doc\index-files\index-10.html
* doc\index-files\index-11.html
* doc\index-files\index-12.html
* doc\index-files\index-13.html
* doc\index-files\index-14.html
* doc\index-files\index-15.html
* doc\index-files\index-16.html
* doc\index-files\index-17.html
* doc\index-files\index-18.html
* doc\index-files\index-2.html
* doc\index-files\index-3.html
* doc\index-files\index-4.html
* doc\index-files\index-5.html
* doc\index-files\index-6.html
* doc\index-files\index-7.html
* doc\index-files\index-8.html
* doc\index-files\index-9.html
* doc\index.html
* doc\overview-frame.html
* doc\overview-summary.html
* doc\overview-tree.html
* doc\package-list
* doc\resources\
* doc\resources\background.gif
* doc\resources\tab.gif
* doc\resources\titlebar.gif
* doc\resources\titlebar_end.gif
* doc\stylesheet.css
* pom.xml

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:22.+

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In slidingmenu:
* AndroidManifest.xml => slidingmenu\src\main\AndroidManifest.xml
* assets\ => slidingmenu\src\main\assets
* res\ => slidingmenu\src\main\res\
* src\ => slidingmenu\src\main\java\
In Library:
* AndroidManifest.xml => library\src\main\AndroidManifest.xml
* assets\ => library\src\main\assets
* res\ => library\src\main\res\
* src\ => library\src\main\java\

Missing Android Support Repository:
-----------------------------------
Some useful libraries, such as the Android Support Library, are
installed from a special Maven repository, which should be installed
via the SDK manager.

It looks like this library is missing from your SDK installation at:
null

To install it, open the SDK manager, and in the Extras category,
select "Android Support Repository". You may also want to install the
"Google Repository" if you want to use libraries like Google Play
Services.

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
